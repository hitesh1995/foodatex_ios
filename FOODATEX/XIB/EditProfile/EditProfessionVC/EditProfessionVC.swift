//
//  EditProfessionVC.swift
//  FOODATEX
//
//  Created by Silver Shark on 16/11/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit

protocol EditProfessionDelegate {
    func saveProfession(profession : Int)
}

class EditProfessionVC: UIViewController {

    
    var delegate : EditProfessionDelegate!
    @IBOutlet weak var imgOther: UIImageView!
    
    @IBOutlet weak var imgWorking: UIImageView!
    @IBOutlet weak var imgStudy: UIImageView!
    
    var profession = 1
    
    //MARK: - LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func show(){
        self.view.frame = CGRect(x: 0, y: 0, width:SCREENWIDTH(), height: SCREENHEIGHT())
        APP_DELEGATE.window?.addSubview(self.view!)
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.view.frame = CGRect(x: 0, y: 0, width: SCREENWIDTH(), height: SCREENHEIGHT())
        }, completion: {(finished: Bool) -> Void in
            
            if let user = getLoginDetails() {
                if let pr = user.whatDoYou, pr != "1"{
                    self.profession = 2
                    self.imgStudy.image = UIImage(named: "icon_RadioChecked")
                    self.imgWorking.image = UIImage(named: "icon_RadioUnchecked")
                }
            }
        })
    }
    
    
    //MARK: - Hide Picker
    func hideCustomPicker() {
        self.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - self.view.frame.size.height, width: UIScreen.main.bounds.size.width, height: self.view.frame.size.height)
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.view.frame = CGRect(x: 0, y: SCREENHEIGHT(), width: SCREENWIDTH(), height: self.view.frame.size.height)
        }, completion: {(finished: Bool) -> Void in
            self.view!.removeFromSuperview()
        })
    }
    
    
    @IBAction func btnWorkingTapped(_ sender: Any) {
        imgWorking.image = UIImage(named: "icon_RadioChecked")
        imgStudy.image = UIImage(named: "icon_RadioUnchecked")
        imgOther.image = UIImage(named: "icon_RadioUnchecked")

        profession = 1
    }
    
    @IBAction func btnStudyTapped(_ sender: Any) {
        imgStudy.image = UIImage(named: "icon_RadioChecked")
        imgWorking.image = UIImage(named: "icon_RadioUnchecked")
        imgOther.image = UIImage(named: "icon_RadioUnchecked")

        profession = 2
    }
    
    @IBAction func btn_Other_Tapped(_ sender: Any) {
        imgOther.image = UIImage(named: "icon_RadioChecked")
        imgWorking.image = UIImage(named: "icon_RadioUnchecked")
        imgStudy.image = UIImage(named: "icon_RadioUnchecked")

        profession = 3
        
    }
    
    
    
    
    @IBAction func btnCloseTapped(_ sender: Any) {
        
        self.view!.removeFromSuperview()
    }
    
    @IBAction func btnSaveTapped(_ sender: Any) {
        
        delegate.saveProfession(profession: profession)
        self.view!.removeFromSuperview()
    }
}
