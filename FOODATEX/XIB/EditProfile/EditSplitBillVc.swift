//
//  EditIntrestedInVC.swift
//  FOODATEX
//
//  Created by Silver Shark on 22/11/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit

protocol EditSplitBillDelegate {
    func saveSplitBill(splitBill : Int)
}

class EditSplitBillVc: UIViewController {
    
    var delegate : EditSplitBillDelegate!
    var interstedIn = 1
    
    @IBOutlet weak var imgYes: UIImageView!
    @IBOutlet weak var imgNo: UIImageView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    func show(){
        self.view.frame = CGRect(x: 0, y: 0, width:SCREENWIDTH(), height: SCREENHEIGHT())
        APP_DELEGATE.window?.addSubview(self.view!)
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.view.frame = CGRect(x: 0, y: 0, width: SCREENWIDTH(), height: SCREENHEIGHT())
        }, completion: {(finished: Bool) -> Void in
        })
    }
    
    
    //MARK: - Hide Picker
    func hideCustomPicker() {
        self.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - self.view.frame.size.height, width: UIScreen.main.bounds.size.width, height: self.view.frame.size.height)
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.view.frame = CGRect(x: 0, y: SCREENHEIGHT(), width: SCREENWIDTH(), height: self.view.frame.size.height)
        }, completion: {(finished: Bool) -> Void in
            self.view!.removeFromSuperview()
        })
    }
    
    @IBAction func btnYesTapped(_ sender: Any) {
        imgYes.image = UIImage(named: "icon_RadioChecked")
        imgNo.image = UIImage(named: "icon_RadioUnchecked")
        interstedIn = 1
    }
    
    @IBAction func btnNoTapped(_ sender: Any) {
        imgNo.image = UIImage(named: "icon_RadioChecked")
        imgYes.image = UIImage(named: "icon_RadioUnchecked")
        interstedIn = 2
    }
    
    //   @IBAction func btnBothTapped(_ sender: Any) {
    //       imgBoth.image = UIImage(named: "icon_RadioChecked")
    //       imgFemale.image = UIImage(named: "icon_RadioUnchecked")
    //       imgMale.image = UIImage(named: "icon_RadioUnchecked")
    //       interstedIn = 3
    //   }
    
    
    @IBAction func btnCloseTapped(_ sender: Any) {
        self.view!.removeFromSuperview()
    }
    
    @IBAction func btnSaveTapped(_ sender: Any) {
        
        delegate.saveSplitBill(splitBill:interstedIn)
        self.view!.removeFromSuperview()
    }
    
}
