//
//  EditAllergyVC.swift
//  FOODATEX
//
//  Created by Silver Shark on 17/11/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit

protocol EditAllergyDelegate {
    func saveAllergy(haveAllergy : Int, allergy : String)
}

class EditAllergyVC: UIViewController {
    
    var delegate : EditAllergyDelegate!
    
    @IBOutlet weak var imgYes: UIImageView!
    @IBOutlet weak var imgNo: UIImageView!
    @IBOutlet weak var txtAllergy: UITextField!
    
    var haveAllergy = 1

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    func show(){
        self.view.frame = CGRect(x: 0, y: 0, width:SCREENWIDTH(), height: SCREENHEIGHT())
        APP_DELEGATE.window?.addSubview(self.view!)
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.view.frame = CGRect(x: 0, y: 0, width: SCREENWIDTH(), height: SCREENHEIGHT())
        }, completion: {(finished: Bool) -> Void in
            
            if let user = getLoginDetails() {
                if let pr = user.anyAllergiesStatus, pr != "1" {
                    self.haveAllergy = 0
                    self.imgNo.image = UIImage(named: "icon_RadioChecked")
                    self.imgYes.image = UIImage(named: "icon_RadioUnchecked")
                }else{
                    self.txtAllergy.text = getLoginDetails()?.anyAllergies ?? ""
                }
            }
        })
    }
    
    
    //MARK: - Hide Picker
    func hideCustomPicker() {
        self.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - self.view.frame.size.height, width: UIScreen.main.bounds.size.width, height: self.view.frame.size.height)
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.view.frame = CGRect(x: 0, y: SCREENHEIGHT(), width: SCREENWIDTH(), height: self.view.frame.size.height)
        }, completion: {(finished: Bool) -> Void in
            self.view!.removeFromSuperview()
        })
    }
    
    @IBAction func btnYesTapped(_ sender: Any) {
        imgYes.image = UIImage(named: "icon_RadioChecked")
        imgNo.image = UIImage(named: "icon_RadioUnchecked")
        haveAllergy = 1
    }
    
    @IBAction func btnStudyTapped(_ sender: Any) {
        imgNo.image = UIImage(named: "icon_RadioChecked")
        imgYes.image = UIImage(named: "icon_RadioUnchecked")
        haveAllergy = 0
    }
    
    
    @IBAction func btnCloseTapped(_ sender: Any) {
        
        self.view!.removeFromSuperview()
    }
    
    @IBAction func btnSaveTapped(_ sender: Any) {
        
        if haveAllergy == 1 && txtAllergy.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            showAppAlertWithMessage("You must specify the allergy you have", viewController: self)
            return
        }
        
        delegate.saveAllergy(haveAllergy: haveAllergy, allergy: txtAllergy.text ?? "" )
        self.view!.removeFromSuperview()
    }

}
