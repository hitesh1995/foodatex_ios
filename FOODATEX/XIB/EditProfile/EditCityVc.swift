//
//  EditCityVc.swift
//  FOODATEX
//
//  Created by SANKET SHARMA on 06/05/19.
//  Copyright © 2019 RudraApps. All rights reserved.
//

import UIKit
protocol EditCityDelegate {
    func saveEditedCity(city : String)
}
class EditCityVc: UIViewController {
 var delegate : EditCityDelegate!
    var strCity:String = ""
     var pText = ""
    @IBOutlet weak var txtCity: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtCity.text = strCity
        // Do any additional setup after loading the view.
    }
    func show(){
        self.view.frame = CGRect(x: 0, y: 0, width:SCREENWIDTH(), height: SCREENHEIGHT())
        APP_DELEGATE.window?.addSubview(self.view!)
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.view.frame = CGRect(x: 0, y: 0, width: SCREENWIDTH(), height: SCREENHEIGHT())
        }, completion: {(finished: Bool) -> Void in
            self.txtCity.text = self.pText
        })
    }
    
    
    //MARK: - Hide Picker
    func hideCustomPicker() {
        self.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - self.view.frame.size.height, width: UIScreen.main.bounds.size.width, height: self.view.frame.size.height)
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.view.frame = CGRect(x: 0, y: SCREENHEIGHT(), width: SCREENWIDTH(), height: self.view.frame.size.height)
        }, completion: {(finished: Bool) -> Void in
            self.view!.removeFromSuperview()
        })
    }

    @IBAction func btn_Save_clicked(_ sender: Any) {
        if txtCity.text!.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            showAppAlertWithMessage("You must enter city to save changes", viewController: self)
            return
        }
        delegate.saveEditedCity(city: txtCity.text ?? "")
        self.view!.removeFromSuperview()
    
    }
    
    @IBAction func btn_close_clicked(_ sender: Any) {
        self.view.endEditing(true)
        self.view!.removeFromSuperview()
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
