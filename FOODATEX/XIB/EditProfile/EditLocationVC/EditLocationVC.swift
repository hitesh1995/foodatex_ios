//
//  EditLocationVC.swift
//  FOODATEX
//
//  Created by Silver Shark on 15/11/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit
import CoreLocation


protocol EditLocationDelegate {
    func saveLocationAge(location : String)
}

class EditLocationVC: UIViewController {

    @IBOutlet weak var txtLocation: UITextField!
    @IBOutlet weak var viewLocation: UIView!
    
    var delegate : EditLocationDelegate!
    let locationManager = CLLocationManager()
    
    //MARK: - LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        viewLocation.createBordersWithColor(color: .lightGray, radius: 4, width: 1)
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
    }
    
    func show(){
        self.view.frame = CGRect(x: 0, y: 0, width:SCREENWIDTH(), height: SCREENHEIGHT())
        APP_DELEGATE.window?.addSubview(self.view!)
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.view.frame = CGRect(x: 0, y: 0, width: SCREENWIDTH(), height: SCREENHEIGHT())
        }, completion: {(finished: Bool) -> Void in
        })
    }
    
    
    //MARK: - Hide Picker
    func hideCustomPicker() {
        self.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - self.view.frame.size.height, width: UIScreen.main.bounds.size.width, height: self.view.frame.size.height)
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.view.frame = CGRect(x: 0, y: SCREENHEIGHT(), width: SCREENWIDTH(), height: self.view.frame.size.height)
        }, completion: {(finished: Bool) -> Void in
            self.view!.removeFromSuperview()
        })
    }
    
    
    @IBAction func btnLocationTapped(_ sender: Any) {
        getMyLocation()
    }
    
    
    @IBAction func btnCloseTapped(_ sender: Any) {
        
        self.view!.removeFromSuperview()
    }

    @IBAction func btnSaveTapped(_ sender: Any) {
        
        if txtLocation.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            showAppAlertWithMessage("Please Enter Address", viewController: self)
            return
        }
        delegate.saveLocationAge(location: txtLocation.text ??  "")
    }
    
    
    //MARK : - CHECK FOR CLLOCATIONMANAGER AUTHORISATION STATUS
    func getMyLocation(){
        let status = CLLocationManager.authorizationStatus()
        if status == .authorizedAlways || status == .authorizedWhenInUse { self.locationManager.startUpdatingLocation()}
        else if status == .notDetermined { self.locationManager.requestAlwaysAuthorization() }
        else if status == .denied || status == .restricted { askToAllowLocation() }
        else{ askToAllowLocation()}
    }
    
    func askToAllowLocation(){
        let alertView = UIAlertController()
        
        let settingAction = UIAlertAction(title: "Setting", style: .default) { (action) in
            if UIApplication.shared.canOpenURL(URL(string: UIApplicationOpenSettingsURLString)!){
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
                } else { UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!) }
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive) { (action) in
            
        }
        alertView.title = APP_NAME
        alertView.message = "Please allow location access in setting "
        alertView.addAction(settingAction)
        alertView.addAction(cancelAction)
        self.present(alertView, animated: true, completion: nil)
    }
    
}


//MARK: - CLLOCATION DELEGATE METHODS
extension EditLocationVC : CLLocationManagerDelegate {
    
    private func locationManager(manager: CLLocationManager!,
                                 didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        switch status {
        case CLAuthorizationStatus.authorizedAlways :
            locationManager.startUpdatingLocation()
            break
        case CLAuthorizationStatus.authorizedWhenInUse :
            locationManager.startUpdatingLocation()
            break
        case CLAuthorizationStatus.restricted :
            askToAllowLocation()
            break
        case CLAuthorizationStatus.denied:
            askToAllowLocation()
            break
        case CLAuthorizationStatus.notDetermined:
            locationManager.requestAlwaysAuthorization()
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if locations.count > 0 {
            let myLocation = locations.last!
            print(myLocation)
            
            CLGeocoder().reverseGeocodeLocation(myLocation) { (placeMarks, error) in
                if error == nil && placeMarks!.count > 0 {
                    let pm = placeMarks![0]
                    self.txtLocation.text = "\(pm.locality ?? ""), \(pm.country ?? "")"
                }
            }
            
            locationManager.stopUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        showAppAlertWithMessage("Failed to get your location, Please try again.", viewController: self)
    }
}
