//
//  EditAboutVC.swift
//  FOODATEX
//
//  Created by Silver Shark on 17/11/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit

protocol EditAboutDelegate {
    func saveEditAbout(aboutMe : String)
}


class EditAboutVC: UIViewController, UITextViewDelegate {

    @IBOutlet weak var txtAbout: UITextView!
    var delegate : EditAboutDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    
    func show(){
        self.view.frame = CGRect(x: 0, y: 0, width:SCREENWIDTH(), height: SCREENHEIGHT())
        APP_DELEGATE.window?.addSubview(self.view!)
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.view.frame = CGRect(x: 0, y: 0, width: SCREENWIDTH(), height: SCREENHEIGHT())
        }, completion: {(finished: Bool) -> Void in
            self.txtAbout.text = getLoginDetails()?.aboutYourself ?? ""
        })
    }
    
    
    //MARK: - Hide Picker
    func hideCustomPicker() {
        self.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - self.view.frame.size.height, width: UIScreen.main.bounds.size.width, height: self.view.frame.size.height)
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.view.frame = CGRect(x: 0, y: SCREENHEIGHT(), width: SCREENWIDTH(), height: self.view.frame.size.height)
        }, completion: {(finished: Bool) -> Void in
            self.view!.removeFromSuperview()
        })
    }
    
    @IBAction func btnCloseTapped(_ sender: Any) {
        self.view.endEditing(true)
        
        self.view!.removeFromSuperview()
    }
    
    @IBAction func btnSaveTapped(_ sender: Any) {
        
        if txtAbout.text!.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            showAppAlertWithMessage("You must enter text to save changes", viewController: self)
        }
        delegate.saveEditAbout(aboutMe: txtAbout.text ?? "")
        self.view!.removeFromSuperview()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars < 150
    }

}
