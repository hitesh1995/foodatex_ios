//
//  EditIntrestedInVC.swift
//  FOODATEX
//
//  Created by Silver Shark on 22/11/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit

protocol EditLookingForDelegate {
    func saveLookingFor(lookingFor : Int)
}

class EditLookingForVc: UIViewController {
    
    var delegate : EditLookingForDelegate!
    var interstedIn = 1
    
    @IBOutlet weak var imgRomance: UIImageView!
    @IBOutlet weak var imgFriendShip: UIImageView!
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    func show(){
        self.view.frame = CGRect(x: 0, y: 0, width:SCREENWIDTH(), height: SCREENHEIGHT())
        APP_DELEGATE.window?.addSubview(self.view!)
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.view.frame = CGRect(x: 0, y: 0, width: SCREENWIDTH(), height: SCREENHEIGHT())
        }, completion: {(finished: Bool) -> Void in
        })
    }
    
    
    //MARK: - Hide Picker
    func hideCustomPicker() {
        self.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - self.view.frame.size.height, width: UIScreen.main.bounds.size.width, height: self.view.frame.size.height)
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.view.frame = CGRect(x: 0, y: SCREENHEIGHT(), width: SCREENWIDTH(), height: self.view.frame.size.height)
        }, completion: {(finished: Bool) -> Void in
            self.view!.removeFromSuperview()
        })
    }
    
    @IBAction func btnRomanceTapped(_ sender: Any) {
        imgRomance.image = UIImage(named: "icon_RadioChecked")
        imgFriendShip.image = UIImage(named: "icon_RadioUnchecked")
        interstedIn = 1
    }
    
    @IBAction func btnFriendShipTapped(_ sender: Any) {
        imgFriendShip.image = UIImage(named: "icon_RadioChecked")
        imgRomance.image = UIImage(named: "icon_RadioUnchecked")
        interstedIn = 2
    }
    
 //   @IBAction func btnBothTapped(_ sender: Any) {
 //       imgBoth.image = UIImage(named: "icon_RadioChecked")
 //       imgFemale.image = UIImage(named: "icon_RadioUnchecked")
 //       imgMale.image = UIImage(named: "icon_RadioUnchecked")
 //       interstedIn = 3
 //   }
    
    
    @IBAction func btnCloseTapped(_ sender: Any) {
        self.view!.removeFromSuperview()
    }
    
    @IBAction func btnSaveTapped(_ sender: Any) {
        
        delegate.saveLookingFor(lookingFor: interstedIn)
        self.view!.removeFromSuperview()
    }
    
}
