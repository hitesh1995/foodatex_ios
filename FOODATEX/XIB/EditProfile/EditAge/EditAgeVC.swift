//
//  EditAgeVC.swift
//  FOODATEX
//
//  Created by Silver Shark on 14/11/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit


protocol EditAgeDelegate {
    func saveEditAge(age : Int)
}

class EditAgeVC: UIViewController {

    @IBOutlet weak var viewAge: AKPickerView!
    var delegate : EditAgeDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewAge.delegate = self
        viewAge.dataSource = self
        viewAge.font = APPFONT_REGULAR(17)
        viewAge.highlightedFont = APPFONT_BOLD(20)
        viewAge.interitemSpacing = 15
        viewAge.pickerViewStyle = .style3D
        viewAge.selectItem(0, animated: true)
    }
    
    
    func show(){
        self.view.frame = CGRect(x: 0, y: 0, width:SCREENWIDTH(), height: SCREENHEIGHT())
        APP_DELEGATE.window?.addSubview(self.view!)
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.view.frame = CGRect(x: 0, y: 0, width: SCREENWIDTH(), height: SCREENHEIGHT())
        }, completion: {(finished: Bool) -> Void in
        })
    }
    
    
    //MARK: - Hide Picker
    func hideCustomPicker() {
        self.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - self.view.frame.size.height, width: UIScreen.main.bounds.size.width, height: self.view.frame.size.height)
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.view.frame = CGRect(x: 0, y: SCREENHEIGHT(), width: SCREENWIDTH(), height: self.view.frame.size.height)
        }, completion: {(finished: Bool) -> Void in
            self.view!.removeFromSuperview()
        })
    }

    
    @IBAction func btnCloseTapped(_ sender: Any) {
        
        self.view!.removeFromSuperview()
    }
    
    @IBAction func btnSaveTapped(_ sender: Any) {
        
        delegate.saveEditAge(age: Int(viewAge.selectedItem + 18))
        self.view!.removeFromSuperview()
    }
}

extension EditAgeVC : AKPickerViewDataSource, AKPickerViewDelegate{
    func numberOfItems(in pickerView: AKPickerView!) -> UInt {
        return 100
    }
    func pickerView(_ pickerView: AKPickerView!, titleForItem item: Int) -> String! {
        return "\(item + 18)"
    }
    
    func pickerView(_ pickerView: AKPickerView!, didSelectItem item: Int) {
    }
}
