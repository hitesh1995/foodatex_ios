//
//  MatchPopUpVC.swift
//  FOODATEX
//
//  Created by Uffizio iMac on 21/12/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit
import SDWebImage

protocol MatchPopupDelegate {
    func sendMessageToUserId(otherUserId : String, userName : String, timeAgo : String, image : String, token : String)
}

class MatchPopUpVC: UIViewController {

    @IBOutlet weak var lblMatchMessage: UILabel!
    @IBOutlet weak var imgUserOne: UIImageView!
    @IBOutlet weak var imgUserTwo: UIImageView!
    @IBOutlet weak var btnSend: UIButton!
    
    var matchDict: [String : Any]?
    var delegate : MatchPopupDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgUserOne.setCornerRadius(radius: imgUserOne.frame.height/2)
        imgUserTwo.setCornerRadius(radius: imgUserTwo.frame.height/2)
    }
    
    @IBAction func btnCloseTapped(_ sender: Any) {
        hideCustomPicker()
    }
    
    @IBAction func btnSendTapped(_ sender: Any) {
        if delegate != nil{
            delegate?.sendMessageToUserId(otherUserId: matchDict?["sender_id"] as? String ?? "0" , userName: matchDict?["sender_name"] as? String ?? "User", timeAgo: matchDict?["sender_name"] as? String ?? "", image : matchDict?["sender_pic"] as? String ?? "", token : matchDict?["deviceToken"] as? String ?? "")
        }
    }
    
    func show(){
        
        self.view.frame = CGRect(x: 0, y: 0, width:SCREENWIDTH(), height: SCREENHEIGHT())
        APP_DELEGATE.window?.addSubview(self.view!)
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.view.frame = CGRect(x: 0, y: 0, width: SCREENWIDTH(), height: SCREENHEIGHT())
        }, completion: {(finished: Bool) -> Void in
            self.populatePopUp()
        })
    }
    
    
    //MARK: - Hide Picker
    func hideCustomPicker() {
        self.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - self.view.frame.size.height, width: UIScreen.main.bounds.size.width, height: self.view.frame.size.height)
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.view.frame = CGRect(x: 0, y: SCREENHEIGHT(), width: SCREENWIDTH(), height: self.view.frame.size.height)
        }, completion: {(finished: Bool) -> Void in
            self.imgUserOne.createCornerRadiusWithShadowNew(cornerRadius: self.imgUserOne.frame.height/2, offset: .zero, opacity: 0.2, radius: 1)
            self.imgUserTwo.createCornerRadiusWithShadowNew(cornerRadius: self.imgUserTwo.frame.height/2, offset: .zero, opacity: 0.3, radius: 1)
            self.view!.removeFromSuperview()
        })
    }
    
    func populatePopUp(){
        
        lblMatchMessage.text = "You and \(matchDict?["sender_name"] as? String ?? "User") have Liked each other"
        
        if let userPic : URL = URL(string: matchDict?["user_pic"] as? String ?? "") {
            imgUserOne.sd_setImage(with: userPic, placeholderImage: Set_Local_Image("image_Placeholder"), options: .refreshCached, completed: nil)
        }
        
        if let senderPic : URL = URL(string: matchDict?["sender_pic"] as? String ?? "") {
            imgUserTwo.sd_setImage(with: senderPic, placeholderImage: Set_Local_Image("image_Placeholder"), options: .refreshCached, completed: nil)
        }
    }
}
