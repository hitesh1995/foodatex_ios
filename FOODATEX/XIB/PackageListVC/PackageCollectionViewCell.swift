//
//  PackageCollectionViewCell.swift
//  FOODATEX
//
//  Created by SANKET SHARMA on 12/03/19.
//  Copyright © 2019 RudraApps. All rights reserved.
//

import UIKit

class PackageCollectionViewCell: UICollectionViewCell {
      @IBOutlet weak var imgBackGround: RemoteImageView!
    @IBOutlet weak var imgBack: UIView!
    
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var lblMonthValue: UILabel!
    
    @IBOutlet weak var lblDuration: UILabel!
    
    @IBOutlet weak var lblPrice: UILabel!
    
    @IBOutlet weak var lblPayDetail: UILabel!
    
    @IBOutlet weak var lblHeaderTop: NSLayoutConstraint!
    
    
}
