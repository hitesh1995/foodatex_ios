//
//  PackageListVC.swift
//  FOODATEX
//
//  Created by Uffizio iMac on 13/12/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit
import SwiftyStoreKit
import SVProgressHUD
class PackageListVC: BaseVC, UIScrollViewDelegate, UICollectionViewDelegateFlowLayout {
    
    // @IBOutlet weak var lblPackageImageTitle: UILabel!
    
    //  @IBOutlet weak var lblPackageImageDescription: UILabel!
    var currentSelectedSubId:String = twelveMonthSubscriptionId
    var currentPackageId:String = "1"
    @IBOutlet weak var collectionViewForImages: UICollectionView!
    
    
    var aryPackageListArray : [PackageList] = []
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var collectionViewPackage: UICollectionView!
    
    var arrPackageList : [PackageData] = []
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewContinue: UIView!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var lblGetGold: GradientLabel!
    var selectedSection = 0
    var onceOnly = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionViewForImages.delegate = self
        
        
        collectionViewPackage.delegate = self
        collectionViewPackage.collectionViewLayout = LineLayout()
        
        show()
        //print("array=",arrPackageList)
        if arrPackageList[0].packageList != nil{
            if (arrPackageList[0].packageList?.count)! > 0{
                aryPackageListArray = arrPackageList[0].packageList!
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationbarleft_imagename("icon_Back", left_action: #selector(navigateToPreviousView), right_imagename: "", right_action: #selector(doNothing), title: "Get Foodates Gold", isLogo: false)
    }
    
    func show(){
        
        self.view.frame = CGRect(x: 0, y: 0, width:SCREENWIDTH(), height: SCREENHEIGHT())
        APP_DELEGATE.window?.addSubview(self.view!)
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.view.frame = CGRect(x: 0, y: 0, width: SCREENWIDTH(), height: SCREENHEIGHT())
        }, completion: {(finished: Bool) -> Void in
            
            self.setUpUI()
        })
    }
    
    //MARK:-
    @IBAction func btnSubscriptionDetailAction(_ sender: Any) {
        guard let url = URL(string: "https://foodatex.com/") else { return }
        
        if UIApplication.shared.canOpenURL(url){
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func btnTermsAction(_ sender: Any) {
        guard let url = URL(string: "https://foodatex.com/terms/") else { return }
        if UIApplication.shared.canOpenURL(url){
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func btnPrivacyPolicyAction(_ sender: Any) {
        guard let url = URL(string: "http://139.59.35.213/foodatex/privacy") else { return }
        if UIApplication.shared.canOpenURL(url){
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    //MARK: - Hide Picker
    func hideCustomPicker() {
        self.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - self.view.frame.size.height, width: UIScreen.main.bounds.size.width, height: self.view.frame.size.height)
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.view.frame = CGRect(x: 0, y: SCREENHEIGHT(), width: SCREENWIDTH(), height: self.view.frame.size.height)
        }, completion: {(finished: Bool) -> Void in
            self.view!.removeFromSuperview()
        })
    }
    
    func setUpUI(){
        // self.viewMain.createAppCornerRadiusWithShadow()
        viewContinue.setCornerRadius(radius: 8)
        let gradient = CAGradientLayer()
        gradient.frame = btnContinue.bounds
        gradient.colors = [ COLOR_CUSTOM(237, 200, 67, 1.0).cgColor, COLOR_CUSTOM(229, 156, 40, 1.0).cgColor ]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        btnContinue.layer.insertSublayer(gradient, at: 0)
    }
    
    @IBAction func btn_Continue_clicked(_ sender: Any) {
        SVProgressHUD.show()
        purchase(subscriptionId: currentSelectedSubId)
        // self.dismiss(animated: false, completion: nil)
    }
    
    
    
    @IBAction func btnCloseTapped(_ sender: Any) {
        // hideCustomPicker()
        self.dismiss(animated: false, completion: nil)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView == collectionViewPackage{
            let visibleRect = CGRect(origin: collectionViewPackage.contentOffset, size: collectionViewPackage.bounds.size)
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            let visibleIndexPath = collectionViewPackage.indexPathForItem(at: visiblePoint)
            print("currentIndex===",visibleIndexPath?.row)
            let selectedPackageId = (arrPackageList[visibleIndexPath!.row]).packageId
            currentPackageId = selectedPackageId ?? ""
            switch (visibleIndexPath?.row) {
            case 0:
                self.currentSelectedSubId = twelveMonthSubscriptionId
                break;
            case 1:
                self.currentSelectedSubId = sixMonthSubscriptionId
                break;
            case 2:
                self.currentSelectedSubId = monthlySubscriptionId
                break;
            default:
                break;
            }
            
            aryPackageListArray = arrPackageList[(visibleIndexPath?.row)!].packageList!
            // Making UI changes for selected and unselected cells
            for j in 0..<arrPackageList.count{
                if let cell = self.collectionViewPackage.cellForItem(at: IndexPath(row: j, section: 0)) as? PackageCollectionViewCell{
                    if j != visibleIndexPath!.row{
                        cell.imgBackGround.isHidden = true
                        deActiveColor(cell: cell)
                    }else{
                        cell.imgBackGround.isHidden = false
//                        DispatchQueue.main.async {
//                            if IS_IPHONE_6_7 {
//                                cell.lblHeaderTop.constant = 7
//                            }
//                            if IS_IPHONE_6P_7P{
//                                cell.lblHeaderTop.constant = 8
//                            }
//                            if IS_IPHONE_5 || IS_IPHONE_4_OR_LESS{
//                                cell.lblHeaderTop.constant = 4
//                            }
//                        }
                        
                        activeColor(cell: cell, colorCode: arrPackageList[j].textColor ?? "")
                    }
                }
            }
            
            DispatchQueue.global(qos: .background).async {
                
                DispatchQueue.main.async {
                    self.pageControl.numberOfPages = self.aryPackageListArray.count
                    self.pageControl.currentPage = 0
                    self.collectionViewForImages.reloadData()
                    self.collectionViewForImages.scrollToItem(at: IndexPath(row: 0, section: 0), at: .centeredHorizontally, animated: false)
                }
            }
            
            
            
            
            
            
        }else{
            let visibleRect = CGRect(origin: collectionViewForImages.contentOffset, size: collectionViewForImages.bounds.size)
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            let visibleIndexPath1 = collectionViewForImages.indexPathForItem(at: visiblePoint)
            self.pageControl.currentPage = (visibleIndexPath1?.row)!
        }
        
        
    }
    func updatePurchaseId( purchaseId : String) {
        
        self.updateProfile(dict: ["user_id": getLoginDetails()?.userId ?? "",
                                  "PurchaseId" : purchaseId]) { isSaved in
                                    if isSaved {
                                        var user = getLoginDetails()
                                        //                                        user?.height = height
                                        //                                        self.userProfileObj.height = height
                                        
                                        setLoginDetails(user)
                                        SVProgressHUD.dismiss()
                                        self.dismiss(animated: false, completion: nil)
                                    }
        }
    }
    //MARK: - UPDATE PROFILE
    func updateProfile( dict : [String : Any], response : @escaping (_ isUploaded : Bool) -> Void ){
        
        if isConnectedToNetwork(){
            
            SVProgressHUD.show()
            WebServicesCollection.sharedInstance.UpdateProfile(dict) { (wresponse, error, message, rstatus) in
                SVProgressHUD.dismiss()
                //print("")
                if rstatus == 1{
                    response(true)
                }else{
                    showAppAlertWithMessage(message ?? "Something went wrong, Please trye Again", viewController: self)
                    response(false)
                }
            }
            
        }else{
            response(false)
        }
    }
    
    func deActiveColor(cell:PackageCollectionViewCell){
        //E99800
        cell.lblHeaderTitle.textColor = .black
        cell.lblPayDetail.textColor = .darkGray
        cell.lblPrice.textColor = .black
        cell.lblDuration.textColor = .black
        cell.lblMonthValue.textColor = .black
        cell.lblHeaderTitle.isHidden = true
    }
    func activeColor(cell:PackageCollectionViewCell,colorCode:String){
        cell.lblHeaderTitle.textColor = UIColor.white
        cell.lblPayDetail.textColor = UIColor(hexString: colorCode)
        cell.lblPrice.textColor = UIColor(hexString: colorCode)
        cell.lblDuration.textColor = UIColor(hexString: colorCode)
        cell.lblMonthValue.textColor = UIColor(hexString: colorCode)
        cell.lblHeaderTitle.isHidden = false
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionViewPackage{
            return CGSize(width: self.collectionViewPackage.frame.size.height-80, height: self.collectionViewPackage.frame.size.height - 35)
        }else{
            return CGSize(width: collectionViewForImages.frame.size.width, height: collectionViewForImages.frame.size.height)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == collectionViewForImages{
            return 0
        }else{
            return 50
        }
    }
}

extension PackageListVC : UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewPackage{
            return arrPackageList.count
        }else{
            return aryPackageListArray.count
        }
    }
    internal func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == collectionViewPackage{
            let cell = cell as! PackageCollectionViewCell
            deActiveColor(cell: cell)
            if !onceOnly {
                let indexToScrollTo = IndexPath(item: 0, section: 0)
                self.collectionViewPackage.scrollToItem(at: indexToScrollTo, at: .centeredHorizontally, animated: false)
                
                cell.imgBackGround.isHidden = false
                //  aryPackageListArray = arrPackageList[indexPath.row].packageList!
                self.pageControl.numberOfPages = aryPackageListArray.count
                cell.lblHeaderTitle.isHidden = false
                cell.imgBackGround.resizeImage = false
                activeColor(cell: cell, colorCode: arrPackageList[indexPath.row].textColor ?? "")
                onceOnly = true
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionViewPackage{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "packageCellId", for: indexPath) as! PackageCollectionViewCell
            cell.imgBackGround.isHidden = true
            cell.lblHeaderTitle.text = arrPackageList[indexPath.row].title
            cell.lblPayDetail.text = arrPackageList[indexPath.row].descriptionField
            cell.lblPrice.text = "£" + arrPackageList[indexPath.row].price!
            cell.lblDuration.text = "Months"
            cell.lblMonthValue.text = arrPackageList[indexPath.row].duration
            let strUrl = (IMAGE_BASE_URL + ((arrPackageList[indexPath.row]).image ?? "") )
            cell.imgBackGround.sd_setImage(with: URL(string: strUrl), placeholderImage: nil, options: .refreshCached, completed: nil)
            
//            if IS_IPHONE_6_7 {
//                cell.lblHeaderTop.constant = 7
//            }
//            if IS_IPHONE_6P_7P{
//                cell.lblHeaderTop.constant = 8
//            }
//            if IS_IPHONE_5 || IS_IPHONE_4_OR_LESS{
//                cell.lblHeaderTop.constant = 4
//            }
            
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imgCellId", for: indexPath) as! ImageCollectionViewCell
            print("imagesArray==",aryPackageListArray)
            //  cell.imgView.imageURL = URL(string: (aryPackageListArray[indexPath.row]).image ?? "")
            cell.lblTitle.text = aryPackageListArray[indexPath.row].title
            //  lblPackageImageDescription.text = aryPackageListArray[indexPath.row].descriptionField
            return cell
        }
    }
    //    func makeActive(cell:PackageCollectionViewCell){
    ////        cell.imgBack.layer.cornerRadius = 5
    ////        cell.imgBack.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 0.9])
    ////        cell.imgBack.layer.masksToBounds = false
    ////        cell.imgBack.layer.cornerRadius = 5.0
    ////        cell.imgBack.layer.shadowOffset = CGSize(width: 0, height: -0.5)
    ////        cell.imgBack.layer.shadowOpacity = 0.2
    ////
    ////        let gradient = CAGradientLayer()
    ////        gradient.frame = CGRect(x: 0, y: 0, width: cell.viewHeader.frame.width + 20, height: cell.viewHeader.frame.size.height)
    ////        gradient.colors = [ COLOR_CUSTOM(237, 200, 67, 1.0).cgColor, COLOR_CUSTOM(229, 156, 40, 1.0).cgColor ]
    ////        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
    ////        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
    ////        cell.viewHeader.layer.insertSublayer(gradient, at: 0)
    ////        cell.viewHeader.clipsToBounds = true
    //    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    //MARK: - Purchasing Method
    func purchase(subscriptionId:String){
        SwiftyStoreKit.purchaseProduct(subscriptionId, quantity: 1, atomically: true) { result in
            
            switch result {
            case .success(let purchase):
                print("Purchase Success: \(purchase.productId)")
                
                UserDefaults.standard.set(true, forKey: self.currentSelectedSubId)
                UserDefaults.standard.synchronize()
                self.updatePurchaseId(purchaseId: self.currentPackageId)//updating purchase id
                
            case .error(let error):
                switch error.code {
                case .unknown: print("Unknown error. Please contact support")
                
                SVProgressHUD.dismiss()
                case .clientInvalid: print("Not allowed to make the payment")
                SVProgressHUD.dismiss()
                case .paymentCancelled:
                    SVProgressHUD.dismiss()
                    break
                case .paymentInvalid: print("The purchase identifier was invalid")
                SVProgressHUD.dismiss()
                case .paymentNotAllowed: print("The device is not allowed to make the payment")
                SVProgressHUD.dismiss()
                case .storeProductNotAvailable: print("The product is not available in the current storefront")
                SVProgressHUD.dismiss()
                case .cloudServicePermissionDenied: print("Access to cloud service information is not allowed")
                SVProgressHUD.dismiss()
                case .cloudServiceNetworkConnectionFailed: print("Could not connect to the network")
                SVProgressHUD.dismiss()
                case .cloudServiceRevoked: print("User has revoked permission to use this cloud service")
                SVProgressHUD.dismiss()
                default: print((error as NSError).localizedDescription)
                SVProgressHUD.dismiss()
                }
            }
        }
        
    }
}
extension UIView {
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
}
extension Int {
    func duplicate4bits() -> Int {
        return (self << 4) + self
    }
}
extension UIColor {
    
    convenience init(r:CGFloat,g:CGFloat,b:CGFloat) {
        self.init(red:r/255,green:g/255,blue:b/255,alpha:1)
    }
    
    convenience init(r:CGFloat,g:CGFloat,b:CGFloat,a:CGFloat) {
        self.init(red:r/255,green:g/255,blue:b/255,alpha:a)
    }
    
    
    public convenience init?(hexString: String) {
        self.init(hexString: hexString, alpha: 1.0)
    }
    
    fileprivate convenience init?(hex3: Int, alpha: Float) {
        self.init(red:   CGFloat( ((hex3 & 0xF00) >> 8).duplicate4bits() ) / 255.0,
                  green: CGFloat( ((hex3 & 0x0F0) >> 4).duplicate4bits() ) / 255.0,
                  blue:  CGFloat( ((hex3 & 0x00F) >> 0).duplicate4bits() ) / 255.0,
                  alpha: CGFloat(alpha))
    }
    
    fileprivate convenience init?(hex6: Int, alpha: Float) {
        self.init(red:   CGFloat( (hex6 & 0xFF0000) >> 16 ) / 255.0,
                  green: CGFloat( (hex6 & 0x00FF00) >> 8 ) / 255.0,
                  blue:  CGFloat( (hex6 & 0x0000FF) >> 0 ) / 255.0, alpha: CGFloat(alpha))
    }
    public convenience init?(hexString: String, alpha: Float) {
        var hex = hexString
        
        // Check for hash and remove the hash
        if hex.hasPrefix("#") {
            hex = hex.substring(from: hex.index(hex.startIndex, offsetBy: 1))
        }
        
        guard let hexVal = Int(hex, radix: 16) else {
            self.init()
            return nil
        }
        
        switch hex.characters.count {
        case 3:
            self.init(hex3: hexVal, alpha: alpha)
        case 6:
            self.init(hex6: hexVal, alpha: alpha)
        default:
            // Note:
            // The swift 1.1 compiler is currently unable to destroy partially initialized classes in all cases,
            // so it disallows formation of a situation where it would have to.  We consider this a bug to be fixed
            // in future releases, not a feature. -- Apple Forum
            self.init()
            return nil
        }
    }
    //    public convenience init?(hex: Int) {
    //       // self.init(hex: hex, alpha: 1.0)
    //    }
    //    public convenience init?(hex: Int, alpha: Float) {
    //        if (0x000000 ... 0xFFFFFF) ~= hex {
    //            self.init(hex6: hex, alpha: alpha)
    //        } else {
    //            self.init()
    //            return nil
    //        }
    //    }
    
}
