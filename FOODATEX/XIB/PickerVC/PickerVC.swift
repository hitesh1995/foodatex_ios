//
//  PickerVC.swift
//  FOODATEX
//
//  Created by Silver Shark on 21/08/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

protocol PickerDelegate {
    func pickerDidFinishedWithIndex( _ selectedIndex : [IndexPath])
    func pickerDidCancelled()
}

import UIKit

class PickerVC: UIViewController {
    
    var arrayPickerTemp : [String] = [String]()
    var isMultiSelection = false
    var titleMessage = "Your Preference"
    var selectedIndex : [IndexPath] = [IndexPath]()
    var delegate : PickerDelegate!
    
    @IBOutlet weak var tblPicker: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblPicker.register(UINib(nibName: "TVCPicker", bundle: Bundle.main), forCellReuseIdentifier: "TVCPicker")
        tblPicker.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnCloseTapped(_ sender: Any) {
        delegate.pickerDidCancelled()
    }
    
    @IBAction func btnDoneTapped(_ sender: Any) {
        if selectedIndex.count == 0 {
            showAppAlertWithMessage("You must select atleast one value", viewController: self)
        }else{
            delegate.pickerDidFinishedWithIndex(selectedIndex)
        }
    }
    
    
    func show(){
        self.view.frame = CGRect(x: 0, y: 0, width:SCREENWIDTH(), height: SCREENHEIGHT())
        APP_DELEGATE.window?.addSubview(self.view!)
        lblTitle.text = titleMessage
        self.tblPicker.reloadData()
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.view.frame = CGRect(x: 0, y: 0, width: SCREENWIDTH(), height: SCREENHEIGHT())
        }, completion: {(finished: Bool) -> Void in
        })
    }

    
    //MARK: - Hide Picker
    func hideCustomPicker() {
        self.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - self.view.frame.size.height, width: UIScreen.main.bounds.size.width, height: self.view.frame.size.height)
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height, width: UIScreen.main.bounds.size.width, height: self.view.frame.size.height)
        }, completion: {(finished: Bool) -> Void in
            self.view!.removeFromSuperview()
        })
    }

}


extension PickerVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayPickerTemp.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : TVCPicker = tableView.dequeueReusableCell(withIdentifier: "TVCPicker", for: indexPath) as! TVCPicker
        cell.textLabel?.text = arrayPickerTemp[indexPath.row]
        if selectedIndex.contains(indexPath){
            cell.accessoryType = .checkmark
        }else{
            cell.accessoryType = .none
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isMultiSelection{
            if selectedIndex.contains(indexPath) {
                selectedIndex.remove(at: selectedIndex.index(of: indexPath)!)
            }else{
                selectedIndex.append(indexPath)
            }
            
            tblPicker.reloadRows(at: [indexPath], with: .none)
        }else{
            if selectedIndex.count != 0{
                selectedIndex.removeAll()
                tblPicker.reloadData()
            }
            selectedIndex.append(indexPath)
            tblPicker.reloadRows(at: [indexPath], with: .none)
        }
    }
}
