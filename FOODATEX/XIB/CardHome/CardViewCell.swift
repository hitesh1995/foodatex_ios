//
//  CardViewCell.swift
//  WhipSnapper
//
//  Created by Silver Shark on 29/04/18.
//  Copyright © 2018 WhipSnapper. All rights reserved.
//

import UIKit


class CardViewCell: UIView {

    
    @IBOutlet weak var imgUser: RemoteImageView!
    
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblFoodType: UILabel!
    @IBOutlet var lblUserLocation: UILabel!
    @IBOutlet weak var uvwMain : UIView!

    @IBOutlet var btnExpandProfile: UIButton!
    

}
