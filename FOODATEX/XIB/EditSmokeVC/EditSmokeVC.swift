//
//  EditSmokeVC.swift
//  FOODATEX
//
//  Created by Silver Shark on 22/11/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit

protocol EditSmokeDelegate {
    func saveSmoke(isDoSmoke : Int, pType : Int)
}

class EditSmokeVC: UIViewController {
    
    var PickerType = 0
    var delegate : EditSmokeDelegate!
    var smoke = 2
    
    @IBOutlet weak var imgYes: UIImageView!
    @IBOutlet weak var imgNo: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var imgOccasionaly: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    func show(){
        
        self.view.frame = CGRect(x: 0, y: 0, width:SCREENWIDTH(), height: SCREENHEIGHT())
        APP_DELEGATE.window?.addSubview(self.view!)
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.view.frame = CGRect(x: 0, y: 0, width: SCREENWIDTH(), height: SCREENHEIGHT())
        }, completion: {(finished: Bool) -> Void in
            
            switch self.PickerType {
            case 0:
                self.lblTitle.text = "Do You smoke?"
                self.lblMessage.text = ""
                if let user = getLoginDetails() {
                    if let pr = user.takeSmoke {
                        switch pr {
                        case "1":
                            self.smoke = 1
                            self.imgYes.image = UIImage(named: "icon_RadioChecked")
                            self.imgNo.image = UIImage(named: "icon_RadioUnchecked")
                            self.imgOccasionaly.image = UIImage(named: "icon_RadioUnchecked")
                            break
                        case "3":
                            self.smoke = 3
                            self.imgYes.image = UIImage(named: "icon_RadioUnchecked")
                            self.imgNo.image = UIImage(named: "icon_RadioUnchecked")
                            self.imgOccasionaly.image = UIImage(named: "icon_RadioChecked")
                            break
                        default:
                            self.smoke = 2
                            self.imgYes.image = UIImage(named: "icon_RadioUnchecked")
                            self.imgNo.image = UIImage(named: "icon_RadioChecked")
                            self.imgOccasionaly.image = UIImage(named: "icon_RadioUnchecked")
                            break
                        }
                        
                    }
                }
                break
            case 1:
                self.lblTitle.text = "Do You Drink?"
                self.lblMessage.text = ""
                if let user = getLoginDetails() {
                    if let pr = user.takeDrink{
                        switch pr {
                        case "1":
                            self.smoke = 1
                            self.imgYes.image = UIImage(named: "icon_RadioChecked")
                            self.imgNo.image = UIImage(named: "icon_RadioUnchecked")
                            self.imgOccasionaly.image = UIImage(named: "icon_RadioUnchecked")
                            break
                        case "3":
                            self.smoke = 3
                            self.imgYes.image = UIImage(named: "icon_RadioUnchecked")
                            self.imgNo.image = UIImage(named: "icon_RadioUnchecked")
                            self.imgOccasionaly.image = UIImage(named: "icon_RadioChecked")
                            break
                        default:
                            self.smoke = 2
                            self.imgYes.image = UIImage(named: "icon_RadioUnchecked")
                            self.imgNo.image = UIImage(named: "icon_RadioChecked")
                            self.imgOccasionaly.image = UIImage(named: "icon_RadioUnchecked")
                            break
                        }
                    }
                }
                break
            case 2:
                self.lblTitle.text = "Do You take Drug?"
                self.lblMessage.text = ""
                if let user = getLoginDetails() {
                    if let pr = user.takeDrug {
                        switch pr {
                        case "1":
                            self.smoke = 1
                            self.imgYes.image = UIImage(named: "icon_RadioChecked")
                            self.imgNo.image = UIImage(named: "icon_RadioUnchecked")
                            self.imgOccasionaly.image = UIImage(named: "icon_RadioUnchecked")
                            break
                        case "3":
                            self.smoke = 3
                            self.imgYes.image = UIImage(named: "icon_RadioUnchecked")
                            self.imgNo.image = UIImage(named: "icon_RadioUnchecked")
                            self.imgOccasionaly.image = UIImage(named: "icon_RadioChecked")
                            break
                        default:
                            self.smoke = 2
                            self.imgYes.image = UIImage(named: "icon_RadioUnchecked")
                            self.imgNo.image = UIImage(named: "icon_RadioChecked")
                            self.imgOccasionaly.image = UIImage(named: "icon_RadioUnchecked")
                            break
                        }
                    }
                }
                break
            default:
                break
            }
        })
    }
    
    
    //MARK: - Hide Picker
    func hideCustomPicker() {
        self.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - self.view.frame.size.height, width: UIScreen.main.bounds.size.width, height: self.view.frame.size.height)
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.view.frame = CGRect(x: 0, y: SCREENHEIGHT(), width: SCREENWIDTH(), height: self.view.frame.size.height)
        }, completion: {(finished: Bool) -> Void in
            self.view!.removeFromSuperview()
        })
    }
    
    
    @IBAction func btnYesTapped(_ sender: Any) {
        imgYes.image = UIImage(named: "icon_RadioChecked")
        imgNo.image = UIImage(named: "icon_RadioUnchecked")
        imgOccasionaly.image = UIImage(named: "icon_RadioUnchecked")
        smoke = 1
    }
    
    @IBAction func btnNoTapped(_ sender: Any) {
        imgNo.image = UIImage(named: "icon_RadioChecked")
        imgYes.image = UIImage(named: "icon_RadioUnchecked")
        imgOccasionaly.image = UIImage(named: "icon_RadioUnchecked")
        smoke = 2
    }
    
    @IBAction func btnOccasionallyTapped(_ sender: Any) {
        imgOccasionaly.image = UIImage(named: "icon_RadioChecked")
        imgYes.image = UIImage(named: "icon_RadioUnchecked")
        imgNo.image = UIImage(named: "icon_RadioUnchecked")
        smoke = 3
    }
    
    @IBAction func btnCloseTapped(_ sender: Any) {
        
        self.view!.removeFromSuperview()
    }
    
    @IBAction func btnSaveTapped(_ sender: Any) {
        
        delegate.saveSmoke(isDoSmoke: smoke, pType: PickerType)
        self.view!.removeFromSuperview()
    }
}
