//
//  BaseVC.swift
//  Whipsnnaper
//
//  Created by Devang Tandel on 09/01/17.
//  Copyright © 2017 Whipsnnaper. All rights reserved.
//

import UIKit
import AVFoundation

class BaseVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: NavigationBar set
    func setNavigationbarleft_imagename(_ left_imagename: String,
                                        left_action: Selector,
                                        right_imagename: String,
                                        right_action: Selector,
                                        title: String, isLogo:Bool){
        
        self.navigationController!.navigationBar.barTintColor = .white
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = nil
        self.navigationItem.titleView = UIView()
        //let lblLine : UILabel = UILabel(frame: CGRect(x: 0,y: 0,width: SCREENWIDTH(),height: 0.002))
       // lblLine.backgroundColor = UIColor.lightGray
        //self.navigationController?.navigationBar.shadowImage = UIColor.white.PixelOneImageImage()
        self.navigationController?.navigationBar.isTranslucent = false
        setNavShadow()
        if !(left_imagename.count == 0 ) {
            let btnleft: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
            btnleft.setTitleColor(UIColor.white, for: UIControlState())
            btnleft.setImage(Set_Local_Image(left_imagename), for: UIControlState())
            btnleft.addTarget(self, action: left_action, for: .touchDown)
            let backBarButon: UIBarButtonItem = UIBarButtonItem(customView: btnleft)
            self.navigationItem.setLeftBarButton(backBarButon, animated: false)
        }else{
            self.navigationItem.hidesBackButton = true;
            self.navigationItem.leftBarButtonItem = nil;
        }
        
        if !(right_imagename.count == 0) {
            let btnright: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
            btnright.layer.cornerRadius = btnright.frame.width/2
            btnright.setImage(Set_Local_Image(right_imagename), for: UIControlState())
            btnright.addTarget(self, action: right_action, for: .touchDown)
            let backBarButon: UIBarButtonItem = UIBarButtonItem(customView: btnright)
            self.navigationItem.setRightBarButton(backBarButon, animated: false)
        }else{ self.navigationItem.rightBarButtonItem = nil }
        
        if isLogo {
            let imgLogo : UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 130, height: 35))
            imgLogo.image = UIImage(named: "image_LoginLogo1")
            imgLogo.contentMode = .scaleAspectFit
            imgLogo.clipsToBounds = true
            self.navigationItem.titleView = imgLogo
        }else if title.count > 0 {
            let lblValues: UILabel = UILabel()
            lblValues.text = title
            lblValues.clipsToBounds = false
            lblValues.backgroundColor = .clear
            lblValues.textColor = .black
           // lblValues.font = UIFont.boldSystemFont(ofSize: 15)
            lblValues.font = UIFont.init(name: "Century Gothic Bold", size: 16)
            //lblValues.font = APPFONT_SEMIBOLD(15)
            lblValues.sizeToFit()
            lblValues.tag = 1212
            self.navigationItem.titleView = lblValues
            
        }else{
            self.navigationItem.titleView = UIView()
        }
    }
    
    //MARK: - POP TO VIEW
     @objc func navigateToPreviousView()  {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - POP TO ROOT VIEW
     @objc func navigateToRootView()  {
        self.view.endEditing(true)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    //MARK: - REMOVE EXTRA SEPARATOR
    func removeExtraSeparator(_ tableToremove:UITableView)  {
        tableToremove.tableFooterView = UIView()
    }
    @objc func doNothing(){}
    
    func setNavShadow(){
        //self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 1
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 0.5)
        self.navigationController?.navigationBar.layer.shadowRadius = 5
    }

}

extension UIColor {
    func PixelOneImageImage() -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        let ctx = UIGraphicsGetCurrentContext()
        self.setFill()
        ctx!.fill(CGRect(x: 0, y: 0, width: 1, height: 2))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
