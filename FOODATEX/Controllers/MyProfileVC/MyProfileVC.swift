//
//  MyProfileVC.swift
//  FOODATEX
//
//  Created by Silver Shark on 25/10/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD
import SwiftyStoreKit
import Alamofire
class MyProfileVC: BaseVC {

    @IBOutlet weak var imgProfile: RemoteImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var viewGold: UIView!
    @IBOutlet weak var btnGetGold: UIButton!
    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var btnDetail: UIButton!
    var userProfileObj : UserProfileData!
    
    var arrPackageList : [PackageData] = [PackageData]()
    var packageListPopUp : PackageListVC!
    
    //MARK: - LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        viewGold.setCornerRadius(radius: 4)
        
        NotificationCenter.default.addObserver(self, selector: #selector(btnGetGoldTapped(_:)), name: NSNotification.Name("GetGold"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        setNavigationbarleft_imagename("icon_UserRed", left_action: #selector(navigateToPreviousView), right_imagename: "Chat@2.png", right_action: #selector(openChatList), title: "", isLogo: true)
        lblName.text = (getLoginDetails()?.fullname ?? "").trimmingCharacters(in: .whitespaces) + ", " + (getLoginDetails()?.dob ?? "")
        
        imgProfile.setCornerRadius(radius: imgProfile.frame.width/2)
        callWSForProfile()
        
        if let userImageUrl = URL(string: getLoginDetails()?.profileImage ?? "") {
            imgProfile.sd_setImage(with: userImageUrl, placeholderImage: UIImage(named: "image_Placeholder"), options: .refreshCached, completed: nil)
        }
        
        /*
        if let userImage = getLoginDetails()?.images?.first {
            
            if let url : URL = URL(string: userImage.image ?? "") {
                imgProfile.image = #imageLiteral(resourceName: "image_Placeholder")
                imgProfile.imageURL = url
//                imgProfile.sd_setImage(with: url, completed: { (image, error, cType, rurl) in
//                    if image == nil{
//                        self.imgProfile.image = Set_Local_Image("image_Placeholder")
//                    }
//                })
            }
        }else{
            imgProfile.image = Set_Local_Image("image_Placeholder")
        }
        */
    }
    
    override func viewDidLayoutSubviews() {
        view.layoutIfNeeded()
        let gradient = CAGradientLayer()
        gradient.frame = btnGetGold.bounds
        gradient.colors = [ COLOR_CUSTOM(237, 200, 67, 1.0).cgColor, COLOR_CUSTOM(229, 156, 40, 1.0).cgColor ]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        btnGetGold.layer.insertSublayer(gradient, at: 0)
        
    }
    
    @objc func openChatList(){
        self.navigationController?.pushViewController(loadVC(AppStoryboards.kSBChat, strVCId: AppViewControllers.Chat.kVCChatList), animated: true)
    }
    
    @IBAction func btnLogoutTapped(_ sender: Any) {
        
        let alert = UIAlertController(title: APP_NAME, message: "Are you sure, you want to logout?", preferredStyle: .alert)
        let logout = UIAlertAction(title: "Logout", style: .default) { (_) in
            self.callWSForLogout()
        }
        let cancel = UIAlertAction(title: "Cancel", style: .default) { (_) in }
        
        alert.addAction(logout)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnSubscriptionDetailTapped(_ sender: Any) {
       guard let url = URL(string: "http://foodatex.com/subscriptions.html") else {
           return
       }
       
       if #available(iOS 10.0, *) {
           UIApplication.shared.open(url, options: [:], completionHandler: nil)
       } else {
           UIApplication.shared.openURL(url)
       }
    }
    
    // MARK: - Call For User Detail
    func callWSForLogout(){
        
        if isConnectedToNetwork(){
            setOnlineStatus(status: "0")
            SVProgressHUD.show()
            
            let dict : [String : Any] = ["user_id" : getLoginDetails()?.userId ?? "0"]
            let endpointurl = (WebServicePrefix.GetWSUrl(WSRequestType.Logout)) as String
            
            Alamofire.request(endpointurl, method: .post , parameters: ["data" : dict] , encoding: JSONEncoding.default)
                .responseJSON { response in
                    ShowNetworkIndicator(false)
                    //print(response.request ?? "")
                    //print(response)
                    APP_DELEGATE.setLOGINAsRoot()
                    if let _ = response.result.error {
                        
                    }else{
                        switch response.result {
                        case .success:
                            guard let responseJSON:NSDictionary = response.result.value as? NSDictionary else {
                                
                                return
                            }
                            var respStatus = 0
                            if let rst : String = responseJSON["status"] as? String{
                                respStatus = Int(rst)!
                            }
                            
                        case .failure(let error):
                            print(error)
                        }
                    }
            }
        }else{
            showNoInternetAlert()
        }
    }
    @IBAction func btnSettingTapped(_ sender: Any) {
        self.navigationController?.pushViewController(loadVC(AppStoryboards.kSBMain, strVCId: AppViewControllers.kVCSettingVC), animated: true)
    }
    
    @IBAction func btnEditInfoTapped(_ sender: Any) {
        self.navigationController?.pushViewController(loadVC(AppStoryboards.kSBMain, strVCId: AppViewControllers.kVCMyProfileDetailVC), animated: true)
    }
    
    @IBAction func btnGetGoldTapped(_ sender: Any) {
        if hasInAppSubscribed() == false{
            if arrPackageList.count == 0 {
                getPackageList()
            }else{
                showPackageSelectionViewController()
            }
        }else{
            showAppAlertWithMessage("You have already subscribed.", viewController: self)
        }
    }
    
    func showPackageSelectionViewController(){
        
        let board = UIStoryboard.init(name: "SB_Main", bundle: nil)
        let packageVc = board.instantiateViewController(withIdentifier: "PackageListVC") as! PackageListVC
        packageVc.arrPackageList = self.arrPackageList
        self.navigationController?.pushViewController(packageVc, animated: true)
    }

}

extension MyProfileVC {
    
    func setUserValues(){
        imgProfile.image = #imageLiteral(resourceName: "image_Placeholder")
        if let userImageUrl = URL(string: userProfileObj.profileImage ?? "") {
            setStringValueToUserDefaults(strValue: userProfileObj.profileImage!, ForKey: AppKeys.UD.kProfileImage)
            imgProfile.sd_setImage(with: userImageUrl, placeholderImage: UIImage(named: "image_Placeholder"), options: .refreshCached, completed: nil)
        }
    }
    
    // MARK: - Call For User Detail
    func callWSForProfile(){
        
        if isConnectedToNetwork(){
            
            SVProgressHUD.show()
            
            let dict : [String : Any] = ["user_id" : getLoginDetails()?.userId ?? "0","other_user_id":getLoginDetails()?.userId ?? "0"]
            
            WebServicesCollection.sharedInstance.GetUserDetails(dict) { (response, error, message) in
                
                SVProgressHUD.dismiss()
                if error == nil {
                    do {
                        let userResult  = try JSONDecoder().decode(UserProfileResult.self, from: response as! Data)
                        //print(userResult)
                        if let userList = userResult.data {
                            self.userProfileObj = userList
                            self.lblName.text = (userList.fullname ?? "").trimmingCharacters(in: .whitespaces) + ", " + (userList.dob ?? "")
                            
                            var userTemp = getLoginDetails()
                            userTemp?.isPurchase = self.userProfileObj.isPurchase ?? ""
                            userTemp?.PurchaseId = self.userProfileObj.purchaseId ?? ""
                            setLoginDetails(userTemp)
                            self.setUserValues()
                        }
                    }catch{
                        SVProgressHUD.showError(withStatus: AppMessages.User_Error)
                    }
                }else{
                    showAppAlertWithMessage("Unable to fetch data", viewController: self)
                }
            }
        }else{
            showNoInternetAlert()
        }
    }
    
    
    //Get List Of Package Available for Purchase
    func getPackageList(){
        
        if isConnectedToNetwork(){
            SVProgressHUD.show()
            WebServicesCollection.sharedInstance.PackageList { (resp, error, message) in
                SVProgressHUD.dismiss()
                if error == nil{
                    do {
                        let packageResult  = try JSONDecoder().decode(PackageResult.self, from: resp as! Data)
                       
                        if let userList = packageResult.data {
                            self.arrPackageList.removeAll()
                            self.arrPackageList.append(contentsOf: userList)
                            print(self.arrPackageList)
//                            self.showPackageListPicker()
                                self.showPackageSelectionViewController()
                        }
                    }catch{
                        SVProgressHUD.showError(withStatus: AppMessages.User_Error)
                    }
                }else{
                    showAppAlertWithMessage(message ?? "Something went wrong", viewController: self)
                }
            }
        }else{
            showNoInternetAlert()
        }
    }
    
    func showPackageListPicker(){
        if packageListPopUp != nil{
            packageListPopUp.hideCustomPicker()
            packageListPopUp = nil
        }
        packageListPopUp = PackageListVC(nibName : "PackageListVC", bundle : nil)
        packageListPopUp.arrPackageList = self.arrPackageList
        packageListPopUp.show()
    }
}
