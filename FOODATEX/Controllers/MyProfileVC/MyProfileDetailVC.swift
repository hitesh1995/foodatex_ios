//
//  MyProfileDetailVC.swift
//  FOODATEX
//
//  Created by Silver Shark on 27/10/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit
import SVProgressHUD
import CropViewController
import AVFoundation
import AVKit
import MobileCoreServices


class MyProfileDetailVC: BaseVC, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    @IBOutlet weak var profileTblView: UITableView!
    @IBOutlet weak var imgProfilePic: RemoteImageView!
    
    //HedarView
    @IBOutlet weak var viewCard: UIView!
    @IBOutlet weak var viewCardBG: UIView!
    @IBOutlet weak var tableHeader: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    
    let imagePicker = UIImagePickerController()
    var userProfileObj : UserProfileData!
    var agePicker : EditAgeVC!
    var locationPicker : EditLocationVC!
    var personalityEditor : EditPersonalityVC!
    var professionEditor : EditProfessionVC!
    var aboutPicker : EditAboutVC!
    var alleryPicker : EditAllergyVC!
    var smokePicker : EditSmokeVC!
    var intrestedInPicker : EditIntrestedInVC!
    var cityPickerVc : EditCityVc!
    var lookingforPicker:EditLookingForVc!
    var splitBillPicker:EditSplitBillVc!
    
    var picker : PickerVC!
    let arrCity = ["Jaipur", "Gandhinagar", "Delhi"]
    var arrMaritalStatus : [String] = ["Single", "Committed", "Married", "Divorced", "Widowed"]
    let arrHeight = ["Less then 3 Feet 5 Inch","3.6 ft", "3.7 ft", "3.8 ft", "3.9 ft", "3.10 ft", "3.11 ft", "4 Feet","4.1 ft","4.2 ft", "4.3 ft", "4.4 ft", "4.5 ft", "4.6 ft", "4.7 ft", "4.8 ft", "4.9 ft", "4.10 ft", "4.11 ft", "5 Feet ft","5.1 ft","5.2 ft", "5.3 ft", "5.4 ft", "5.5 ft", "5.6 ft", "5.7 ft", "5.8 ft", "5.9 ft", "5.10 ft", "5.11 ft", "6 Feet ft","6.1 ft","6.2 ft", "6.3 ft", "6.4 ft", "6.5 ft", "6.6 ft", "6.7 ft", "6.8 ft", "6.9 ft", "6.10 ft", "6.11 ft", "7 Feet ft", "Above 7 Feet"]
    var arrayKids = ["No", "1", "2", "3", "4", "4+"]
    var pickerSelectedFor = 0
    var arryFoodPrefrence : [FoodPrefrenceData] = [FoodPrefrenceData]()
    var foodSelectedIndex : [IndexPath] = [IndexPath]()
    var foodType = ["Vegan","Vegetarian", "Eggiterian", "Pescatarian", "Non Halal", "Kosher", "No Preference"]
    var isProfilUpdated = false
    
    enum ENUM_SECTIONONE : Int {
        case AGE
        case LOCATION
        //case PHONENUMBER
        case CITY
        //case HEIGHT
        case FOODPREFRENCE
        case LOOKINGFOR
        case SPLITBILL
    }

    enum ENUM_SECTIONTWO : Int {
        case PERSONALITY
        case PROFESSION
        case ABOUT
        case ALLERGY
    }
    
    enum ENUM_SECTIONTHREE : Int {
        case MARITALSTATUS
//        case HASKIDS
//        case WANTKIDSS
    }
    enum ENUM_SECTIONFOUR : Int {
        case FOOD
        case GENDER
        case INTRESTEDIN
        case SMOKES
        case DRINK
        case DRUG
    }
    
    //MARK: - LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
          lblName.text = (getLoginDetails()?.fullname ?? "").trimmingCharacters(in: .whitespaces) + ", " + (getLoginDetails()?.dob ?? "")
        callWSForProfile()
        tableHeader.frame = CGRect(x: 0, y: 0, width: SCREENWIDTH(), height: SCREENWIDTH()*1.3281)
        profileTblView.tableHeaderView = tableHeader
      
        lblCity.text = getLoginDetails()?.city ?? ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationbarleft_imagename("icon_Back", left_action: #selector(navigateToPreviousView), right_imagename: "", right_action: #selector(doNothing), title: (getLoginDetails()?.fullname ?? "User") + "'s Profile", isLogo: false)
        
    }
    override func viewWillLayoutSubviews() {
        viewCard.setCornerRadius(radius: 20)
        viewCardBG.createRectShadowWithOffset(offset: .zero, opacity: 0.3, radius: 2)
    }

    @IBAction func btnAddPhotosTapped(_ sender: Any) {
        openImagePicker(false)
    }
}

extension MyProfileDetailVC {
    
    func encodeVideo(videoURL: URL, resultClosure: @escaping (URL?) -> Void ){
        let avAsset = AVURLAsset(url: videoURL, options: nil)
        
        //Create Export session
        if let exportSession = AVAssetExportSession(asset: avAsset, presetName: AVAssetExportPresetPassthrough){

            let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let myDocumentPath = URL(fileURLWithPath: documentsDirectory)
                .appendingPathComponent("temp.mp4").absoluteString
            
            
            let documentsDirectory2 = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            
            let filePath = documentsDirectory2.appendingPathComponent("video.mp4")
            deleteFile(filePath: filePath)
            
            //Check if the file already exists then remove the previous file
            if FileManager.default.fileExists(atPath: myDocumentPath) {
                do {
                    try FileManager.default.removeItem(atPath: myDocumentPath)
                }
                catch let error {
                    print(error)
                }
            }
            exportSession.outputURL = filePath
            exportSession.outputFileType = AVFileType.mp4
            exportSession.shouldOptimizeForNetworkUse = true
            let start = CMTimeMakeWithSeconds(0.0, 0)
            let range = CMTimeRangeMake(start, avAsset.duration)
            exportSession.timeRange = range
            
            exportSession.exportAsynchronously(completionHandler: {() -> Void in
                switch exportSession.status {
                case .failed:
                    resultClosure(nil)
                    print("video encode failed.. %@",exportSession.error as Any)
                case .cancelled:
                    resultClosure(nil)
                    print("Export canceled")
                case .completed:
                    if let outputURL = exportSession.outputURL{
                        resultClosure(outputURL)
                    }
                    else{
                        resultClosure(nil)
                    }
                    
                default:
                    break
                }
            })
        }
    }
    func deleteFile(filePath: URL) {
        guard FileManager.default.fileExists(atPath: filePath.path) else {
            return
        }
        do {
            try FileManager.default.removeItem(atPath: filePath.path)
        }catch{
            fatalError("Unable to delete file: \(error) : \(#function).")
        }
    }
    
    
    //MARK :- OPEN PICKER
    @objc func openImagePicker( _ isVideo : Bool){
        let gallary : UIAlertAction = UIAlertAction(title: "Gallery", style: .default) { (isSelected) in
            self.checkLibrary(isVideo)
        }
        
        let camera : UIAlertAction = UIAlertAction(title: "Camera", style: .default) { (isSelected) in
            self.checkCamera(isVideo)
        }
        let cancel : UIAlertAction = UIAlertAction(title: "Cancel", style: .destructive) { (isSelected) in}
        
        let alert : UIAlertController = UIAlertController(title: APP_NAME, message: "Select Image from", preferredStyle: .actionSheet)
        alert.addAction(gallary)
        alert.addAction(camera)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    //MARK: - CAMEAR/GALLERY METHODS
    func checkCamera( _ isVideo : Bool) {
        
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch authStatus {
        case .authorized: callCamera(isVideo) // Do your stuff here i.e. callCameraMethod()
        case .denied: alertToEncourageAccessInitially(type: 0, isVideo)
        case .notDetermined: alertPromptToAllowAccessViaSetting(type:1, isVideo)
        default: alertToEncourageAccessInitially(type: 0, isVideo)
        }
    }
    func checkLibrary( _ isVideo : Bool) {
        
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch authStatus {
        case .authorized: callLibrary(isVideo)
        case .denied: alertToEncourageAccessInitially(type:1, isVideo)
        case .notDetermined: alertPromptToAllowAccessViaSetting(type:1, isVideo)
        default: alertToEncourageAccessInitially(type:1, isVideo)
        }
    }
    
    //ASK FOR CAMERA PERMISSION
    func alertToEncourageAccessInitially(type:Int, _ isVideo : Bool) {
        
        let alert = UIAlertController(
            title: APP_NAME,
            message: type == 0 ? "Please allow App to access your camera" : " Please allow App to access your Library",
            preferredStyle: UIAlertControllerStyle.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Settings", style: .cancel, handler: { (alert) -> Void in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(URL(string:UIApplicationOpenSettingsURLString)!)
            }
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func alertPromptToAllowAccessViaSetting(type:Int, _ isVideo : Bool) {
        
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
            DispatchQueue.main.async {
                if response{
                    if type == 0{
                        self.callCamera(isVideo)
                    }else{
                        self.callLibrary(isVideo)
                    }
                }else{
                    if type == 0{
                        self.checkCamera(isVideo)
                    }else{
                        self.checkLibrary(isVideo)
                    }
                }
            }
        }
    }
    
    func callCamera(_ isVideo : Bool){
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = isVideo ? false : true
            if isVideo{
                imagePicker.mediaTypes = [kUTTypeMovie as String]
                imagePicker.videoMaximumDuration = 20
            }
            imagePicker.cameraCaptureMode = isVideo ? .video : .photo
            if #available(iOS 11.0, *) {
                imagePicker.videoExportPreset = AVAssetExportPresetPassthrough
            }else{
                imagePicker.videoQuality = .typeMedium
            }
            
            self.present(imagePicker,animated: true,completion: nil)
        } else { showAppAlertWithMessage("Unable to access your camera", viewController: self) }
    }
    
    func callLibrary(_ isVideo : Bool){
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = isVideo ? false : true
        imagePicker.delegate = self
        imagePicker.mediaTypes = isVideo ? ["public.movie"] : ["public.image"]
        if isVideo{
            if #available(iOS 11.0, *) {
                imagePicker.videoExportPreset = AVAssetExportPresetPassthrough
            }else{
                imagePicker.videoQuality = .typeMedium
            }
        }
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    //MARK: - IMAGE PICKER DELEGATE
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        if picker.mediaTypes == ["public.movie"] {
            
            SVProgressHUD.show()
            
            let videoURL = info[UIImagePickerControllerMediaURL] as? URL
            print(videoURL!)
            encodeVideo(videoURL: videoURL!) { (vURL) in
                
                if vURL != nil {
                    //SVProgressHUD.show(withStatus: "Uploading video..")
                    WebServicesCollection.sharedInstance.uploadImage([:], filePath: [vURL!], isImage: false) { (respDict, error, message) in
                        
                        SVProgressHUD.dismiss()
                        if error != nil {
                            showAppAlertWithMessage(message!, viewController: self)
                        }else{
                            guard let dict : NSDictionary = respDict as? NSDictionary else {
                                showAppAlertWithMessage("Something went wrong , Please try again!", viewController: self)
                                return
                            }
                            self.updateVideo(videoUrl: dict.value(forKey: "image") as! String)
                        }
                    }
                } else {
                    SVProgressHUD.dismiss()
                    showAppAlertWithMessage("Something went wrong , Please try again!", viewController: self)
                }
            }
        }else{
            if let image = info[UIImagePickerControllerEditedImage]! as? UIImage{
                //presentCropViewController(image)
                
                let format = DateFormatter()
                format.dateFormat="dd-mm-yyyy-HH-mm-ss"
                let fileName = "Image_\(format.string(from: Date())).jpg".replacingOccurrences(of: "-", with: "_")
                let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                let fileURL = documentsDirectory.appendingPathComponent(fileName)
                
                if let data = UIImageJPEGRepresentation(image, 0.5){
                    do {
                        try data.write(to: fileURL)
                        SVProgressHUD.show()
                        WebServicesCollection.sharedInstance.uploadImage([:], filePath: [fileURL], isImage: true) { (respDict, error, message) in
                            
                            SVProgressHUD.dismiss()
                            if error != nil {
                                showAppAlertWithMessage(message!, viewController: self)
                            }else{
                                guard let dict : NSDictionary = respDict as? NSDictionary else {
                                    showAppAlertWithMessage("Something went wrong , Please try again!", viewController: self)
                                    return
                                }
                                self.updateImage(imageName: dict.value(forKey: "image") as! String)
                            }
                        }
                    } catch {
                        showAppAlertWithMessage("Something went wrong , Please try again!", viewController: self)
                    }
                }
            }
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func presentCropViewController(_ selectedImage : UIImage) {
        let cropViewController = CropViewController(image: selectedImage)
        cropViewController.delegate = self
        cropViewController.aspectRatioLockEnabled = true
        cropViewController.customAspectRatio = CGSize(width: 300, height: 300)
        cropViewController.resetAspectRatioEnabled = false
        cropViewController.aspectRatioPickerButtonHidden = true
        present(cropViewController, animated: true, completion: nil)
    }
    
    // MARK: - Call For User Detail
    func callWSForProfile(){
        
        if isConnectedToNetwork(){
            
            SVProgressHUD.show()
            
            let dict : [String : Any] = ["user_id" : getLoginDetails()?.userId ?? "0","other_user_id":getLoginDetails()?.userId ?? "0"]
            
            WebServicesCollection.sharedInstance.GetUserDetails(dict) { (response, error, message) in
                
                SVProgressHUD.dismiss()
                if error == nil {
                    do {
                        let userResult  = try JSONDecoder().decode(UserProfileResult.self, from: response as! Data)
                        
                        if let userList = userResult.data {
                            
                            self.userProfileObj = userList
                            self.lblName.text = (self.userProfileObj.fullname ?? "").trimmingCharacters(in: .whitespaces) + ", " + (self.userProfileObj.dob ?? "")
                            
                             self.setNavigationbarleft_imagename("icon_Back", left_action: #selector(self.navigateToPreviousView), right_imagename: "", right_action: #selector(self.doNothing), title: (self.userProfileObj.fullname ?? "User") + "'s Profile", isLogo: false)
                            
                            self.setUserValues()
                            self.profileTblView.reloadData()
                        }
                    }catch{
                        SVProgressHUD.showError(withStatus: AppMessages.User_Error)
                    }
                }else{
                    showAppAlertWithMessage("Unable to fetch data", viewController: self)
                }
            }
        }else{
            showNoInternetAlert()
        }
    }
    
    func setUserValues(){
        
        if let userImage = userProfileObj.images?.first {
            if let url : URL = URL(string: userImage.image ?? "") {
                imgProfilePic.sd_setImage(with: url, placeholderImage: UIImage(named: "image_Placeholder"), options: .refreshCached, completed: nil)
            }
        }
        getFoodprefrence()
    }
    
    func setSelectedFoodPrefrence(){
        var selectedFood = userProfileObj.foodPreference ?? ""
        if selectedFood.count > 0 {
            
            if selectedFood.last == ","{
                selectedFood.remove(at: selectedFood.lastIndex(of: ",")!)
            }
            let arrIndex = selectedFood.split(separator: ",")
            var arrFd : [String] = [String]()
            
            for (_, s) in arrIndex.enumerated(){
                arrFd.append(String(s))
            }
            
            if arryFoodPrefrence.count > 0 && arrIndex.count > 0 {
                
                for (index, object) in arryFoodPrefrence.enumerated() {
                    if arrFd.contains(object.foodPreferenceId!){
                        self.foodSelectedIndex.append(IndexPath(item: index, section: 0))
                    }
                }
            }
        }
    }
    
    
    func editGeneralDetails (indexPath : IndexPath) {
        
        switch indexPath.row {
        case ENUM_SECTIONONE.AGE.rawValue:
            openAgeEditor()
            return
        case ENUM_SECTIONONE.LOCATION.rawValue:
            openLocationEditor()
            return
        case ENUM_SECTIONONE.CITY.rawValue :
            openCityPicker()
            return
//        case ENUM_SECTIONONE.HEIGHT.rawValue :
//            openHeightPicker()
//            return
        case ENUM_SECTIONONE.FOODPREFRENCE.rawValue :
            showFoodTypePicker()
            return
        case ENUM_SECTIONONE.LOOKINGFOR.rawValue :
            //Looking for
            openLookingForPicker()
            return
        case ENUM_SECTIONONE.SPLITBILL.rawValue :
            // Split bill
            openSplitBillPicker()
            return
        default:
            break
        }
    }
    
    func editAboutMe (indexPath : IndexPath) {
        
        switch indexPath.row {
        case ENUM_SECTIONTWO.PERSONALITY.rawValue:
            openPersonalitytPicker()
            return
        case ENUM_SECTIONTWO.PROFESSION.rawValue:
            openProfessionEditor()
            return
        case ENUM_SECTIONTWO.ABOUT.rawValue :
            openAboutPicker()
            return
        case ENUM_SECTIONTWO.ALLERGY.rawValue :
            openAlleryPicker()
            return
        default:
            break
        }
    }
    
    func editMaritalInfo (indexPath : IndexPath) {
        
        switch indexPath.row {
        case ENUM_SECTIONTHREE.MARITALSTATUS.rawValue:
            openMaritalPicker()
            return
//        case ENUM_SECTIONTHREE.HASKIDS.rawValue:
//            openKidsPicker(forValue: 4)
//            return
//        case ENUM_SECTIONTHREE.WANTKIDSS.rawValue :
//            openKidsPicker(forValue: 5)
//            return
        default:
            break
        }
    }
    
    func editExtraInfo (indexPath : IndexPath) {
        
        switch indexPath.row {
        case ENUM_SECTIONFOUR.SMOKES.rawValue:
            openSmokePicker(pickerType : 0)
            return
        case ENUM_SECTIONFOUR.DRINK.rawValue:
            openSmokePicker(pickerType : 1)
            return
        case ENUM_SECTIONFOUR.DRUG.rawValue:
            openSmokePicker(pickerType : 2)
            return
        case ENUM_SECTIONFOUR.GENDER.rawValue:
            // open gender Picker here
            return
        case ENUM_SECTIONFOUR.INTRESTEDIN.rawValue:
            openIntrestedPicker()
            return
        case ENUM_SECTIONFOUR.FOOD.rawValue :
            showFoodPrefrencePicker()
            return
        default:
            break
        }
    }
    
    //MARK: - OPEN AGE PICKER
    func openAgeEditor(){
        
        if agePicker != nil {
            agePicker.view.removeFromSuperview()
            agePicker = nil
        }
        
        agePicker = EditAgeVC(nibName : "EditAgeVC", bundle : nil)
        agePicker.delegate = self
        agePicker.show()
        
    }
    
    //MARK: - OPEN LOCATION PICKER
    func openLocationEditor(){
        
        if locationPicker != nil {
            locationPicker.view.removeFromSuperview()
            locationPicker = nil
        }
        locationPicker = EditLocationVC(nibName : "EditLocationVC", bundle : nil)
        locationPicker.delegate = self
        locationPicker.show()
    }
    
    func openCityPicker(){
        if cityPickerVc != nil {
            cityPickerVc.view.removeFromSuperview()
            cityPickerVc = nil
        }
        cityPickerVc = EditCityVc(nibName : "EditCityVc", bundle : nil)
        cityPickerVc.delegate = self
        cityPickerVc.strCity = userProfileObj.city ?? ""
        cityPickerVc.show()
//        if picker != nil{
//            picker.hideCustomPicker()
//            picker = nil
//        }
//
//        picker = PickerVC(nibName: "PickerVC", bundle: nil)
//        pickerSelectedFor = 0
//        picker.isMultiSelection = false
//        picker.delegate = self
//        picker.selectedIndex = [IndexPath(item: 0, section: 0)]
//        picker.titleMessage = "Select City"
//        picker.arrayPickerTemp = arrCity
//        picker.show()
        
    }
    
    func openMaritalPicker(){
        
        if picker != nil{
            picker.hideCustomPicker()
            picker = nil
        }
        
        picker = PickerVC(nibName: "PickerVC", bundle: nil)
        pickerSelectedFor = 3
        picker.isMultiSelection = false
        picker.delegate = self
        picker.selectedIndex = [IndexPath(item: 0, section: 0)]
        picker.titleMessage = "Select Marital Status"
        picker.arrayPickerTemp = arrMaritalStatus
        picker.show()
    }
    
    func openHeightPicker(){
        
        if picker != nil{
            picker.hideCustomPicker()
            picker = nil
        }
        
        picker = PickerVC(nibName: "PickerVC", bundle: nil)
        pickerSelectedFor = 1
        picker.isMultiSelection = false
        picker.delegate = self
        picker.selectedIndex = [IndexPath(item: 0, section: 0)]
        picker.titleMessage = "Select Height"
        picker.arrayPickerTemp = arrHeight
        picker.show()
        
    }
    
    func showFoodTypePicker(){
        
        if picker != nil {
            picker.hideCustomPicker()
            picker = nil
        }
        
        picker = PickerVC(nibName: "PickerVC", bundle: nil)
        pickerSelectedFor = 6
        picker.isMultiSelection = true
        picker.delegate = self
        picker.selectedIndex = []
        picker.titleMessage = "Select food Preference"
        picker.arrayPickerTemp = foodType
        picker.show()
    }
    
    func showFoodPrefrencePicker(){
        
        if arryFoodPrefrence.count == 0 {
            getFoodprefrence()
            return
        }
        
        var arrPref : [String] = [String]()
        for  (_, object) in arryFoodPrefrence.enumerated(){
            arrPref.append(object.preference ?? "")
        }
        
        if picker != nil {
            picker.hideCustomPicker()
            picker = nil
        }
        
        picker = PickerVC(nibName: "PickerVC", bundle: nil)
        pickerSelectedFor = 2
        picker.isMultiSelection = true
        picker.delegate = self
        picker.selectedIndex = foodSelectedIndex
        picker.titleMessage = "Select food Preference"
        picker.arrayPickerTemp = arrPref
        picker.show()
    }
    
    
    func openPersonalitytPicker(){
        
        if personalityEditor != nil{
            personalityEditor.hideCustomPicker()
            personalityEditor = nil
        }
        
        personalityEditor = EditPersonalityVC(nibName: "EditPersonalityVC", bundle: nil)
        personalityEditor.delegate = self
        personalityEditor.pText = userProfileObj.personality ?? ""
        personalityEditor.show()
    }
    
    func openProfessionEditor(){
        
        if professionEditor != nil{
            professionEditor.hideCustomPicker()
            professionEditor = nil
        }
        professionEditor = EditProfessionVC(nibName: "EditProfessionVC", bundle: nil)
        professionEditor.delegate = self
        professionEditor.show()
    }
    
    func openAboutPicker(){
        
        if aboutPicker != nil{
            aboutPicker.hideCustomPicker()
            aboutPicker = nil
        }
        aboutPicker = EditAboutVC(nibName: "EditAboutVC", bundle: nil)
        aboutPicker.delegate = self
        aboutPicker.show()
    }
    
    func openAlleryPicker(){
        
        if alleryPicker != nil{
            alleryPicker.hideCustomPicker()
            alleryPicker = nil
        }
        alleryPicker = EditAllergyVC(nibName: "EditAllergyVC", bundle: nil)
        alleryPicker.delegate = self
        alleryPicker.show()
    }
    
    
    func openKidsPicker( forValue : Int){
        
        if picker != nil{
            picker.hideCustomPicker()
            picker = nil
        }
        
        picker = PickerVC(nibName: "PickerVC", bundle: nil)
        pickerSelectedFor = forValue
        picker.isMultiSelection = false
        picker.delegate = self
        picker.selectedIndex = [IndexPath(item: 0, section: 0)]
        picker.titleMessage = "Select Kids"
        picker.arrayPickerTemp = arrayKids
        picker.show()
        
    }
    
    
    func openSmokePicker(pickerType : Int){
        
        if smokePicker != nil{
            smokePicker.hideCustomPicker()
            smokePicker = nil
        }
        smokePicker = EditSmokeVC(nibName: "EditSmokeVC", bundle: nil)
        smokePicker.PickerType = pickerType
        smokePicker.delegate = self
        smokePicker.show()
    }
    
    func openIntrestedPicker(){
        
        if intrestedInPicker != nil{
            intrestedInPicker.hideCustomPicker()
            intrestedInPicker = nil
        }
        intrestedInPicker = EditIntrestedInVC(nibName: "EditIntrestedInVC", bundle: nil)
        intrestedInPicker.delegate = self
        intrestedInPicker.show()
    }
    func openSplitBillPicker(){
        
        if splitBillPicker != nil{
            splitBillPicker.hideCustomPicker()
            splitBillPicker = nil
        }
        splitBillPicker = EditSplitBillVc(nibName: "EditSplitBillVc", bundle: nil)
        splitBillPicker.delegate = self
        splitBillPicker.show()
    }
    func openLookingForPicker(){
        
        if lookingforPicker != nil{
            lookingforPicker.hideCustomPicker()
            lookingforPicker = nil
        }
        lookingforPicker = EditLookingForVc(nibName: "EditLookingForVc", bundle: nil)
        lookingforPicker.delegate = self
        lookingforPicker.show()
    }
    
    //MARK: - SAVE CITY
    func saveCity( city : String) {
        self.updateProfile(dict: ["user_id": getLoginDetails()?.userId ?? "",
                                  "city" : city]) { isSaved in
                                    if isSaved {
                                        var user = getLoginDetails()
                                        user?.city = city
                                        self.userProfileObj.city = city
                                        setLoginDetails(user)
                                        self.profileTblView.reloadRows(at: [IndexPath(item: ENUM_SECTIONONE.CITY.rawValue, section: 1)], with: .none)
                                        self.lblCity.text = city
                                    }
        }
    }
    
    //MARK: - SAVE HEIGHT
    func saveHeight( height : String) {
        
        self.updateProfile(dict: ["user_id": getLoginDetails()?.userId ?? "",
                                  "height" : height]) { isSaved in
                                    if isSaved {
                                        var user = getLoginDetails()
                                        user?.height = height
                                        self.userProfileObj.height = height
                                        setLoginDetails(user)
                                        //self.profileTblView.reloadRows(at: [IndexPath(item: ENUM_SECTIONONE.HEIGHT.rawValue, section: 1)], with: .none)
                                    }
        }
    }
    func saveSplitBillForProfile(splitBill:String){
        self.updateProfile(dict: ["user_id": getLoginDetails()?.userId ?? "",
                                  "split_bill" : splitBill]) { isSaved in
                                    if isSaved {
                                        var user = getLoginDetails()
                                        user?.splitBill = splitBill
                                        self.userProfileObj.splitBill = splitBill
                                        setLoginDetails(user)
                                        self.profileTblView.reloadRows(at: [IndexPath(item: ENUM_SECTIONONE.SPLITBILL.rawValue, section: 1)], with: .none)
                                    }
        }
    }
    
    func saveLookingForProfile(lookingFor:String){
        self.updateProfile(dict: ["user_id": getLoginDetails()?.userId ?? "",
                                  "looking_for" : lookingFor]) { isSaved in
                                    if isSaved {
                                        var user = getLoginDetails()
                                        user?.lookingFor = lookingFor
                                        self.userProfileObj.lookingFor = lookingFor
                                        setLoginDetails(user)
                                        self.profileTblView.reloadRows(at: [IndexPath(item: ENUM_SECTIONONE.LOOKINGFOR.rawValue, section: 1)], with: .none)
                                    }
        }
    }
    
    //MARK: - SAVE PERSONALITY PREFRENCE
    func savePersonality( personlaity : String) {
        
        self.updateProfile(dict: ["user_id": getLoginDetails()?.userId ?? "",
                                  "personality" : personlaity]) { isSaved in
                                    if isSaved {
                                        var user = getLoginDetails()
                                        user?.personality = personlaity
                                        self.userProfileObj.personality = personlaity
                                        setLoginDetails(user)
                                        self.profileTblView.reloadRows(at: [IndexPath(item: ENUM_SECTIONTWO.PERSONALITY.rawValue, section: 2)], with: .none)
                                    }
        }
    }
    
    //MARK: - SAVE ABOUT ME PREFRENCE
    func saveAboutMe( aboutMe : String) {
        
        self.updateProfile(dict: ["user_id": getLoginDetails()?.userId ?? "",
                                  "about_yourself" : aboutMe]) { isSaved in
                                    if isSaved {
                                        var user = getLoginDetails()
                                        user?.aboutYourself = aboutMe
                                        self.userProfileObj.aboutYourself = aboutMe
                                        setLoginDetails(user)
                                        self.profileTblView.reloadRows(at: [IndexPath(item: ENUM_SECTIONTWO.ABOUT.rawValue, section: 2)], with: .none)
                                    }
        }
    }
    
    //MARK: - SAVE ALLERGY PREFRENCE
    func saveAllegries( haveAllergy : Int, allergy : String) {
        
        self.updateProfile(dict: ["user_id": getLoginDetails()?.userId ?? "",
                                  "any_allergies_status" : haveAllergy,
                                  "any_allergies" : haveAllergy == 0 ? "" : allergy]) { isSaved in
                                    if isSaved {
                                        var user = getLoginDetails()
                                        user?.anyAllergiesStatus = "\(haveAllergy)"
                                        self.userProfileObj.anyAllergiesStatus = "\(haveAllergy)"
                                        user?.anyAllergies = haveAllergy == 0 ? "" : allergy
                                        self.userProfileObj.anyAllergies = haveAllergy == 0 ? "" : allergy
                                        setLoginDetails(user)
                                        self.profileTblView.reloadRows(at: [IndexPath(item: ENUM_SECTIONTWO.ALLERGY.rawValue, section: 2)], with: .none)
                                    }
        }
    }
    
    //MARK: - SAVE FOOD PREFRENCE
    func saveFoodPrefrence( foodPrefrence : String) {
        
        self.updateProfile(dict: ["user_id": getLoginDetails()?.userId ?? "",
                                  "food_preference" : foodPrefrence]) { isSaved in
                                    if isSaved {
                                        var user = getLoginDetails()
                                        user?.foodPreference = foodPrefrence
                                        self.userProfileObj.foodPreference = foodPrefrence
                                        setLoginDetails(user)
                                        self.profileTblView.reloadRows(at: [IndexPath(item: ENUM_SECTIONFOUR.FOOD.rawValue, section: 4)], with: .none)
                                    }
        }
    }
    
    //MARK: - SAVE FOOD Type
    func saveFoodType( foodType : String) {
        
        self.updateProfile(dict: ["user_id": getLoginDetails()?.userId ?? "",
                                  "are_you_a" : foodType]) { isSaved in
                                    if isSaved {
                                        var user = getLoginDetails()
                                        user?.areYouA = foodType
                                        self.userProfileObj.areYouA = foodType
                                        setLoginDetails(user)
                                        self.profileTblView.reloadRows(at: [IndexPath(item: ENUM_SECTIONONE.FOODPREFRENCE.rawValue, section: 1)], with: .none)
                                        self.setUserValues()
                                    }
        }
    }
    
    //MARK: - SAVE WORK PREFRENCE
    func saveProfession( profession : String) {
        
        self.updateProfile(dict: ["user_id": getLoginDetails()?.userId ?? "",
                                  "what_do_you" : profession]) { isSaved in
                                    if isSaved {
                                        var user = getLoginDetails()
                                        user?.whatDoYou = profession
                                        self.userProfileObj.whatDoYou = profession
                                        setLoginDetails(user)
                                        self.profileTblView.reloadRows(at: [IndexPath(item: ENUM_SECTIONTWO.PROFESSION.rawValue, section: 2)], with: .none)
                                    }
        }
    }
    
    //MARK: - SAVE MARITAL STATUS PREFRENCE
    func saveMaritalStatus( mStatus : String) {
        
        self.updateProfile(dict: ["user_id": getLoginDetails()?.userId ?? "",
                                  "marital_status" : mStatus]) { isSaved in
                                    if isSaved {
                                        var user = getLoginDetails()
                                        user?.maritalStatus = mStatus
                                        self.userProfileObj.maritalStatus = mStatus
                                        setLoginDetails(user)
                                        self.profileTblView.reloadRows(at: [IndexPath(item: ENUM_SECTIONTHREE.MARITALSTATUS.rawValue, section: 3)], with: .none)
                                    }
        }
    }
    
    
    //MARK: - SAVE KIDS PREFRENCE
    func saveKids( value : String, type : Int) {
        
        let keyName = type == 5 ?  "do_you_want_kids" : "do_you_have_kids"
        
        self.updateProfile(dict: ["user_id": getLoginDetails()?.userId ?? "",
                                  keyName : value]) { isSaved in
                                    
                                    if isSaved {
                                        var user = getLoginDetails()
                                        if type == 5 {
                                            user?.doYouWantKids = "\(value)"
                                            self.userProfileObj.doYouWantKids = "\(value)"
                                            setLoginDetails(user)
//                                            self.profileTblView.reloadRows(at: [IndexPath(item: ENUM_SECTIONTHREE.WANTKIDSS.rawValue, section: 3)], with: .none)
                                        }else{
                                            user?.doYouHaveKids = "\(value)"
                                            self.userProfileObj.doYouHaveKids = "\(value)"
                                            setLoginDetails(user)
//                                            self.profileTblView.reloadRows(at: [IndexPath(item: ENUM_SECTIONTHREE.HASKIDS.rawValue, section: 3)], with: .none)
                                        }
                                    }
        }
    }
    
    //MARK: - SAVE SMOKE, DRUG PREFRENCE
    func saveSmoke( doSmoke : Int, pType : Int) {
        
        var keyType = ""
        switch pType {
        case 0:
            keyType = "take_smoke"
            break
        case 1:
            keyType = "take_drink"
            break
        case 2:
            keyType = "take_drug"
            break
        default:
            break
        }
        
        self.updateProfile(dict: ["user_id": getLoginDetails()?.userId ?? "",
                                  keyType : doSmoke]) { isSaved in
                                    if isSaved {
                                        var user = getLoginDetails()
                                        switch pType {
                                        case 0:
                                            user?.takeSmoke = "\(doSmoke)"
                                            self.userProfileObj.takeSmoke = "\(doSmoke)"
                                            setLoginDetails(user)
                                            self.profileTblView.reloadRows(at: [IndexPath(item: ENUM_SECTIONFOUR.SMOKES.rawValue, section: 4)], with: .none)
                                            break
                                        case 1:
                                            user?.takeDrink = "\(doSmoke)"
                                            self.userProfileObj.takeDrink = "\(doSmoke)"
                                            setLoginDetails(user)
                                            self.profileTblView.reloadRows(at: [IndexPath(item: ENUM_SECTIONFOUR.DRINK.rawValue, section: 4)], with: .none)
                                            break
                                        case 2:
                                            user?.takeDrug = "\(doSmoke)"
                                            self.userProfileObj.takeDrug = "\(doSmoke)"
                                            setLoginDetails(user)
                                            self.profileTblView.reloadRows(at: [IndexPath(item: ENUM_SECTIONFOUR.DRUG.rawValue, section: 4)], with: .none)
                                            break
                                        default:
                                            break
                                        }
                                    }
        }
    }
    
    //MARK: - SAVE INTRESET
    func saveIntrestedIn( intersted : Int) {
        
        self.updateProfile(dict: ["user_id": getLoginDetails()?.userId ?? "",
                                  "interest_in" : intersted]) { isSaved in
                                    if isSaved {
                                        var user = getLoginDetails()
                                        user?.interestIn = "\(intersted)"
                                        self.userProfileObj.interestIn = "\(intersted)"
                                        setLoginDetails(user)
                                        self.profileTblView.reloadRows(at: [IndexPath(item: ENUM_SECTIONFOUR.INTRESTEDIN.rawValue, section: 4)], with: .none)
                                    }
        }
    }
    //MARK: - SAVE INTRESET
    func saveMyGender( myGender : Int) {
        
        self.updateProfile(dict: ["user_id": getLoginDetails()?.userId ?? "",
                                  "gender" : myGender]) { isSaved in
                                    if isSaved {
                                        var user = getLoginDetails()
                                        user?.gender = "\(myGender)"
                                        self.userProfileObj.gender = "\(myGender)"
                                        setLoginDetails(user)
                                        self.profileTblView.reloadRows(at: [IndexPath(item: ENUM_SECTIONFOUR.GENDER.rawValue, section: 4)], with: .none)
                                    }
        }
    }

    //MARK: - UPDATE PROFILE
    func updateProfile( dict : [String : Any], response : @escaping (_ isUploaded : Bool) -> Void ){
        
        if isConnectedToNetwork(){
            
            SVProgressHUD.show()
            WebServicesCollection.sharedInstance.UpdateProfile(dict) { (wresponse, error, message, rstatus) in
                SVProgressHUD.dismiss()
                if rstatus == 1{
                    response(true)
                }else{
                    showAppAlertWithMessage(message ?? "Something went wrong, Please trye Again", viewController: self)
                    response(false)
                }
            }
            
        }else{
            response(false)
        }
    }
    
    
    //MARK: - GET FOOD PREFRENCE
    func getFoodprefrence(){
        
        if isConnectedToNetwork(){
            SVProgressHUD.show()
            WebServicesCollection.sharedInstance.FoodPreferences { (response, error, message) in
                SVProgressHUD.dismiss()
                if error == nil {
                    do {
                        let loginResult  = try JSONDecoder().decode(FoodPrefrenceResult.self, from: response as! Data)
                        if let foodDATA = loginResult.data {
                            self.arryFoodPrefrence = foodDATA
                        }
                        self.setSelectedFoodPrefrence()
                        self.profileTblView.reloadRows(at: [IndexPath(item: ENUM_SECTIONONE.FOODPREFRENCE.rawValue, section: 1)], with: .none)
                    }catch{
                        SVProgressHUD.showError(withStatus: AppMessages.FB_Error)
                    }
                }else{
                    showAppAlertWithMessage("Failed to get Food Preference , Please trye again!", viewController: self)
                }
            }
        }
    }
    
    
    //MARK: - UPDATE IMAGE
    func updateImage(imageName : String) {
        
        if isConnectedToNetwork(){
            let dictImage : [String : Any] = ["image" : imageName ]
            SVProgressHUD.show()
            WebServicesCollection.sharedInstance.UpdateProfile(["user_id": getLoginDetails()?.userId ?? "", "images" : [dictImage]]) { (wresponse, error, message, rstatus) in
                SVProgressHUD.dismiss()
                if rstatus == 1{
                    self.callWSForProfile()
                    self.profileTblView.reloadData()
                }else{
                    showAppAlertWithMessage(message ?? "Something went wrong, PLease trye Again", viewController: self)
                }
            }
            
        }else{
            showNoInternetAlert()
        }
    }
    
    //MARK: - UPDATE Video
    func updateVideo(videoUrl : String) {
        
        if isConnectedToNetwork(){
            WebServicesCollection.sharedInstance.UpdateProfile(["user_id": getLoginDetails()?.userId ?? "", "video" : videoUrl]) { (wresponse, error, message, rstatus) in
                SVProgressHUD.dismiss()
                if rstatus == 1{
                    self.callWSForProfile()
                    self.profileTblView.reloadData()
                }else{
                    showAppAlertWithMessage(message ?? "Something went wrong, PLease trye Again", viewController: self)
                }
            }
        }else{
            showNoInternetAlert()
        }
    }
    
    //MARK: - UPLOAD VIDEO TAPPED
    @IBAction func selectVideoTapped( _ sender : UIButton){
        openImagePicker(true)
    }
    
    //MARK: - UPLOAD VIDEO TAPPED
    @IBAction func playVideoTapped( _ sender : UIButton){
        let videoURL = URL(string: userProfileObj.video ?? "")
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    func createThumbnailOfVideoFromRemoteUrl(url: String, response : @escaping (_ image : UIImage?) -> Void){
        
        runInBackground {
            let asset = AVAsset(url: URL(string: url)!)
            let assetImgGenerate = AVAssetImageGenerator(asset: asset)
            assetImgGenerate.appliesPreferredTrackTransform = true
            let time = CMTimeMakeWithSeconds(0.5, 100)
            do {
                let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
                let thumbnail = UIImage(cgImage: img)
                response(thumbnail)
            } catch {
                print(error.localizedDescription)
                response(nil)
            }
        }
    }
}
extension MyProfileDetailVC: EditCityDelegate{
    func saveEditedCity(city: String) {
         saveCity(city: city)
    }
}

extension MyProfileDetailVC : UserInfoCellDelegate {
    
    func userInfoEditTapped(indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            return
        case 1 :
            editGeneralDetails(indexPath: indexPath)
            return
        case 2 :
            editAboutMe(indexPath: indexPath)
            return
        case 3 :
            editMaritalInfo(indexPath: indexPath)
            return
        case 4:
            editExtraInfo(indexPath: indexPath)
            return
        default:
            return
        }
    }
}

extension MyProfileDetailVC : EditAgeDelegate {
    
    func saveEditAge(age: Int) {
        
        agePicker.hideCustomPicker()
        self.updateProfile(dict: ["user_id": getLoginDetails()?.userId ?? "",
                                  "dob" : age]) { isSaved in
            if isSaved {
                var user = getLoginDetails()
                user?.dob = "\(age)"
                self.userProfileObj.dob = "\(age)"
                setLoginDetails(user)
                self.profileTblView.reloadRows(at: [IndexPath(item: 0, section: 1)], with: .none)
            }
        }
    }
}

extension MyProfileDetailVC : EditLocationDelegate {
    
    func saveLocationAge(location: String) {
        
        locationPicker.hideCustomPicker()
        self.updateProfile(dict: ["user_id": getLoginDetails()?.userId ?? "",
                                  "address" : location]) { isSaved in
                                    if isSaved {
                                        var user = getLoginDetails()
                                        user?.address = location
                                        self.userProfileObj.address = location
                                        setLoginDetails(user)
                                        self.profileTblView.reloadRows(at: [IndexPath(item: 1, section: 1)], with: .none)
                                    }
        }
    }
}

extension MyProfileDetailVC : PickerDelegate {
    
    func pickerDidFinishedWithIndex(_ selectedIndex: [IndexPath]) {
        
        picker.hideCustomPicker()
        
        switch pickerSelectedFor {
        case 0:
            saveCity(city: arrCity[selectedIndex[0].row])
            return
        case 1:
            saveHeight(height: arrHeight[selectedIndex[0].row])
            return
        case 2:
            if selectedIndex.count == 0 {
                showAppAlertWithMessage("You must select food", viewController: self)
                return
            }
            foodSelectedIndex.removeAll()
            foodSelectedIndex.append(contentsOf: selectedIndex)
            if foodSelectedIndex.count > 3{
                showAppAlertWithMessage("You can select only 3 favourite foods.", viewController: self)
                return
            }
            var foodString = ""
            for ( _ , object) in selectedIndex.enumerated() {
                let objFood = arryFoodPrefrence[object.item]
                foodString.append(objFood.preference ?? "")
                foodString.append(",")
                foodString.append(" ")
                
            }
            
            if foodString.count > 2 {
                foodString.removeLast()
            }
            saveFoodPrefrence(foodPrefrence: foodString)
            return
        case 3:
            saveMaritalStatus(mStatus: arrMaritalStatus[selectedIndex[0].row])
            return
        case 4:
            saveKids(value: arrayKids[selectedIndex[0].row], type: 4)
            return
        case 5:
            saveKids(value: arrayKids[selectedIndex[0].row], type: 5)
            return
        case 6:
            if selectedIndex.count == 0 {
                showAppAlertWithMessage("You must select food", viewController: self)
                return
            }
            var foodString = ""
            for ( _ , object) in selectedIndex.enumerated() {
                foodString.append(foodType[object.item])
                foodString.append(",")
                foodString.append(" ")
            }
            if foodString.count > 2 {
                foodString.removeLast()
            }
            saveFoodType(foodType: foodString)
            return
        default:
            return
        }
    }
    
    func pickerDidCancelled() {
        picker.hideCustomPicker()
    }
}
extension MyProfileDetailVC:EditSplitBillDelegate{
    
    func saveSplitBill(splitBill: Int) {
        splitBillPicker.hideCustomPicker()
        saveSplitBillForProfile(splitBill: splitBill.description)
    }
}
extension MyProfileDetailVC : EditLookingForDelegate{
    func saveLookingFor(lookingFor: Int) {
        lookingforPicker.hideCustomPicker()
        //1 == Romance
        //2 == Friendship
        saveLookingForProfile(lookingFor: lookingFor.description)
        
    }
}
extension MyProfileDetailVC : EditPersonalityDelegate {
    func saveEditPersonality(personality: String) {
        personalityEditor.hideCustomPicker()
        savePersonality(personlaity: personality)
    }
}

extension MyProfileDetailVC : EditAboutDelegate {
    func saveEditAbout(aboutMe: String) {
        aboutPicker.hideCustomPicker()
        saveAboutMe(aboutMe: aboutMe)
    }
}

extension MyProfileDetailVC : EditProfessionDelegate {
    func saveProfession(profession: Int) {
        professionEditor.hideCustomPicker()
        var passProfesstion:String = "Working"
        switch profession {
        case 2:
            passProfesstion = "Studying"
            break
        case 3:
            passProfesstion = "Other"
            break
        default:
            passProfesstion = "Working"
            break
        }
        
        saveProfession(profession: passProfesstion)
    }
}

extension MyProfileDetailVC : EditAllergyDelegate {
    
    func saveAllergy(haveAllergy: Int, allergy: String) {
        alleryPicker.hideCustomPicker()
        saveAllegries(haveAllergy: haveAllergy, allergy: allergy)
    }
}


extension MyProfileDetailVC : EditSmokeDelegate {
    func saveSmoke(isDoSmoke: Int, pType: Int) {
        smokePicker.hideCustomPicker()
        saveSmoke(doSmoke: isDoSmoke, pType : pType)
    }
}

extension MyProfileDetailVC : EditIntrestedInDelegate {
    func saveIntrested(interstedIn: Int) {
        intrestedInPicker.hideCustomPicker()
        saveIntrestedIn(intersted: interstedIn)
    }
}


extension MyProfileDetailVC : ProfileImageDelegate {
    
    func profileImageDeletedAtIndex(indexPath: IndexPath, imageId: String) {
        
        if isConnectedToNetwork(){
            SVProgressHUD.show()
            WebServicesCollection.sharedInstance.deleteUserImage(["user_images_id": imageId]) { (response, error, message, rstatus) in
                self.callWSForProfile()
            }
        }else{
            showNoInternetAlert()
        }
    }
}


//MARK: - CROP VIEW CONTROLLER DELEGATE
extension MyProfileDetailVC : CropViewControllerDelegate{
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
        cropViewController.dismiss(animated: true, completion: nil)
        let format = DateFormatter()
        format.dateFormat="dd-mm-yyyy-HH-mm-ss"
        let fileName = "Image_\(format.string(from: Date())).jpg".replacingOccurrences(of: "-", with: "_")
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        
        if let data = UIImageJPEGRepresentation(image, 0.5){
            do {
                try data.write(to: fileURL)
                SVProgressHUD.show()
                WebServicesCollection.sharedInstance.uploadImage([:], filePath: [fileURL], isImage: true) { (respDict, error, message) in
                    
                    SVProgressHUD.dismiss()
                    if error != nil {
                        showAppAlertWithMessage(message!, viewController: self)
                    }else{
                        guard let dict : NSDictionary = respDict as? NSDictionary else {
                            showAppAlertWithMessage("Something went wrong , Please try again!", viewController: self)
                            return
                        }
                        self.updateImage(imageName: dict.value(forKey: "image") as! String)
                    }
                }
            } catch {
                showAppAlertWithMessage("Something went wrong , Please try again!", viewController: self)
            }
        }
        
    }
    
    func cropViewController(_ cropViewController: CropViewController, didFinishCancelled cancelled: Bool) {
        cropViewController.dismiss(animated: true, completion: nil)
    }
    
}


//MARK: - EXTENSION
extension MyProfileDetailVC : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if userProfileObj == nil {
            return 0
        }
        return 5
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return 2
        case 1:
            return 6
        case 2:
            return 4
        case 3:
            return 1
        case 4:
            return 4
        default:
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                return GetCellCollectionView(tableView: tableView, indexPath: indexPath)
            }else{
                return CellVideoView(tableView: tableView, indexPath: indexPath)
            }
        } else {
            return GetCellGeneralDetails(tableView:tableView, indexPath: indexPath)
        }
    }
    
    
    func GetCellCollectionView(tableView : UITableView, indexPath : IndexPath) -> UITableViewCell {
        
        let cell : ProfileImgGalleryCell = tableView.dequeueReusableCell(withIdentifier: "cellCollectionView", for: indexPath) as! ProfileImgGalleryCell
        cell.arrImg = userProfileObj.images ?? [UserProfileImage]()
        cell.LoadCollectionView()
        cell.delegate = self
        cell.selectionStyle = .none
        return cell
    }
    
    func CellVideoView(tableView : UITableView, indexPath : IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellVideoView", for: indexPath)
        let btnUploadVideo = cell.viewWithTag(1001) as! UIButton
        let btnPlayVideo = cell.viewWithTag(1002) as! UIButton
        cell.selectionStyle = .none
        btnPlayVideo.isUserInteractionEnabled = (userProfileObj.video?.count ?? 0) > 0  ? true : false
        btnPlayVideo.setTitle((userProfileObj.video?.count ?? 0) > 0 ? "" : "Add Video", for: .normal)
        btnPlayVideo.setTitleColor((userProfileObj.video?.count ?? 0) > 0 ? UIColor.black : UIColor.lightGray, for: .normal)
        btnUploadVideo.addTarget(self, action: #selector(selectVideoTapped(_
            :)), for: .touchUpInside)
        btnPlayVideo.addTarget(self, action: #selector(playVideoTapped(_
            :)), for: .touchUpInside)
        
        if self.userProfileObj.video != nil && self.userProfileObj.video!.count > 0 {
            self.createThumbnailOfVideoFromRemoteUrl(url: self.userProfileObj.video!) { tImage in
                if tImage != nil{
                    runOnMainThread {
                        btnPlayVideo.setImage(tImage, for: .normal)
                    }
                }
            }
        }
        
        return cell
    }
    
    func GetCellGeneralDetails(tableView : UITableView, indexPath : IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 1:
            return CellGeneralDetails(tableView: tableView, indexPath: indexPath)
        case 2:
            return CellKnowMore(tableView: tableView, indexPath: indexPath)
        case 3:
            return CellMaritialDetail(tableView: tableView, indexPath: indexPath)
        case 4:
            return CellIntrest(tableView: tableView, indexPath: indexPath)
        default:
            return UITableViewCell()
        }
    }
    
    
    func CellGeneralDetails(tableView : UITableView, indexPath : IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TVCEditProfileCell", for: indexPath) as! TVCEditProfileCell
        cell.btnEdit.isHidden = false
        cell.myIndex = indexPath
        cell.delegate = self
        cell.selectionStyle = .none
        switch indexPath.row {
        case ENUM_SECTIONONE.AGE.rawValue:
            cell.lblTitle.text = "Age"
            cell.lblValue.text = userProfileObj.dob ?? "--"
            break
        case ENUM_SECTIONONE.LOCATION.rawValue:
            cell.lblTitle.text = "Location"
            cell.lblValue.text = userProfileObj.address ?? "--"
            break
//        case ENUM_SECTIONONE.PHONENUMBER.rawValue:
//            cell.lblTitle.text = "Telephone Number"
//            cell.lblValue.text = userProfileObj.phone ?? "--"
//            cell.btnEdit.isHidden = true
//            break
        case ENUM_SECTIONONE.CITY.rawValue:
            cell.lblTitle.text = "City You Live In"
            cell.lblValue.text = userProfileObj.city ?? "--"
            break
//        case ENUM_SECTIONONE.HEIGHT.rawValue:
//            cell.lblTitle.text = "Height"
//            cell.lblValue.text = userProfileObj.height ?? "--"
//            break
        case ENUM_SECTIONONE.FOODPREFRENCE.rawValue:
            cell.lblTitle.text = "Food Requirements"
            cell.lblValue.text = userProfileObj.areYouA ?? "--"
            break
        case ENUM_SECTIONONE.LOOKINGFOR.rawValue:
            cell.lblTitle.text = "Looking For"
            if userProfileObj.lookingFor ?? "" == "2"{
                cell.lblValue.text = "Friendship"
            }else{
                cell.lblValue.text = "Romance"
            }
            break
        case ENUM_SECTIONONE.SPLITBILL.rawValue:
            cell.lblTitle.text = "SPLIT BILL"
            if userProfileObj.splitBill ?? "" == "1"{
                 cell.lblValue.text = "Yes"
            }else{
                cell.lblValue.text = "No"
            }
            break
        default:
            return UITableViewCell()
        }
        return cell
    }
    
    func CellKnowMore(tableView : UITableView, indexPath : IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TVCEditProfileCell", for: indexPath) as! TVCEditProfileCell
        cell.myIndex = indexPath
        cell.delegate = self
        cell.btnEdit.isHidden = false
        cell.selectionStyle = .none
        switch indexPath.row {
        case ENUM_SECTIONTWO.PERSONALITY.rawValue:
            cell.lblTitle.text = "Personality"
            cell.lblValue.text = userProfileObj.personality ?? "--"
            cell.lblValue.numberOfLines = 0
            cell.lblValue.sizeToFit()
            break
        case ENUM_SECTIONTWO.PROFESSION.rawValue:
            cell.lblTitle.text = "Occupation"
            if userProfileObj.whatDoYou == "1" || userProfileObj.whatDoYou == "2" || userProfileObj.whatDoYou == "3"{
                if userProfileObj.whatDoYou == "1"{
                    cell.lblValue.text = "Working"
                }else if userProfileObj.whatDoYou == "2"{
                    cell.lblValue.text = "Studying"
                }else{
                    cell.lblValue.text = "Other"
                }
            }
            else{
                cell.lblValue.text = userProfileObj.whatDoYou ?? "Working"
            }
            
            break
        case ENUM_SECTIONTWO.ABOUT.rawValue:
            cell.lblTitle.text = "About"
            cell.lblValue.text = userProfileObj.aboutYourself ?? "--"
            cell.lblValue.numberOfLines = 0
            cell.lblValue.sizeToFit()
            break
        case ENUM_SECTIONTWO.ALLERGY.rawValue:
            cell.lblTitle.text = "Allergies"
            if getLoginDetails()?.anyAllergiesStatus == "1" || getLoginDetails()?.anyAllergiesStatus?.lowercased() == "yes" {
                cell.lblValue.text = userProfileObj.anyAllergies ?? ""
            }else{
                cell.lblValue.text = "No"
            }
            break
        default:
            return UITableViewCell()
        }
        return cell
    }
    
    func CellMaritialDetail(tableView : UITableView, indexPath : IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TVCEditProfileCell", for: indexPath) as! TVCEditProfileCell
        cell.myIndex = indexPath
        cell.delegate = self
        cell.btnEdit.isHidden = false
        cell.selectionStyle = .none
        switch indexPath.row {
        case ENUM_SECTIONTHREE.MARITALSTATUS.rawValue:
            cell.lblTitle.text = "Marital Status"
            cell.lblValue.text = userProfileObj.maritalStatus ?? "--"
            break
//        case ENUM_SECTIONTHREE.HASKIDS.rawValue:
//            cell.lblTitle.text = "Do You Have Kids?"
//            cell.lblValue.text = userProfileObj.doYouHaveKids ?? "No"
//            break
//        case ENUM_SECTIONTHREE.WANTKIDSS.rawValue:
//            cell.lblTitle.text = "Do you want Kids"
//            cell.lblValue.text = userProfileObj.doYouWantKids ?? "No"
//            break
        default:
            return UITableViewCell()
        }
        return cell
    }
    
    func CellIntrest(tableView : UITableView, indexPath : IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TVCEditProfileCell", for: indexPath) as! TVCEditProfileCell
        cell.myIndex = indexPath
        cell.delegate = self
        cell.btnEdit.isHidden = false
        cell.selectionStyle = .none
        switch indexPath.row {
        case ENUM_SECTIONFOUR.FOOD.rawValue:
            cell.lblTitle.text = "Food Preference"
            cell.lblValue.text = userProfileObj.foodPreference ?? "--"
            break
       
        case ENUM_SECTIONFOUR.GENDER.rawValue:
            cell.lblTitle.text = "Gender"
            cell.btnEdit.isHidden = true
            if userProfileObj.gender ?? "1" == "1"{
                cell.lblValue.text = "Male"
            }else{
                cell.lblValue.text = "Female"
            }
            break
        case ENUM_SECTIONFOUR.INTRESTEDIN.rawValue:
            cell.lblTitle.text = "Interested in"
            var interestIn = "Both"
            if userProfileObj.interestIn == "2" {
                interestIn = "Female"
            }else if userProfileObj.interestIn == "1" {
                interestIn = "Male"
            }
            cell.lblValue.text = interestIn
            break
        case ENUM_SECTIONFOUR.SMOKES.rawValue:
            cell.lblTitle.text = "Do you Smoke?"
            cell.lblValue.text = "No"
            if let pr = userProfileObj.takeSmoke {
                switch pr {
                case "1":
                    cell.lblValue.text = "Yes"
                    break
                case "3":
                    cell.lblValue.text = "Occasionally"
                    break
                default:
                    cell.lblValue.text = "No"
                    break
                }
            }
            break
        case ENUM_SECTIONFOUR.DRINK.rawValue:
            cell.lblTitle.text = "Do you Drink?"
            cell.lblValue.text = "No"
            if let pr = userProfileObj.takeDrink {
                switch pr {
                case "1":
                    cell.lblValue.text = "Yes"
                    break
                case "3":
                    cell.lblValue.text = "Occasionally"
                    break
                default:
                    cell.lblValue.text = "No"
                    break
                }
            }
            break
        case ENUM_SECTIONFOUR.DRUG.rawValue:
            cell.lblTitle.text = "Do you take Drugs?"
            cell.lblValue.text = "No"
            if let pr = userProfileObj.takeDrug {
                switch pr {
                case "1":
                    cell.lblValue.text = "Yes"
                    break
                case "3":
                    cell.lblValue.text = "Occasionally"
                    break
                default:
                    cell.lblValue.text = "No"
                    break
                }
            }
            break
        default:
            return UITableViewCell()
        }
        return cell
    }
}

extension MyProfileDetailVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            if indexPath.row == 1{
                return 180
            }
            if userProfileObj.images?.count == 0 {
                return 60
            }
            let width = ((SCREENWIDTH() - 50) / 4) + 5
            let line = (userProfileObj.images?.count ?? 0) % 4
            var count = (userProfileObj.images?.count ?? 0) / 4
            count = count + (line > 0 ? 1 : 0)
            return (CGFloat(count > 0 ? count : 1) * width ) + 80.0
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        return 60
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 {
            return UIView()
        }
        
        let header = UIView(frame: CGRect(x: 0, y: 0, width: SCREENWIDTH(), height: 60))
        let lblseprator = UILabel(frame: CGRect(x: 0, y: 59, width: SCREENWIDTH(), height: 1))
        lblseprator.text = ""
        lblseprator.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        header.addSubview(lblseprator)
        header.backgroundColor = .white
        let lblHeading = UILabel(frame: CGRect(x: 20, y: 20, width: SCREENWIDTH() - 20 , height: 20))
        lblHeading.textColor = .black
        lblHeading.font = APPFONT_BOLD(20)
        switch section {
        case 1:
            lblHeading.text = "General Details"
            break
        case 2:
            lblHeading.text = "More About Me"
            break
        case 3:
            lblHeading.text = "Marital Details"
            break
        case 4:
            lblHeading.text = "Interests"
            break
        default:
            lblHeading.text = ""
            break
        }
        header.addSubview(lblHeading)
             return header
    }
}
