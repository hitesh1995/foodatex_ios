//
//  InfoPageEightVC.swift
//  FOODATEX
//
//  Created by Uffizio iMac on 24/12/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit
import SVProgressHUD
import AVFoundation

class InfoPageEightVC: BaseVC {
    
    var arrImageName : [String] = [String]()
    var arrImages : [UIImage] = [UIImage]()
    var userDict = UserInfo()
    var splitBiill = true
    var videoUrl : URL?

    @IBOutlet weak var viewASplitbill: UIView!
    @IBOutlet weak var viewAbout: UIView!
    @IBOutlet weak var iconYes: UIImageView!
    @IBOutlet weak var iconNo: UIImageView!
    @IBOutlet weak var txtAbout: UITextView!
    @IBOutlet weak var lblAbout: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewASplitbill.createAppBorder()
        viewAbout.createAppBorder()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil;
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationbarleft_imagename("icon_Back", left_action: #selector(navigateToPreviousView), right_imagename: "", right_action: #selector(doNothing), title: "", isLogo: true)
    }
    
    @IBAction func btnNextTapped(_ sender: Any) {
        if txtAbout.text.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            showAppAlertWithMessage("You must enter some word describing yourself.", viewController: self)
            return
        }
        uploadImages()
    }
    
    @IBAction func btnYesTapped(_ sender: Any) {
        splitBiill = true
        iconYes.image = #imageLiteral(resourceName: "icon_RadioChecked")
        iconNo.image = #imageLiteral(resourceName: "icon_RadioUnchecked")
    }
    
    @IBAction func btnNoTapped(_ sender: Any) {
        splitBiill = false
        iconNo.image = #imageLiteral(resourceName: "icon_RadioChecked")
        iconYes.image = #imageLiteral(resourceName: "icon_RadioUnchecked")
    }
}

extension InfoPageEightVC {
    
    func uploadImages(){
        
        if isConnectedToNetwork(){
            
            SVProgressHUD.show()
            
            var arrFileUrl: [URL] = [URL]()
            
            for (index, image) in arrImages.enumerated(){
                
                let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                let fileURL = documentsDirectory.appendingPathComponent(arrImageName[index])
                
                if let data = UIImageJPEGRepresentation(image, 0.5){
                    do {
                        try data.write(to: fileURL)
                        arrFileUrl.append(fileURL)
                    } catch {
                        showAppAlertWithMessage("Something went wrong , Please try again!", viewController: self)
                    }
                }
            }
            
            
            WebServicesCollection.sharedInstance.uploadImage([:], filePath: arrFileUrl, isImage: true) { (respDict, error, message) in
                
                if error != nil {
                    SVProgressHUD.dismiss()
                    showAppAlertWithMessage(message!, viewController: self)
                }else{
                    guard let dict : NSDictionary = respDict as? NSDictionary else {
                        SVProgressHUD.dismiss()
                        showAppAlertWithMessage("Something went wrong , Please try again!", viewController: self)
                        return
                    }
                    var arrImages : [Any] = [Any]()
                    for (_,object) in dict.allValues.enumerated(){
                        arrImages.append(["image" : object])
                    }
                    self.encodeVideoAndUpload(arrImages)
                }
            }
        }else{
            showNoInternetAlert()
        }
    }
    
    func encodeVideo(videoURL: URL, resultClosure: @escaping (URL?) -> Void ){
        let avAsset = AVURLAsset(url: videoURL, options: nil)
        
        //Create Export session
        if let exportSession = AVAssetExportSession(asset: avAsset, presetName: AVAssetExportPresetPassthrough){

            let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let myDocumentPath = URL(fileURLWithPath: documentsDirectory)
                .appendingPathComponent("temp.mp4").absoluteString
            
            let documentsDirectory2 = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            
            let filePath = documentsDirectory2.appendingPathComponent("video.mp4")
            deleteFile(filePath: filePath)
            
            //Check if the file already exists then remove the previous file
            if FileManager.default.fileExists(atPath: myDocumentPath) {
                do {
                    try FileManager.default.removeItem(atPath: myDocumentPath)
                }
                catch let error {
                    print(error)
                }
            }
            exportSession.outputURL = filePath
            exportSession.outputFileType = AVFileType.mp4
            exportSession.shouldOptimizeForNetworkUse = true
            let start = CMTimeMakeWithSeconds(0.0, 0)
            let range = CMTimeRangeMake(start, avAsset.duration)
            exportSession.timeRange = range
            
            exportSession.exportAsynchronously(completionHandler: {() -> Void in
                switch exportSession.status {
                case .failed:
                    resultClosure(nil)
                    print("video encode failed.. %@",exportSession.error as Any)
                case .cancelled:
                    resultClosure(nil)
                    print("Export canceled")
                case .completed:
                    if let outputURL = exportSession.outputURL{
                        resultClosure(outputURL)
                    }
                    else{
                        resultClosure(nil)
                    }
                    
                default:
                    break
                }
            })
        }
    }
    func deleteFile(filePath: URL) {
        guard FileManager.default.fileExists(atPath: filePath.path) else {
            return
        }
        do {
            try FileManager.default.removeItem(atPath: filePath.path)
        }catch{
            fatalError("Unable to delete file: \(error) : \(#function).")
        }
    }
    
    func encodeVideoAndUpload(_ images : Any){
        
        if videoUrl != nil && videoUrl!.absoluteString.count > 0 {
            
            encodeVideo(videoURL: self.videoUrl!) { (vURL) in
                
                if vURL != nil {
                    
                    WebServicesCollection.sharedInstance.uploadImage([:], filePath: [vURL!], isImage: false) { (respDict, error, message) in
                        
                        SVProgressHUD.dismiss()
                        if error != nil {
                            SVProgressHUD.dismiss()
                            showAppAlertWithMessage(message!, viewController: self)
                        }else{
                            guard let dict : NSDictionary = respDict as? NSDictionary else {
                                SVProgressHUD.dismiss()
                                showAppAlertWithMessage("Something went wrong , Please try again!", viewController: self)
                                return
                            }
                            self.CallWSForProfileUpdate(dict.value(forKey: "image") as! String, images)
                        }
                    }
                } else {
                    showAppAlertWithMessage("Something went wrong , Please try again!", viewController: self)
                }
            }
            
        }else{
            self.CallWSForProfileUpdate("", images)
        }
    }
    
    func CallWSForProfileUpdate(_ video : String, _ images : Any){
        
        var fullname = getLoginDetails()?.fullname ?? ""
        var email = getLoginDetails()?.email ?? ""
        if fullname.count == 0 {
            fullname = userDict.fullname
            email = userDict.email
        }
        
        if isConnectedToNetwork(){
            
            let param : [String : Any] = ["user_id": getLoginDetails()?.userId ?? "",
                                          "fullname" : fullname,
                                          "email" : email,
                                          "dob" : userDict.dob,
                                          "password" : "",
                                          "address" : userDict.address,
                                          "latitude" : "0.0",
                                          "longitude" : "0.0",
                                          "phone" : userDict.phone,
                                          "country_code": userDict.countryCode.replacingOccurrences(of: "+", with: ""),
                                          "food_preference": userDict.foodPreference.trimmingCharacters(in: .whitespaces),
                                          "gender": userDict.gender,
                                          "interest_in" : userDict.interestIn,
                                          "looking_for" : userDict.lookingFor,
                                          "are_you_a" : userDict.areYouA.trimmingCharacters(in: .whitespaces),
                                          "any_allergies_status" : userDict.anyAllergiesStatus ? "1" : "0",
                                          "any_allergies" : userDict.anyAllergies,
                                          "city" : userDict.city,
                                          "height" : userDict.height,
                                          "personality" : userDict.personality,
                                          "what_do_you" : userDict.whatDoYou == "0" ? "1" : "2",
                                          "do_you_want_kids" : userDict.doYouWantKids ,
                                          "marital_status": userDict.maritalStatus,
                                          "do_you_have_kids": userDict.doYouHaveKids,
                                          "take_smoke": userDict.doYouSmoke,
                                          "take_drug" : userDict.doYouTakeDrug,
                                          "take_drink" : userDict.doYouDrink,
                                          "video": video,
                                          "about_yourself": txtAbout.text.trimmingCharacters(in: .whitespacesAndNewlines),
                                          "split_bill": splitBiill ? "1" : "0",
                                          "unlimited_likes":"1",
                                          "foodatext_boost":"1",
                                          "allowing_you":"1",
                                          "hide_adverts":"1",
                                          "dont_show_my_age":"1",
                                          "distance_invisible":"1",
                                          "who_see_you":"1",
                                          "profile_image" : "",
                                          "images":images,
                                          "device_type" : 1,
                                          "device_token" : getStringValueFromUserDefaults_ForKey(strKey: AppKeys.UD.kAPNSToken) ?? ""]
            SVProgressHUD.show(withStatus: "Updating Profile")
            WebServicesCollection.sharedInstance.UpdateProfile(param) { (response, error, message, rstatus) in
                
                SVProgressHUD.dismiss()
                if error == nil && rstatus == 1{
                    showAppAlertWithMessage("Profile Updated", viewController: self)
                    // save here
                    var user = getLoginDetails()
                    let interstIn = self.userDict.interestIn
                    
                    user?.interestIn = interstIn
                    setLoginDetails(user)
                    
                    if  let data  = response as? Data{
                        do {
                            if let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary{
                                if let image = (json["data"] as? NSDictionary)?["profile_image"] as? String{
                                    setStringValueToUserDefaults(strValue: image, ForKey: AppKeys.UD.kProfileImage)
                                }
                            }
                        } catch { }
                    }
                    self.dismiss(animated: true, completion: {
                        setBooleanValueToUserDefaults(booleanValue: true, ForKey: AppKeys.UD.kProfileUpdated)
                      
                        APP_DELEGATE.setHomeAsRoot()
                    })
                }else{
                    showAppAlertWithMessage(message!, viewController: self)
                }
            }
            
        }else{
            showNoInternetAlert()
        }
        
    }
}

extension InfoPageEightVC : UITextViewDelegate{
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.count == 0 {
            lblAbout.isHidden = false
        }else{
            lblAbout.isHidden = true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars < 150
    }
}
