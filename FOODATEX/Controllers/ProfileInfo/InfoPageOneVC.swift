//
//  InfoPageOneVC.swift
//  FOODATEX
//
//  Created by Silver Shark on 11/08/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit
import CoreLocation
import CountryPickerView

class InfoPageOneVC: BaseVC {

    @IBOutlet weak var viewFullName: UIView!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewAge: UIView!
    @IBOutlet weak var viewLocation: UIView!
    @IBOutlet weak var viewContactNumber: UIView!
    @IBOutlet weak var viewtxtNumber: UIView!
    @IBOutlet weak var viewlblLocation: UIView!
    @IBOutlet weak var pickerAge: AKPickerView!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var txtLocation: UITextField!
    @IBOutlet weak var btnCCode: UIButton!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var btncCode: UIButton!
    @IBOutlet weak var txtFullName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    
    
    let locationManager = CLLocationManager()
    
    var cp: CountryPickerView!
    var isFromMobile = false
    var userDict = UserInfo()
    var phone = ""
    var ccode = ""
    var email = ""
    
    //MARK: - LIFE CYCLE
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        
        if let isPhone = getLoginDetails()?.phone , isPhone.count > 0 {
            isFromMobile = true
            txtPhone.text = phone
            btnCCode.setTitle("+\(ccode)", for: .normal)
        }
        txtEmail.text = email
        pickerAge.delegate = self
        pickerAge.dataSource = self
        pickerAge.font = APPFONT_REGULAR(17)
        pickerAge.highlightedFont = APPFONT_BOLD(20)
        pickerAge.interitemSpacing = 15
        pickerAge.pickerViewStyle = .style3D
        pickerAge.selectItem(0, animated: true)
        
        viewEmail.createAppBorder()
        viewFullName.createAppBorder()
        viewLocation.createAppBorder()
        viewContactNumber.createAppBorder()
        viewtxtNumber.createBordersWithColor(color: .lightGray, radius: 8, width: 1)
        viewlblLocation.createBordersWithColor(color: .lightGray, radius: 8, width: 1)
        btnNext.setCornerRadius(radius: 8)

        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        viewEmail.isHidden = !isFromMobile
        viewFullName.isHidden = !isFromMobile
        viewContactNumber.isHidden = isFromMobile
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationbarleft_imagename("", left_action: #selector(doNothing), right_imagename: "", right_action: #selector(doNothing), title: "", isLogo: true)
    }
    
    
    @IBAction func btnLocationTapped(_ sender: Any) {
        getMyLocation()
    }
    
    @IBAction func btnCountryCodeTapped(_ sender: Any) {
        
        cp = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 120, height: 20))
        cp.dataSource = self
        cp.delegate = self
        cp.showCountriesList(from: self)
    }
    //MARK : - CHECK FOR CLLOCATIONMANAGER AUTHORISATION STATUS
    func getMyLocation(){
        let status = CLLocationManager.authorizationStatus()
        if status == .authorizedAlways || status == .authorizedWhenInUse { self.locationManager.startUpdatingLocation()}
        else if status == .notDetermined { self.locationManager.requestAlwaysAuthorization() }
        else if status == .denied || status == .restricted { askToAllowLocation() }
        else{ askToAllowLocation()}
    }
    
    func askToAllowLocation(){
        let alertView = UIAlertController()
        
        let settingAction = UIAlertAction(title: "Setting", style: .default) { (action) in
            if UIApplication.shared.canOpenURL(URL(string: UIApplicationOpenSettingsURLString)!){
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
                } else { UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!) }
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive) { (action) in

        }
        alertView.title = APP_NAME
        alertView.message = "Please allow location access in setting "
        alertView.addAction(settingAction)
        alertView.addAction(cancelAction)
        self.present(alertView, animated: true, completion: nil)
    }
    
    @IBAction func btnNextTapped(_ sender: Any) {
        
        
        if txtLocation.text!.count == 0 {
            showAppAlertWithMessage("Please enter your location", viewController: self)
            return
        }
        
        if !isFromMobile {
            
            if txtPhone.text!.count == 0 {
                showAppAlertWithMessage("Please enter your valid mobile number", viewController: self)
                return
            }
            
            if isFromMobile && !txtPhone.text!.isValidPhone(){
                showAppAlertWithMessage("Please enter valid mobile number", viewController: self)
                return
            }
        }else{
            if txtFullName.text!.count == 0 {
                showAppAlertWithMessage("Please enter Fullname", viewController: self)
                return
            }
            
            if txtEmail.text!.count == 0 {
                showAppAlertWithMessage("Please enter valid email", viewController: self)
                return
            }else if !txtEmail.text!.isEmail(){
                showAppAlertWithMessage("Please enter valid email", viewController: self)
                return
            }
            
        }
        
        
        let second = loadVC(AppStoryboards.kSBLogin, strVCId: AppViewControllers.InfoPage.kVCInfoTwo) as! InfoPageTwo
        userDict.dob = "\(Int(pickerAge.selectedItem + 18))"
      //  userDict.phone = btnCCode.title(for: .normal)! + txtPhone.text!
         userDict.phone = txtPhone.text!
        userDict.countryCode = btncCode.title(for: .normal)!
        if isFromMobile {
            userDict.fullname = txtFullName.text ?? ""
            userDict.email = txtEmail.text ?? ""
        }
        userDict.address = txtLocation.text!
        second.userDict = userDict
        
        self.navigationController?.pushViewController(second, animated: true)
        
    }

}

extension InfoPageOneVC : AKPickerViewDataSource, AKPickerViewDelegate{
    func numberOfItems(in pickerView: AKPickerView!) -> UInt {
        return 100
    }
    func pickerView(_ pickerView: AKPickerView!, titleForItem item: Int) -> String! {
        return "\(item + 18)"
    }
    
    func pickerView(_ pickerView: AKPickerView!, didSelectItem item: Int) {
    }
}

//MARK: - CLLOCATION DELEGATE METHODS
extension InfoPageOneVC : CLLocationManagerDelegate {
    
    private func locationManager(manager: CLLocationManager!,
                                 didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        switch status {
        case CLAuthorizationStatus.authorizedAlways :
            locationManager.startUpdatingLocation()
            break
        case CLAuthorizationStatus.authorizedWhenInUse :
            locationManager.startUpdatingLocation()
            break
        case CLAuthorizationStatus.restricted :
            askToAllowLocation()
            break
        case CLAuthorizationStatus.denied:
            askToAllowLocation()
            break
        case CLAuthorizationStatus.notDetermined:
            locationManager.requestAlwaysAuthorization()
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if locations.count > 0 {
            let myLocation = locations.last!
            print(myLocation)
            
            CLGeocoder().reverseGeocodeLocation(myLocation) { (placeMarks, error) in
                if error == nil && placeMarks!.count > 0 {
                    let pm = placeMarks![0]
                    self.txtLocation.text = "\(pm.locality ?? ""), \(pm.country ?? "")"
                }
            }
            
            locationManager.stopUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        showAppAlertWithMessage("Failed to get your location, Please try again.", viewController: self)
    }
}

extension InfoPageOneVC: CountryPickerViewDataSource {
    
    func preferredCountries(in countryPickerView: CountryPickerView) -> [Country] {
        return []
    }
    
    func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView) -> String? {
        return nil
    }
    
    func showOnlyPreferredSection(in countryPickerView: CountryPickerView) -> Bool {
        return false
    }
    
    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
        return "Select a Country"
    }
    
    func searchBarPosition(in countryPickerView: CountryPickerView) -> SearchBarPosition {
       return .navigationBar
    }
    
    func showPhoneCodeInList(in countryPickerView: CountryPickerView) -> Bool {
        return true
    }
}

extension InfoPageOneVC : CountryPickerViewDelegate{
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        print(country)
        btnCCode.setTitle(country.phoneCode, for: .normal)
    }
}


extension UITextField {
    func showDoneButtonOnKeyboard() {
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(resignFirstResponder))
        
        var toolBarItems = [UIBarButtonItem]()
        toolBarItems.append(flexSpace)
        toolBarItems.append(doneButton)
        
        let doneToolbar = UIToolbar()
        doneToolbar.items = toolBarItems
        doneToolbar.sizeToFit()
        
        inputAccessoryView = doneToolbar
    }
}
