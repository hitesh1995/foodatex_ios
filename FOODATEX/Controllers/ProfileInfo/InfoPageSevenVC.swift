
//
//  InfoPageSevenVC.swift
//  FOODATEX
//
//  Created by Silver Shark on 16/08/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit
import CropViewController
import AVFoundation
import SVProgressHUD
import MobileCoreServices

class InfoPageSevenVC: BaseVC, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var userDict = UserInfo()
    var selectedInd = 0
    var arrImages : [UIImage] = [UIImage]()
    var arrImageName : [String] = [String]()
    let imagePicker = UIImagePickerController()
    
    var videoUrl : URL?

    @IBOutlet weak var tblInfo: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationbarleft_imagename("icon_Back", left_action: #selector(navigateToPreviousView), right_imagename: "", right_action: #selector(doNothing), title: "", isLogo: true)
    }
    
    @IBAction func btnNextTapped(_ sender: Any) {
        if arrImages.count == 0 {
            showAppAlertWithMessage("You must select atleast one image", viewController: self)
            return
        }
        
        let eightVC = loadVC(AppStoryboards.kSBLogin, strVCId: AppViewControllers.InfoPage.kVCInfoEight) as! InfoPageEightVC
        eightVC.arrImages = arrImages
        eightVC.arrImageName = arrImageName
        eightVC.userDict = userDict
        eightVC.videoUrl = videoUrl
        self.navigationController?.pushViewController(eightVC, animated: true)
    }
    
    @IBAction func takePictures(){
        openImagePicker(false)
    }
    
    @IBAction func takeVideo(){
        openImagePicker(true)
    }
    
    //MARK :- OPEN PICKER
    @objc func openImagePicker( _ isVideo : Bool){
        
        let gallary : UIAlertAction = UIAlertAction(title: "Gallery", style: .default) { (isSelected) in
            self.checkLibrary(isVideo)
        }
        
        let camera : UIAlertAction = UIAlertAction(title: "Camera", style: .default) { (isSelected) in
            self.checkCamera(isVideo)
        }
        let cancel : UIAlertAction = UIAlertAction(title: "Cancel", style: .destructive) { (isSelected) in}
        
        let alert : UIAlertController = UIAlertController(title: APP_NAME, message: "Select Image from", preferredStyle: .actionSheet)
        alert.addAction(gallary)
        alert.addAction(camera)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    //MARK: - CAMEAR/GALLERY METHODS
    func checkCamera( _ isVideo : Bool) {
        
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch authStatus {
        case .authorized: callCamera(isVideo) // Do your stuff here i.e. callCameraMethod()
        case .denied: alertToEncourageAccessInitially(type: 0, isVideo)
        case .notDetermined: alertPromptToAllowAccessViaSetting(type:1, isVideo)
        default: alertToEncourageAccessInitially(type: 0, isVideo)
        }
    }
    func checkLibrary( _ isVideo : Bool) {
        
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch authStatus {
        case .authorized: callLibrary(isVideo)
        case .denied: alertToEncourageAccessInitially(type:1, isVideo)
        case .notDetermined: alertPromptToAllowAccessViaSetting(type:1, isVideo)
        default: alertToEncourageAccessInitially(type:1, isVideo)
        }
    }
    
    //ASK FOR CAMERA PERMISSION
    func alertToEncourageAccessInitially(type:Int, _ isVideo : Bool) {
        
        let alert = UIAlertController(
            title: APP_NAME,
            message: type == 0 ? "Please allow App to access your camera" : " Please allow App to access your Library",
            preferredStyle: UIAlertControllerStyle.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Settings", style: .cancel, handler: { (alert) -> Void in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(URL(string:UIApplicationOpenSettingsURLString)!)
            }
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func alertPromptToAllowAccessViaSetting(type:Int, _ isVideo : Bool) {
        
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
            if response{
                if type == 0{
                    DispatchQueue.main.async {
                        self.callCamera(isVideo)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.callLibrary(isVideo)
                    }
                }
            }else{
                if type == 0{
                    self.checkCamera(isVideo)
                }else{
                    self.checkLibrary(isVideo)
                }
            }
        }
    }
    
    func callCamera(_ isVideo : Bool){
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.allowsEditing = isVideo ? false : true
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            
            imagePicker.delegate = self
            if isVideo{
                imagePicker.mediaTypes = [kUTTypeMovie as String]
            }
            if #available(iOS 11.0, *) {
                imagePicker.videoExportPreset = AVAssetExportPresetPassthrough
            }else{
                imagePicker.videoQuality = .typeMedium
            }
            imagePicker.cameraCaptureMode = isVideo ? .video : .photo
            self.present(imagePicker,animated: true,completion: nil)
        } else { showAppAlertWithMessage("Unable to access your camera", viewController: self) }
    }
    
    func callLibrary(_ isVideo : Bool){
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = isVideo ? false : true
        imagePicker.delegate = self
        imagePicker.mediaTypes = isVideo ? ["public.movie"] : ["public.image"]
        if isVideo{
            if #available(iOS 11.0, *) {
                imagePicker.videoExportPreset = AVAssetExportPresetPassthrough
            }else{
                imagePicker.videoQuality = .typeMedium
            }
        }
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    //MARK: - IMAGE PICKER DELEGATE
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        if picker.mediaTypes == ["public.movie"] {
            let videoURL = info[UIImagePickerControllerMediaURL] as? URL
            print(videoURL!)
            self.videoUrl = videoURL
            tblInfo.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .none)
        }else{
            if let image = info[UIImagePickerControllerEditedImage]! as? UIImage{
                //presentCropViewController(image)
                let format = DateFormatter()
                format.dateFormat="dd-mm-yyyy-HH-mm-ss"
                let fileName = "Image_\(format.string(from: Date())).jpeg".replacingOccurrences(of: "-", with: "_")
                arrImages.append(image)
                arrImageName.append(fileName)
                tblInfo.reloadData()
            }
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func presentCropViewController(_ selectedImage : UIImage) {
        let cropViewController = CropViewController(image: selectedImage)
        cropViewController.delegate = self
        cropViewController.aspectRatioLockEnabled = true
        cropViewController.customAspectRatio = CGSize(width: 300, height: 300)
        cropViewController.resetAspectRatioEnabled = false
        cropViewController.aspectRatioPickerButtonHidden = true
        present(cropViewController, animated: true, completion: nil)
    }

}

extension InfoPageSevenVC : CropViewControllerDelegate{
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        cropViewController.dismiss(animated: true, completion: nil)
        let format = DateFormatter()
        format.dateFormat="dd-mm-yyyy-HH-mm-ss"
        let fileName = "Image_\(format.string(from: Date())).jpeg".replacingOccurrences(of: "-", with: "_")
        arrImages.append(image)
        arrImageName.append(fileName)
        tblInfo.reloadData()
    }
    
    func cropViewController(_ cropViewController: CropViewController, didFinishCancelled cancelled: Bool) {
        cropViewController.dismiss(animated: true, completion: nil)
    }
    
}


extension InfoPageSevenVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            return imageCell( tableView, indexPath)
        case 1:
            return videoCell( tableView, indexPath)
        default:
            return UITableViewCell()
        }
    }
    
    func imageCell( _ tableView : UITableView , _ indexPath : IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TVCInfoImage", for: indexPath) as!
        TVCInfoImage
        cell.btnImage.addTarget(self, action: #selector(takePictures), for: .touchUpInside)
        cell.delegate = self
        cell.viewBG.createAppBorder()
        cell.arrImages = arrImageName
        cell.loadColView()
        return cell
    }
    
    func videoCell( _ tableView : UITableView , _ indexPath : IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TVCInfoVideo", for: indexPath) as! TVCInfoVideo
        cell.btnVideo.addTarget(self, action: #selector(takeVideo), for: .touchUpInside)
        cell.lblVideoName.text = videoUrl?.absoluteString.count == 0 ? "No Video recorded Yet" : videoUrl?.pathComponents.last
        cell.viewBG.createAppBorder()
        return cell
    }
    
}


extension InfoPageSevenVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            return CGFloat(160.0 + Double(arrImages.count*35))
        }
        return 160
    }
}

extension InfoPageSevenVC : TVCInfoImageDelegate {
    func imageDeleted(_ index : Int){
        arrImages.remove(at: index)
        arrImageName.remove(at: index)
        self.tblInfo.reloadData()
    }
}

