//
//  InfoPageFiveVC.swift
//  FOODATEX
//
//  Created by Silver Shark on 16/08/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit
import SVProgressHUD

class InfoPageFiveVC: BaseVC {

    var userDict = UserInfo()
    var occupation = 0
    var height = ""
    var picker : PickerVC!
    var cCode : String = "+1"
    var foodType = ""
    var selectedHeight = ""
    var aboutMe = ""
    var selectedInd = 0
    var wantKids = ""
    var mStatus = ""
    var haveKids = ""
    var arrayKids : [String] = ["No", "1", "2", "3", "4", "4+"]
    var arrMaritalStatus : [String] = ["Single", "Committed", "Married", "Divorced", "Widowed"]
    
    @IBOutlet weak var tblinfo: UITableView!
    //MARK: - LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationbarleft_imagename("icon_Back", left_action: #selector(navigateToPreviousView), right_imagename: "", right_action: #selector(doNothing), title: "", isLogo: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnNextTapped(_ sender: Any) {
        
        for i in 0..<3{
            let index = IndexPath.init(row: i, section: 0)
            let cell = tblinfo.cellForRow(at: index) as! TVCInfoSelection
            tblinfo.scrollToRow(at: index, at: .none, animated: false)
            switch i {
            case 0:
                wantKids = cell.lblValue.text ?? ""
                break
            case 1:
                mStatus = cell.lblValue.text ?? ""
                break
            case 2:
                haveKids = cell.lblValue.text ?? ""
                break
            default:
                break
            }
        }
        
        if mStatus == ""{
            showAppAlertWithMessage("You must select Marital Status", viewController: self)
            return
        }
        
        if haveKids == ""{
            showAppAlertWithMessage("You must select if you have kids or not", viewController: self)
            return
        }
        
        
        let sixVC = loadVC(AppStoryboards.kSBLogin, strVCId: AppViewControllers.InfoPage.kVCInfoSix) as! InfoPageSixVC
        userDict.maritalStatus = mStatus
        userDict.doYouHaveKids = haveKids
        userDict.doYouWantKids = wantKids
        sixVC.userDict = userDict
        self.navigationController?.pushViewController(sixVC, animated: true)
        
        
    }
    
}

extension InfoPageFiveVC {
    
    func showPicker(){
        
        if picker != nil {
            picker.hideCustomPicker()
            picker = nil
        }
        
        picker = PickerVC(nibName: "PickerVC", bundle: nil)
        picker.isMultiSelection = false
        picker.delegate = self
        switch selectedInd {
        case 0:
            picker.arrayPickerTemp = arrayKids
            picker.titleMessage = "Do you want Kids?"
            break
        case 1:
            picker.arrayPickerTemp = arrMaritalStatus
            picker.titleMessage = "Select Marital Status"
            break
        default:
            picker.titleMessage = "Do you have kids?"
            picker.arrayPickerTemp = arrayKids
            break
        }
        picker.selectedIndex = []
        
        
        picker.show()
    }
    
}

extension InfoPageFiveVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellInfoSelection", for: indexPath) as! TVCInfoSelection
        cell.viewBG.createAppBorder()
        switch indexPath.row {
        case 0:
            cell.lblTitle.text = "DO YOU WANT KIDS? (Optional)"
            cell.lblSubTitle.text = "Select if you want kids"
            break
        case 1:
            cell.lblTitle.text = "MARITAL STATUS"
            cell.lblSubTitle.text = "Select your marital status"
            break
        default:
            cell.lblTitle.text = "DO YOU HAVE KIDS?"
            cell.lblSubTitle.text = "Select your choice"
            break
        }
        return cell
    }
}

extension InfoPageFiveVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedInd = indexPath.row
        showPicker()
    }
}

extension InfoPageFiveVC : PickerDelegate {
    
    func pickerDidFinishedWithIndex(_ selectedIndex: [IndexPath]) {
        
        let cell = tblinfo.cellForRow(at: IndexPath.init(row: selectedInd, section: 0)) as! TVCInfoSelection
        tblinfo.scrollToRow(at: IndexPath.init(row: selectedInd, section: 0), at: .none, animated: false)
        
        switch selectedInd {
        case 1:
        cell.lblValue.text = arrMaritalStatus[selectedIndex[0].row]
            break
        default:
            cell.lblValue.text = arrayKids[selectedIndex[0].row]
            break
        }
        
        if picker != nil{
            picker.hideCustomPicker()
            picker = nil
        }
    }
    
    func pickerDidCancelled() {
        if picker != nil{
            picker.hideCustomPicker()
            picker = nil
        }
    }
}
