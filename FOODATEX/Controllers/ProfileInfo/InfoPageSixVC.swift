//
//  InfoPageSixVC.swift
//  FOODATEX
//
//  Created by Silver Shark on 16/08/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit


class InfoPageSixVC: BaseVC {
    
    @IBOutlet weak var tblInfo: UITableView!
    var userDict = UserInfo()
    
    //MARK: - LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationbarleft_imagename("icon_Back", left_action: #selector(navigateToPreviousView), right_imagename: "", right_action: #selector(doNothing), title: "", isLogo: true)
    }
    
    @IBAction func btnNextTapped(_ sender: Any) {
        
        for i in 0...1 {
            
            tblInfo.scrollToRow(at: IndexPath(row: i, section: 0), at: .none, animated: false)
            let cell = tblInfo.cellForRow(at: IndexPath(row: i, section: 0)) as! TVCInfoRadioCell
            
            switch i{
            case 0:
                userDict.doYouSmoke = cell.isTrue
                break
            case 1:
                userDict.doYouDrink = cell.isTrue
                break
            default:
                //userDict.doYouTakeDrug = cell.isTrue
                break
            }
        }
        
        let infoSeven = loadVC(AppStoryboards.kSBLogin, strVCId: AppViewControllers.InfoPage.kVCInfoSeven) as! InfoPageSevenVC
        infoSeven.userDict = userDict
        self.navigationController?.pushViewController(infoSeven, animated: true)
    }
}

extension InfoPageSixVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TVCInfoRadioCell", for: indexPath) as! TVCInfoRadioCell
        cell.viewBG.createAppBorder()
        switch indexPath.row {
        case 0:
            cell.lblTitle.text = "Do you Smoke?"
            break
        case 1:
            cell.lblTitle.text = "Do you Drink?"
            break
        case 2:
            cell.lblTitle.text = "Do you take Drugs?"
            break
        default:
            break
        }
        return cell
    }
    
}


extension InfoPageSixVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return SCREENWIDTH()*0.4062
    }
}
