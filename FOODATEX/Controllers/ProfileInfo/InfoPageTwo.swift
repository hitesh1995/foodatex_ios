//
//  InfoPageTwo.swift
//  FOODATEX
//
//  Created by Silver Shark on 16/08/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit
import SVProgressHUD

enum EnumGender : Int{
    case Male
    case Female
    case Both
}
enum EnumMyGender:Int{
    case Male
    case Female
    case Other
}
enum EnumLookingFor : Int{
    case test
    case Romance
    case Friendship
}

class InfoPageTwo: BaseVC {

    var userDict = UserInfo()
    var arryFoodPrefrence : [FoodPrefrenceData] = [FoodPrefrenceData]()
    var picker : PickerVC!
    var foodSelectedIndex : [IndexPath] = [IndexPath]()
    
    
    @IBOutlet var viewMyGender: UIView!
    
    @IBOutlet var imgMyGenderMale: UIImageView!
    
    @IBOutlet var imgMyGenderFemale: UIImageView!
    
    @IBOutlet weak var imgMyGenderOther: UIImageView!
    
    
    @IBOutlet weak var viewFood: UIView!
    @IBOutlet weak var viewLblFood: UIView!
    @IBOutlet weak var viewIntreset: UIView!
    @IBOutlet weak var viewLookngFor: UIView!
    @IBOutlet weak var iconMale: UIImageView!
    @IBOutlet weak var iconFeMale: UIImageView!
    @IBOutlet weak var iconBoth: UIImageView!
    @IBOutlet weak var iconRomance: UIImageView!
    @IBOutlet weak var iconFriendship: UIImageView!
    @IBOutlet weak var lblFood: UITextField!
    @IBOutlet weak var btnNext: UIButton!
    
    
    var gender : Int = 0
    var myGender:Int = 0
    var lookingfor : Int = 0
    
    //MARK: - LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        viewFood.createAppBorder()
        viewIntreset.createAppBorder()
        viewLookngFor.createAppBorder()
        viewLblFood.createAppBorder()
        viewMyGender.createAppBorder()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationbarleft_imagename("icon_Back", left_action: #selector(navigateToPreviousView), right_imagename: "", right_action: #selector(doNothing), title: "", isLogo: true)
    }

    @IBAction func btnFoodTapped(_ sender: Any) {
        
        if arryFoodPrefrence.count == 0 {
            getFoodprefrence()
        }else{
            showFoodPrefrencePicker()
        }
    }
    //MARK: - My Gender Selection
    
    @IBAction func btn_MyGender_Male_clicked(_ sender: Any) {
        myGender = EnumMyGender.Male.rawValue
        setMyGenderSelection()
    }
    
    @IBAction func btn_MyGender_Female_clicked(_ sender: Any) {
        myGender = EnumMyGender.Female.rawValue
        setMyGenderSelection()
    }
    
    @IBAction func btn_MyGender_Other_Clicked(_ sender: Any) {
        myGender = EnumMyGender.Other.rawValue
        setMyGenderSelection()
    }
    
    
    //MARK: - GENDR SELECTION
    
    @IBAction func btnMaleTapped(_ sender: Any) {
        
        gender = EnumGender.Male.rawValue
        setGenderSelection()
    }
    @IBAction func btnFemaleTapped(_ sender: Any) {
        gender = EnumGender.Female.rawValue
        setGenderSelection()
    }
    @IBAction func btnBothTapped(_ sender: Any) {
        gender = EnumGender.Both.rawValue
        setGenderSelection()
    }
    
    //MARK: - LOOKING FOR SELECTION
    
    @IBAction func btnRomanceTapped(_ sender: Any) {
        lookingfor = EnumLookingFor.Romance.rawValue
        setLookingforSelectionSelection()
    }
    
    @IBAction func btnFriendshipTapped(_ sender: Any) {
        lookingfor = EnumLookingFor.Friendship.rawValue
        setLookingforSelectionSelection()
    }
    
    //MARK: - NEXT ACTION
    @IBAction func btnNextTapped(_ sender: Any) {
        
        var foodString = ""
        
        for (_, object) in foodSelectedIndex.enumerated(){
            
            let objFood = arryFoodPrefrence[object.item]
            foodString.append(objFood.preference ?? "")
            foodString.append(",")
        }
        
        if foodString.count > 2 {
            foodString.removeLast()
        }
        if foodSelectedIndex.count == 0 {
            showAppAlertWithMessage("You must select any food Preference", viewController: self)
            return
        }
        
        let third = loadVC(AppStoryboards.kSBLogin, strVCId: AppViewControllers.InfoPage.kVCInforThree) as! InfoPageThreeVC
        userDict.interestIn = "\(gender+1)"
        userDict.gender = "\(myGender+1)"
        userDict.foodPreference = foodString
        userDict.lookingFor = self.lookingfor.description
        third.userDict = userDict
        third.lookingFor = lookingfor
        self.navigationController?.pushViewController(third, animated: true)
    }
}

extension InfoPageTwo {
    func setMyGenderSelection(){
        
        imgMyGenderMale.image = #imageLiteral(resourceName: "icon_RadioUnchecked")
        imgMyGenderFemale.image = #imageLiteral(resourceName: "icon_RadioUnchecked")
        imgMyGenderOther.image = #imageLiteral(resourceName: "icon_RadioUnchecked")
       
        switch myGender {
        case EnumMyGender.Female.rawValue:
            imgMyGenderFemale.image = #imageLiteral(resourceName: "icon_RadioChecked")
            break
            
        case EnumMyGender.Other.rawValue:
            imgMyGenderOther.image = #imageLiteral(resourceName: "icon_RadioChecked")
            break
            
        default:
            imgMyGenderMale.image = #imageLiteral(resourceName: "icon_RadioChecked")
            break
        }
    }
    func setGenderSelection(){
        
        iconMale.image = #imageLiteral(resourceName: "icon_RadioUnchecked")
        iconFeMale.image = #imageLiteral(resourceName: "icon_RadioUnchecked")
        iconBoth.image = #imageLiteral(resourceName: "icon_RadioUnchecked")
        switch gender {
        case EnumGender.Female.rawValue:
            iconFeMale.image = #imageLiteral(resourceName: "icon_RadioChecked")
            break
        case EnumGender.Both.rawValue:
            iconBoth.image = #imageLiteral(resourceName: "icon_RadioChecked")
            break
        default:
            iconMale.image = #imageLiteral(resourceName: "icon_RadioChecked")
            break
        }
        
    }
    
    func setLookingforSelectionSelection(){

        if lookingfor == EnumLookingFor.Friendship.rawValue {
            iconRomance.image = #imageLiteral(resourceName: "icon_RadioUnchecked")
            iconFriendship.image = #imageLiteral(resourceName: "icon_RadioChecked")
        }else{
            iconFriendship.image = #imageLiteral(resourceName: "icon_RadioUnchecked")
            iconRomance.image = #imageLiteral(resourceName: "icon_RadioChecked")
        }
        
    }
    
    func getFoodprefrence(){
        
        if isConnectedToNetwork(){
            SVProgressHUD.show()
            WebServicesCollection.sharedInstance.FoodPreferences { (response, error, message) in
                SVProgressHUD.dismiss()
                if error == nil {
                    do {
                        let loginResult  = try JSONDecoder().decode(FoodPrefrenceResult.self, from: response as! Data)
                        if let foodDATA = loginResult.data {
                            self.arryFoodPrefrence = foodDATA
                            self.showFoodPrefrencePicker()
                        }
                    }catch{
                        SVProgressHUD.showError(withStatus: AppMessages.FB_Error)
                    }
                }else{
                    showAppAlertWithMessage("Failed to get Food Preference , Please trye again!", viewController: self)
                }
            }
            
        }
        
    }
    
    func showFoodPrefrencePicker(){
        
        var arrPref : [String] = [String]()
        for  (_, object) in arryFoodPrefrence.enumerated(){
            arrPref.append(object.preference ?? "")
        }
        
        if picker != nil {
            picker.hideCustomPicker()
            picker = nil
        }
        
        picker = PickerVC(nibName: "PickerVC", bundle: nil)
        picker.isMultiSelection = true
        
        picker.delegate = self
        picker.selectedIndex = foodSelectedIndex
        picker.titleMessage = "Select food Preference"
        picker.arrayPickerTemp = arrPref
        picker.show()
    }
}

extension InfoPageTwo : PickerDelegate {
    
    func pickerDidFinishedWithIndex(_ selectedIndex: [IndexPath]) {
        foodSelectedIndex.removeAll()
        
        foodSelectedIndex.append(contentsOf: selectedIndex)
        if foodSelectedIndex.count > 3{
            showAppAlertWithMessage("You can select only 3 favourite foods.", viewController: self)
            return
        }
        var foodString = ""
        for ( _ , object) in selectedIndex.enumerated() {
            let objFood = arryFoodPrefrence[object.item]
            foodString.append(objFood.preference ?? "")
            foodString.append(",")
             foodString.append(" ")
        }
        if foodString.count > 2 {
            foodString.removeLast()
        }
        lblFood.text = foodString
        if picker != nil{
            picker.hideCustomPicker()
            picker = nil
        }
    }
    
    func pickerDidCancelled() {
        if picker != nil{
            picker.hideCustomPicker()
            picker = nil
        }
    }
}
