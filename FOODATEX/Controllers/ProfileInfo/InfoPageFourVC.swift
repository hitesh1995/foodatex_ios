//
//  InfoPageFourVC.swift
//  FOODATEX
//
//  Created by Silver Shark on 16/08/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit
import SVProgressHUD

class InfoPageFourVC: BaseVC {
    
    var userDict = UserInfo()
    var cityName = ""
    var occupation = 0
    var arrHeight : [String] = [String]()
    var picker : PickerVC!
    var cCode : String = "+1"
    var selectedHeight = ""
    var userProfileObj : UserProfileData!
    let arrMaritalStatus : [String] = ["Single", "Committed", "Married", "Divorced", "Widowed"]
    
    enum ENUM_OCCUPATION : Int {
        case Working
        case Study
    }
    
    
    @IBOutlet weak var viewHeight: UIView!
    @IBOutlet weak var viewPersonality: UIView!
    @IBOutlet weak var viewOccupation: UIView!
    @IBOutlet weak var txtABout: UITextView!
    @IBOutlet weak var iconWorking: UIImageView!
    @IBOutlet weak var iconStudy: UIImageView!
    @IBOutlet weak var lblHeight: UITextField!
    @IBOutlet weak var lblAboutPlaceholder: UILabel!
    
    
    //MARK: - LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeight.createAppBorder()    //Now used for marital status
        viewPersonality.createAppBorder()
        viewOccupation.createAppBorder()
        arrHeight.append(contentsOf: ["Less then 3 Feet 5 Inch","3.6 ft", "3.7 ft", "3.8 ft", "3.9 ft", "3.10 ft", "3.11 ft", "4 Feet","4.1 ft","4.2 ft", "4.3 ft", "4.4 ft", "4.5 ft", "4.6 ft", "4.7 ft", "4.8 ft", "4.9 ft", "4.10 ft", "4.11 ft", "5 Feet ft","5.1 ft","5.2 ft", "5.3 ft", "5.4 ft", "5.5 ft", "5.6 ft", "5.7 ft", "5.8 ft", "5.9 ft", "5.10 ft", "5.11 ft", "6 Feet ft","6.1 ft","6.2 ft", "6.3 ft", "6.4 ft", "6.5 ft", "6.6 ft", "6.7 ft", "6.8 ft", "6.9 ft", "6.10 ft", "6.11 ft", "7 Feet ft", "Above 7 Feet"])
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationbarleft_imagename("icon_Back", left_action: #selector(navigateToPreviousView), right_imagename: "", right_action: #selector(doNothing), title: "", isLogo: true)
    }
    
    @IBAction func btnHeightTapped(_ sender: Any) {
        //showHeightPicker()
        showMaritalStatusPicker()
    }
    
    @IBAction func btnWorkingTapped(_ sender: Any) {
         occupation = ENUM_OCCUPATION.Working.rawValue
        iconWorking.image = #imageLiteral(resourceName: "icon_RadioChecked")
        iconStudy.image = #imageLiteral(resourceName: "icon_RadioUnchecked")
    }
    
    @IBAction func btnStudyTapped(_ sender: Any) {
        
        occupation = ENUM_OCCUPATION.Study.rawValue
        iconStudy.image = #imageLiteral(resourceName: "icon_RadioChecked")
        iconWorking.image = #imageLiteral(resourceName: "icon_RadioUnchecked")
    }
    
    @IBAction func btnNextTapped(_ sender: Any) {
        
        if txtABout.text.trimmingCharacters(in:.whitespacesAndNewlines).count == 0{
            showAppAlertWithMessage("You must enter some details about yourself", viewController: self)
            return
        }
        
        if selectedHeight.count == 0 {
            showAppAlertWithMessage("Select your marital status", viewController: self)
            return
        }
        
        let sixVC = loadVC(AppStoryboards.kSBLogin, strVCId: AppViewControllers.InfoPage.kVCInfoSix) as! InfoPageSixVC
        userDict.maritalStatus = selectedHeight
        userDict.personality = txtABout.text.trimmingCharacters(in: .whitespacesAndNewlines)
        sixVC.userDict = userDict
        self.navigationController?.pushViewController(sixVC, animated: true)
    }
    
    func showHeightPicker(){
        
        if picker != nil {
            picker.hideCustomPicker()
            picker = nil
        }
        
        picker = PickerVC(nibName: "PickerVC", bundle: nil)
        picker.isMultiSelection = false
        picker.delegate = self
        picker.selectedIndex = []
        picker.titleMessage = "Select your height"
        picker.arrayPickerTemp = arrHeight
        picker.show()
    }
    
    func showMaritalStatusPicker(){
        if picker != nil {
            picker.hideCustomPicker()
            picker = nil
        }
        
        picker = PickerVC(nibName: "PickerVC", bundle: nil)
        picker.isMultiSelection = false
        picker.delegate = self
        picker.selectedIndex = []
        picker.arrayPickerTemp = arrMaritalStatus
        picker.titleMessage = "Select Marital Status"
        picker.show()
    }
    
}

extension InfoPageFourVC : PickerDelegate {
    
    func pickerDidFinishedWithIndex(_ selectedIndex: [IndexPath]) {
        
        lblHeight.text = arrMaritalStatus[selectedIndex.first!.row]
        selectedHeight = arrMaritalStatus[selectedIndex.first!.row]
        if picker != nil{
            picker.hideCustomPicker()
            picker = nil
        }
    }
    
    func pickerDidCancelled() {
        if picker != nil{
            picker.hideCustomPicker()
            picker = nil
        }
    }
}

extension InfoPageFourVC : UITextViewDelegate{
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.count == 0 {
            lblAboutPlaceholder.isHidden = false
        }else{
            lblAboutPlaceholder.isHidden = true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars < 150
    }
}
