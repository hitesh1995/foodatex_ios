//
//  InfoPageThreeVC.swift
//  FOODATEX
//
//  Created by Silver Shark on 16/08/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit

class InfoPageThreeVC: BaseVC {

    
    var userDict = UserInfo()
    var intrestIn : Int = 0
    var lookingFor : Int = 0
    var isAllergy = true
    var picker : PickerVC!
    var foodType : String = ""
    
    @IBOutlet weak var viewFood: UIView!
    @IBOutlet weak var viewlblFood: UIView!
    @IBOutlet weak var viewAllergies: UIView!
    @IBOutlet weak var viewCity: UIView!
    
    @IBOutlet weak var iconAllergy: UIImageView!
    @IBOutlet weak var iconNoAllergy: UIImageView!
    @IBOutlet weak var txtAllergy: UITextField!
    @IBOutlet weak var viewlblCity: UIView!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var lblFood: UITextField!
    var arrFood : [String] = [String]()
    
     var foodSelectedIndex : [IndexPath] = [IndexPath]()
    
    
    //MARK: - LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        viewFood.createAppBorder()
        viewlblFood.createAppBorder()
        viewAllergies.createAppBorder()
        viewCity.createAppBorder()
        viewlblCity.createAppBorder()
        arrFood.append(contentsOf: ["Vegan", "Vegetarian", "Eggiterian", "Pescatarian", "Halal", "Kosher", "No Preference"])
        txtCity.text = userDict.address
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationbarleft_imagename("icon_Back", left_action: #selector(navigateToPreviousView), right_imagename: "", right_action: #selector(doNothing), title: "", isLogo: true)
    }
    
    @IBAction func btnFoodTapped(_ sender: Any) {
        showFoodPrefrencePicker()
    }
    
    @IBAction func btnNoAllergyTapped(_ sender: Any) {
        isAllergy = false
        iconAllergy.image = #imageLiteral(resourceName: "icon_RadioUnchecked")
        iconNoAllergy.image = #imageLiteral(resourceName: "icon_RadioChecked")
    }
    
    @IBAction func btnAllergyTapped(_ sender: Any) {
        isAllergy = true
        iconNoAllergy.image = #imageLiteral(resourceName: "icon_RadioUnchecked")
        iconAllergy.image = #imageLiteral(resourceName: "icon_RadioChecked")
    }
    
    @IBAction func btnNextTapped(_ sender: Any) {
        
        if foodType.count == 0 {
            showAppAlertWithMessage("Please select your food Preference", viewController: self)
            return
        }
        
        if isAllergy && txtAllergy.text?.count == 0 {
            showAppAlertWithMessage("You must enter little info about allergy", viewController: self)
            return
        }
        
        if txtCity.text?.count == 0 {
            showAppAlertWithMessage("Please enter the city you live in", viewController: self)
            return
        }
        var foodString = ""
        for (_, object) in foodSelectedIndex.enumerated(){
            
            let objFood = arrFood[object.item]
            foodString.append(objFood)
            foodString.append(",")
            foodString.append(" ")
        }
        
        if foodString.count > 2 {
            foodString.removeLast()
        }
        
        let four = loadVC(AppStoryboards.kSBLogin, strVCId: AppViewControllers.InfoPage.kVCInfoFour) as! InfoPageFourVC
        userDict.anyAllergiesStatus = isAllergy
        userDict.lookingFor = "\(lookingFor)"
        userDict.anyAllergies = txtAllergy.text ?? ""
        userDict.city = txtCity.text ?? ""
        userDict.areYouA = foodString
        four.userDict = userDict
        self.navigationController?.pushViewController(four, animated: true)
        
    }
    
    func showFoodPrefrencePicker(){

        if picker != nil {
            picker.hideCustomPicker()
            picker = nil
        }
        
        picker = PickerVC(nibName: "PickerVC", bundle: nil)
        picker.isMultiSelection = true
        picker.delegate = self
        picker.selectedIndex = []
        picker.titleMessage = "Select Food Requirements"
        picker.arrayPickerTemp = arrFood
        picker.show()
    }
    
    
}

extension InfoPageThreeVC : PickerDelegate {
    
    func pickerDidFinishedWithIndex(_ selectedIndex: [IndexPath]) {
        foodSelectedIndex.removeAll()
        foodSelectedIndex.append(contentsOf: selectedIndex)
        
        var foodString = ""
        for ( _ , object) in selectedIndex.enumerated() {
            let objFood = arrFood[object.item]
            foodString.append(objFood)
            foodString.append(",")
            foodString.append(" ")
        }
        if foodString.count > 2 {
            foodString.removeLast()
        }
        lblFood.text = foodString
        //lblFood.text = arrFood[selectedIndex.first!.row]
        
        foodType = arrFood[selectedIndex.first!.row]
        if picker != nil{
            picker.hideCustomPicker()
            picker = nil
        }
    }
    
    func pickerDidCancelled() {
        if picker != nil{
            picker.hideCustomPicker()
            picker = nil
        }
    }
}
