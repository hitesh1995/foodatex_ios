//
//  ImageShowViewController.swift
//  SRain
//
//  Created by SANKET SHARMA on 09/05/19.
//  Copyright © 2019 SANKET SHARMA. All rights reserved.
//

import UIKit

class ImageShowViewController: UIViewController {

    
    @IBOutlet weak var imgToShow: RemoteImageView!
    var passImage:UIImage?
    var passImgStrUrl:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        if passImage != nil{
        self.imgToShow.image = passImage
        }else{
            self.imgToShow.imageURL = URL(string: passImgStrUrl!)
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btn_BlankSpace_clicked(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
