//
//  SettingVC.swift
//  FOODATEX
//
//  Created by Uffizio iMac on 28/12/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit
import SVProgressHUD

class SettingVC: BaseVC {
    
    @IBOutlet weak var viewBlockList: UIView!
    
    @IBOutlet weak var viewDeleteAccount: UIView!
    //1 Not in use
    @IBOutlet weak var viewSuperLike: UIView!
    @IBOutlet weak var viewGetBoost: UIView!
    
    //1
    @IBOutlet weak var viewSwipeCount: UIView!
    @IBOutlet weak var imgSwipe : UIImageView!{
        didSet{
            imgSwipe.setImageColor(color: #colorLiteral(red: 0.6235294118, green: 0.07843137255, blue: 0.168627451, alpha: 1))
        }
    }
    @IBOutlet weak var lblSwipeCount : UILabel!{
        didSet{
            lblSwipeCount.text = "\(USERDEFAULT.value(forKey: pendingSwipes) as? Int ?? 15)"
        }
    }
    @IBOutlet weak var switchUnlimitedSwipe : UISwitch!
    
    //2
    @IBOutlet weak var viewUnlimittedSwipe: UIView!
    @IBOutlet weak var switchUnlimitedLike: UISwitch!
    
    //3
    @IBOutlet weak var viewSkipQueue: UIView!
    @IBOutlet weak var switchSkipQueue: UISwitch!
    
    //4
    @IBOutlet weak var viewWhoSeeYou: UIView!
    @IBOutlet weak var imgStandard: UIImageView!
    @IBOutlet weak var imgPeopleILiked: UIImageView!
    @IBOutlet weak var lblStandard: UILabel!
    @IBOutlet weak var lblPeopleILike: UILabel!
    
    //5
    @IBOutlet weak var viewMatchWithPeopleArround: UIView!
    @IBOutlet weak var switchMatchPeopleArround: UISwitch!
    
    //6
    @IBOutlet weak var viewHideAds: UIView!
    @IBOutlet weak var switchHideAds: UISwitch!
    
    //7
    @IBOutlet weak var viewControlProfile: UIView!
    @IBOutlet weak var switchShowAge: UISwitch!
    @IBOutlet weak var switchShowDistance: UISwitch!
    
    var userProfileObj : UserProfileData!
    
    //MARK: - LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationbarleft_imagename("icon_Back", left_action: #selector(navigateToPreviousView), right_imagename: "", right_action: #selector(doNothing), title: "Settings", isLogo: false)
        callWSForProfile()
    }
    
    func setupUI(){
        viewSuperLike.createAppCornerRadiusWithShadow()
        viewGetBoost.createAppCornerRadiusWithShadow()
        viewSwipeCount.createAppCornerRadiusWithShadow()
        viewUnlimittedSwipe.createAppCornerRadiusWithShadow()
        viewSkipQueue.createAppCornerRadiusWithShadow()
        viewWhoSeeYou.createAppCornerRadiusWithShadow()
        viewMatchWithPeopleArround.createAppCornerRadiusWithShadow()
        viewHideAds.createAppCornerRadiusWithShadow()
        viewHideAds.createAppCornerRadiusWithShadow()
        viewControlProfile.createAppCornerRadiusWithShadow()
        viewDeleteAccount.createAppCornerRadiusWithShadow()
        viewBlockList.createAppCornerRadiusWithShadow()
    }
    
    //MARK: - BUTTON GET SUPER LIKE TAPPED
    @IBAction func btnGetSuperLikeTapped(_ sender: Any) {
    }
    
    @IBAction func btnGetBoostTapped(_ sender: Any) {
    }
    
    @IBAction func unlimitedSwipeChanged(_ sender : Any){
        if !hasInAppSubscribed(){
            showSubscriptionView()
        }
    }
    
    @IBAction func unlimitedSwitchChanged(_ sender: Any) {
        if hasInAppSubscribed() == true{
            self.updateProfile(dict: ["user_id": getLoginDetails()?.userId ?? "",
                                      "unlimitedLikes" : switchUnlimitedLike.isOn ? "1" : "0"]) { isSaved in
                                        if isSaved {
                                            self.userProfileObj?.unlimitedLikes = self.switchSkipQueue.isOn ? "1" : "0"
                                        }
            }
        }
        else{
            showSubscriptionView()
        }
    }
    
    
    @IBAction func btn_DeleteAccount_clicked(_ sender: Any) {
        
        let alert = UIAlertController(title: APP_NAME, message: "Are you sure you want to delete your account?", preferredStyle: .alert)
        
        let delete = UIAlertAction(title: "yes", style: .default) { (_) in
            
            // Call Delete Account API here
            self.callWSForDeleteAccount()
            
        }
        let cancel = UIAlertAction(title: "No", style: .default) { (_) in
            
        }
        alert.addAction(delete)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
        
    }
    func callWSForDeleteAccount(){
        
        if isConnectedToNetwork(){
            
            SVProgressHUD.show()
            
            let dict : [String : Any] = ["user_id" : getLoginDetails()?.userId ?? "0"]
            
            WebServicesCollection.sharedInstance.deleteAccount(dict) { (response, error, message) in
                
                SVProgressHUD.dismiss()
                if error == nil {
                    do {
                        print(response ?? "")
                        if (message ?? "").contains("successfully"){
                            APP_DELEGATE.setLOGINAsRoot()
                        }
                    }catch{
                        SVProgressHUD.showError(withStatus: AppMessages.User_Error)
                    }
                }else{
                    showAppAlertWithMessage("Unable to perform action", viewController: self)
                    
                }
            }
        }else{
            showNoInternetAlert()
        }
    }
    
    @IBAction func skipSwitchChanged(_ sender: Any) {
        if hasInAppSubscribed() == true{
            self.updateProfile(dict: ["user_id": getLoginDetails()?.userId ?? "",
                                      "skipTheQueue" : switchSkipQueue.isOn ? "1" : "0"]) { isSaved in
                                        if isSaved {
                                            self.userProfileObj?.skipTheQueue = self.switchSkipQueue.isOn ? "1" : "0"
                                        }
            }
        }
        else{
            showSubscriptionView()
        }
    }
    
    @IBAction func btnStandardTapped(_ sender: Any) {
        if hasInAppSubscribed() == true{
            self.updateProfile(dict: ["user_id": getLoginDetails()?.userId ?? "",
                                      "controlWhoSeesYou" : "1"]) { isSaved in
                                        if isSaved {
                                            self.imgPeopleILiked.image = UIImage()
                                            self.imgStandard.image = UIImage(named: "icon_TickMark")
                                            self.lblStandard.textColor = COLOR_CUSTOM(159, 20, 43, 1.0)
                                            self.lblPeopleILike.textColor = .black
                                            self.userProfileObj?.controlWhoSeesYou = "1"
                                        }
            }
        }
        else{
            showSubscriptionView()
        }
    }
    
    @IBAction func btnOnlyPoepleLikedTapped(_ sender: Any) {
        if hasInAppSubscribed() == true{
            self.updateProfile(dict: ["user_id": getLoginDetails()?.userId ?? "",
                                      "controlWhoSeesYou" : "2"]) { isSaved in
                                        if isSaved {
                                            self.imgStandard.image = UIImage()
                                            self.imgPeopleILiked.image = UIImage(named: "icon_TickMark")
                                            self.lblStandard.textColor = .black
                                            self.lblPeopleILike.textColor = COLOR_CUSTOM(159, 20, 43, 1.0)
                                        }
            }
        }
        else{
            showSubscriptionView()
        }
    }
    
    @IBAction func switchPeopleArroundChanged(_ sender: Any) {
        if hasInAppSubscribed() == true{
            self.updateProfile(dict: ["user_id": getLoginDetails()?.userId ?? "",
                                      "allowingYou" : switchMatchPeopleArround.isOn ? "1" : "0"]) { isSaved in
                                        if isSaved {
                                            self.userProfileObj?.skipTheQueue = self.switchMatchPeopleArround.isOn ? "1" : "0"
                                        }
            }
        }
        else{
            showSubscriptionView()
        }
    }
    
    
    @IBAction func switchHideAdsChanged(_ sender: Any) {
        if hasInAppSubscribed() == true{
            self.updateProfile(dict: ["user_id": getLoginDetails()?.userId ?? "",
                                      "hideAdverts" : switchHideAds.isOn ? "1" : "0"]) { isSaved in
                                        if isSaved {
                                            self.userProfileObj?.hideAdverts = self.switchHideAds.isOn ? "1" : "0"
                                        }
            }
        }
        else{
            showSubscriptionView()
        }
    }
    
    @IBAction func switchShowAge(_ sender: Any) {
        if hasInAppSubscribed() == true{
            self.updateProfile(dict: ["user_id": getLoginDetails()?.userId ?? "",
                                      "showMyAge" : switchShowAge.isOn ? "1" : "0"]) { isSaved in
                                        if isSaved {
                                            self.userProfileObj?.dontShowMyAge = self.switchShowAge.isOn ? "1" : "0"
                                        }
            }
        }
        else{
            showSubscriptionView()
        }
    }
    
    @IBAction func switchShowDistance(_ sender: Any) {
        if hasInAppSubscribed() == true{
            self.updateProfile(dict: ["user_id": getLoginDetails()?.userId ?? "",
                                      "distanceInvisible" : switchShowDistance.isOn ? "0" : "1"]) { isSaved in
                                        if isSaved {
                                            self.userProfileObj?.distanceInvisible = self.switchShowDistance.isOn ? "1" : "0"
                                        }
            }
        }
        else{
            showSubscriptionView()
        }
    }
    
    func showSubscriptionView(){
        UIView.animate(withDuration: 1, animations: {
            self.navigationController?.popViewController(animated: true)
        }) { (isComplete) in
            if isComplete{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GetGold"), object: nil)
            }
        }
    }
    @IBAction func btnBlockListAction(_ sender: Any) {
        let vc = loadVC(AppStoryboards.kSBMain, strVCId: AppViewControllers.kVCBlockListVC) as! BlockListViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension SettingVC {
    
    // MARK: - Call For User Detail
    func callWSForProfile(){
        
        if isConnectedToNetwork(){
            
            SVProgressHUD.show()
            
            let dict : [String : Any] = ["user_id" : getLoginDetails()?.userId ?? "0","other_user_id":getLoginDetails()?.userId ?? "0"]
            
            WebServicesCollection.sharedInstance.GetUserDetails(dict) { (response, error, message) in
                
                SVProgressHUD.dismiss()
                if error == nil {
                    do {
                        let userResult  = try JSONDecoder().decode(UserProfileResult.self, from: response as! Data)
                        
                        if let userList = userResult.data {
                            self.userProfileObj = userList
                            
                            
                            
                            self.setUserValues()
                        }
                    }catch{
                        SVProgressHUD.showError(withStatus: AppMessages.User_Error)
                    }
                }else{
                    showAppAlertWithMessage("Unable to fetch data", viewController: self)
                    
                }
            }
        }else{
            showNoInternetAlert()
        }
    }
    
    func showRetryMessage(){
        
        let alertView = UIAlertController()
        
        let settingAction = UIAlertAction(title: "Retry", style: .default) { (action) in
            self.callWSForProfile()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive) { (action) in
            self.navigationController?.popViewController(animated: true)
        }
        alertView.title = APP_NAME
        alertView.message = "Unable to get data for user, Please try again!"
        alertView.addAction(settingAction)
        alertView.addAction(cancelAction)
        self.present(alertView, animated: true, completion: nil)
        
    }
    
    func setUserValues(){
        
        //Hide Ads
        if let hideAds : String = userProfileObj.hideAdverts, hideAds == "1" {
            switchHideAds.isOn =  true
        }else{
            switchHideAds.isOn = false
        }
        //Unlimited likes
        if let unlimitedLike : String = userProfileObj.unlimitedLikes, unlimitedLike == "1" {
            switchUnlimitedLike.isOn =  true
        }else{
            switchUnlimitedLike.isOn = false
        }
        
        if let skipQueue : String = userProfileObj.skipTheQueue, skipQueue == "1" {
            switchSkipQueue.isOn =  true
        }else{
            switchSkipQueue.isOn = false
        }
        
        if let matchPeopleArround : String = userProfileObj.allowingYou, matchPeopleArround == "1" {
            switchMatchPeopleArround.isOn =  true
        }else{
            switchMatchPeopleArround.isOn = false
        }
        
        if let showAge : String = userProfileObj.showMyAge, showAge == "1" {
            switchShowAge.isOn =  true
        }else{
            switchShowAge.isOn = false
        }
        if let showDistance : String = userProfileObj.distanceInvisible, showDistance == "1" {
            switchShowDistance.isOn =  false
        }else{
            switchShowDistance.isOn = true
        }
        
        
        if let whoCanSee : String = userProfileObj.controlWhoSeesYou,  whoCanSee == "2" {
            imgPeopleILiked.image = UIImage(named: "icon_TickMark")
            imgStandard.image = UIImage()
            lblStandard.textColor = .black
            lblPeopleILike.textColor = COLOR_CUSTOM(159, 20, 43, 1.0)
        }else{
            imgPeopleILiked.image = UIImage()
            imgStandard.image = UIImage(named: "icon_TickMark")
            lblStandard.textColor = COLOR_CUSTOM(159, 20, 43, 1.0)
            lblPeopleILike.textColor = .black
        }
    }
    
    
    //MARK: - UPDATE PROFILE
    func updateProfile( dict : [String : Any], response : @escaping (_ isUploaded : Bool) -> Void ){
        
        if isConnectedToNetwork(){
            
            SVProgressHUD.show()
            WebServicesCollection.sharedInstance.UpdateProfile(dict) { (wresponse, error, message, rstatus) in
                SVProgressHUD.dismiss()
                if rstatus == 1{
                    response(true)
                }else{
                    showAppAlertWithMessage(message ?? "Something went wrong, Please trye Again", viewController: self)
                    response(false)
                }
            }
            
        }else{
            response(false)
        }
    }
}

extension UIImageView {
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}
