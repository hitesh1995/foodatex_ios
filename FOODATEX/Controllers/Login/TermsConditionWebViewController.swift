//
//  TermsConditionWebViewController.swift
//  FOODATEX
//
//  Created by SANKET SHARMA on 08/07/19.
//  Copyright © 2019 RudraApps. All rights reserved.
//

import UIKit
import SVProgressHUD
class TermsConditionWebViewController: UIViewController,UIWebViewDelegate {
    
    @IBOutlet var webView: UIWebView!
    var strUrl:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        SVProgressHUD.show()
        let req = URLRequest(url: URL(string: strUrl)!)
        self.webView.loadRequest(req)
        
        // Do any additional setup after loading the view.
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView){
        SVProgressHUD.dismiss()
    }
    
   
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error){
        SVProgressHUD.dismiss()
    }
    
    
    @IBAction func btn_Close_clicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
