//
//  PhoneVerificationVC.swift
//  FOODATEX
//
//  Created by Hitesh Prajapati on 21/03/20.
//  Copyright © 2020 RudraApps. All rights reserved.
//

import UIKit
import FirebaseAuth
import CountryPickerView
import SVProgressHUD

class PhoneVerificationVC: UIViewController {

    //MARK:- IBActions
    @IBOutlet weak var viewPhoneNumber: UIView!
    @IBOutlet weak var btnCountryCode: UIButton!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var btnNext: UIButton!{
        didSet{
            btnNext.setCornerRadius(radius: 8)
        }
    }
    @IBOutlet weak var viewConfirmation: UIView!{
        didSet{
            viewConfirmation.alpha = 0
        }
    }
    @IBOutlet weak var lblCodeSent: UILabel!
    @IBOutlet weak var txtCode1: UITextField!
    @IBOutlet weak var txtCode2: UITextField!
    @IBOutlet weak var txtCode3: UITextField!
    @IBOutlet weak var txtCode4: UITextField!
    @IBOutlet weak var txtCode5: UITextField!
    @IBOutlet weak var txtCode6: UITextField!
    @IBOutlet weak var viewVerify: UIView!{
        didSet{
            viewVerify.setCornerRadius(radius: 8)
        }
    }
    @IBOutlet weak var btnVerify: UIButton!{
        didSet{
            btnVerify.setCornerRadius(radius: 8)
        }
    }
    
    //MARK:- Variables
    var isVerificationOpen = false{
        didSet{
            handleVerificationView(show: isVerificationOpen)
        }
    }
    var cp: CountryPickerView!
    var countryCode = "+44"{
        didSet{
            btnCountryCode.setTitle(countryCode, for: .normal)
        }
    }
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        viewPhoneNumber.setCornerRadius(radius: 8)
        viewPhoneNumber.dropShadow(color: .red, opacity: 0.1, offSet: CGSize(width: 0, height: 1), radius: 10, scale: true)
    }
    
    //MARK:- IBActions
    @IBAction func backAction(_ sender: Any) {
        if isVerificationOpen{
            isVerificationOpen = false
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func countryCodeAction(_ sender: Any) {
        cp = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 120, height: 20))
        cp.dataSource = self
        cp.delegate = self
        cp.showCountriesList(from: self)
    }
    
    @IBAction func nextAction(_ sender: Any) {
        if txtPhoneNumber.text!.count == 0 {
            showAppAlertWithMessage("Please enter valid mobile number", viewController: self)
            return
        }
        else if !txtPhoneNumber.text!.isValidPhone(){
            showAppAlertWithMessage("Please enter valid mobile number", viewController: self)
            return
        }
        sentOTP()
    }
    
    @IBAction func verifyAction(_ sender: Any) {
        if txtCode1.text!.count == 0 ||
        txtCode2.text!.count == 0 ||
        txtCode3.text!.count == 0 ||
        txtCode4.text!.count == 0 ||
        txtCode5.text!.count == 0 ||
        txtCode6.text!.count == 0 {
            showAppAlertWithMessage("Please enter valid Verification code.", viewController: self)
        }
        else {
            let verificationCode = String(format: "%@%@%@%@%@%@", txtCode1.text!, txtCode2.text!, txtCode3.text!, txtCode4.text!, txtCode5.text!, txtCode6.text!)
            verifyOTP(verificationCode: verificationCode)
        }
    }
    
    //MARK:-
    func handleVerificationView(show : Bool){
        UIView.animate(withDuration: 0.3) {
            self.viewConfirmation.alpha = show ? 1 : 0
        }
    }
    
    func sentOTP(){
        SVProgressHUD.show()
        let phoneNumber = "\(countryCode)\(txtPhoneNumber.text!)"
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
            SVProgressHUD.dismiss()
            if let error = error {
                showAppAlertWithMessage(error.localizedDescription, viewController: self)
                return
            }
            self.lblCodeSent.text = "We have sent you a 6 digit confirmation code via SMS to (\(self.countryCode)) \(self.txtPhoneNumber.text!)"
            
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
            self.viewVerify.dropShadow(color: .red, opacity: 0.1, offSet: CGSize(width: 0, height: 1), radius: 10, scale: true)
            self.isVerificationOpen = true
        }
    }
    
    func verifyOTP(verificationCode : String){
        SVProgressHUD.show()
        let verificationID = UserDefaults.standard.string(forKey: "authVerificationID")
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID ?? "", verificationCode: verificationCode)
        
        Auth.auth().signIn(with: credential) { (authResult, error) in
            SVProgressHUD.dismiss()
            if let error = error {
                print(error)
                showAppAlertWithMessage(error.localizedDescription, viewController: self)
                return
            }
            else{
                NotificationCenter.default.post(name: NSNotification.Name("mobileLogin"), object: nil, userInfo: ["mobile" : self.txtPhoneNumber.text!, "code": self.countryCode])
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}

extension PhoneVerificationVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            
            if string.count > 0{//textField.text!.count < 1  &&
                let nextTag = textField.tag + 1
                let nextResponder = textField.superview?.viewWithTag(nextTag)
                
                if (nextResponder == nil){
                    self.view.endEditing(true)
                }
                textField.text = string
                nextResponder?.becomeFirstResponder()
                return false
            }
            else if textField.text!.count >= 1  && string.count == 0{
                let previousTag = textField.tag - 1
                var previousResponder = textField.superview?.viewWithTag(previousTag)
                if (previousResponder == nil){
                    previousResponder = textField.superview?.viewWithTag(1)
                }
                textField.text = ""
                previousResponder?.becomeFirstResponder()
                return false
            }
            return true
        }
        
        //MARK:- Textfield Delegate Methods
        @IBAction func textFieldEditingChanged(_ textfield : UITextField){
            textfield.text = String(textfield.text!.suffix(1))
        }
        
        func textFieldDidBeginEditing(_ textField: UITextField) {
    //        if Utility.isEmpty(textField.text){
    //            textField.text = "\u{200B}"
    //        }
        }
}

extension PhoneVerificationVC : CountryPickerViewDataSource, CountryPickerViewDelegate{
    
    func preferredCountries(in countryPickerView: CountryPickerView) -> [Country] {
        return []
    }
    
    func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView) -> String? {
        return nil
    }
    
    func showOnlyPreferredSection(in countryPickerView: CountryPickerView) -> Bool {
        return false
    }
    
    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
        return "Country Code"
    }
    
    func searchBarPosition(in countryPickerView: CountryPickerView) -> SearchBarPosition {
       return .navigationBar
    }
    
    func showPhoneCodeInList(in countryPickerView: CountryPickerView) -> Bool {
        return true
    }
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        print(country)
        countryCode = country.phoneCode
    }
}
