//
//  LoginVC.swift
//  FOODATEX
//
//  Created by Silver Shark on 07/08/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import FBSDKLoginKit
import SVProgressHUD
import AccountKit
import SDWebImage
import AuthenticationServices

class LoginVC: UIViewController, ASAuthorizationControllerDelegate {

    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var viewCol: UIView!
    @IBOutlet weak var colIntro: UICollectionView!
    @IBOutlet weak var lblIntroText: UILabel!
    @IBOutlet weak var btnFB: UIButton!
    @IBOutlet weak var btnPhone: UIButton!
    @IBOutlet weak var btnApple: UIButton!{
        didSet{
            btnApple.layer.cornerRadius = 8
        }
    }
    
    var currentIndex = 0
    var arrIntroText = [WelcomeQuestionsForList]()
    
    fileprivate var accountKit = AccountKitManager(responseType: .accessToken)
    fileprivate var pendingLoginViewController: AKFViewController? = nil
    fileprivate var showAccountOnAppear = false
    
    //MARK: - LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        getCollectionData()
        btnPhone.createBordersWithColor(color: .darkGray, radius: 8, width: 1)
        setIntroText()
        let cellsize:CGFloat = CGFloat(SCREENWIDTH()*0.7187) - 4
        let collectionViewLayout: UICollectionViewFlowLayout = (colIntro.collectionViewLayout as! UICollectionViewFlowLayout)
        collectionViewLayout.itemSize = CGSize(width: cellsize, height: cellsize)
        colIntro.reloadData()
        viewCol.createCornerRadiusWithShadowNew(cornerRadius: 12, offset: .zero, opacity: 0.2, radius: 2)
        colIntro.setCornerRadius(radius: 12)
        showAccountOnAppear = accountKit.currentAccessToken != nil
        pendingLoginViewController = accountKit.viewControllerForLoginResume()
        
        NotificationCenter.default.addObserver(self, selector: #selector(mobileVerified(notification:)), name: NSNotification.Name(rawValue: "mobileLogin"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillLayoutSubviews() {
        
        let gradient = CAGradientLayer()
        gradient.frame = btnFB.bounds
        gradient.colors = [ COLOR_CUSTOM(74, 176, 252, 1.0).cgColor, COLOR_CUSTOM(99, 202, 252, 1.0).cgColor ]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        btnFB.layer.insertSublayer(gradient, at: 0)
    }

    //MARK: - BUTTON ACTION
    @IBAction func btnLoginWithFBTapped(_ sender: Any) {
        doFBLogin()
    }
    
    @IBAction func btnPhoneLoginTapped(_ sender : Any){
        let vc = loadVC(AppStoryboards.kSBLogin, strVCId: AppViewControllers.kPhoneVerificationVC)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnLoginWithPhoneTapped(_ sender: Any) {
        
        let inputState = UUID().uuidString
        let vc = (accountKit.viewControllerForPhoneLogin(with: nil, state: inputState))
        vc.isSendToFacebookEnabled = true
        vc.delegate = self
        vc.defaultCountryCode = Locale.current.regionCode ?? "US"
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnTermsTapped(_ sender: Any) {
//        let con = self.storyboard?.instantiateViewController(withIdentifier: "TermsConditionWebViewController") as! TermsConditionWebViewController
//        con.strUrl = "http://139.59.35.213/foodatex/terms"
//        self.present(con, animated: true, completion: nil)
        
        guard let url = URL(string: "http://foodatex.com/terms-conditions.html") else {
            return
        }

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func btnPrivacyPolicyTapped(_ sender: Any) {
        //
        let con = self.storyboard?.instantiateViewController(withIdentifier: "TermsConditionWebViewController") as! TermsConditionWebViewController
        con.strUrl = "http://139.59.35.213/foodatex/privacy"
        self.present(con, animated: true, completion: nil)
        
//        guard let url = URL(string: "http://139.59.35.213/foodatex/privacy") else {
//            return
//        }
//        
//        if #available(iOS 10.0, *) {
//            UIApplication.shared.open(url, options: [:], completionHandler: nil)
//        } else {
//            UIApplication.shared.openURL(url)
//        }
    }
    
    @IBAction func btn_TermsAndPrivacyPolicyClicked(_ sender: Any) {
//        guard let url = URL(string: "http://foodatex.com/") else {
//            return 
//        }
//        
//        if #available(iOS 10.0, *) {
//            UIApplication.shared.open(url, options: [:], completionHandler: nil)
//        } else {
//            UIApplication.shared.openURL(url)
//        }
        
    }
    
    @IBAction func btnAppleLoginClick (_ sender:Any){
        if #available(iOS 13.0, *){
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            let authorizationalcontroller = ASAuthorizationController(authorizationRequests: [request])
            authorizationalcontroller.delegate = self
            authorizationalcontroller.performRequests()
        }
        else{
            let alertController = UIAlertController(title: "Login", message: "Please upgrade your ios version to ios 13 to use this functionality", preferredStyle: .alert)
            let retryAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(retryAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential{
            let useridentifire = appleIDCredential.user
            let fullname = appleIDCredential.fullName
            
            let param : [String : Any] =
                ["logintype":"facebook",
                 "social_id" :  useridentifire.replacingOccurrences(of: ".", with: ""),
                 "fullname" : "\(appleIDCredential.fullName?.givenName ?? "") \(appleIDCredential.fullName?.middleName ?? "")",
                 "email" : appleIDCredential.email ?? "",
                    "device_type" : 1,
                    "device_token" : getStringValueFromUserDefaults_ForKey(strKey: AppKeys.UD.kAPNSToken) ?? ""]
            self.callWSForFBLogin(param)

            
            print("Success Apple Login  - \(useridentifire) - name : \(appleIDCredential.fullName?.givenName) - \(appleIDCredential.fullName?.middleName) = \(appleIDCredential.fullName?.familyName)")
        }
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        let alertController = UIAlertController(title: "Login", message: error.localizedDescription, preferredStyle: .alert)
        
        let retryAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(retryAction)
        self.present(alertController, animated: true, completion: nil)

    }
    
   
}

extension LoginVC : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrIntroText.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIntro", for: indexPath)
        let cellImage = cell.viewWithTag(1001) as! RemoteImageView
        let cellObj = arrIntroText[indexPath.item]
        let loader = cell.viewWithTag(101) as! UIActivityIndicatorView
        if let cellImageURL = URL(string: cellObj.image ?? ""){
            loader.isHidden = false
            loader.startAnimating()
            cellImage.sd_setImage(with: cellImageURL, placeholderImage: UIImage(named: "Intro\(indexPath.item)"), options: .refreshCached){ (image, error, type, url) in
                loader.stopAnimating()
                loader.isHidden = true
            }
        }else{
            cellImage.image = UIImage(named: "Intro\(indexPath.item)")
        }
        return cell
    }
}

extension LoginVC : UIScrollViewDelegate{
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == colIntro {
            DispatchQueue.main.async {
                self.currentIndex = self.currentIndex == 1 ? self.colIntro.indexPathsForVisibleItems.last!.item : self.colIntro.indexPathsForVisibleItems.first!.item
                self.setIntroText()
                
                let visibleRect = CGRect(origin: self.colIntro.contentOffset, size: self.colIntro.bounds.size)
                let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
                let visibleIndexPath = self.colIntro.indexPathForItem(at: visiblePoint)
                //print("currentIndex===",visibleIndexPath?.row)
                // currentIndex = (visibleIndexPath?.row)!
                
                self.pageController.currentPage = (visibleIndexPath?.row)!
            }
           
            
        }
    }
}

extension LoginVC {
    
    func setIntroText(){
        if arrIntroText.count > 0 {
            lblIntroText.text = arrIntroText[currentIndex].descriptionField ?? ""
            pageController.currentPage = currentIndex
            }else{
            lblIntroText.text = "Swipe right to sweet and left to sour"
        }
    }
    
    
    func doFBLogin(){
        
        let loginManager = LoginManager()
        loginManager.logIn(permissions: [.publicProfile, .email], viewController: self) { (loginResult) in
            switch loginResult {
            case .failed( _):
                showAppAlertWithMessage("Unable to connect to Facebook, Please try again", viewController: self)
                break
            case .cancelled:
                print("User cancelled login.")
                break
            case .success(let grantedPermissions, _, let accessToken):
                print("Logged in! Token : \(accessToken)")
                if grantedPermissions.contains("email") &&  grantedPermissions.contains("public_profile") {
                    self.getFBProfile()
                }else{
                    SVProgressHUD.showError(withStatus: AppMessages.FB_Permission)
                }
                break
            }
        }
    }
    
    func getFBProfile(){
        
        GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, email, last_name"]).start(completionHandler: { (connection, result, error) -> Void in
            if (error == nil){
                let fbDetails = result as! NSDictionary
                print(fbDetails)
                let param : [String : Any] =
                    ["logintype":"facebook",
                     "social_id" : fbDetails["id"] as! String,
                     "fullname" : "\(fbDetails["first_name"] as? String ?? "") \(fbDetails["last_name"] as? String ?? "")",
                        "email" : fbDetails["email"] as? String ?? "",
                        "device_type" : 1,
                        "device_token" : getStringValueFromUserDefaults_ForKey(strKey: AppKeys.UD.kAPNSToken) ?? ""]
                self.callWSForFBLogin(param)
            }
        })
    }
    
    //MARK: - WS CALL FOR LOGIN
    func callWSForFBLogin(_ param : [String : Any]){
        
            if isConnectedToNetwork(){
                SVProgressHUD.show()
                WebServicesCollection.sharedInstance.SocialRegister(param) { (response, error, message)
                    in
                    SVProgressHUD.dismiss()
                    if error == nil {
                        do {
                            let loginResult  = try JSONDecoder().decode(UserLoginResult.self, from: response as! Data)
                            if let loginDATA = loginResult.data {
                                setLoginDetails(loginDATA)
                                if loginResult.profileStaus != "1"{
                                    setBooleanValueToUserDefaults(booleanValue: false, ForKey: AppKeys.UD.kProfileUpdated)
                                    let vc = loadVC(AppStoryboards.kSBLogin, strVCId: AppViewControllers.InfoPage.kVCInfoOne) as! InfoPageOneVC
                                    vc.email = param["email"] as! String
                                    let nav = UINavigationController(rootViewController: vc)
                                    nav.modalPresentationStyle = .overFullScreen
                                    nav.modalTransitionStyle = .coverVertical
                                    self.present(nav, animated: true, completion: nil)
                                }else{
                                    setBooleanValueToUserDefaults(booleanValue: true, ForKey: AppKeys.UD.kProfileUpdated)
                                    APP_DELEGATE.setHomeAsRoot()
                                }
                            }
                        }catch{
                            SVProgressHUD.showError(withStatus: AppMessages.FB_Error)
                        }
                    }else{
                        SVProgressHUD.showError(withStatus: message ?? "")
                    }
                }
            }else{
                showNoInternetAlert()
            }
    }
    
    //MARK: - WS CALL FOR LOGIN
    @objc func mobileVerified(notification : Notification){
        if let values = notification.userInfo as NSDictionary?{
            let mobile = values["mobile"] as! String
            let ccode  = values["code"] as! String
            callWSForMobileLogin(mobile, ccode: ccode)
        }
    }
    
    func callWSForMobileLogin(_ mobileNumber : String, ccode : String){
        
        if isConnectedToNetwork(){
            SVProgressHUD.show()
            
            let param : [String : Any] = ["phone": mobileNumber,
                                          "device_type" : 1,
                                          "device_token" : getStringValueFromUserDefaults_ForKey(strKey: AppKeys.UD.kAPNSToken) ?? ""]
            
            WebServicesCollection.sharedInstance.loginWithMobile(param) { (response, error, message)
                in
                SVProgressHUD.dismiss()
                if error == nil {
                    do {
                        //print("Login response = ", try JSONSerialization.jsonObject(with: response as! Data, options: []))
                        let loginResult  = try JSONDecoder().decode(UserLoginResult.self, from: response as! Data)
                        if let loginDATA = loginResult.data {
                            setLoginDetails(loginDATA)
                            
                            if loginResult.profileStaus != "1"{
                                setBooleanValueToUserDefaults(booleanValue: false, ForKey: AppKeys.UD.kProfileUpdated)
                                let vc = loadVC(AppStoryboards.kSBLogin, strVCId: AppViewControllers.InfoPage.kVCInfoOne) as! InfoPageOneVC
                                vc.isFromMobile = true
                                vc.phone = mobileNumber
                                vc.ccode = ccode.replacingOccurrences(of: "+", with: "")
                                let nav = UINavigationController(rootViewController: vc)
                                nav.modalPresentationStyle = .overFullScreen
                                nav.modalTransitionStyle = .coverVertical
                                self.present(nav, animated: true, completion: nil)
                            }else{
                                setBooleanValueToUserDefaults(booleanValue: true, ForKey: AppKeys.UD.kProfileUpdated)
                                setOnlineStatus(status: "1")
                                if  let data  = response as? Data{
                                    do {
                                        if let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary{
                                            if let image = (json["data"] as? NSDictionary)?["profile_image"] as? String{
                                                setStringValueToUserDefaults(strValue: image, ForKey: AppKeys.UD.kProfileImage)
                                            }
                                        }
                                    } catch { }
                                }
                                APP_DELEGATE.setHomeAsRoot()
                            }
                        }
                    }catch{
                        SVProgressHUD.showError(withStatus: AppMessages.FB_Error)
                    }
                }else{
                    SVProgressHUD.showError(withStatus: message ?? "")
                }
            }
        }else{
            showNoInternetAlert()
        }
    }
    
    
    func getCollectionData(){
        if isConnectedToNetwork(){
            
            WebServicesCollection.sharedInstance.WelcomeScreenList { (data, error, message) in
                
                SVProgressHUD.dismiss()
                if error == nil {
                    do {
                        let welcomeResult  = try JSONDecoder().decode(WelcomeResult.self, from: data as! Data)
                        if let welcomeDATA = welcomeResult.data {
                            if let question = welcomeDATA.questionsForList {
                                self.arrIntroText.removeAll()
                                self.arrIntroText.append(contentsOf: question)
                                self.pageController.numberOfPages = self.arrIntroText.count
                                self.colIntro.reloadData()
                                self.setIntroText()
                            }
                        }
                    }catch{
                        SVProgressHUD.showError(withStatus: AppMessages.FB_Error)
                    }
                }else{
                    SVProgressHUD.showError(withStatus: message ?? "")
                }
                
            }
            
        }
    }
}


// MARK: - AKFViewControllerDelegate extension
extension LoginVC: AKFViewControllerDelegate {
    
    func viewController(_ viewController: (UIViewController & AKFViewController), didCompleteLoginWith accessToken: AKFAccessToken, state: String) {
        
        print("Login Done")
        
        self.accountKit.requestAccount { (account, error) in
            
            if let accountID = account?.accountID {
                print("Account ID : \(accountID)")
            }
            if let email = account?.emailAddress {
                print("Account ID : \(email)")
                self.callWSForMobileLogin(email, ccode:"")
            } else if let phoneNumber = account?.phoneNumber{
                print("Account ID : \(phoneNumber.stringRepresentation())")
                self.callWSForMobileLogin(phoneNumber.stringRepresentation(), ccode: phoneNumber.countryCode)
            }
        }
    }
    
    private func viewController(_ viewController: UIViewController!, didFailWithError error: Error!) {
        print("Apple login failed : ", error)
        //showAppAlertWithMessage(error.localizedDescription , viewController: self)
    }
}
