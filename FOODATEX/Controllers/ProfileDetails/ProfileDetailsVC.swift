//
//  ProfileDetailsVC.swift
//  FOODATEX
//
//  Created by Shailesh Panchal on 10/11/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit
import Koloda
import CoreLocation
import SVProgressHUD
import SDWebImage
import AVFoundation
import AVFoundation
import AVKit
protocol HomeSwipeDelegate {
    func updateSwipeAtHome(swipeType:Int,userId:String)
}
class ProfileDetailsVC: BaseVC {
  var delegate:HomeSwipeDelegate!
    //MARK: - IBOUTLET AND PROPERTY
    @IBOutlet weak var profileTblView: UITableView!
    var userProfileObj : UserProfileData!
    var userID : String = ""
    var hideBottomButtons:Bool = false
    var userName : String = ""
    var passedDistance: String = ""
    @IBOutlet weak var imgProfilePic: RemoteImageView!
    @IBOutlet weak var lblDistance: UILabel!
    
    @IBOutlet var viewBottom: UIView!
    //HedarView
    @IBOutlet weak var viewCard: UIView!
    @IBOutlet weak var viewCardBG: UIView!
    @IBOutlet weak var tableHeader: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    
    @IBOutlet var btnCloseProfile: UIButton!
    
    @IBOutlet var viewBottomHeight: NSLayoutConstraint!
    
    
    enum ENUM_SECTIONONE : Int {
        case AGE
//        case LOCATION
//        case PHONENUMBER
        case CITY
        case HEIGHT
        case FOODPREFRENCE
        case SPLITBILL
        case LOOKINGFOR
    }
    
    enum ENUM_SECTIONTWO : Int {
        case PERSONALITY
        case PROFESSION
        case ABOUT
        case ALLERGY
    }
    
    enum ENUM_SECTIONTHREE : Int {
        case MARITALSTATUS
        case HASKIDS
        case WANTKIDSS
    }
    enum ENUM_SECTIONFOUR : Int {
        case FOOD
        case MyGENDER
        case INTRESTEDIN
        case SMOKES
        case DRINK
        case DRUG
    }
    
    var otherOptionSheet = UIAlertController()
    
    //MARK: - LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        callWSForProfile()
        tableHeader.frame = CGRect(x: 0, y: 0, width: SCREENWIDTH(), height: SCREENWIDTH()*1.3281)
        profileTblView.tableHeaderView = tableHeader
        profileTblView.isHidden = true
        btnCloseProfile.addTarget(self, action: #selector(btn_Back_clicked(_:)), for: .touchUpInside)
        prepareOptionSheet()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationbarleft_imagename("icon_Back", left_action: #selector(dismissVC), right_imagename: "icon_dots", right_action: #selector(showOtherOptions), title: userName + "'s Profile", isLogo: false)
        
        viewBottom.isHidden = hideBottomButtons
        if hideBottomButtons == true{
            viewBottomHeight.constant = 0
        }else{
          //  viewBottomHeight.constant = 88
        }
       
    }
    
    override func viewWillLayoutSubviews() {
        viewCard.setCornerRadius(radius: 20)
        viewCardBG.createRectShadowWithOffset(offset: .zero, opacity: 0.3, radius: 2)
    }
    
    func prepareOptionSheet(){
        otherOptionSheet = UIAlertController(title: APP_NAME, message: "Select option", preferredStyle: .actionSheet)
        
        otherOptionSheet.addAction(UIAlertAction(title: "Block", style: .default, handler: { (_) in
            self.blockUser()
        }))
        
        otherOptionSheet.addAction(UIAlertAction(title: "Report", style: .default, handler: { (_) in
            self.reportUser(response: { (reason) in
                self.callWSReportUser(reason: reason)
            })
        }))
        
        otherOptionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
        }))
        
    }
    
    @objc func dismissVC() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func showOtherOptions() {
        self.present(otherOptionSheet, animated: true, completion: nil)
    }
    
    //MARK:- Block User
    func blockUser(){
        let alert = UIAlertController(title: APP_NAME, message: "Are you sure?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Block", style: .default, handler: { (_) in
            self.callWSBlockUser()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func callWSBlockUser(){
        if isConnectedToNetwork(){
            SVProgressHUD.show()
            
            let params : [String : Any] = ["user_id" : self.userProfileObj.userId!, "block_by" : getLoginDetails()!.userId!]
            print(params)
            WebServicesCollection.sharedInstance.blockUser(params) { (response, error, message) in
                
                SVProgressHUD.dismiss()
                if error == nil {
                    do {
                        let userResult  = try JSONDecoder().decode(UserProfileResult.self, from: response as! Data)
                        //print(userResult)
                        showAlert(vc: self, message: userResult.message ?? "")
                    }catch{
                        SVProgressHUD.showError(withStatus: AppMessages.Error_Message)
                    }
                }else{
                    SVProgressHUD.showError(withStatus: message ?? "")
                }
            }
        }
        else{
            showNoInternetAlert()
        }
    }
    
    //MARK:- Report User
    func reportUser(response : @escaping(_ value : String) -> Void){
        let alert = UIAlertController(title: APP_NAME, message: "Enter reason to report user.", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "Reason"
            textField.addTarget(alert, action: #selector(alert.isEmpty), for: .editingChanged)
        }
        
        let actionSubmit = UIAlertAction(title: "Submit", style: .default) { [weak alert] (_) in
            let textField = alert!.textFields![0]
            response(textField.text!)
        }
        actionSubmit.isEnabled = false
        
        alert.addAction(actionSubmit)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func callWSReportUser(reason : String){
        if isConnectedToNetwork(){
            SVProgressHUD.show()
            
            let params : [String : Any] = ["user_id" : self.userProfileObj.userId!,
                                           "reported_by" : getLoginDetails()!.userId!,
                                           "reason" : reason]
            print(params)
            WebServicesCollection.sharedInstance.reportUser(params) { (response, error, message) in
                
                SVProgressHUD.dismiss()
                if error == nil {
                    do {
                        let userResult  = try JSONDecoder().decode(UserProfileResult.self, from: response as! Data)
                        
                        showAlert(vc: self, message: userResult.message ?? "")
                    }catch{
                        SVProgressHUD.showError(withStatus: AppMessages.Error_Message)
                    }
                }else{
                    SVProgressHUD.showError(withStatus: message ?? "")
                }
            }
        }
        else{
            showNoInternetAlert()
        }
    }
    
    // MARK: - Call For User Detail
    func callWSForProfile(){
        
        if isConnectedToNetwork(){
            
            SVProgressHUD.show()
            
            let latitude = UserDefaults.standard.double(forKey: "lat")
            let longitude = UserDefaults.standard.double(forKey: "lon")
            
            let dict : [String : Any] = ["user_id" : userID ?? 0,"other_user_id": getLoginDetails()?.userId,"latitude" : latitude.description, "longitude" : longitude.description]
            
            WebServicesCollection.sharedInstance.GetUserDetails(dict) { (response, error, message) in
                
                SVProgressHUD.dismiss()
                if error == nil {
                    do {
                       let userResult  = try JSONDecoder().decode(UserProfileResult.self, from: response as! Data)
                        if let userList = userResult.data {
                            self.userProfileObj = userList
                            self.profileTblView.isHidden = false
                            self.profileTblView.reloadData()
                            self.setUserValues()
                        }
                    }catch{
                        SVProgressHUD.showError(withStatus: AppMessages.User_Error)
                    }
                }else{
                    SVProgressHUD.showError(withStatus: message ?? "")
                }
            }
        }else{
            showNoInternetAlert()
        }
    }
    
    
    @IBAction func btnSweetTapped(_ sender: Any) {
       // WSCallSwipeUser(1, receiveriD: userID)
        delegate.updateSwipeAtHome(swipeType: 1,userId: userID)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSourTapped(_ sender: Any) {
       // WSCallSwipeUser(2, receiveriD: userID)
         delegate.updateSwipeAtHome(swipeType: 2,userId: userID)
         self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnSuperlikeTapped(_ sender: Any) {
       // WSCallSwipeUser(3, receiveriD: userID)
         delegate.updateSwipeAtHome(swipeType: 3,userId: userID)
         self.dismiss(animated: true, completion: nil)
    }
    
    func WSCallSwipeUser( _ swipeType : Int, receiveriD : String) {
        
        if isConnectedToNetwork(){
            
            let swipe = ["receiver_u_id": receiveriD , "swipe" : swipeType] as [String : Any]
            let param : [String : Any] = ["sender_u_id" : getLoginDetails()?.userId ?? 0,
                                          "swipes" : [swipe]]
            WebServicesCollection.sharedInstance.swipeuser(param) { (response, error, message) in
               // showAppAlertWithMessage(message ?? "", viewController: self)
               
            }
        }else{
            showNoInternetAlert()
        }
    }
    
    func createThumbnailOfVideoFromRemoteUrl(url: String, response : @escaping (_ image : UIImage?) -> Void){
        
        runInBackground {
            let asset = AVAsset(url: URL(string: url)!)
            let assetImgGenerate = AVAssetImageGenerator(asset: asset)
            assetImgGenerate.appliesPreferredTrackTransform = true
            let time = CMTimeMakeWithSeconds(0.5, 100)
            do {
                let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
                let thumbnail = UIImage(cgImage: img)
                response(thumbnail)
            } catch {
                print(error.localizedDescription)
                response(nil)
            }
        }
    }
}

//MARK: - EXTENSION

extension ProfileDetailsVC : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if userProfileObj == nil {
            return 0
        }
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return 2
        case 1:
            return 6
        case 2:
            return 4
        case 3:
            return 1
        case 4:
            return 5
        default:
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                return GetCellCollectionView(tableView: tableView, indexPath: indexPath)
            }else{
                return CellVideoView(tableView: tableView, indexPath: indexPath)
            }
        } else {
            return GetCellGeneralDetails(tableView:tableView, indexPath: indexPath)
        }
    }
    

    func GetCellCollectionView(tableView : UITableView, indexPath : IndexPath) -> UITableViewCell {
        
        let cell : TVCProfileDetailImage = tableView.dequeueReusableCell(withIdentifier: "TVCProfileDetailImage", for: indexPath) as! TVCProfileDetailImage
        cell.arrImg = userProfileObj.images ?? [UserProfileImage]()
        cell.delegate = self
        cell.LoadCollectionView()
        return cell
    }
    
    func CellVideoView(tableView : UITableView, indexPath : IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellVideoView", for: indexPath)
        let btnPlayVideo = cell.viewWithTag(1002) as! UIButton
        btnPlayVideo.isUserInteractionEnabled = (userProfileObj.video?.count ?? 0) > 0  ? true : false
        btnPlayVideo.setTitle((userProfileObj.video?.count ?? 0) > 0 ? "" : "Add Video", for: .normal)
        btnPlayVideo.setTitleColor((userProfileObj.video?.count ?? 0) > 0 ? UIColor.black : UIColor.lightGray, for: .normal)
        btnPlayVideo.addTarget(self, action: #selector(playVideoTapped(_:)), for: .touchUpInside)
        if self.userProfileObj.video != nil && self.userProfileObj.video!.count > 0 {
            self.createThumbnailOfVideoFromRemoteUrl(url: self.userProfileObj.video!) { tImage in
                if tImage != nil{
                    runOnMainThread {
                        btnPlayVideo.setImage(tImage, for: .normal)
                    }
                }
            }
        }
        
        return cell
    }
    @IBAction func btn_Back_clicked(_ sender:Any){
        dismissVC()
    }
    
    //MARK: - UPLOAD VIDEO TAPPED
    @IBAction func playVideoTapped( _ sender : UIButton){
        let videoURL = URL(string: userProfileObj.video ?? "")
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    func GetCellGeneralDetails(tableView : UITableView, indexPath : IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 1:
            return CellGeneralDetails(tableView: tableView, indexPath: indexPath)
        case 2:
            return CellKnowMore(tableView: tableView, indexPath: indexPath)
        case 3:
            return CellMaritialDetail(tableView: tableView, indexPath: indexPath)
        case 4:
            return CellIntrest(tableView: tableView, indexPath: indexPath)
        default:
            return UITableViewCell()
        }
    }
    
    
    func CellGeneralDetails(tableView : UITableView, indexPath : IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TVCProfileCell", for: indexPath) as! TVCProfileCell
        
        switch indexPath.row {
        case ENUM_SECTIONONE.AGE.rawValue:
            cell.lblTitle.text = "Age"
            cell.lblValue.text = userProfileObj.dob ?? "--"
            break
//        case ENUM_SECTIONONE.LOCATION.rawValue:
//            cell.lblTitle.text = "Location"
//            cell.lblValue.text = userProfileObj.address ?? "--"
//            break
//        case ENUM_SECTIONONE.PHONENUMBER.rawValue:
//            cell.lblTitle.text = "Telephone Number"
//            cell.lblValue.text = userProfileObj.phone ?? "--"
//            break
        case ENUM_SECTIONONE.CITY.rawValue:
            cell.lblTitle.text = "City You Live In"
            cell.lblValue.text = userProfileObj.city ?? "--"
            break
        case ENUM_SECTIONONE.HEIGHT.rawValue:
            cell.lblTitle.text = "Height"
            cell.lblValue.text = userProfileObj.height ?? "--"
            break
        case ENUM_SECTIONONE.FOODPREFRENCE.rawValue:
            cell.lblTitle.text = "Food Requirements"
            cell.lblValue.text = userProfileObj.areYouA ?? "--"
            break
        case ENUM_SECTIONONE.LOOKINGFOR.rawValue:
            cell.lblTitle.text = "Looking For"
            if userProfileObj.lookingFor ?? "" == "2"{
                 cell.lblValue.text = "Friendship"
            }else{
                cell.lblValue.text = "Romance"
            }
           
            break
        case ENUM_SECTIONONE.SPLITBILL.rawValue:
            cell.lblTitle.text = "Split Bill"
            if userProfileObj.splitBill ?? "" == "1"{
              cell.lblValue.text = "Yes"
            }else{
                cell.lblValue.text = "No"
            }
 
            break
        default:
            return UITableViewCell()
        }
        return cell
    }
    
    func CellKnowMore(tableView : UITableView, indexPath : IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TVCProfileCell", for: indexPath) as! TVCProfileCell
        switch indexPath.row {
        case ENUM_SECTIONTWO.PERSONALITY.rawValue:
            cell.lblTitle.text = "Personality"
            cell.lblValue.text = userProfileObj.personality ?? "--"
            cell.lblValue.numberOfLines = 0
            cell.lblValue.sizeToFit()
            break
        case ENUM_SECTIONTWO.PROFESSION.rawValue:
            cell.lblTitle.text = "Occupation"
            if userProfileObj.whatDoYou == "1" || userProfileObj.whatDoYou == "2" || userProfileObj.whatDoYou == "3"{
                if userProfileObj.whatDoYou == "1"{
                    cell.lblValue.text = "Working"
                }else if userProfileObj.whatDoYou == "2"{
                    cell.lblValue.text = "Studying"
                }else{
                    cell.lblValue.text = "Other"
                }
            }
            else{
                cell.lblValue.text = userProfileObj.whatDoYou ?? "Working"
            }
            break
        case ENUM_SECTIONTWO.ABOUT.rawValue:
            cell.lblTitle.text = "About"
            cell.lblValue.text = userProfileObj.aboutYourself ?? "--"
            cell.lblValue.numberOfLines = 0
            cell.lblValue.sizeToFit()
            break
        case ENUM_SECTIONTWO.ALLERGY.rawValue:
            cell.lblTitle.text = "Allergies"
            if getLoginDetails()?.anyAllergiesStatus == "1" || getLoginDetails()?.anyAllergiesStatus?.lowercased() == "yes" {
                cell.lblValue.text = userProfileObj.anyAllergies ?? ""
            }else{
                cell.lblValue.text = "No"
            }
            break
        default:
            return UITableViewCell()
        }
        return cell
    }
    
    func CellMaritialDetail(tableView : UITableView, indexPath : IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TVCProfileCell", for: indexPath) as! TVCProfileCell
        switch indexPath.row {
        case ENUM_SECTIONTHREE.MARITALSTATUS.rawValue:
            cell.lblTitle.text = "Marital Status"
            cell.lblValue.text = userProfileObj.maritalStatus ?? "--"
            break
        case ENUM_SECTIONTHREE.HASKIDS.rawValue:
            cell.lblTitle.text = "Do You Have Kids?"
            cell.lblValue.text = userProfileObj.doYouHaveKids ?? "No"
            break
        case ENUM_SECTIONTHREE.WANTKIDSS.rawValue:
            cell.lblTitle.text = "Do you want Kids"
            cell.lblValue.text = userProfileObj.doYouWantKids ?? "No"
            break
        default:
            return UITableViewCell()
        }
        return cell
    }
    
    func CellIntrest(tableView : UITableView, indexPath : IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TVCProfileCell", for: indexPath) as! TVCProfileCell
        switch indexPath.row {
        case ENUM_SECTIONFOUR.FOOD.rawValue:
            cell.lblTitle.text = "Food Preference"
            cell.lblValue.text = userProfileObj.foodPreference ?? "--"
            break
        case ENUM_SECTIONFOUR.MyGENDER.rawValue:
            cell.lblTitle.text = "Gender"
            if userProfileObj.gender ?? "1" == "1"{
                cell.lblValue.text = "Male"
            }else{
                cell.lblValue.text = "Female"
            }
            
            break
        case ENUM_SECTIONFOUR.INTRESTEDIN.rawValue:
            cell.lblTitle.text = "Interested in"
            var interestIn = "Both"
            if userProfileObj.interestIn == "2" {
                interestIn = "Female"
            }else if userProfileObj.interestIn == "1" {
                interestIn = "Male"
            }
            cell.lblValue.text = interestIn
            break
        case ENUM_SECTIONFOUR.SMOKES.rawValue:
            cell.lblTitle.text = "Do you Smoke?"
            cell.lblValue.text = "No"
            if let pr = userProfileObj.takeSmoke {
                switch pr {
                case "1":
                    cell.lblValue.text = "Yes"
                    break
                case "3":
                    cell.lblValue.text = "Occasionally"
                    break
                default:
                    cell.lblValue.text = "No"
                    break
                }
            }
            break
        case ENUM_SECTIONFOUR.DRINK.rawValue:
            cell.lblTitle.text = "Do you Drink?"
            cell.lblValue.text = "No"
            if let pr = userProfileObj.takeDrink {
                switch pr {
                case "1":
                    cell.lblValue.text = "Yes"
                    break
                case "3":
                    cell.lblValue.text = "Occasionally"
                    break
                default:
                    cell.lblValue.text = "No"
                    break
                }
            }
            break
        case ENUM_SECTIONFOUR.DRUG.rawValue:
            cell.lblTitle.text = "Do you take Drugs?"
            cell.lblValue.text = "No"
            if let pr = userProfileObj.takeDrug {
                switch pr {
                case "1":
                    cell.lblValue.text = "Yes"
                    break
                case "3":
                    cell.lblValue.text = "Occasionally"
                    break
                default:
                    cell.lblValue.text = "No"
                    break
                }
            }
            break
        default:
            return UITableViewCell()
        }
        return cell
    }
    
    
    
    func setUserValues(){
        
        //Header
        lblName.text = (userProfileObj.fullname ?? "User").trimmingCharacters(in: .whitespaces) + ", " + (userProfileObj.dob ?? "")
        lblCity.text = userProfileObj.city ?? ""
        
        if let fDistance = Double(userProfileObj.twoUserDistance?.description ?? "0"){
            lblDistance?.text =  String(format: "%.2f Km", fDistance)
        }else{
            lblDistance?.text = "--"
        }
        
        if let url : URL = URL(string: userProfileObj.profileImage ?? "") {
            imgProfilePic.image = UIImage(named: "image_Placeholder")
            imgProfilePic.sd_setImage(with: url, placeholderImage: UIImage(named: "image_Placeholder"), options: .refreshCached, completed: nil)
        }
    }
}

extension ProfileDetailsVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if indexPath.row == 1{
                return 180
            }
            if userProfileObj.images?.count == 0 {
                return 60
            }
            let width = ((SCREENWIDTH() - 50) / 4) + 5
            let line = (userProfileObj.images?.count ?? 0) % 4
            var count = (userProfileObj.images?.count ?? 0) / 4
            count = count + (line > 0 ? 1 : 0)
            return (CGFloat(count > 0 ? count : 1) * width ) + 80.0
        }
        return UITableViewAutomaticDimension
    }
}
extension ProfileDetailsVC : ProfileDetailImageDelegate{
    
    func customCollectionViewdidSelectAt(indexpath: IndexPath,strUrl:String?,passImage:UIImage?){
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ImageShowViewController") as! ImageShowViewController
        
        if passImage == nil{
            controller.passImgStrUrl = strUrl ?? ""
        }else{
            controller.passImage = passImage ?? nil
        }
        controller.modalPresentationStyle = .overCurrentContext
        self.present(controller, animated: false, completion: nil)
    }
}


extension UIAlertController {
    @objc func isEmpty(){
        var isEnable = false
        if let text = textFields?[0].text{
            isEnable = text.replacingOccurrences(of: " ", with: "").count > 0 ? true : false
        }
        else{
            isEnable = false
        }
        
        actions[0].isEnabled = isEnable
    }
}
