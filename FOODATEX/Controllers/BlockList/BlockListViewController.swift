//
//  BlockListViewController.swift
//  FOODATEX
//
//  Created by Hitesh Prajapati on 13/09/19.
//  Copyright © 2019 RudraApps. All rights reserved.
//

import UIKit
import SVProgressHUD

class BlockListViewController: BaseVC {
    
    //MARK:- IBOutlets
    @IBOutlet weak var tblBlockList: UITableView!
    
    //MARK:-  Variables
    var arrayBlockedUser = [BlockListData]()

    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        tblBlockList.tableFooterView = UIView()
        tblBlockList.register(UINib(nibName: "BlockListTableCell", bundle: nil), forCellReuseIdentifier: "BlockListTableCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationbarleft_imagename("icon_Back", left_action: #selector(navigateToPreviousView), right_imagename: "", right_action: #selector(doNothing), title: "Block List", isLogo: false)
        getBlockList()
    }
    
}

extension BlockListViewController : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayBlockedUser.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BlockListTableCell", for: indexPath) as! BlockListTableCell
        
        cell.lblName.text = arrayBlockedUser[indexPath.row].fullname
        cell.imgProfile.sd_setImage(with: URL(string: arrayBlockedUser[indexPath.row].profileImage), placeholderImage: UIImage(named: "image_Placeholder"), options: .refreshCached, completed: nil)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let action = UITableViewRowAction(style: .default, title: "Unblock") { (action, index) in
            
            let alert = UIAlertController(title: APP_NAME, message: "Are you sure?", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Unblock", style: .default, handler: { (_) in
                self.callWSUnblockUser(index: index.row)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in }))
            self.present(alert, animated: true, completion: nil)
        }
        return [action]
    }
}

extension BlockListViewController {
    func getBlockList(){
        
        if isConnectedToNetwork(){
            arrayBlockedUser.removeAll()
            
            SVProgressHUD.show()
            
            WebServicesCollection.sharedInstance.getBlockList(["user_id" : getLoginDetails()?.userId ?? ""]) { (response, error, message) in
                SVProgressHUD.dismiss()
                
                if error == nil {
                    do {
                        if let jsonDict = try JSONSerialization.jsonObject(with: response as! Data, options: []) as? NSDictionary {
                            print("Main DIct = \(jsonDict)")
                            
                            let status = jsonDict["status"] as! String
                            
                            if status == "1"{
                                if let array = (jsonDict["data"] as! NSDictionary)["block_user"] as? NSArray{
                                    for user in array{
                                        guard let objUser = BlockListData(dict: user as! [String : Any]) else {return}
                                        self.arrayBlockedUser.append(objUser)
                                    }
                                }
                            }
                            else{
                                //showAlert(vc: self, message: jsonDict["message"] as? String ?? AppMessages.Error_Message)
                            }
                        }
                    }
                    catch let error{
                        SVProgressHUD.showError(withStatus: error.localizedDescription)
                    }
                }else{
                    SVProgressHUD.showError(withStatus: error?.localizedDescription)
                }
                //reload table here
                self.tblBlockList.reloadData()
            }
            
        }else{
            showNoInternetAlert()
        }
    }
    
    func callWSUnblockUser(index : Int){
        if isConnectedToNetwork(){
            SVProgressHUD.show()
            
            let params : [String : Any] = ["user_id" : arrayBlockedUser[index].uId, "block_by" : getLoginDetails()!.userId!]
            print(params)
            WebServicesCollection.sharedInstance.unBlockUser(params) { (response, error, message) in
                
                SVProgressHUD.dismiss()
                if error == nil {
                    do {
                        let userResult  = try JSONDecoder().decode(UserProfileResult.self, from: response as! Data)
                        
                        if userResult.status == "1"{
                            self.arrayBlockedUser.remove(at: index)
                            self.tblBlockList.deleteRows(at: [IndexPath(item: index, section: 0)], with: .automatic)
                        }
                        else{
                            showAlert(vc: self, message: userResult.message ?? "")
                        }
                    }catch{
                        SVProgressHUD.showError(withStatus: AppMessages.Error_Message)
                    }
                }else{
                    SVProgressHUD.showError(withStatus: message ?? "")
                }
            }
        }
        else{
            showNoInternetAlert()
        }
    }
}
