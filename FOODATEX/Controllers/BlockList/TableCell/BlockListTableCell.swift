//
//  BlockListTableCell.swift
//  FOODATEX
//
//  Created by Hitesh Prajapati on 13/09/19.
//  Copyright © 2019 RudraApps. All rights reserved.
//

import UIKit

class BlockListTableCell: UITableViewCell {

    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
