//
//  NewChatVC.swift
//  FOODATEX
//
//  Created by Hitesh Prajapati on 30/05/20.
//  Copyright © 2020 RudraApps. All rights reserved.
//

import UIKit
import FirebaseDatabase
import SVProgressHUD
import IQKeyboardManagerSwift

class NewChatVC: BaseVC {
    
    //MARK:- IBOutlet
    @IBOutlet var txtViewHeight: NSLayoutConstraint!
    @IBOutlet var containerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var txtMessageView: UITextView!{
        didSet{
            txtMessageView.createBordersWithColor(color: COLOR_CUSTOM(159, 20, 43, 1.0), radius: txtMessageView.frame.height/2, width: 1)
            txtMessageView.delegate = self
        }
    }
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var tblChat: UITableView!{
        didSet{
            tblChat.rowHeight = UITableViewAutomaticDimension
            tblChat.estimatedRowHeight = 10
            tblChat.register(UINib(nibName: "TVChatHeaderCell", bundle: nil), forCellReuseIdentifier: "TVChatHeaderCell")
        }
    }
    @IBOutlet weak var lblMessagePlaceholder: UILabel!
    @IBOutlet weak var nslcTextViewBottom: NSLayoutConstraint!
    
    //MARK:- Variables
    var utCdateFormatter = DateFormatter()
    var chatNode = ""
    var senderID = ""
    var receiverImage = ""
    var receiverID = ""
    var receiverToken = ""
    var statusNode = ""
    
    var arrayChat = [Chat]()
    var chatDictionary = [String : [Chat]]()
    
    var userName : String = ""
    var timeAgo : String = ""
    var lastContentOffset : CGFloat = 0
    var isOffline = false
    var canMoveToBottom = true
    
    var entryCount : Int = 0
    var currentCount : Int = 0
    var loadedAllMessages = false
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.titleView = setTitle(title: userName, subtitle: "")
        
        statusNode = "user\(receiverID)"
        chatNode = getChatNodeID()
        getAllMessages()

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.enable = false
        setNavigationBar()
        enableUserStatusObserver()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.enable = true
        if self.isMovingToParentViewController{
            disableChatObserver()
            disableUserStatusObserver()
        }
    }
    
    //MARK:-
    func getChatNodeID() -> String{
        if Int(senderID)! < Int(receiverID)!{
            return "Chat_\(senderID)_\(receiverID)"
        }
        else{
            return "Chat_\(receiverID)_\(senderID)"
        }
    }
    
    //MARK:-
    func setNavigationBar(){
        let btnleft: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
        btnleft.setTitleColor(UIColor.white, for: UIControlState())
        btnleft.setImage(Set_Local_Image("icon_Back"), for: UIControlState())
        btnleft.addTarget(self, action: #selector(navigateToPreviousView), for: .touchDown)
        let backBarButon: UIBarButtonItem = UIBarButtonItem(customView: btnleft)
        self.navigationItem.setLeftBarButton(backBarButon, animated: false)
        
        let btnRight: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 8, height: 35))
        btnRight.setTitleColor(COLOR_CUSTOM(217, 23, 51, 1.0), for: UIControlState())
        btnRight.setTitle("Unmatch", for: .normal)
        btnRight.titleLabel?.font = APPFONT_REGULAR(15)
        btnRight.addTarget(self, action: #selector(btnUnmatchTapped(_:)), for: .touchDown)
        let rightBarButon: UIBarButtonItem = UIBarButtonItem(customView: btnRight)
        self.navigationItem.setRightBarButton(rightBarButon, animated: false)
    }
    
    func setTitle(title:String, subtitle:String) -> UIView {

        let titleLabel = UILabel(frame: CGRect(x: 0, y: subtitle == "" ? 7 : -2, width: 0, height: 0))
        titleLabel.font = UIFont.boldSystemFont(ofSize: 15)
        titleLabel.text = title
        titleLabel.sizeToFit()

        let subtitleLabel = UILabel(frame: CGRect(x:0, y:18, width:0, height:0))
        subtitleLabel.font = UIFont.systemFont(ofSize: 12)
        subtitleLabel.text = subtitle
        subtitleLabel.sizeToFit()


        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: max(titleLabel.frame.size.width, subtitleLabel.frame.size.width), height: 30))
        titleView.addSubview(titleLabel)
        titleView.addSubview(subtitleLabel)

        let widthDiff = subtitleLabel.frame.size.width - titleLabel.frame.size.width

        if widthDiff < 0 {
            let newX = widthDiff / 2
            subtitleLabel.frame.origin.x = abs(newX)
        } else {
            let newX = widthDiff / 2
            titleLabel.frame.origin.x = newX
        }
        
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: max(titleLabel.frame.width, subtitleLabel.frame.width), height: 35))
        btn.backgroundColor = .clear
        btn.addTarget(self, action: #selector(btn_Header_clicked), for: .touchUpInside)
        titleView.addSubview(btn)

        return titleView
    }
    
    //MARK :- Header clicked Method
    @objc func btn_Header_clicked(){
        self.view.endEditing(true)
        let profileVC = loadVC(AppStoryboards.kSBMain, strVCId:AppViewControllers.kVCProfileDetail) as! ProfileDetailsVC
        profileVC.userID = receiverID
        profileVC.userName = userName
        profileVC.hideBottomButtons = true
        self.present(UINavigationController(rootViewController: profileVC), animated: true, completion: nil)
    }
    @IBAction func btnSendChatTapped(_ sender: Any) {
        //self.view.endEditing(true)
        if txtMessageView.text.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 {
            sendMessage(message: txtMessageView.text!)
            DispatchQueue.main.async {
                self.txtViewHeight.constant = 44.0
                self.containerViewHeight.constant = 60.0
                self.txtMessageView.isScrollEnabled = false
            }
            
        }else{
            //showAppAlertWithMessage("You Must Enter Message", viewController: self)
        }
    }
    
    @IBAction func btnUnmatchTapped(_ sender: Any) {
        
        let alertUnmatch = UIAlertController(title: APP_NAME, message: "Are you sure you want to unmatch " + userName + "?", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Dismiss", style: .cancel) { (_) in
        }
        let unmatch = UIAlertAction(title: "Unmatch", style: .default) { (_) in
            self.unmatchUser()
        }
        alertUnmatch.addAction(unmatch)
        alertUnmatch.addAction(cancel)
        self.present(alertUnmatch, animated: true, completion: nil)
    }
    
    func unmatchUser(){
        
        if isConnectedToNetwork(){
            //remove my entry from receiver chat list.
            APP_DELEGATE.FIRDBReference.child(ChatListNode)
                .child("\(ChatListChildNode)\(receiverID)")
                .child("\(ChatListChildNode)\(getLoginDetails()!.userId!)").removeValue { (error, dbRef) in
                    if error != nil{
                        print("Remove my entry failed..", error!)
                    }
            }
            
            //remove receiver's entery from my chat list.
            APP_DELEGATE.FIRDBReference.child(ChatListNode)
                .child("\(ChatListChildNode)\(getLoginDetails()!.userId!)")
                .child("\(ChatListChildNode)\(receiverID)").removeValue { (error, dbRef) in
                    if error != nil{
                        print("Remove receiver's entery failed..", error!)
                    }
            }
            
            //Removing firebase chat records
            APP_DELEGATE.FIRDBReference.child(chatParentNode).child(self.chatNode).removeValue { (error, dbRef) in
                if error != nil{
                    print("Remove chat history failed.. ", error!)
                }
            }
            self.navigationController?.popViewController(animated: true)
            
            let param : [String : Any] = ["user_id" : senderID,
                                          "friend_id" : receiverID]
            WebServicesCollection.sharedInstance.Unmatch(param) { (response, error, message) in
            }
        }else{
            showNoInternetAlert()
        }
    }
    
    func reloadChatWithNewUser(){
        self.navigationItem.titleView = setTitle(title: userName, subtitle: "")
        arrayChat.removeAll()
        tblChat.reloadData()
        entryCount = 0
        currentCount = 0
        loadedAllMessages = false
        getAllMessages()
    }
}

//MARK:- Tableview Methods
extension NewChatVC : UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1//chatDictionary.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayChat.count
//        let key = ((chatDictionary as NSDictionary).allKeys as! [String])[section]
//        return chatDictionary[key]!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let key = ((chatDictionary as NSDictionary).allKeys as! [String])[indexPath.section]
        
        if arrayChat[indexPath.row].SenderID == senderID{
            return senderCell(tableView, cellForRowAt: indexPath)
        }
        else{
            return reciverCell(tableView, cellForRowAt: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
    }
    
    //MARK:- Table Cells
    func senderCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "TVCChatSenderCell", for: indexPath) as! TVCChatSenderCell
        cell.selectionStyle = .none
        //let key = ((chatDictionary as NSDictionary).allKeys as! [String])[indexPath.section]
        cell.lblMessage.text = arrayChat[indexPath.row].Message.decode()
        cell.lblDateTime.text = getDateFrom(arrayChat[indexPath.row].DateTime, givenFormat: "yyyy-MM-dd HH:mm:ss Z", returnFormat: "yyyy-MM-dd HH:mm:ss")?.components(separatedBy: ": ").last ?? arrayChat[indexPath.row].DateTime
        
        cell.viewMessage.setCornerRadius(radius: 8)
        return cell
    }
    
    func reciverCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "TVCChatReciverCell", for: indexPath) as! TVCChatReciverCell
        cell.selectionStyle = .none
        //let key = ((chatDictionary as NSDictionary).allKeys as! [String])[indexPath.section]
        
        cell.lblMessage.text = arrayChat[indexPath.row].Message
        cell.lblDateTime.text = getDateFrom(arrayChat[indexPath.row].DateTime, givenFormat: "yyyy-MM-dd HH:mm:ss Z", returnFormat: "yyyy-MM-dd HH:mm:ss")?.components(separatedBy: ": ").last ?? arrayChat[indexPath.row].DateTime
        
        cell.viewUserPic.setCornerRadius(radius: cell.viewUserPic.frame.height/2)
        cell.imgUser.setCornerRadius(radius: cell.imgUser.frame.width/2)
        cell.imgUser.sd_setImage(with: URL(string: receiverImage), placeholderImage: UIImage(named: "image_Placeholder"), options: [.refreshCached, .scaleDownLargeImages] , completed: nil)
        
        return cell
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let view = tblChat.dequeueReusableCell(withIdentifier: "TVChatHeaderCell") as! TVChatHeaderCell
//        view.lblTitle.text = ((chatDictionary as NSDictionary).allKeys as! [String])[section]
//        return view
//    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0//50
    }
}

//MARK:- Firebase
extension NewChatVC{
    
    func getAllMessages(){
        APP_DELEGATE.FIRDBReference.child(chatParentNode).child(chatNode).observeSingleEvent(of: .value) { (snapshot) in
            if let chatDict = snapshot.value as? NSDictionary{
                //print("Total count = ", chatDict.count)
                self.entryCount = chatDict.count
                self.enableChatObserver()
            }
            else{
                self.enableChatObserver()
            }
        }
    }
    
    func enableChatObserver(){
        
        APP_DELEGATE.FIRDBReference.child(chatParentNode).child(chatNode).observe(.childAdded) { (snapshot) in
            if let detail = snapshot.value as? NSDictionary{
                self.currentCount += 1
                self.arrayChat.append(Chat(dict: detail))
                if self.loadedAllMessages{
                    //print("New Message")
                    self.tblChat.reloadData()
                    self.scrollToBottom()
                }
                else if self.entryCount == 0 || self.entryCount == self.currentCount{
                    //print("Reload after receive all..")
                    self.loadedAllMessages = true
                    self.tblChat.reloadData()
                    self.scrollToBottom()
                }
            }
        }
    }
    
    func disableChatObserver(){
        self.setReadAllMessages(receiverID: self.receiverID)
        APP_DELEGATE.FIRDBReference.child(chatParentNode).child(chatNode).removeAllObservers()
    }
    
    func sendMessage(message : String){
        
        let date = "\(Date())"
        
        let detail : NSDictionary = [
            ChatKeys.message : message,
            ChatKeys.date : date,
            ChatKeys.senderId : "\(senderID)",
            ChatKeys.receiverId : "\(receiverID)",
        ]
        
        APP_DELEGATE.FIRDBReference.child(chatParentNode).child(chatNode).childByAutoId().setValue(Chat(dict: detail).toAny()) { (error, dbRef) in
            if error != nil{
                print("Failed to send message ", error!)
                //AppNotification.showErrorMessage(error!.localizedDescription)
            }else{
                self.txtMessageView.text = ""
                self.scrollToBottom()
                
                //update receiver chat list entry.
                APP_DELEGATE.FIRDBReference.child(ChatListNode)
                    .child("\(ChatListChildNode)\(self.receiverID)")
                    .child("\(ChatListChildNode)\(getLoginDetails()!.userId!)")
                    .observeSingleEvent(of: .value, with: { (snapshot) in
                        if let dict = snapshot.value as? NSDictionary{
                            dict.setValue(date, forKey: ChatListKeys.dateTime)
                            dict.setValue(message, forKey: ChatListKeys.message)
                            
                            if let image = getLoginDetails()?.profileImage as? String, image != ""{
                                dict.setValue(image, forKey: ChatListKeys.profileImage)
                            }
                            else if let image = getStringValueFromUserDefaults_ForKey(strKey: AppKeys.UD.kProfileImage), image != ""{
                                dict.setValue(image, forKey: ChatListKeys.profileImage)
                            }
                            if let unreadCount = dict[ChatListKeys.unreadCount] as? Int{
                                dict.setValue( unreadCount + 1, forKey: ChatListKeys.unreadCount)
                            }
                            if let token = getStringValueFromUserDefaults_ForKey(strKey: AppKeys.UD.kAPNSToken), token != ""{
                                dict.setValue(token, forKey: ChatListKeys.deviceToken)
                            }
                            snapshot.ref.updateChildValues(dict as! [AnyHashable : Any])
                        }
                    })
                
                //update my chat list entry.
                APP_DELEGATE.FIRDBReference.child(ChatListNode)
                    .child("\(ChatListChildNode)\(getLoginDetails()!.userId!)")
                    .child("\(ChatListChildNode)\(self.receiverID)")
                    .observeSingleEvent(of: .value, with: { (snapshot) in
                        if let dict = snapshot.value as? NSDictionary{
                            dict.setValue(date, forKey: ChatListKeys.dateTime)
                            dict.setValue(message, forKey: ChatListKeys.message)
                            if self.receiverToken != ""{
                                dict.setValue(self.receiverToken, forKey: ChatListKeys.deviceToken)
                            }
                            snapshot.ref.updateChildValues(dict as! [AnyHashable : Any])
                        }
                    })
                
                self.sendPush(message: message)
            }
        }
    }
    
    func sendPush(message : String){
        if self.isOffline && self.receiverToken != ""{
            let data : NSDictionary = [
                "click_action" : pushClickAction.chat.rawValue,
                "sender_id" : senderID,
                "sender_name" : getLoginDetails()?.fullname ?? getLoginDetails()?.username ?? "",
                "sender_image" : getStringValueFromUserDefaults_ForKey(strKey: AppKeys.UD.kProfileImage) ?? "",
                "deviceToken" : getStringValueFromUserDefaults_ForKey(strKey: AppKeys.UD.kAPNSToken) ?? "" ]
            PushNotificationSender.sendPushNotification(to: self.receiverToken, title: "\(getLoginDetails()?.fullname ?? "Your match") send you a message!", body: message, data: data)
        }
    }
    
    func setReadAllMessages(receiverID : String){
        APP_DELEGATE.FIRDBReference.child(ChatListNode)
        .child("\(ChatListChildNode)\(getLoginDetails()!.userId!)")
        .child("\(ChatListChildNode)\(receiverID)")
        .observeSingleEvent(of: .value, with: { (snapshot) in
            if let dict = snapshot.value as? NSDictionary{
                dict.setValue(0, forKey: ChatListKeys.unreadCount)
                snapshot.ref.updateChildValues(dict as! [AnyHashable : Any])
            }
        })
    }
    
    //MARK:-
    func enableUserStatusObserver(){
        APP_DELEGATE.FIRDBReference.child(statusParentNode).child(statusNode).observe(.value) { (snapshot) in
            
            if let dict = snapshot.value as? NSDictionary{
                if let status = dict["isOnline"] as? String{
                    if status == "1"{
                        self.isOffline = false
                        self.navigationItem.titleView = self.setTitle(title: self.userName, subtitle: "Online")
                    }
                    else if let strDate = dict["date"] as? String, let date = getDateFrom(strDate, givenFormat: "yyyy-MM-dd HH:mm:ss Z", returnFormat: "dd MMM yyyy hh:mm a"){
                        self.navigationItem.titleView = self.setTitle(title: self.userName, subtitle: date)
                        self.isOffline = true
                    }
                    else{
                        self.isOffline = true
                        self.navigationItem.titleView = self.setTitle(title: self.userName, subtitle: "Offline")
                    }
                }
            }
        }
    }
    
    func disableUserStatusObserver(){
        APP_DELEGATE.FIRDBReference.child(statusParentNode).child(statusNode).removeAllObservers()
    }
}

//MARK:- TextViewDelegate
extension NewChatVC : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        lblMessagePlaceholder.isHidden = true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text!.count > 0 {
            lblMessagePlaceholder.isHidden = true
        }else{
            lblMessagePlaceholder.isHidden = false
        }
        if txtMessageView.text.isEmpty{
            txtViewHeight.constant = 44.0
            containerViewHeight.constant = 60.0
            txtMessageView.isScrollEnabled = false
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if txtMessageView.isScrollEnabled == false{
            if txtMessageView.frame.size.height > 80.0{
                txtMessageView.isScrollEnabled = true
                txtViewHeight.constant = txtMessageView.frame.size.height
                containerViewHeight.constant = txtMessageView.frame.size.height + 16
            }else{
                txtMessageView.isScrollEnabled = false
            }
        }
    }
}

//MARK:- Keyboard Methods
extension NewChatVC {
    @objc func keyboardWillShow(notification: Notification) {
        print("Keyabord show")
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.nslcTextViewBottom.constant == 0{
                DispatchQueue.main.async {
                    self.nslcTextViewBottom.constant -= keyboardSize.height
                    Timer.scheduledTimer(withTimeInterval: 0.045, repeats: false) { (_) in
                        self.scrollToBottom()
                    }
                }
            }
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        self.nslcTextViewBottom.constant = 0
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if scrollView == tblChat{
            lastContentOffset = tblChat.contentOffset.y
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == tblChat{
            if scrollView.contentOffset.y < lastContentOffset{
                canMoveToBottom = false
            }
            else{
                let height = scrollView.frame.size.height
                let contentYoffset = scrollView.contentOffset.y
                let distanceFromBottom = scrollView.contentSize.height - contentYoffset
                if distanceFromBottom < height {
                    canMoveToBottom = true
                }
            }
        }
    }
    
    func scrollToBottom(){
        if !self.canMoveToBottom{
            return
        }
        if self.arrayChat.count > 0{
            DispatchQueue.main.async {
                let indexPath = IndexPath(
                    row: self.tblChat.numberOfRows(inSection:  self.tblChat.numberOfSections - 1) - 1,
                    section: self.tblChat.numberOfSections - 1)
                self.tblChat.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }
}
