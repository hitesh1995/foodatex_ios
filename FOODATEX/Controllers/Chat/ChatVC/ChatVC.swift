//
//  ChatVC.swift
//  FOODATEX
//
//  Created by Uffizio iMac on 05/11/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage
import IQKeyboardManagerSwift

class ChatVC: BaseVC {
    
    @IBOutlet var txtViewHeight: NSLayoutConstraint!
    
    @IBOutlet var containerViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var txtMessageView: UITextView!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var tblChat: UITableView!
    @IBOutlet weak var lblMessagePlaceholder: UILabel!
    @IBOutlet weak var nslcTextViewBottom: NSLayoutConstraint!
    
    var chatArray = NSMutableArray()
    var userId : String?
    var userName : String = ""
    var timeAgo : String = ""
    var utCdateFormatter = DateFormatter()
    var timer : Timer?
    var lastContentOffset : CGFloat = 0
    var canMoveToBottom = true
    var isFirstLoad = true
    
    //MARK: - LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        tblChat.rowHeight = UITableViewAutomaticDimension
        tblChat.estimatedRowHeight = 10
        txtMessageView.createBordersWithColor(color: COLOR_CUSTOM(159, 20, 43, 1.0), radius: txtMessageView.frame.height/2, width: 1)
        txtMessageView.delegate = self
        tblChat.register(UINib(nibName: "TVChatHeaderCell", bundle: nil), forCellReuseIdentifier: "TVChatHeaderCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.enable = false
        setNavigationBar()
        self.getChats()
        timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true, block: { (_) in
            self.getChats()
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.enable = true
        if timer?.isValid ?? false{
            timer?.invalidate()
        }
    }
    
    //MARK:-
    func setNavigationBar(){
        
        self.navigationController!.navigationBar.barTintColor = .white
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = nil
        self.navigationItem.titleView = UIView()
        let lblLine : UILabel = UILabel(frame: CGRect(x: 0,y: 0,width: SCREENWIDTH(),height: 0.2))
        lblLine.backgroundColor = UIColor.lightGray
        //self.navigationController?.navigationBar.shadowImage = UIColor.white.PixelOneImageImage()
        self.navigationController?.navigationBar.isTranslucent = false
        setNavShadow()
        
        let btnleft: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
        btnleft.setTitleColor(UIColor.white, for: UIControlState())
        btnleft.setImage(Set_Local_Image("icon_Back"), for: UIControlState())
        btnleft.addTarget(self, action: #selector(navigateToPreviousView), for: .touchDown)
        let backBarButon: UIBarButtonItem = UIBarButtonItem(customView: btnleft)
        self.navigationItem.setLeftBarButton(backBarButon, animated: false)
        
        
        let btnRight: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 8, height: 35))
        btnRight.setTitleColor(COLOR_CUSTOM(217, 23, 51, 1.0), for: UIControlState())
        btnRight.setTitle("Unmatch", for: .normal)
        btnRight.titleLabel?.font = APPFONT_REGULAR(15)
        btnRight.addTarget(self, action: #selector(btnUnmatchTapped(_:)), for: .touchDown)
        let rightBarButon: UIBarButtonItem = UIBarButtonItem(customView: btnRight)
        self.navigationItem.setRightBarButton(rightBarButon, animated: false)
        
        let lblValues: UILabel = UILabel()
        lblValues.text = title
        lblValues.clipsToBounds = false
        lblValues.backgroundColor = .clear
        lblValues.textColor = .black
        lblValues.font = APPFONT_BOLD(15)
        lblValues.sizeToFit()
        lblValues.tag = 1212
        self.navigationItem.setTitle(title: userName, subtitle: timeAgo)
        addButtonOnHeader(title: userName, subtitle: timeAgo)
        
    }
    func addButtonOnHeader(title:String,subtitle:String){
        let one = UILabel()
        one.text = title
        one.font = UIFont.systemFont(ofSize: 17)
        one.sizeToFit()
        
        let two = UILabel()
        two.text = subtitle
        two.font = UIFont.systemFont(ofSize: 12)
        two.textAlignment = .center
        two.sizeToFit()
        
        let width = max(one.frame.size.width, two.frame.size.width)
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: width, height: 35))
        btn.backgroundColor = .clear
        btn.addTarget(self, action: #selector(btn_Header_clicked), for: .touchUpInside)
        self.navigationItem.titleView?.addSubview(btn)
    }
    
    //MARK :- Header clicked Method
    @objc func btn_Header_clicked(){
        self.view.endEditing(true)
        let profileVC = loadVC(AppStoryboards.kSBMain, strVCId:AppViewControllers.kVCProfileDetail) as! ProfileDetailsVC
        profileVC.userID = userId ?? ""
        profileVC.userName = userName
        profileVC.hideBottomButtons = true
        self.present(UINavigationController(rootViewController: profileVC), animated: true, completion: nil)
    }
    @IBAction func btnSendChatTapped(_ sender: Any) {
        self.view.endEditing(true)
        if txtMessageView.text.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 {
            sendMessage()
            DispatchQueue.main.async {
                self.txtViewHeight.constant = 44.0
                self.containerViewHeight.constant = 60.0
                self.txtMessageView.isScrollEnabled = false
            }
            
        }else{
            //showAppAlertWithMessage("You Must Enter Message", viewController: self)
        }
    }
    
    @IBAction func btnUnmatchTapped(_ sender: Any) {
        
        let alertUnmatch = UIAlertController(title: APP_NAME, message: "Are you sure you want to unmatch " + userName + "?", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Dismiss", style: .cancel) { (_) in
        }
        let unmatch = UIAlertAction(title: "Unmatch", style: .default) { (_) in
            self.unmatchUser()
        }
        alertUnmatch.addAction(unmatch)
        alertUnmatch.addAction(cancel)
        self.present(alertUnmatch, animated: true, completion: nil)
    }
    
    func unmatchUser(){
        
        if isConnectedToNetwork(){
            let param : [String : Any] = ["user_id" : getLoginDetails()?.userId ?? "",
                                          "friend_id" : userId ?? "0"]
            WebServicesCollection.sharedInstance.Unmatch(param) { (response, error, message) in
                if error == nil {
                    showAppAlertWithMessage(message ?? "", viewController: self)
                    if message!.contains("successfully"){
                        self.navigationController?.popViewController(animated: true)
                    }
                }else{
                    SVProgressHUD.showError(withStatus: message ?? "")
                }
            }
        }else{
            showNoInternetAlert()
        }
    }
    
    func reloadChatWithNewUser(){
        setNavigationBar()
        chatArray.removeAllObjects()
        tblChat.reloadData()
        getChats()
    }
    
    //MARK:- Keyboard Methods
    @objc func keyboardWillShow(notification: Notification) {
        print("Keyabord show")
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.nslcTextViewBottom.constant == 0{
                DispatchQueue.main.async {
                    if self.canMoveToBottom && self.tblChat.numberOfSections > 0{
                        let indexPath = IndexPath(
                        row: self.tblChat.numberOfRows(inSection:  self.tblChat.numberOfSections - 1) - 1,
                        section: self.tblChat.numberOfSections - 1)
                        self.tblChat.scrollToRow(at: indexPath, at: .bottom, animated: false)
                    }
                    self.nslcTextViewBottom.constant -= keyboardSize.height
                }
            }
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        print("Keyabord hide")
        self.nslcTextViewBottom.constant = 0
    }
}

extension ChatVC {
    
    //MARK: - GET CHATS
    func getChats(){
        
        if isConnectedToNetwork() {
            if isFirstLoad{
                SVProgressHUD.show()
                isFirstLoad = false
            }
            
            let param : [String : Any] = ["sender_u_id" : getLoginDetails()?.userId ?? 0,
                                          "recevier_u_id" : userId ?? "0",
                                          "uch_id" : "0"]
            WebServicesCollection.sharedInstance.getChat(param) { (response, error, message) in
                
                SVProgressHUD.dismiss()
                if error == nil {
                    do {
                        let chatResult  = try JSONDecoder().decode(ChatResult.self, from: response as! Data)
                        if let data = chatResult.data{
                            self.chatArray.removeAllObjects()
                            self.chatArray = self.arrangeSection(data)
                        }
                        
                        self.tblChat.reloadData()
                        if self.chatArray.count > 0 {
                            DispatchQueue.main.async {
                                if self.canMoveToBottom{
                                    let indexPath = IndexPath(
                                        row: self.tblChat.numberOfRows(inSection:  self.tblChat.numberOfSections - 1) - 1,
                                        section: self.tblChat.numberOfSections - 1)
                                    self.tblChat.scrollToRow(at: indexPath, at: .bottom, animated: false)
                                }
                            }
                        }
                    }catch{
                        SVProgressHUD.showError(withStatus: AppMessages.User_Error)
                    }
                }else{
                    SVProgressHUD.showError(withStatus: message ?? "")
                }
            }
        }else{
            showNoInternetAlert()
        }
        
    }
    
    //MARK: - SEND MESSAGE
    func sendMessage(){
        
        if isConnectedToNetwork(){
            
            SVProgressHUD.show()
            
            let param : [String : Any] = ["sender_u_id" : getLoginDetails()?.userId ?? 0,
                                          "recevier_u_id" : userId ?? "0",
                                          "message" : txtMessageView.text ?? "",
                                          "image" : ""]
            
            WebServicesCollection.sharedInstance.sendMessage(param) { (response, error, message) in
                SVProgressHUD.dismiss()
                if error == nil {
                    self.txtMessageView.text = ""
                    self.getChats()
//                    do {
//                        let chatResult  = try JSONDecoder().decode(ChatResult.self, from: response as! Data)
//                        if let chatList = chatResult.data {
//                            self.chatArray.removeAll()
//                            self.chatArray.append(contentsOf: chatList)
//                            self.tblChat.reloadData()
//                        }
//                    }catch{
//                        SVProgressHUD.showError(withStatus: AppMessages.User_Error)
//                    }
                }else{
                    SVProgressHUD.showError(withStatus: message ?? "")
                }
            }
            
        }else{
            showNoInternetAlert()
        }
        
    }
    
    func arrangeSection(_ source: [ChatData]) -> NSMutableArray {
        let arrayMain = NSMutableArray()
        
        for i in 0..<source.count {
            let msg = source[i]
            
            var date = ""
            var time = ""
            
            if let value = msg.createdOn {
                date = "\(value)".components(separatedBy: " ")[0]
                time = "\(value)".components(separatedBy: " ")[1]
            }
            
            let my = getLocalTime(fromWS: date, str: time)
            let msgdate = my["date"] as! String
            
            let secDict = NSMutableDictionary()
            let secArray = NSMutableArray()
            
            if i == 0 {
                
                secDict["Date"] = msgdate
                secArray.add(msg)
                secDict["Data"] = secArray
                arrayMain.add(secDict)
                
            } else {
                
                var flg = false
                var sDict = NSMutableDictionary()
                
                for j in 0..<arrayMain.count {
                    
                    sDict = (arrayMain[j] as! NSDictionary).mutableCopy() as! NSMutableDictionary
                    
                    if (sDict["Date"] as! String == msgdate) {
                        flg = true
                        let array = (sDict["Data"] as! NSArray).mutableCopy() as! NSMutableArray
                        array.add(msg)
                        sDict.setValue(array, forKey: "Data")
                        arrayMain.replaceObject(at: j, with: sDict)
                    }
                }
                
                
                if !flg {
                    secDict["Date"] = msgdate
                    secArray.add(msg)
                    secDict["Data"] = secArray
                    arrayMain.add(secDict)
                }
            }
        }
        return arrayMain
    }
    
}

extension ChatVC : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return chatArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ((chatArray[section] as! NSDictionary)["Data"] as! NSArray).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let obj = ((chatArray[indexPath.section] as! NSDictionary)["Data"] as! NSArray)[indexPath.row] as! ChatData
        
        if obj.senderUId == getLoginDetails()?.userId {
            return senderCell(tableView,cellForRowAt: indexPath)
        }else{
            return reciverCell(tableView,cellForRowAt: indexPath)
        }
    }
    
    
    
    func senderCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "TVCChatSenderCell", for: indexPath) as! TVCChatSenderCell
        
        let obj = ((chatArray[indexPath.section] as! NSDictionary)["Data"] as! NSArray)[indexPath.row] as! ChatData
        
        cell.lblMessage.text = obj.message?.decode() ?? ""
        cell.lblDateTime.text = obj.createdOn
        
        cell.viewMessage.setCornerRadius(radius: 8)
        return cell
    }
    
    func reciverCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  tableView.dequeueReusableCell(withIdentifier: "TVCChatReciverCell", for: indexPath) as! TVCChatReciverCell
        let obj = ((chatArray[indexPath.section] as! NSDictionary)["Data"] as! NSArray)[indexPath.row] as! ChatData
        
        cell.lblMessage.text = obj.message ?? ""
        cell.lblDateTime.text = obj.createdOn
        
        cell.viewUserPic.setCornerRadius(radius: cell.viewUserPic.frame.height/2)
        cell.imgUser.setCornerRadius(radius: cell.imgUser.frame.width/2)
        
        if let userPicUrl = URL(string: obj.profileImage ?? "") {
            cell.imgUser.sd_setImage(with: userPicUrl, placeholderImage: UIImage(named: "image_Placeholder"), options: [.refreshCached, .retryFailed]) { (image, error, cType, rurl) in
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tblChat.dequeueReusableCell(withIdentifier: "TVChatHeaderCell") as! TVChatHeaderCell
        view.lblTitle.text = getDateFrom((chatArray[section] as! NSDictionary)["Date"] as? String ?? "", givenFormat: "yyyy-MM-dd", returnFormat: "dd-MMM-yyyy")
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if scrollView == tblChat{
            lastContentOffset = tblChat.contentOffset.y
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == tblChat{            
            if scrollView.contentOffset.y < lastContentOffset{
                canMoveToBottom = false
            }
            else{
                let height = scrollView.frame.size.height
                let contentYoffset = scrollView.contentOffset.y
                let distanceFromBottom = scrollView.contentSize.height - contentYoffset
                if distanceFromBottom < height {
                    canMoveToBottom = true
                }
            }
        }
    }
  
}

extension UINavigationItem {
    
    
    func setTitle(title:String, subtitle:String) {
        
        let one = UILabel()
        one.text = title
        one.font = UIFont.systemFont(ofSize: 17)
        one.textAlignment = .center
        one.sizeToFit()
        
        let two = UILabel()
        two.text = subtitle
        two.font = UIFont.systemFont(ofSize: 12)
        two.textAlignment = .center
        two.sizeToFit()
        
        let stackView = UIStackView(arrangedSubviews: [one, two])
        stackView.distribution = .equalCentering
        stackView.axis = .vertical
        
        let width = max(one.frame.size.width, two.frame.size.width)
        stackView.frame = CGRect(x: 0, y: 0, width: width, height: 35)
        one.sizeToFit()
        two.sizeToFit()
        self.titleView = stackView
       
    }
}

extension ChatVC : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        lblMessagePlaceholder.isHidden = true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        print("End editing..")
        if textView.text!.count > 0 {
            lblMessagePlaceholder.isHidden = true
        }else{
            lblMessagePlaceholder.isHidden = false
        }
        if txtMessageView.text.isEmpty{
            txtViewHeight.constant = 44.0
            containerViewHeight.constant = 60.0
            txtMessageView.isScrollEnabled = false
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if txtMessageView.isScrollEnabled == false{
            if txtMessageView.frame.size.height > 80.0{
                txtMessageView.isScrollEnabled = true
                txtViewHeight.constant = txtMessageView.frame.size.height
                containerViewHeight.constant = txtMessageView.frame.size.height + 16
            }else{
                txtMessageView.isScrollEnabled = false
            }
        }
    }
}

extension ChatVC{
    
    func getUTCDate(from strDate: String?, format Strformat: String?) -> Date? {
        self.utCdateFormatter.dateFormat = Strformat
        let date = utCdateFormatter.date(from: strDate ?? "")
        return date
    }
    
    func getLocalTime(fromWS strUTCDate: String?, str strUTCStartTime: String?) -> NSDictionary {
        var UTCdate: Date?
        
        UTCdate = self.getUTCDate(from: "\(strUTCDate ?? "") \(strUTCStartTime ?? "")", format: "yyyy-MM-dd HH:mm:ss")
        
        let strReturnDate = self.getStringFrom(UTCdate, format: "yyyy-MM-dd") ?? ""
        let strReturnTime = self.getStringFrom(UTCdate, format: "HH:mm:ss") ?? ""
        
        let dic = [
            "date": strReturnDate,
            "time": strReturnTime
        ]
        
        return dic as NSDictionary
    }
    
    func getStringFrom(_ dateval: Date?, format Strformat: String?) -> String? {
        self.utCdateFormatter.dateFormat = Strformat
        var dateString: String? = nil
        if let dateval = dateval {
            dateString = self.utCdateFormatter.string(from: dateval)
        }
        return dateString
    }
    
    func getDateFrom(_ str: String?, givenFormat strGivenFormat: String?, returnFormat strReturnFormat: String?) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = strGivenFormat ?? "" // here give the format which you get in TimeStart
        let date: Date? = dateFormatter.date(from: str ?? "")
        let strDate = self.getStringFrom(date, format: strReturnFormat)
        
        guard let todayDate = dateFormatter.date(from: str!)
            else { return strDate }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        
        return String(format: "%@ : %@", getDay(weekDay: weekDay), strDate!)
    }
    
    func getDay(weekDay : Int) -> String{
        switch weekDay {
        case 1:
            return "Sun"
        case 2:
            return "Mon"
        case 3:
            return "Tue"
        case 4:
            return "Wed"
        case 5:
            return "Thu"
        case 6:
            return "Fri"
        case 7:
            return "Sat"
        default:
            return ""
        }
    }
    
}

extension String{
    func encode() -> String{
        return self.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""
    }
    
    func decode() -> String{
        return self.removingPercentEncoding ?? ""
    }
}
