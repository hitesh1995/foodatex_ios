//
//  ChatListVC.swift
//  FOODATEX
//
//  Created by Silver Shark on 02/11/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage

class ChatListVC: BaseVC {

    var arrChatList : [ChatListData] = [ChatListData]()
    
    @IBOutlet weak var tblChatList: UITableView!
    @IBOutlet weak var lblNoData: UILabel!
    var isPushed =  false
    
    var userId : String?
    var userName : String = ""
    var timeAgo : String = ""
    
    //MARK: - LIFE CYCLE
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        if isPushed {
            let chat = loadVC(AppStoryboards.kSBChat, strVCId: AppViewControllers.Chat.kVCChat) as! ChatVC
            chat.userId = userId
            chat.userName = userName
            chat.timeAgo = timeAgo
            self.navigationController?.pushViewController(chat, animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNavigationbarleft_imagename("icon_Back", left_action: #selector(navigateToPreviousView), right_imagename: "", right_action: #selector(doNothing), title: "Chats", isLogo: false)
        getChatList()
    }
}

extension ChatListVC {
    
    //MARK: - GET CHAT LIST
    func getChatList(){
        
        if isConnectedToNetwork(){
            
            SVProgressHUD.show()
            
            lblNoData.isHidden = true
            
            WebServicesCollection.sharedInstance.getChatList(["user_id" : getLoginDetails()?.userId ?? ""]) { (response, error, message) in
                print(response ?? "")
                SVProgressHUD.dismiss()
                self.arrChatList.removeAll()
                if error == nil {
                    do {
                        let chatResult  = try JSONDecoder().decode(ChatListResult.self, from: response as! Data)
                        if let chatList = chatResult.data {
                            self.arrChatList.append(contentsOf: chatList)
                            self.lblNoData.isHidden =  self.arrChatList.count > 0 ? true : false
                        }else{
                            self.lblNoData.isHidden = false
                        }
                    }catch{
                        self.lblNoData.isHidden = false
                        SVProgressHUD.showError(withStatus: AppMessages.User_Error)
                    }
                }else{
                    self.lblNoData.isHidden = false
                    SVProgressHUD.showError(withStatus: message ?? "")
                }
                self.tblChatList.reloadData()
            }
            
        }else{
            showNoInternetAlert()
            lblNoData.isHidden = false
        }
    }
}

extension ChatListVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrChatList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TVCChatList", for: indexPath) as! TVCChatList
        cell.viewImage.createCornerRadiusWithShadowNew(cornerRadius: cell.viewImage.frame.width/2, offset: .zero, opacity: 0.7, radius: 3)
        cell.lblName.text = arrChatList[indexPath.row].fullname ?? ""
        cell.lblMessage.text = arrChatList[indexPath.row].message ?? ""
        cell.imgUser.setCornerRadius(radius: cell.imgUser.frame.width/2)
        let strImageUrl = arrChatList[indexPath.row].profileImage ?? ""
        if strImageUrl == ""{
            cell.imgUser.image = UIImage(named: "image_Placeholder")
        }
        else if let url = URL(string: arrChatList[indexPath.row].profileImage ?? "") {
            cell.imgUser.sd_setImage(with: url, placeholderImage: UIImage(named: "image_Placeholder"), options: .refreshCached) { (image, error, cType, rurl) in
            }
        }
        return cell
    }
    
}

extension ChatListVC : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let senderID = getLoginDetails()?.userId, let receiverID = arrChatList[indexPath.row].uId{
            let chat = loadVC(AppStoryboards.kSBChat, strVCId: AppViewControllers.Chat.kVCChat) as! NewChatVC
            chat.senderID = senderID
            chat.receiverID = receiverID
            chat.receiverImage = arrChatList[indexPath.row].profileImage ?? ""
            chat.userName = arrChatList[indexPath.row].fullname ?? ""
            self.navigationController?.pushViewController(chat, animated: true)
        }
        
    }
}
