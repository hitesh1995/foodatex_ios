//
//  NewChatListVC.swift
//  FOODATEX
//
//  Created by Hitesh Prajapati on 06/06/20.
//  Copyright © 2020 RudraApps. All rights reserved.
//

import UIKit

class NewChatListVC: BaseVC {

    //MARK:- IBOutlets
    @IBOutlet weak var tblChatList: UITableView!
    @IBOutlet weak var lblNoData: UILabel!
    
    //MARK:- Variables
    var isPushed =  false
    var arrayChatList = [ChatList]()
    
    var receiverID : String!
    var receiverName : String = ""
    var receiverImage : String = ""
    var receiverToken : String = ""
    
    let chatsNode = String(format : "%@%@", ChatListChildNode, getLoginDetails()!.userId!)
    var entryCount : Int = 0
    var currentCount : Int = 0
    var loadedAllChats = false
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getAllChats()
        
        if isPushed, let senderID = getLoginDetails()?.userId{
            let chat = loadVC(AppStoryboards.kSBChat, strVCId: AppViewControllers.Chat.kVCChat) as! NewChatVC
            chat.senderID = senderID
            chat.receiverID = receiverID
            chat.receiverImage = receiverImage
            chat.receiverToken = receiverToken
            chat.userName = receiverName
            self.navigationController?.pushViewController(chat, animated: true)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        setNavigationbarleft_imagename("icon_Back", left_action: #selector(navigateToPreviousView), right_imagename: "", right_action: #selector(doNothing), title: "Chats", isLogo: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.isMovingFromParentViewController{
            self.disableChatListObserver()
        }
    }
}

//MARK:- TableView Methods
extension NewChatListVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayChatList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TVCChatList", for: indexPath) as! TVCChatList
        
        let strImageUrl = arrayChatList[indexPath.row].ProfileImage
        if strImageUrl == ""{
            cell.imgUser.image = UIImage(named: "image_Placeholder")
        }
        else{
            cell.imgUser.sd_setImage(with: URL(string: strImageUrl), placeholderImage: UIImage(named: "image_Placeholder"), options: .refreshCached) { (image, error, cType, rurl) in
            }
        }
        cell.lblDateTime.text = getDateFrom(arrayChatList[indexPath.row].DateTime, givenFormat: "yyyy-MM-dd HH:mm:ss Z", returnFormat: "yyyy-MM-dd hh:mm a")?.components(separatedBy: ": ").last ?? arrayChatList[indexPath.row].DateTime
        cell.lblName.text = arrayChatList[indexPath.row].ReceiverName
        cell.lblMessage.text = arrayChatList[indexPath.row].Message
        
        cell.lblUnreadCount.isHidden = arrayChatList[indexPath.row].UnreadCount > 0 ? false : true
        cell.lblUnreadCount.text = "  \(arrayChatList[indexPath.row].UnreadCount)  "
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let senderID = getLoginDetails()?.userId{
            setReadAllMessages(receiverID: arrayChatList[indexPath.row].ReceiverID)
            
            let chat = loadVC(AppStoryboards.kSBChat, strVCId: AppViewControllers.Chat.kVCChat) as! NewChatVC
            chat.senderID = senderID
            chat.receiverID = arrayChatList[indexPath.row].ReceiverID
            chat.receiverImage = arrayChatList[indexPath.row].ProfileImage
            chat.userName = arrayChatList[indexPath.row].ReceiverName
            chat.receiverToken = arrayChatList[indexPath.row].DeviceToken
            self.navigationController?.pushViewController(chat, animated: true)
        }
    }
    
    func setReadAllMessages(receiverID : String){
        APP_DELEGATE.FIRDBReference.child(ChatListNode)
        .child("\(ChatListChildNode)\(getLoginDetails()!.userId!)")
        .child("\(ChatListChildNode)\(receiverID)")
        .observeSingleEvent(of: .value, with: { (snapshot) in
            if let dict = snapshot.value as? NSDictionary{
                dict.setValue(0, forKey: ChatListKeys.unreadCount)
                if self.receiverToken != ""{
                    dict.setValue(self.receiverToken, forKey: ChatListKeys.deviceToken)
                }
                snapshot.ref.updateChildValues(dict as! [AnyHashable : Any])
            }
        })
    }
}

//MARK:- Firebase services
extension NewChatListVC{
    
    func getAllChats(){
        APP_DELEGATE.FIRDBReference.child(ChatListNode).child(chatsNode).observeSingleEvent(of: .value) { (snapshot) in
            if let dict = snapshot.value as? NSDictionary{
                self.entryCount = dict.count
                self.lblNoData.isHidden = dict.count == 0 ? false : true
            }
            self.enableChildAddedObserver()
            self.enableChildRemovedObserver()
            self.enableChatListObserver()
        }
    }
    
    func enableChildAddedObserver(){
        APP_DELEGATE.FIRDBReference.child(ChatListNode).child(chatsNode).observe(.childAdded) { (snapshot) in
            self.lblNoData.isHidden = true
            
            if let dict = snapshot.value as? NSDictionary{
                self.currentCount += 1
                self.arrayChatList.append(ChatList(dict: dict))
                
                
                
                //Updating latest image and device token in firebase chat list record.
                if let temp = dict.mutableCopy() as? [String : Any]{
                    if let userID = getLoginDetails()?.userId, let receiverID = temp[ChatListKeys.receiverID] as? String{
                        
                        APP_DELEGATE.FIRDBReference.child(ChatListNode)
                        .child("\(ChatListChildNode)\(receiverID)")
                        .child("\(ChatListChildNode)\(userID)")
                        .observeSingleEvent(of: .value, with: { (snapshot) in
                            if let dict = snapshot.value as? NSDictionary{
                                dict.setValue(getStringValueFromUserDefaults_ForKey(strKey: AppKeys.UD.kProfileImage), forKey: ChatListKeys.profileImage)
                                if let token = getStringValueFromUserDefaults_ForKey(strKey: AppKeys.UD.kAPNSToken), token != ""{
                                    dict.setValue(token, forKey: ChatListKeys.deviceToken)
                                }
                                snapshot.ref.updateChildValues(dict as! [AnyHashable : Any])
                            }
                        })
                    }
                }
                
                if self.loadedAllChats{
                    self.tblChatList.reloadData()
                }
                else if self.entryCount == 0 || self.entryCount == self.currentCount{
                    self.loadedAllChats = true
                    self.tblChatList.reloadData()
                }
            }
        }
    }
    
    func enableChildRemovedObserver(){
        APP_DELEGATE.FIRDBReference.child(ChatListNode).child(chatsNode).observe(.childRemoved) { (snapshot) in
            if let dict = snapshot.value as? NSDictionary{
                if let receiverID = dict[ChatListKeys.receiverID] as? String{
                    for (index, chat) in self.arrayChatList.enumerated(){
                        if chat.ReceiverID == receiverID{
                            self.tblChatList.beginUpdates()
                            self.arrayChatList.remove(at: index)
                            self.tblChatList.deleteRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                            self.tblChatList.endUpdates()
                            
                            if self.arrayChatList.count == 0{
                                self.lblNoData.isHidden = false
                            }
                            break
                        }
                    }
                }
            }
        }
    }
    
    func enableChatListObserver(){
        APP_DELEGATE.FIRDBReference.child(ChatListNode).child(chatsNode).observe(.childChanged) { (snapshot) in
            if let dict = snapshot.value as? NSDictionary{
                if let receiverID = dict[ChatListKeys.receiverID] as? String{
                    for (index, chat) in self.arrayChatList.enumerated(){
                        if chat.ReceiverID == receiverID{
                            self.arrayChatList.remove(at: index)
                            self.arrayChatList.insert(ChatList(dict: dict), at: index)
                            self.tblChatList.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                            break
                        }
                    }
                }
            }
        }
    }
    
    func disableChatListObserver(){
        APP_DELEGATE.FIRDBReference.child(ChatListNode).child(chatsNode).removeAllObservers()
    }
}
