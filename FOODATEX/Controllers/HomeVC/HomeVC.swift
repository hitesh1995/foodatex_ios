//
//  HomeVC.swift
//  FOODATEX
//
//  Created by Silver Shark on 10/08/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit
import Koloda
import CoreLocation
import SVProgressHUD
import SDWebImage
import GoogleMobileAds
import FirebaseAnalytics

class HomeVC: BaseVC, GADInterstitialDelegate {
    
    //MARK:-
    @IBOutlet weak var viewProfile: KolodaView!
    @IBOutlet weak var lblSwipeCount : UILabel!{
        didSet{
            lblSwipeCount.alpha = 0
        }
    }
    @IBOutlet weak var viewBtnFooter: UIView!
    @IBOutlet weak var viewNoResult: UIView!
    @IBOutlet weak var viewSwipe : UIView!{
        didSet{
            viewSwipe.backgroundColor = .clear
            viewSwipe.alpha = 0
        }
    }
    
    //MARK:-
    var hasUpdatedLoc:Bool = false
    let locationManager = CLLocationManager()
    var currentLocation : CLLocation!
    var arrUsers : [NearByUserUserList] = [NearByUserUserList]()
    var interstitial: GADInterstitial!
    let request = GADRequest()
    var arrPackageList : [PackageData] = [PackageData]()
    var packageListPopUp : PackageListVC!
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        
        //custom gesture view to disable card swipe
        setupSwipGestures()
        
        //Google Ad
        interstitial = createAndLoadInterstitial()
        
        //callWSForProfile()
        viewNoResult.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        setNavigationbarleft_imagename("Profile-Unselect@2.png", left_action: #selector(openMyProfile), right_imagename: "Chat@2.png", right_action: #selector(openChatList), title: "", isLogo: true)
        hasUpdatedLoc = false
        getMyLocation()
    }
    
    //MARK:-
    func setupSwipGestures(){
        self.view.bringSubview(toFront: viewSwipe)
        let leftSwipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(_:)))
        leftSwipeGesture.direction = .left
        let rightSwipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(_:)))
        rightSwipeGesture.direction = .right
        let upSwipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(_:)))
        upSwipeGesture.direction = .up
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapSwipe(_:)))
        tapGesture.numberOfTapsRequired = 1
        viewSwipe.addGestureRecognizer(leftSwipeGesture)
        viewSwipe.addGestureRecognizer(rightSwipeGesture)
        viewSwipe.addGestureRecognizer(upSwipeGesture)
        viewSwipe.addGestureRecognizer(tapGesture)
    }
    
    func loadCard(){
        viewProfile.delegate = nil
        viewProfile.dataSource = nil
        viewProfile.reloadData()
        viewProfile.delegate = self
        viewProfile.dataSource = self
        viewProfile.countOfVisibleCards = 3
        viewProfile.alphaValueSemiTransparent = 0.8
       
        DispatchQueue.main.async {
            self.viewProfile.reloadData()
             SVProgressHUD.dismiss()
        }
    }
    
    //-- Google Ad Setup --//
    func createAndLoadInterstitial() -> GADInterstitial {
        interstitial = GADInterstitial(adUnitID: GoogleAdUnitID)
        interstitial.delegate = self
        interstitial.load(GADRequest())
        print("New ad created..")
        return interstitial
    }

    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
        print("Dismissed..")
    }
    
    func loadAd(){
        APP_DELEGATE.swipeCount = 0
        if self.interstitial.isReady {
            self.interstitial.present(fromRootViewController: self)
        }else{
            print("ad is not ready..")
        }
    }
    //--   --//
    
    @objc func openMyProfile(){
        self.navigationController?.pushViewController(loadVC(AppStoryboards.kSBMain, strVCId: AppViewControllers.kVCMyProfile), animated: true)
    }
    
    @objc func openChatList(){
        self.navigationController?.pushViewController(loadVC(AppStoryboards.kSBChat, strVCId: AppViewControllers.Chat.kVCChatList), animated: true)
    }
    
    @IBAction func btnReloadTapped(_ sender: Any) {
        getMyLocation()
    }
    
    @IBAction func btnSweetTapped(_ sender: Any) {
        viewProfile.swipe(.right, force: true)
    }
    
    @IBAction func btnSourTapped(_ sender: Any) {
         viewProfile.swipe(.left, force: true)
    }
    
    @IBAction func btnSuperLikeTapped(_ sender: Any) {
         viewProfile.swipe(.right, force: true) // before it was up
    }
    
    @IBAction func btnRetryTapped(_ sender: Any) {
        callWSForProfile()
    }
    
}
//MARK:-
extension HomeVC {
    //MARK : - CHECK FOR CLLOCATIONMANAGER AUTHORISATION STATUS
    func getMyLocation(){
        
        let status = CLLocationManager.authorizationStatus()
        if status == .authorizedAlways || status == .authorizedWhenInUse { self.locationManager.startUpdatingLocation()}
        else if status == .notDetermined { self.locationManager.requestAlwaysAuthorization() }
        else if status == .denied || status == .restricted { askToAllowLocation() }
        else{ askToAllowLocation()}
    }
    
    func askToAllowLocation(){
        let alertView = UIAlertController()
        
        let settingAction = UIAlertAction(title: "Setting", style: .default) { (action) in
            if UIApplication.shared.canOpenURL(URL(string: UIApplicationOpenSettingsURLString)!){
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
                } else { UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!) }
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive) { (action) in
            self.callWSForProfile()
        }
        alertView.title = APP_NAME
        alertView.message = "Please allow location access in setting "
        alertView.addAction(settingAction)
        alertView.addAction(cancelAction)
        self.present(alertView, animated: true, completion: nil)
    }
    
    func callWSForProfile(){
        
        if isConnectedToNetwork(){
            
            SVProgressHUD.show()
            //self.viewNoResult.isHidden = true
            self.viewBtnFooter.isHidden = true
            
            var interest = getLoginDetails()?.interestIn
            if interest == "0"{
                interest = "3"
            }
            let latitude = UserDefaults.standard.double(forKey: "lat")
            let longitude = UserDefaults.standard.double(forKey: "lon")
            
            let dict : [String : Any] = ["user_id" : getLoginDetails()?.userId ?? "",
                                         "interest" : interest ?? "3",
                                         "latitude" : latitude.description,
                                         "longitude" : longitude.description]// uncommented sank
            print("Users/userslistnearby = \(dict)")
            WebServicesCollection.sharedInstance.userslistnearby(dict) { (response, error, message) in
                
                if error == nil {
                    do {
                        if !hasInAppSubscribed(){
                            if var pending = (try JSONSerialization.jsonObject(with: response as! Data, options: []) as? NSDictionary)?["pending"] as? Int{
                                print("========")
                                print("Pending swipe count = ", pending)
                                print("========")
                                //pending = 0
                                USERDEFAULT.set(pending, forKey: pendingSwipes)
                                if pending <= 0{
                                    self.lblSwipeCount.text = "No swipes left for today"
                                    self.lblSwipeCount.alpha = 1
                                    self.enableSwipeGesture()
                                }
                                else{
                                    self.lblSwipeCount.text = pending > 1 ? "\(pending) Swipes Left" : "\(pending) Swipe Left"
                                    self.lblSwipeCount.alpha = 1
                                    self.disableSwipeGesture()
                                }
                            }
                            else{
                                USERDEFAULT.set(15, forKey: pendingSwipes)
                                self.lblSwipeCount.text = "15 Swipes Left"
                                self.lblSwipeCount.alpha = 1
                                self.disableSwipeGesture()
                            }
                        }
                        else{
                            self.lblSwipeCount.alpha = 0
                            self.disableSwipeGesture()
                        }
                        
                        let userResult  = try JSONDecoder().decode(NearByUserResult.self, from: response as! Data)
                        if let userList = userResult.data {
                            self.arrUsers.removeAll()
                            self.arrUsers.append(contentsOf: userList.userList ?? [])
                            self.viewNoResult.isHidden = self.arrUsers.count > 0 ? true : false
                            self.viewBtnFooter.isHidden = self.arrUsers.count > 0 ? false : true
                            self.loadCard()
                        }
                    }catch{
                        SVProgressHUD.dismiss()
                        self.viewNoResult.isHidden = false
                    }
                }else{
                    SVProgressHUD.showError(withStatus: message ?? "")
                    self.viewNoResult.isHidden = false
                }
            }
        }else{
            SVProgressHUD.dismiss()
            showNoInternetAlert()
            self.viewNoResult.isHidden = false
        }
        
    }
    
    func WSCallSwipeUser( _ swipeType : Int, user : NearByUserUserList) {
        //swipeType : 1 = Like, 2 = dislike, 3 = Super Like
        
        //Loading google Ad
        if !hasInAppSubscribed(){
            APP_DELEGATE.swipeCount += 1
            if APP_DELEGATE.swipeCount >= 3{
                self.loadAd()
            }
        }
        
        if isConnectedToNetwork(){
            
            let swipe = ["receiver_u_id": user.userId ?? "" , "swipe" : swipeType] as [String : Any]
            let param : [String : Any] = ["sender_u_id" : getLoginDetails()?.userId ?? 0,
                                          "swipes" : [swipe]]
            print(param)
            if !hasInAppSubscribed(){
                if var pending = USERDEFAULT.value(forKey: pendingSwipes) as? Int{
                    if pending <= 0{
                        self.lblSwipeCount.text = "No swipes left for today"
                        self.lblSwipeCount.alpha = 1
                        self.enableSwipeGesture()
                    }
                    else{
                        pending -= 1
                        USERDEFAULT.set(pending, forKey: pendingSwipes)
                        self.lblSwipeCount.text = pending > 1 ? "\(pending) Swipes Left" : "\(pending) Swipe Left"
                        self.lblSwipeCount.alpha = 1
                        self.disableSwipeGesture()
                    }
                }
            }
            else{
                self.lblSwipeCount.alpha = 0
            }
            
            WebServicesCollection.sharedInstance.swipeuser(param) { (response, error, message) in
                guard let data  = response as? Data else{ return }
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary{
                        var match = false
                        if let isMatch = (json["data"] as? NSDictionary)?["match"] as? String{
                            match = isMatch.lowercased() == "true" ? true : false
                        }
                        else if let isMatch = (json["data"] as? NSDictionary)?["match"] as? Bool{
                            match = isMatch
                        }
                        print("Match = \(match)")
                        if match && swipeType == 1{
                            self.createChatListRecord(user : user, token : (json["data"] as? NSDictionary)?["deviceToken"] as? String ?? "")
                        }
                    }else{
                        print("Unable to access swipe response json..")
                    }
                } catch let error {
                    print("unable to parse swipe response json : ",error)
                }
            }
        }else{
            showNoInternetAlert()
        }
    }
    
    func updateProfile( dict : [String : Any], response : @escaping (_ isUploaded : Bool) -> Void ){
        
        if isConnectedToNetwork(){
            SVProgressHUD.show()
            WebServicesCollection.sharedInstance.UpdateProfile(dict) { (wresponse, error, message, rstatus) in
                SVProgressHUD.dismiss()
                if rstatus == 1{
                    response(true)
                }else{
                    showAppAlertWithMessage(message ?? "Something went wrong, Please trye Again", viewController: self)
                    response(false)
                }
            }
        }else{
            response(false)
        }
    }
    
    func createChatListRecord(user : NearByUserUserList, token : String){
        //Opposite users record in my list..
        let date = "\(Date())"
        var detail : NSDictionary = [ ChatListKeys.message  	: "You have a new match!",
                                      ChatListKeys.dateTime 	: date,
                                      ChatListKeys.unreadCount  : 0,
                                      ChatListKeys.profileImage : user.profileImage ?? "",
                                      ChatListKeys.receiverName : user.fullname ?? user.username ?? "",
                                      ChatListKeys.receiverID   : user.userId!,
                                      ChatListKeys.deviceToken  : token]
        
        var dbRef = APP_DELEGATE.FIRDBReference.child(ChatListNode)
            .child("\(ChatListChildNode)\(getLoginDetails()!.userId!)")
            .child("\(ChatListChildNode)\(user.userId!)")
        
        dbRef.setValue(ChatList(dict: detail).toAny()) { (error, dbRef) in
            if error != nil{
                print("Sender chat list record creation failed.. ", error!)
            }
        }
        
        //My record in opposite users list..
        detail = [ ChatListKeys.message     	: "You have a new match!",
                   ChatListKeys.dateTime    	: date,
                   ChatListKeys.unreadCount     : 0,
                   ChatListKeys.profileImage 	: getLoginDetails()?.profileImage ?? "",
                   ChatListKeys.receiverName 	: getLoginDetails()?.fullname ?? getLoginDetails()?.username ?? "",
                   ChatListKeys.receiverID   	: getLoginDetails()!.userId!,
                   ChatListKeys.deviceToken     : getStringValueFromUserDefaults_ForKey(strKey: AppKeys.UD.kAPNSToken) ?? ""]
        
        dbRef = APP_DELEGATE.FIRDBReference.child(ChatListNode)
            .child("\(ChatListChildNode)\(user.userId!)")
            .child("\(ChatListChildNode)\(getLoginDetails()!.userId!)")
        
        dbRef.setValue(ChatList(dict: detail).toAny()) { (error, dbRef) in
            if error != nil{
                print("Receiver chat list record creation failed.. ", error!)
            }
        }
    }
    
    func enableSwipeGesture(){
        print("swipe disabled..")
        viewSwipe.alpha = 1
    }
    
    func disableSwipeGesture(){
        print("swipe enabled..")
        viewSwipe.alpha = 0
    }
    
    @objc func handleSwipe(_ sender : UISwipeGestureRecognizer){
        if arrPackageList.count == 0 {
            getPackageList()
        }else{
            showPackageSelectionViewController()
        }
    }
    
    
    @objc func handleTapSwipe(_ sender : UISwipeGestureRecognizer){
        if arrPackageList.count == 0 {
            getPackageList()
        }else{
            showPackageSelectionViewController()
            
            /*
            let profileVC = loadVC(AppStoryboards.kSBMain, strVCId:AppViewControllers.kVCProfileDetail) as! ProfileDetailsVC
            profileVC.userID = arrUsers[0].userId ?? ""
             profileVC.hideBottomButtons = false
            let cellobject = arrUsers[0]
            profileVC.delegate = self
            
            if let fDistance = Float(cellobject.distance ?? "0"){
                profileVC.passedDistance =  String(format: "%.2f Kms Away", fDistance)
            }else{
                profileVC.passedDistance = "--"
            }
            
            profileVC.userName = arrUsers[0].fullname ?? ""
            self.present(UINavigationController(rootViewController: profileVC), animated: true, completion: nil)
            */
        }
    }
    
    func showPackageSelectionViewController(){
        let board = UIStoryboard.init(name: "SB_Main", bundle: nil)
        let packageVc = board.instantiateViewController(withIdentifier: "PackageListVC") as! PackageListVC
        packageVc.arrPackageList = self.arrPackageList
        self.navigationController?.pushViewController(packageVc, animated: true)
    }
}

//MARK: - CLLOCATION DELEGATE METHODS
extension HomeVC : CLLocationManagerDelegate {
    
    private func locationManager(manager: CLLocationManager!,
                                 didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        switch status {
        case CLAuthorizationStatus.authorizedAlways :
            locationManager.startUpdatingLocation()
            break
        case CLAuthorizationStatus.authorizedWhenInUse :
            locationManager.startUpdatingLocation()
            break
        case CLAuthorizationStatus.restricted :
            askToAllowLocation()
            break
        case CLAuthorizationStatus.denied:
            askToAllowLocation()
            break
        case CLAuthorizationStatus.notDetermined:
            locationManager.requestAlwaysAuthorization()
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if locations.count > 0 {
            let myLocation = locations.last!
            print("+++++++++++")
            print(myLocation)
            print("+++++++++++")
            self.currentLocation = myLocation
            UserDefaults.standard.set(self.currentLocation.coordinate.latitude, forKey: "lat")
            UserDefaults.standard.set(self.currentLocation.coordinate.longitude, forKey: "lon")
            UserDefaults.standard.synchronize()
            if self.hasUpdatedLoc == false{
                self.hasUpdatedLoc = true
                let dict = ["user_id": getLoginDetails()?.userId ?? "",
                            "latitude" : self.currentLocation.coordinate.latitude,
                            "longitude": self.currentLocation.coordinate.longitude] as [String : Any]
                self.updateProfile(dict: dict) { (isSaved) in
                    self.callWSForProfile()
                }
            }
            locationManager.stopUpdatingLocation()
            
        }else{
            self.callWSForProfile()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        //showAppAlertWithMessage("Failed to get your location, Please try again.", viewController: self)
        self.callWSForProfile()
    }
}



extension HomeVC : KolodaViewDataSource {
    
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return .default
    }
    
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        return arrUsers.count
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        let cellCard =  Bundle.main.loadNibNamed("CardViewCell", owner: self, options: nil)?[0] as? CardViewCell
        
        
        //cellCard?.createCornerRadiusWithShadow(cornerRadius: 10, offset: .zero, opacity: 0.5, radius: 2)
        
        cellCard?.uvwMain.createCornerRadiusWithShadow(cornerRadius: 8, offset: .zero, opacity: 1, radius: 5)
        
        let cellobject = arrUsers[index]
        if let url = URL(string: cellobject.profileImage ?? "") {
            cellCard?.imgUser.sd_setImage(with: url, placeholderImage: nil, options: .refreshCached, completed: nil)
        }
        cellCard?.lblUsername.text = (cellobject.fullname ?? "").trimmingCharacters(in: .whitespaces) + ", " + (cellobject.dob ?? "")
        if let fDistance = Float(cellobject.distance ?? "0"){
            cellCard?.lblDistance.text =  String(format: "%.2f Kms Away", fDistance)
        }else{
            cellCard?.lblDistance.text = "--"
        }
        cellCard?.lblFoodType.text = cellobject.foodPreference ?? "--"
        cellCard?.lblUserLocation.text = cellobject.city ?? ""
        cellCard?.btnExpandProfile.tag = index
        cellCard?.btnExpandProfile.addTarget(self, action: #selector(btn_CardExpand_clicked(_:)), for: .touchUpInside)
        return cellCard!
    }
    @IBAction func btn_CardExpand_clicked(_ sender: UIButton){
       
        let profileVC = loadVC(AppStoryboards.kSBMain, strVCId:AppViewControllers.kVCProfileDetail) as! ProfileDetailsVC
        profileVC.userID = arrUsers[sender.tag].userId ?? ""
         profileVC.hideBottomButtons = false
        let cellobject = arrUsers[sender.tag]
        profileVC.delegate = self
        
        if let fDistance = Float(cellobject.distance ?? "0"){
            profileVC.passedDistance =  String(format: "%.2f Kms Away", fDistance)
        }else{
            profileVC.passedDistance = "--"
        }
        
        profileVC.userName = arrUsers[sender.tag].fullname ?? ""
        self.present(UINavigationController(rootViewController: profileVC), animated: true, completion: nil)
    }
}

extension HomeVC : HomeSwipeDelegate{
    
    func updateSwipeAtHome(swipeType:Int,userId:String) {
        if swipeType == 1{
            btnSweetTapped(self)
        }else if swipeType == 2{
            btnSourTapped(self)
        }else{
            btnSuperLikeTapped(self)
        }
          //  WSCallSwipeUser(swipeType, receiveriD: userId)
    }
}
//MARK: - KolodaViewDelegate
extension HomeVC : KolodaViewDelegate {
    
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        koloda.dataSource = nil
        koloda.delegate = nil
        koloda.reloadData()
        callWSForProfile()
    }
    
    func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
        
        let profileVC = loadVC(AppStoryboards.kSBMain, strVCId:AppViewControllers.kVCProfileDetail) as! ProfileDetailsVC
        profileVC.userID = arrUsers[index].userId ?? ""
        profileVC.hideBottomButtons = false
        let cellobject = arrUsers[index]
      
        profileVC.delegate = self
        if let fDistance = Float(cellobject.distance ?? "0"){
            profileVC.passedDistance =  String(format: "%.2f Kms Away", fDistance)
        }else{
            profileVC.passedDistance = "--"
        }
        
        profileVC.userName = arrUsers[index].fullname ?? ""
        self.present(UINavigationController(rootViewController: profileVC), animated: true, completion: nil)
        
        Analytics.logEvent("check_profile", parameters: ["by_user" : getLoginDetails()?.userId ?? "0", "of_user" : arrUsers[index].userId ?? "0"])
    }
    
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        let user = arrUsers[index]
        Analytics.logEvent("swipe_profile", parameters: ["by_user" : getLoginDetails()?.userId ?? "0", "for_user" : user.userId ?? "0", "swipe_direction" : direction == .left ? "Left" : "Right"])
        if direction == .right {
            WSCallSwipeUser(1, user : user)
        }else if direction == .left{
            WSCallSwipeUser(2, user : user)
        }else if direction == .up{
            WSCallSwipeUser(3, user : user)
        }
    }
    
    func koloda(_ koloda: KolodaView, allowedDirectionsForIndex index: Int) -> [SwipeResultDirection] {
        return [.up, .left, .right]
    }
}

extension HomeVC{
        func getPackageList(){
            
            if isConnectedToNetwork(){
                SVProgressHUD.show()
                WebServicesCollection.sharedInstance.PackageList { (resp, error, message) in
                    SVProgressHUD.dismiss()
                    if error == nil{
                        do {
                            let packageResult  = try JSONDecoder().decode(PackageResult.self, from: resp as! Data)
                            
                            if let userList = packageResult.data {
                                self.arrPackageList.removeAll()
                                self.arrPackageList.append(contentsOf: userList)
                                self.showPackageSelectionViewController()
                            }
                        }catch{
                            SVProgressHUD.showError(withStatus: AppMessages.User_Error)
                        }
                    }else{
                        showAppAlertWithMessage(message ?? "Something went wrong", viewController: self)
                    }
                }
            }else{
                showNoInternetAlert()
            }
    }

}
