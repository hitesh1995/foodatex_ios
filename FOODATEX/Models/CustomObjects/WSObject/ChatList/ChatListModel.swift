//
//  ChatListModel.swift
//  FOODATEX
//
//  Created by Hitesh Prajapati on 06/06/20.
//  Copyright © 2020 RudraApps. All rights reserved.
//

import Foundation

struct ChatListKeys {
    static let profileImage = "profileImage"
    static let dateTime     = "date"
    static let unreadCount  = "unreadCount"
    static let message      = "lastMessage"
    static let receiverID   = "receiverId"
    static let receiverName = "receiverName"
    static let deviceToken  = "deviceToken"
}

struct ChatList{
    let ProfileImage    : String
    let DateTime        : String
    let UnreadCount     : Int
    let Message         : String
    let ReceiverID      : String
    let ReceiverName    : String
    let DeviceToken     : String
    
    init(dict : NSDictionary) {
        self.ProfileImage   = dict[ChatListKeys.profileImage] as? String ?? ""
        self.DateTime       = dict[ChatListKeys.dateTime] as? String ?? ""
        self.UnreadCount    = dict[ChatListKeys.unreadCount] as? Int ?? 0
        self.Message        = dict[ChatListKeys.message] as? String ?? ""
        self.ReceiverID     = dict[ChatListKeys.receiverID] as? String ?? ""
        self.ReceiverName   = dict[ChatListKeys.receiverName] as? String ?? ""
        self.DeviceToken    = dict[ChatListKeys.deviceToken] as? String ?? ""
    }
    
    func toAny() -> Any{
        return [ ChatListKeys.message       : Message,
                 ChatListKeys.dateTime      : DateTime,
                 ChatListKeys.profileImage  : ProfileImage,
                 ChatListKeys.unreadCount   : UnreadCount,
                 ChatListKeys.receiverID    : ReceiverID,
                 ChatListKeys.receiverName  : ReceiverName,
                 ChatListKeys.deviceToken   : DeviceToken]
    }
}
