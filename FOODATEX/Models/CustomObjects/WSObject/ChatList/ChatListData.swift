//
//  ChatListData.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on November 2, 2018

import Foundation

struct ChatListData : Codable {

        let email : String?
        let fullname : String?
        let message : String?
        let messageTime : String?
        let messageTimeAgo : String?
        let messangeSenderId : String?
        let notification : String?
        let profileImage : String?
        let uId : String?

        enum CodingKeys: String, CodingKey {
                case email = "email"
                case fullname = "fullname"
                case message = "message"
                case messageTime = "messageTime"
                case messageTimeAgo = "messageTimeAgo"
                case messangeSenderId = "messangeSenderId"
                case notification = "notification"
                case profileImage = "profile_image"
                case uId = "u_id"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                email = try values.decodeIfPresent(String.self, forKey: .email)
                fullname = try values.decodeIfPresent(String.self, forKey: .fullname)
                message = try values.decodeIfPresent(String.self, forKey: .message)
                messageTime = try values.decodeIfPresent(String.self, forKey: .messageTime)
                messageTimeAgo = try values.decodeIfPresent(String.self, forKey: .messageTimeAgo)
                messangeSenderId = try values.decodeIfPresent(String.self, forKey: .messangeSenderId)
                notification = try values.decodeIfPresent(String.self, forKey: .notification)
                profileImage = try values.decodeIfPresent(String.self, forKey: .profileImage)
                uId = try values.decodeIfPresent(String.self, forKey: .uId)
        }

}
