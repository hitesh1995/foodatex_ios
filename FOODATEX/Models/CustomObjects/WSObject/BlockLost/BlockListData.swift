//
//  BlockListData.swift
//  FOODATEX
//
//  Created by Hitesh Prajapati on 13/09/19.
//  Copyright © 2019 RudraApps. All rights reserved.
//

import Foundation

class BlockListData : NSObject {
    
    let uId             : String
    let fullname        : String
    let email           : String
    let profileImage    : String
    
    init?(dict:[String:Any]){
        self.uId            = dict["user_id"] as? String ?? ""
        self.fullname       = dict["fullname"] as? String ?? ""
        self.email          = dict["email"] as? String ?? ""
        self.profileImage   = dict["profile_image"] as? String ?? ""
    }
    
}
