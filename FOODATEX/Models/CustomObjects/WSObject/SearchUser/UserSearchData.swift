//
//  UserSearchData.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on September 14, 2018

import Foundation

struct UserSearchData : Codable {

        var address : String?
        let email : String?
        let fullname : String?
        let phone : String?
        let profileImage : String?
        let userId : String?
        let username : String?

        enum CodingKeys: String, CodingKey {
                case address = "address"
                case email = "email"
                case fullname = "fullname"
                case phone = "phone"
                case profileImage = "profile_image"
                case userId = "user_id"
                case username = "username"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                address = try values.decodeIfPresent(String.self, forKey: .address)
                email = try values.decodeIfPresent(String.self, forKey: .email)
                fullname = try values.decodeIfPresent(String.self, forKey: .fullname)
                phone = try values.decodeIfPresent(String.self, forKey: .phone)
                profileImage = try values.decodeIfPresent(String.self, forKey: .profileImage)
                userId = try values.decodeIfPresent(String.self, forKey: .userId)
                username = try values.decodeIfPresent(String.self, forKey: .username)
        }

}
