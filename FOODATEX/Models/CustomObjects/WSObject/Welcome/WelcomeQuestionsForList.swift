//
//  WelcomeQuestionsForList.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 18, 2019

import Foundation

struct WelcomeQuestionsForList : Codable {

        let descriptionField : String?
        let image : String?
        let title : String?
        let welcomeScreenId : String?

        enum CodingKeys: String, CodingKey {
                case descriptionField = "description"
                case image = "image"
                case title = "title"
                case welcomeScreenId = "welcome_screen_id"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                descriptionField = try values.decodeIfPresent(String.self, forKey: .descriptionField)
                image = try values.decodeIfPresent(String.self, forKey: .image)
                title = try values.decodeIfPresent(String.self, forKey: .title)
                welcomeScreenId = try values.decodeIfPresent(String.self, forKey: .welcomeScreenId)
        }

}
