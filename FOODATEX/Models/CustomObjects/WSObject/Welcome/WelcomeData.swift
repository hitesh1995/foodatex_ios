//
//  WelcomeData.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 18, 2019

import Foundation

struct WelcomeData : Codable {

        let questionsForList : [WelcomeQuestionsForList]?

        enum CodingKeys: String, CodingKey {
                case questionsForList = "questionsFor_list"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                questionsForList = try values.decodeIfPresent([WelcomeQuestionsForList].self, forKey: .questionsForList)
        }

}
