//
//  UserData.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 10, 2018

import Foundation

struct UserData : Codable {
    
    var aboutYourself : String?
    var address : String?
    var anyAllergies : String?
    var anyAllergiesStatus : String?
    var areYouA : String?
    var city : String?
    var countryCode : String?
    var doYouHaveKids : String?
    var doYouSmoke : String?
    var doYouWantKids : String?
    var dob : String?
    var email : String?
    var foodPreference : String?
    var fullname : String?
    var height : String?
    var interestIn : String?
    var latitude : String?
    var longitude : String?
    var lookingFor : String?
    var maritalStatus : String?
    var personality : String?
    var phone : String?
    var profileImage : String?
    var splitBill : String?
    var userId : String?
    var username : String?
    var video : String?
    var whatDoYou : String?
    var gender:String?
    var images : [UserProfileImage]?
    var takeDrink : String?
    var takeDrug : String?
    var takeSmoke : String?
    var isPurchase:String = ""
    var PurchaseId:String = ""
    
    
    enum CodingKeys: String, CodingKey {
        case aboutYourself = "about_yourself"
        case address = "address"
        case anyAllergies = "any_allergies"
        case anyAllergiesStatus = "any_allergies_status"
        case areYouA = "are_you_a"
        case city = "city"
        case countryCode = "country_code"
        case doYouHaveKids = "do_you_have_kids"
        case doYouSmoke = "do_you_smoke"
        case doYouWantKids = "do_you_want_kids"
        case dob = "dob"
        case email = "email"
        case foodPreference = "food_preference"
        case fullname = "fullname"
        case height = "height"
        case interestIn = "interest_in"
        case latitude = "latitude"
        case longitude = "longitude"
        case lookingFor = "looking_for"
        case maritalStatus = "marital_status"
        case personality = "personality"
        case phone = "phone"
        case profileImage = "profile_image"
        case splitBill = "split_bill"
        case userId = "user_id"
        case username = "username"
        case video = "video"
        case whatDoYou = "what_do_you"
        case images = "images"
        case takeDrink = "take_drink"
        case takeDrug = "take_drug"
        case takeSmoke = "take_smoke"
        case isPurchase = "IsPurchase"
        case PurchaseId = "PurchaseId"
        case gender = "gender"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        aboutYourself = try values.decodeIfPresent(String.self, forKey: .aboutYourself)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        anyAllergies = try values.decodeIfPresent(String.self, forKey: .anyAllergies)
        anyAllergiesStatus = try values.decodeIfPresent(String.self, forKey: .anyAllergiesStatus)
        areYouA = try values.decodeIfPresent(String.self, forKey: .areYouA)
        city = try values.decodeIfPresent(String.self, forKey: .city)
        countryCode = try values.decodeIfPresent(String.self, forKey: .countryCode)
        doYouHaveKids = try values.decodeIfPresent(String.self, forKey: .doYouHaveKids)
        doYouSmoke = try values.decodeIfPresent(String.self, forKey: .doYouSmoke)
        doYouWantKids = try values.decodeIfPresent(String.self, forKey: .doYouWantKids)
        dob = try values.decodeIfPresent(String.self, forKey: .dob)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        foodPreference = try values.decodeIfPresent(String.self, forKey: .foodPreference)
        fullname = try values.decodeIfPresent(String.self, forKey: .fullname)
        height = try values.decodeIfPresent(String.self, forKey: .height)
        interestIn = try values.decodeIfPresent(String.self, forKey: .interestIn)
        latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
        lookingFor = try values.decodeIfPresent(String.self, forKey: .lookingFor)
        maritalStatus = try values.decodeIfPresent(String.self, forKey: .maritalStatus)
        personality = try values.decodeIfPresent(String.self, forKey: .personality)
        phone = try values.decodeIfPresent(String.self, forKey: .phone)
        profileImage = try values.decodeIfPresent(String.self, forKey: .profileImage)
        splitBill = try values.decodeIfPresent(String.self, forKey: .splitBill)
        userId = try values.decodeIfPresent(String.self, forKey: .userId)
        username = try values.decodeIfPresent(String.self, forKey: .username)
        video = try values.decodeIfPresent(String.self, forKey: .video)
        whatDoYou = try values.decodeIfPresent(String.self, forKey: .whatDoYou)
        images = try values.decodeIfPresent([UserProfileImage].self, forKey: .images)
        takeDrug = try values.decodeIfPresent(String.self, forKey: .takeDrug)
        takeDrink = try values.decodeIfPresent(String.self, forKey: .takeDrink)
        takeSmoke = try values.decodeIfPresent(String.self, forKey: .takeSmoke)
        isPurchase = try values.decodeIfPresent(String.self, forKey: .isPurchase) ?? ""
        PurchaseId = try values.decodeIfPresent(String.self, forKey: .PurchaseId) ?? ""
        
        //PurchaseId
    }
    
}
