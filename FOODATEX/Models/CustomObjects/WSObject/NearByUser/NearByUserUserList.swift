//
//  NearByUserUserList.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on November 29, 2018

import Foundation

struct NearByUserUserList : Codable {

        let aboutYourself : String?
        let address : String?
        let allowingYou : String?
        let anyAllergies : String?
        let anyAllergiesStatus : String?
        let areYouA : String?
        let city : String?
        let countryCode : String?
        let distance : String?
        let distanceInvisible : String?
        let doYouHaveKids : String?
        let doYouSmoke : String?
        let doYouWantKids : String?
        let dob : String?
        let dontShowMyAge : String?
        let email : String?
        let foodPreference : String?
        let foodatextBoost : String?
        let fullname : String?
        let height : String?
        let hideAdverts : String?
        let interestIn : String?
        let latitude : String?
        let longitude : String?
        let lookingFor : String?
        let maritalStatus : String?
        let personality : String?
        let phone : String?
        let profileImage : String?
        let splitBill : String?
        let unlimitedLikes : String?
        let userId : String?
        let username : String?
        let whatDoYou : String?
        let whoSeeYou : String?

        enum CodingKeys: String, CodingKey {
                case aboutYourself = "about_yourself"
                case address = "address"
                case allowingYou = "allowing_you"
                case anyAllergies = "any_allergies"
                case anyAllergiesStatus = "any_allergies_status"
                case areYouA = "are_you_a"
                case city = "city"
                case countryCode = "country_code"
                case distance = "distance"
                case distanceInvisible = "distance_invisible"
                case doYouHaveKids = "do_you_have_kids"
                case doYouSmoke = "do_you_smoke"
                case doYouWantKids = "do_you_want_kids"
                case dob = "dob"
                case dontShowMyAge = "dont_show_my_age"
                case email = "email"
                case foodPreference = "food_preference"
                case foodatextBoost = "foodatext_boost"
                case fullname = "fullname"
                case height = "height"
                case hideAdverts = "hide_adverts"
                case interestIn = "interest_in"
                case latitude = "latitude"
                case longitude = "longitude"
                case lookingFor = "looking_for"
                case maritalStatus = "marital_status"
                case personality = "personality"
                case phone = "phone"
                case profileImage = "profile_image"
                case splitBill = "split_bill"
                case unlimitedLikes = "unlimited_likes"
                case userId = "user_id"
                case username = "username"
                case whatDoYou = "what_do_you"
                case whoSeeYou = "who_see_you"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                aboutYourself = try values.decodeIfPresent(String.self, forKey: .aboutYourself)
                address = try values.decodeIfPresent(String.self, forKey: .address)
                allowingYou = try values.decodeIfPresent(String.self, forKey: .allowingYou)
                anyAllergies = try values.decodeIfPresent(String.self, forKey: .anyAllergies)
                anyAllergiesStatus = try values.decodeIfPresent(String.self, forKey: .anyAllergiesStatus)
                areYouA = try values.decodeIfPresent(String.self, forKey: .areYouA)
                city = try values.decodeIfPresent(String.self, forKey: .city)
                countryCode = try values.decodeIfPresent(String.self, forKey: .countryCode)
                distance = try values.decodeIfPresent(String.self, forKey: .distance)
                distanceInvisible = try values.decodeIfPresent(String.self, forKey: .distanceInvisible)
                doYouHaveKids = try values.decodeIfPresent(String.self, forKey: .doYouHaveKids)
                doYouSmoke = try values.decodeIfPresent(String.self, forKey: .doYouSmoke)
                doYouWantKids = try values.decodeIfPresent(String.self, forKey: .doYouWantKids)
                dob = try values.decodeIfPresent(String.self, forKey: .dob)
                dontShowMyAge = try values.decodeIfPresent(String.self, forKey: .dontShowMyAge)
                email = try values.decodeIfPresent(String.self, forKey: .email)
                foodPreference = try values.decodeIfPresent(String.self, forKey: .foodPreference)
                foodatextBoost = try values.decodeIfPresent(String.self, forKey: .foodatextBoost)
                fullname = try values.decodeIfPresent(String.self, forKey: .fullname)
                height = try values.decodeIfPresent(String.self, forKey: .height)
                hideAdverts = try values.decodeIfPresent(String.self, forKey: .hideAdverts)
                interestIn = try values.decodeIfPresent(String.self, forKey: .interestIn)
                latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
                longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
                lookingFor = try values.decodeIfPresent(String.self, forKey: .lookingFor)
                maritalStatus = try values.decodeIfPresent(String.self, forKey: .maritalStatus)
                personality = try values.decodeIfPresent(String.self, forKey: .personality)
                phone = try values.decodeIfPresent(String.self, forKey: .phone)
                profileImage = try values.decodeIfPresent(String.self, forKey: .profileImage)
                splitBill = try values.decodeIfPresent(String.self, forKey: .splitBill)
                unlimitedLikes = try values.decodeIfPresent(String.self, forKey: .unlimitedLikes)
                userId = try values.decodeIfPresent(String.self, forKey: .userId)
                username = try values.decodeIfPresent(String.self, forKey: .username)
                whatDoYou = try values.decodeIfPresent(String.self, forKey: .whatDoYou)
                whoSeeYou = try values.decodeIfPresent(String.self, forKey: .whoSeeYou)
        }

}
