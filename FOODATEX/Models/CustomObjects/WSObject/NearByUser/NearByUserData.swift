//
//  NearByUserData.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on November 29, 2018

import Foundation

struct NearByUserData : Codable {

        let userList : [NearByUserUserList]?

        enum CodingKeys: String, CodingKey {
                case userList = "user_list"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                userList = try values.decodeIfPresent([NearByUserUserList].self, forKey: .userList)
        }

}
