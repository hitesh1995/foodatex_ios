//
//  NearByUserResult.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on November 29, 2018

import Foundation

struct NearByUserResult : Codable {

        let data : NearByUserData?
        let message : String?
        let status : String?

        enum CodingKeys: String, CodingKey {
                case data = "data"
                case message = "message"
                case status = "status"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                data = try values.decodeIfPresent(NearByUserData.self, forKey: .data)
                message = try values.decodeIfPresent(String.self, forKey: .message)
                status = try values.decodeIfPresent(String.self, forKey: .status)
        }

}
