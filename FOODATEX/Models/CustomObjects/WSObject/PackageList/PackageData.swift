//
//  PackageData.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on December 13, 2018

import Foundation

struct PackageData : Codable {

        let descriptionField : String?
        let duration : String?
        let packageId : String?
        let packageList : [PackageList]?
        let price : String?
        let title : String?
    
        let image:String?
        let textColor:String?

        enum CodingKeys: String, CodingKey {
                case descriptionField = "description"
                case duration = "duration"
                case packageId = "package_id"
                case packageList = "package_list"
                case price = "price"
                case title = "title"
                case image = "image"
                case textColor = "textcolor"
            
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                descriptionField = try values.decodeIfPresent(String.self, forKey: .descriptionField)
                duration = try values.decodeIfPresent(String.self, forKey: .duration)
                packageId = try values.decodeIfPresent(String.self, forKey: .packageId)
                packageList = try values.decodeIfPresent([PackageList].self, forKey: .packageList)
                price = try values.decodeIfPresent(String.self, forKey: .price)
                title = try values.decodeIfPresent(String.self, forKey: .title)
            
                image = try values.decodeIfPresent(String.self, forKey: .image)
                textColor = try values.decodeIfPresent(String.self, forKey: .textColor)
        }

}
