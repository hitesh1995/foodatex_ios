//
//  PackageList.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on December 13, 2018

import Foundation

struct PackageList : Codable {

        let descriptionField : String?
        let image : String?
        let packageId : String?
        let packageListId : String?
        let text : String?
        let title : String?

        enum CodingKeys: String, CodingKey {
                case descriptionField = "description"
                case image = "image"
                case packageId = "package_id"
                case packageListId = "package_list_id"
                case text = "text"
                case title = "title"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                descriptionField = try values.decodeIfPresent(String.self, forKey: .descriptionField)
                image = try values.decodeIfPresent(String.self, forKey: .image)
                packageId = try values.decodeIfPresent(String.self, forKey: .packageId)
                packageListId = try values.decodeIfPresent(String.self, forKey: .packageListId)
                text = try values.decodeIfPresent(String.self, forKey: .text)
                title = try values.decodeIfPresent(String.self, forKey: .title)
        }

}
