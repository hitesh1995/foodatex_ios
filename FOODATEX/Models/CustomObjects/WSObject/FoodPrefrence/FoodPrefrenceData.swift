//
//  FoodPrefrenceData.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 21, 2018

import Foundation

struct FoodPrefrenceData : Codable {

        let foodPreferenceId : String?
        let preference : String?

        enum CodingKeys: String, CodingKey {
                case foodPreferenceId = "food_preference_id"
                case preference = "preference"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                foodPreferenceId = try values.decodeIfPresent(String.self, forKey: .foodPreferenceId)
                preference = try values.decodeIfPresent(String.self, forKey: .preference)
        }

}
