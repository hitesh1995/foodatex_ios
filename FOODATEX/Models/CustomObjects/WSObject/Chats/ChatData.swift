//
//  ChatData.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on November 5, 2018

import Foundation

struct ChatData : Codable {

        let createdOn : String?
        let fullname : String?
        let image : String?
        let message : String?
        let messageTimeAgo : String?
        let profileImage : String?
        let recevierUId : String?
        let senderUId : String?
        let uchId : String?

        enum CodingKeys: String, CodingKey {
                case createdOn = "created_on"
                case fullname = "fullname"
                case image = "image"
                case message = "message"
                case messageTimeAgo = "messageTimeAgo"
                case profileImage = "profile_image"
                case recevierUId = "recevier_u_id"
                case senderUId = "sender_u_id"
                case uchId = "uch_id"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                createdOn = try values.decodeIfPresent(String.self, forKey: .createdOn)
                fullname = try values.decodeIfPresent(String.self, forKey: .fullname)
                image = try values.decodeIfPresent(String.self, forKey: .image)
                message = try values.decodeIfPresent(String.self, forKey: .message)
                messageTimeAgo = try values.decodeIfPresent(String.self, forKey: .messageTimeAgo)
                profileImage = try values.decodeIfPresent(String.self, forKey: .profileImage)
                recevierUId = try values.decodeIfPresent(String.self, forKey: .recevierUId)
                senderUId = try values.decodeIfPresent(String.self, forKey: .senderUId)
                uchId = try values.decodeIfPresent(String.self, forKey: .uchId)
        }

}
