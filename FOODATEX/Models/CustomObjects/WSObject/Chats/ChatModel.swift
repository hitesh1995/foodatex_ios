//
//  ChatModel.swift
//  FOODATEX
//
//  Created by Hitesh Prajapati on 30/05/20.
//  Copyright © 2020 RudraApps. All rights reserved.
//

import Foundation

struct Chat {
    let Message    	    : String
    let DateTime   	    : String
    let SenderID   	    : String
    let ReceiverID 	    : String
    
    init(dict : NSDictionary) {
        self.Message    	= dict[ChatKeys.message] as? String ?? ""
        self.DateTime   	= dict[ChatKeys.date] as? String ?? ""
        self.SenderID   	= dict[ChatKeys.senderId] as? String ?? "\(dict[ChatKeys.senderId] as? Int ?? 0)"
        self.ReceiverID 	= dict[ChatKeys.receiverId] as? String ?? "\(dict[ChatKeys.receiverId] as? Int ?? 0)"
    }
    
    func toAny() -> Any{
        return [ ChatKeys.message       : Message,
                 ChatKeys.date          : DateTime,
                 ChatKeys.senderId      : SenderID,
                 ChatKeys.receiverId    : ReceiverID ]
    }
}

struct ChatKeys {
    static let message    	    = "message"
    static let date       	    = "date"
    static let senderId   	    = "senderId"
    static let receiverId 	    = "receiverId"
}



struct StatusKeys{
    static let date     = "date"
    static let isOnline = "isOnline"
}

struct OnlineStatus{
    let dateTime : String
    let isOnline : String
    
    init(dict : NSDictionary) {
        self.dateTime = dict[StatusKeys.date] as? String ?? ""
        self.isOnline = dict[StatusKeys.isOnline] as? String ?? "0"
    }
    
    func toAny() -> Any{
        return [ StatusKeys.date : dateTime,
                 StatusKeys.isOnline : isOnline ]
    }
}
