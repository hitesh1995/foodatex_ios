//
//  UserProfileImage.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on October 24, 2018

import Foundation

struct UserProfileImage : Codable {

        let image : String?
        let userImagesId : String?

        enum CodingKeys: String, CodingKey {
                case image = "image"
                case userImagesId = "user_images_id"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                image = try values.decodeIfPresent(String.self, forKey: .image)
                userImagesId = try values.decodeIfPresent(String.self, forKey: .userImagesId)
        }

}
