//
//  UserProfileData.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on October 24, 2018

import Foundation

struct UserProfileData : Codable {

        var aboutYourself : String?
        var address : String?
        var allowingYou : String?
        var anyAllergies : String?
        var anyAllergiesStatus : String?
        var areYouA : String?
        var city : String?
        var countryCode : String?
        var distanceInvisible : String?
        var doYouHaveKids : String?
        var doYouSmoke : String?
        var doYouWantKids : String?
        var dob : String?
        var dontShowMyAge : String?
        var email : String?
        var foodPreference : String?
        var gender : String?
        var foodatextBoost : String?
        var fullname : String?
        var height : String?
        var hideAdverts : String?
        var images : [UserProfileImage]?
        var interestIn : String?
        var latitude : String?
        var longitude : String?
        var lookingFor : String?
        var maritalStatus : String?
        var personality : String?
        var phone : String?
        var profileImage : String?
        var splitBill : String?
        var unlimitedLikes : String?
    var skipTheQueue : String?
        var userId : String?
        var username : String?
        var video : String?
        var whatDoYou : String?
        var whoSeeYou : String?
    var takeDrink : String?
    var takeDrug : String?
    var takeSmoke : String?
    var showMyAge : String?
    var twoUserDistance : Double?
    var controlWhoSeesYou : String?
    var isPurchase : String?
    var purchaseId:String?
    
    var planStatus : String?

        enum CodingKeys: String, CodingKey {
                case aboutYourself = "about_yourself"
                case address = "address"
                case allowingYou = "allowingYou"
                case anyAllergies = "any_allergies"
                case anyAllergiesStatus = "any_allergies_status"
                case areYouA = "are_you_a"
                case city = "city"
                case countryCode = "country_code"
                case distanceInvisible = "distanceInvisible"
                case doYouHaveKids = "do_you_have_kids"
                case doYouSmoke = "do_you_smoke"
                case doYouWantKids = "do_you_want_kids"
                case dob = "dob"
                case dontShowMyAge = "dont_show_my_age"
                case email = "email"
                case foodPreference = "food_preference"
                case foodatextBoost = "foodatext_boost"
                case fullname = "fullname"
                case height = "height"
                case hideAdverts = "hideAdverts"
                case images = "images"
                case interestIn = "interest_in"
                case latitude = "latitude"
                case longitude = "longitude"
                case lookingFor = "looking_for"
                case maritalStatus = "marital_status"
                case personality = "personality"
                case phone = "phone"
                case profileImage = "profile_image"
                case splitBill = "split_bill"
                case unlimitedLikes = "unlimitedLikes"
                case userId = "user_id"
                case username = "username"
                case video = "video"
                case whatDoYou = "what_do_you"
                case whoSeeYou = "who_see_you"
            case takeDrink = "take_drink"
            case takeDrug = "take_drug"
            case takeSmoke = "take_smoke"
            case skipTheQueue = "skipTheQueue"
            case showMyAge = "showMyAge"
            case twoUserDistance = "two_user_distance"
            case controlWhoSeesYou = "controlWhoSeesYou"
            case isPurchase = "IsPurchase"
            case purchaseId = "PurchaseId"
            case gender = "gender"
            case planStatus = "planstatus"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                aboutYourself = try values.decodeIfPresent(String.self, forKey: .aboutYourself)
             gender = try values.decodeIfPresent(String.self, forKey: .gender)
                address = try values.decodeIfPresent(String.self, forKey: .address)
                allowingYou = try values.decodeIfPresent(String.self, forKey: .allowingYou)
                anyAllergies = try values.decodeIfPresent(String.self, forKey: .anyAllergies)
                anyAllergiesStatus = try values.decodeIfPresent(String.self, forKey: .anyAllergiesStatus)
                areYouA = try values.decodeIfPresent(String.self, forKey: .areYouA)
                city = try values.decodeIfPresent(String.self, forKey: .city)
                countryCode = try values.decodeIfPresent(String.self, forKey: .countryCode)
                distanceInvisible = try values.decodeIfPresent(String.self, forKey: .distanceInvisible)
                doYouHaveKids = try values.decodeIfPresent(String.self, forKey: .doYouHaveKids)
                doYouSmoke = try values.decodeIfPresent(String.self, forKey: .doYouSmoke)
                doYouWantKids = try values.decodeIfPresent(String.self, forKey: .doYouWantKids)
                dob = try values.decodeIfPresent(String.self, forKey: .dob)
                dontShowMyAge = try values.decodeIfPresent(String.self, forKey: .dontShowMyAge)
                email = try values.decodeIfPresent(String.self, forKey: .email)
                foodPreference = try values.decodeIfPresent(String.self, forKey: .foodPreference)
                foodatextBoost = try values.decodeIfPresent(String.self, forKey: .foodatextBoost)
                fullname = try values.decodeIfPresent(String.self, forKey: .fullname)
                height = try values.decodeIfPresent(String.self, forKey: .height)
                hideAdverts = try values.decodeIfPresent(String.self, forKey: .hideAdverts)
                images = try values.decodeIfPresent([UserProfileImage].self, forKey: .images)
                interestIn = try values.decodeIfPresent(String.self, forKey: .interestIn)
                latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
                longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
                lookingFor = try values.decodeIfPresent(String.self, forKey: .lookingFor)
                maritalStatus = try values.decodeIfPresent(String.self, forKey: .maritalStatus)
                personality = try values.decodeIfPresent(String.self, forKey: .personality)
                phone = try values.decodeIfPresent(String.self, forKey: .phone)
                profileImage = try values.decodeIfPresent(String.self, forKey: .profileImage)
                splitBill = try values.decodeIfPresent(String.self, forKey: .splitBill)
                unlimitedLikes = try values.decodeIfPresent(String.self, forKey: .unlimitedLikes)
                userId = try values.decodeIfPresent(String.self, forKey: .userId)
                username = try values.decodeIfPresent(String.self, forKey: .username)
                video = try values.decodeIfPresent(String.self, forKey: .video)
                whatDoYou = try values.decodeIfPresent(String.self, forKey: .whatDoYou)
                whoSeeYou = try values.decodeIfPresent(String.self, forKey: .whoSeeYou)
            takeDrug = try values.decodeIfPresent(String.self, forKey: .takeDrug)
            takeDrink = try values.decodeIfPresent(String.self, forKey: .takeDrink)
            takeSmoke = try values.decodeIfPresent(String.self, forKey: .takeSmoke)
            skipTheQueue = try values.decodeIfPresent(String.self, forKey: .skipTheQueue)
            showMyAge = try values.decodeIfPresent(String.self, forKey: .showMyAge)
            twoUserDistance = try values.decodeIfPresent(Double.self, forKey: .twoUserDistance)//
            controlWhoSeesYou = try values.decodeIfPresent(String.self, forKey: .controlWhoSeesYou)
            
            isPurchase = try values.decodeIfPresent(String.self, forKey: .isPurchase)
            
            purchaseId = try values.decodeIfPresent(String.self, forKey: .purchaseId)
            
            planStatus = try values.decodeIfPresent(String.self, forKey: .planStatus)
            
          
        }

}
