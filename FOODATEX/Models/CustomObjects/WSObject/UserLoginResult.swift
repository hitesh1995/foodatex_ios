//
//  UserLoginResult.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 10, 2018

import Foundation

struct UserLoginResult : Codable {

        let data : UserData?
        let message : String?
        let profileStaus : String?
        let status : String?

        enum CodingKeys: String, CodingKey {
                case data = "data"
                case message = "message"
                case profileStaus = "profile_staus"
                case status = "status"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                data = try values.decodeIfPresent(UserData.self, forKey: .data)
                message = try values.decodeIfPresent(String.self, forKey: .message)
                profileStaus = try values.decodeIfPresent(String.self, forKey: .profileStaus)
                status = try values.decodeIfPresent(String.self, forKey: .status)
        }

}
