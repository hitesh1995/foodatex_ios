//
//  UserInfo.swift
//  FOODATEX
//
//  Created by Devang Tandel on 02/12/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import Foundation


class UserInfo {
    var gender : String = ""
    var aboutYourself : String = ""
    var address : String = ""
    var allowingYou : String = ""
    var anyAllergies : String = ""
    var anyAllergiesStatus = false
    var areYouA : String = ""
    var city : String = ""
    var countryCode : String = ""
    var distance : String = ""
    var distanceInvisible : String = ""
    var doYouHaveKids : String = ""
    var doYouSmoke : String = ""
    var doYouDrink : String = ""
    var doYouTakeDrug : String = ""
    var doYouWantKids : String = ""
    var dob : String = ""
    var dontShowMyAge : String = ""
    var email : String = ""
    var foodPreference : String = ""
    var foodatextBoost : String = ""
    var fullname : String = ""
    var height : String = ""
    var hideAdverts : String = ""
    var interestIn : String = ""
    var latitude : String = ""
    var longitude : String = ""
    var lookingFor : String = ""
    var maritalStatus : String = ""
    var personality : String = ""
    var phone : String = ""
    var profileImage : String = ""
    var splitBill : String = ""
    var unlimitedLikes : String = ""
    var userId : String = ""
    var username : String = ""
    var whatDoYou : String = ""
    var whoSeeYou : String = ""
}
