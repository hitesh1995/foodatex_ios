//
//  DateHelper.swift
//  FOODATEX
//
//  Created by Hitesh Prajapati on 07/06/20.
//  Copyright © 2020 RudraApps. All rights reserved.
//

import UIKit

var utCdateFormatter = DateFormatter()

func getUTCDate(from strDate: String?, format Strformat: String?) -> Date? {
    utCdateFormatter.dateFormat = Strformat
    let date = utCdateFormatter.date(from: strDate ?? "")
    return date
}

func getLocalTime(fromWS strUTCDate: String?, str strUTCStartTime: String?) -> NSDictionary {
    var UTCdate: Date?
    
    UTCdate = getUTCDate(from: "\(strUTCDate ?? "") \(strUTCStartTime ?? "")", format: "yyyy-MM-dd HH:mm:ss")
    
    let strReturnDate = getStringFrom(UTCdate, format: "yyyy-MM-dd") ?? ""
    let strReturnTime = getStringFrom(UTCdate, format: "HH:mm:ss") ?? ""
    
    let dic = [
        "date": strReturnDate,
        "time": strReturnTime
    ]
    
    return dic as NSDictionary
}

func getStringFrom(_ dateval: Date?, format Strformat: String?) -> String? {
    utCdateFormatter.dateFormat = Strformat
    var dateString: String? = nil
    if let dateval = dateval {
        dateString = utCdateFormatter.string(from: dateval)
    }
    return dateString
}

func getDateFrom(_ str: String?, givenFormat strGivenFormat: String?, returnFormat strReturnFormat: String?) -> String? {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = strGivenFormat ?? "" // here give the format which you get in TimeStart
    let date: Date? = dateFormatter.date(from: str ?? "")
    return getStringFrom(date, format: strReturnFormat)
}

func getHeaderDate(_ str: String?, givenFormat strGivenFormat: String?, returnFormat strReturnFormat: String?) -> String? {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = strGivenFormat ?? "" // here give the format which you get in TimeStart
    let date: Date? = dateFormatter.date(from: str ?? "")
    let strDate = getStringFrom(date, format: strReturnFormat)
    
    guard let todayDate = dateFormatter.date(from: str!)
        else { return strDate }
    let myCalendar = Calendar(identifier: .gregorian)
    let weekDay = myCalendar.component(.weekday, from: todayDate)
    
    return String(format: "%@ : %@", getDay(weekDay: weekDay), strDate!)
}

func getDay(weekDay : Int) -> String{
    switch weekDay {
    case 1:
        return "Sun"
    case 2:
        return "Mon"
    case 3:
        return "Tue"
    case 4:
        return "Wed"
    case 5:
        return "Thu"
    case 6:
        return "Fri"
    case 7:
        return "Sat"
    default:
        return ""
    }
}

