//
//  Constant.swift
//  FOODATEX
//
//  Created by Silver Shark on 07/08/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import SystemConfiguration
import AVFoundation
import FirebaseDatabase

//MARK:- Application related variables

let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate
public let APP_NAME: String = "FOODATES"
public let APP_VERSION: String = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String

//MARK: - URLS
//let IMAGE_BASE_URL = "http://jobandoffers.com/demo/foodatex/upload/images/" // Old Image Url
let IMAGE_BASE_URL = "http://139.59.35.213/foodatex/upload/images/" // Old Image Url

//let BASE_URL = "http://jobandoffers.com/demo/foodatex/" //Old Url
let BASE_URL = "http://139.59.35.213/foodatex/" // New URL
let SERVER_URL = "\(BASE_URL)api/"
let KEY_FB_SECRET = "6b37ffd4537a72b20407da6d5dca3c80"
let INAppReceiptID = "63ff0d24e5244765809f69a1014062f8"

let GoogleAdTestUnitID = "ca-app-pub-3940256099942544/4411468910"
let GoogleAdUnitID  =   "ca-app-pub-8689193388192023/1699659209"

let monthlySubscriptionId = "com.foodatex.goldOnemonth"
let sixMonthSubscriptionId = "com.foodatex.silverSixmonth"
let twelveMonthSubscriptionId = "com.Foodatex.GoldTwelveMonth"

let ChatListNode = "ChatList"
let ChatListChildNode = "User"
let chatParentNode = "Chats"
let statusParentNode = "Status"

let pendingSwipes = "pendingSwipes"
let USERDEFAULT = UserDefaults.standard

struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}
//MARK: - Check devices
let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
 let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
 let IS_IPHONE_6_7          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0

 let IS_IPHONE_6P_7P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
 let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
 let IS_IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0



//MARK: - IS SIMULATOR
public let isSimulator: Bool = {
    var isSim = false
    #if arch(i386) || arch(x86_64)
    isSim = true
    #endif
    return isSim
}()

public let isiPhoneX : Bool = {
    if UIDevice().userInterfaceIdiom == .phone {
        switch UIScreen.main.nativeBounds.height {
        case 2436:
            return true
        default:
            return false
        }
    }
    return false
}()

//MARK: - APPKEYS
struct  AppKeys{
    struct  UD{
        static let kUser : String = "AppUser"
        static let kAPNSToken : String = "App device Token"
        static let kProfileUpdated : String = "UserProfileUpdated"
        static let kProfileImage : String = ""
    }
}

//MARK: - STORYBORAD NAME
struct  AppStoryboards{
    static let kSBLogin : String = "SB_Login"
    static let kSBMain :  String = "SB_Main"
    static let kSBChat : String = "SB_Chat"
}

//MARK: -  VIEW CONTROLLER  ID
struct  AppViewControllers{
    
    static let kVCLogin : String = "idLoginVC"
    static let kVCHome : String = "idHomeVC"
    static let kVCProfileDetail : String = "idProfileDetailsVC"
    static let kVCMyProfile : String = "idMyProfileVC"
    static let kVCMyProfileDetailVC : String = "idMyProfileDetailVC"
    static let kVCSettingVC : String = "idSettingVC"
    static let kVCBlockListVC : String = "idBlockListVC"
    static let kPhoneVerificationVC : String = "idPhoneVerificationVC"
    
    struct Chat {
        static let kVCChatList : String = "idChatListVC"
        static let kVCChat : String = "idChatVC"
    }
    
    struct InfoPage {
        static let kVCInfoOne : String = "idInfoPageOneVC"
        static let kVCInfoTwo : String = "idInfoPageTwo"
        static let kVCInforThree : String = "idInfoPageThreeVC"
        static let kVCInfoFour : String = "idInfoPageFourVC"
        static let kVCInfoFive : String = "idInfoPageFiveVC"
        static let kVCInfoSix : String = "idInfoPageSixVC"
        static let kVCInfoSeven : String = "idInfoPageSevenVC"
        static let kVCInfoEight : String = "idInfoPageEightVC"
    }
}

//MARK: - APP NAVIGATION
struct AppNavigation {
    static let kNavLogin : String = "navLogin"
    static let kNavHome : String = "navHome"
}
//MARK: -  GET VC FOR NAVIGATION
public func getStoryboard(_ storyboardName: String) -> UIStoryboard {
    return UIStoryboard(name: "\(storyboardName)", bundle: nil)
}

public func loadVC(_ strStoryboardId: String, strVCId: String) -> UIViewController {
    return getStoryboard(strStoryboardId).instantiateViewController(withIdentifier: strVCId)
}

/** Get image from image name */
//MARK: - Get image from image name
public func Set_Local_Image(_ imageName :String) -> UIImage{
    return UIImage(named:imageName)!
}

//MARK: - iOS version checking Functions
public func IOS_VERSION() -> String {
    return UIDevice.current.systemVersion
}

public func IOS_EQUAL_TO(_ xx: Float) -> Bool {
    return IOS_VERSION().compare(String(xx), options: .numeric) == .orderedSame
}

public func IOS_OLDER_THAN_X(_ xx: Float) -> Bool{
    return  (IOS_VERSION)().floatValue < xx
}

public func IOS_NEWER_OR_EQUAL_TO_X(_ xx: Float) -> Bool{
    return (IOS_VERSION()).floatValue >= xx
}


//MARK: - Get full screen size
public func fixedScreenSize() -> CGSize {
    let screenSize = UIScreen.main.bounds.size
    if NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1 && UIInterfaceOrientationIsLandscape(UIApplication.shared.statusBarOrientation) {
        return CGSize(width: screenSize.height, height: screenSize.width)
    }
    return screenSize
}

public func SCREENWIDTH() -> CGFloat{
    return fixedScreenSize().width
}

public func SCREENHEIGHT() -> CGFloat{
    return fixedScreenSize().height
}


//MARK: - Network indicator
public func ShowNetworkIndicator(_ xx :Bool){
    UIApplication.shared.isNetworkActivityIndicatorVisible = xx
}

/**
 Color functions
 */
//MARK: - Color functions
public func COLOR_CUSTOM(_ Red: Float, _ Green: Float , _ Blue: Float, _ Alpha: Float) -> UIColor {
    return  UIColor (red:  CGFloat(Red)/255.0, green: CGFloat(Green)/255.0, blue: CGFloat(Blue)/255.0, alpha: CGFloat(Alpha))
}


public func UIColorFromRGB(_ rgbValue: UInt) -> UIColor {
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

public func randomColorWithOpacity( _ alpha : CGFloat) -> UIColor {
    let r: UInt32 = arc4random_uniform(255)
    let g: UInt32 = arc4random_uniform(255)
    let b: UInt32 = arc4random_uniform(255)
    return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: alpha)
}


/**
 Log trace
 */
//MARK: - Log trace
public func DLog<T>(_ message:T,  file: String = #file, function: String = #function, lineNumber: Int = #line ) {
    #if DEBUG
    if let text = message as? String {
        print("\((file as NSString).lastPathComponent) -> \(function) line: \(lineNumber): \(text)")
    }
    #endif
}


/**
 Check string is available or not
 */
//MARK: - Check string is available or not
public func isLike(_ source: String , compare: String) ->Bool{
    var exists = true
    ((source).lowercased().range  (of: compare as String) != nil) ? (exists = true) :  (exists = false)
    return exists
}


/**
 Calculate heght of label
 */
//MARK: - Calculate heght of label
public func calculatedHeight(_ Label: UILabel) -> CGFloat {
    let text: String = Label.text!
    return text.heightForWidth(Label.frame.size.width, font: Label.font)
}


/**
 Get current time stamp
 */
//MARK: - Get current time stamp
public var CurrentTimeStamp: String {
    return "\(Date().timeIntervalSince1970 * 1000)"
}

//MARK: NULL to NIL
public func NULL_TO_NIL(_ value : Any?) -> Any? {
    if value is NSNull || value == nil{
        return "" as Any
    } else {
        return value
    }
}


//MARK:- Random string Generator
/*
 Generate random string with specified length
 */
func randomString(_ length: Int, justLowerCase: Bool = false) -> String {
    
    var text = ""
    for _ in 1...length {
        var decValue = 0  // ascii decimal value of a character
        var charType = 3  // default is lowercase
        if justLowerCase == false {
            // randomize the character type
            charType =  Int(arc4random_uniform(4))
        }
        switch charType {
        case 1:  // digit: random Int between 48 and 57
            decValue = Int(arc4random_uniform(10)) + 48
        case 2:  // uppercase letter
            decValue = Int(arc4random_uniform(26)) + 65
        case 3:  // lowercase letter
            decValue = Int(arc4random_uniform(26)) + 97
        default:  // space character
            decValue = 32
        }
        // get ASCII character from random decimal value
        let char = String(describing: UnicodeScalar(decValue))
        text = text + char
        // remove double spaces
        text = text.replacingOccurrences(of: "  ", with: "D")
        text = text.replacingOccurrences(of: " ", with: "T")
    }
    text = text.replacingOccurrences(of: ".", with: "")
    return text
}


//MARK: - Check Internet connection
func isConnectedToNetwork() -> Bool {
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    }
    var flags = SCNetworkReachabilityFlags()
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
        return false
    }
    let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
    let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
    return (isReachable && !needsConnection)
}


//MARK: - ISIPAD
func isiPad()->Bool{
    return UIDevice.current.userInterfaceIdiom == .pad ? true : false
}

//MARK: - GET UDID
func getUDID() -> String {
    var UDID = ""
    let theUUID : CFUUID = CFUUIDCreate(nil);
    let string : CFString = CFUUIDCreateString(nil, theUUID);
    UDID =  string as String
    return UDID
}


//MARK: - APP FONT
func APPFONT_REGULAR(_ size:CGFloat) -> UIFont{
    return UIFont(name: "CenturyGothic", size: size)!
}

func APPFONT_BOLD(_ size:CGFloat) -> UIFont{
    return UIFont(name: "CenturyGothic-Bold", size: size)!
}

func printFonts() {
    let fontFamilyNames = UIFont.familyNames
    for familyName in fontFamilyNames {
        print("------------------------------")
        print("Font Family Name = [\(familyName)]")
        let names = UIFont.fontNames(forFamilyName: familyName)
        print("Font Names = [\(names)]")
    }
}

//MARK: - CUSTOM ALERT
func showAppAlertWithMessage(_ message:String, viewController : UIViewController?)  {
    let noInternet = UIAlertController(title: APP_NAME, message: message, preferredStyle: .alert)
    let ok = UIAlertAction(title: "Dismiss", style: .default) { (_) in
    }
    noInternet.addAction(ok)
    if viewController == nil {
        APP_DELEGATE.window?.rootViewController?.present(noInternet, animated: true, completion: nil)
    }else{
        viewController?.present(noInternet, animated: true, completion: nil)
    }
}
func hasInAppSubscribed()->Bool{
    
    /*
    let expiryDate = getLoginDetails()?.isPurchase

    if (expiryDate?.isEmpty)!{
        return false
    }
    
    let currentDateFormatter = DateFormatter()
    // initially set the format based on your datepicker date / server String
    currentDateFormatter.dateFormat = "yyyy-MM-dd"
    let currDate = currentDateFormatter.string(from: Date())
    
    
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale(identifier: "en_GB")
    //  dateFormatter.setLocalizedDateFormatFromTemplate("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    dateFormatter.dateFormat = "yyyy-MM-dd"
    
    let dateformatter1 = DateFormatter()
    dateformatter1.locale = Locale(identifier: "en_GB")
    dateformatter1.dateFormat = "yyyy-MM-dd"
    // dateformatter1.setLocalizedDateFormatFromTemplate("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    
    let expdate = dateFormatter.date(from: expiryDate!)
    let currentDate = dateformatter1.date(from: currDate)
    if expdate! < currentDate!{
        //expired
        return false
    }else{
        // not expired
        return true
    }
    */
    
    if UserDefaults.standard.bool(forKey: monthlySubscriptionId) == true{
        return true
    }
    else if UserDefaults.standard.bool(forKey: sixMonthSubscriptionId) == true{
        return true
    }
    else if UserDefaults.standard.bool(forKey: twelveMonthSubscriptionId) == true{
        return true
    }
    return false
}
//func isExpiredWith(purchasedDate:String)->Bool{
//    let dateFormatter = DateFormatter()
//    dateFormatter.locale = Locale(identifier: "en_GB")
//    //  dateFormatter.setLocalizedDateFormatFromTemplate("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
//    dateFormatter.dateFormat = "yyyy-MM-dd"
//    
//    
//    let dateformatter1 = DateFormatter()
//    dateformatter1.locale = Locale(identifier: "en_GB")
//    dateformatter1.dateFormat = "yyyy-MM-dd"
//    // dateformatter1.setLocalizedDateFormatFromTemplate("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
//    let currentDate = Date()
//    let purchaseDate = dateFormatter.date(from: purchasedDate)
//    let currentDate = dateformatter1.date(from: currentDate)
//    if purchaseDate! < currentDate!{
//        //expired
//        return true
//    }else{
//        // not expired
//        return false
//    }
//    
//    //        if (expdate?.timeIntervalSince(serverTime!).sign == .plus) {
//    //            // date is in future
//    //            print("not expired")
//    //            return false
//    //        }else{
//    //            print("expired")
//    //            return true
//    //        }
//}

func showNoInternetAlert()  {
    let noInternet = UIAlertController()
    let ok = UIAlertAction(title: "Dismiss", style: .default) { (_) in
    }
    noInternet.addAction(ok)
    noInternet.title = APP_NAME
    noInternet.message = "No internet available at this moment, Please check your network connection"
    APP_DELEGATE.window?.rootViewController?.present(noInternet, animated: true, completion: nil)
}

func showAlert(vc : UIViewController, message : String)  {
    let noInternet = UIAlertController(title: APP_NAME, message: message, preferredStyle: .alert)
    noInternet.addAction(UIAlertAction(title: "OK", style: .default) { (_) in})
    vc.present(noInternet, animated: true, completion: nil)
}

func getLoginDetails() -> UserData? {
    if let userData = UserDefaults.standard.object(forKey: AppKeys.UD.kUser) as? Data {
        if let decode = try? JSONDecoder().decode(UserData.self, from: userData) {
            return decode
        }
    }
    return nil
}

func setLoginDetails(_ dictionary: UserData?){
    if dictionary != nil {
        if let encode = try? JSONEncoder().encode(dictionary) {
            UserDefaults.standard.set(encode, forKey:AppKeys.UD.kUser)
            UserDefaults.standard.synchronize()
        }
    }
}

func compressVideo(_ inputURL: URL?, outputURL: URL?, handler completion: @escaping (AVAssetExportSession?) -> Void) {
    var urlAsset: AVURLAsset? = nil
    if let inputURL = inputURL {
        urlAsset = AVURLAsset(url: inputURL, options: nil)
    }
    var exportSession: AVAssetExportSession? = nil
    if let urlAsset = urlAsset {
        exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality)
    }
    exportSession?.outputURL = outputURL
    exportSession?.outputFileType = .mov
    exportSession?.shouldOptimizeForNetworkUse = true
    exportSession?.exportAsynchronously(completionHandler: {
        completion(exportSession)
    })
}


func setOnlineStatus(status : String){
    if let userID = getLoginDetails()?.userId{
        let statusNode = "user\(userID)"
        let detail = OnlineStatus(dict: NSDictionary(dictionary: [StatusKeys.date : "\(Date())", StatusKeys.isOnline: status])).toAny()
        
        let dbRef = APP_DELEGATE.FIRDBReference.child(statusParentNode).child(statusNode)
        dbRef.setValue(detail) { (error, ref) in
            if error == nil{
                //print("status updated..")
            }
            else{
                print("Update fail..", error!)
            }
        }
    }
}
