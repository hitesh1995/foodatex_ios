//
//  Message.swift
//  FOODATEX
//
//  Created by Silver Shark on 08/08/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import Foundation
struct AppMessages {
    static let FB_Permission : String = "Please give permission to access you Emai and Public Profile to continue login"
    static let FB_Error : String = "Unable to get User from Facebook, Please check permission given to FOODATES"
    static let User_Error : String = "Unable to get User around"
    static let Error_Message : String = "Something went wrong. Please try again."
}
