//
//  WebServicePrefix.swift
//
//  Created by Devang Tandel  on 09/01/17.
//  Copyright © 2017 Devang Tandel. All rights reserved.
//

import Foundation

public enum WSRequestType : Int {
    case SocialRegister
    case Login
    case ForgotPassword
    case Logout
    case FoodPreferences
    case userslistnearby
    case SearchUsers
    case UpdateProfile
    case sendMessage
    case ChangePassword
    case DeleteUserImage
    case CheckLoginwithphone
    case Unmatch
    case PackageList
    case FollowingList
    case MyFollowerFeed
    case AddFeedView
    case DeleteFeedComment
    case GetUserDetail
    case swipeuser
    case chatlist
    case Chats
    case WelcomeScreen
    case DeleteAccount
    case BlockUser
    case UnblockUser
    case ReportUser
    case BlockList
}

public enum RESPONSE_STATUS : NSInteger {
    case INVALID
    case VALID
    case MESSAGE
}

struct WebServicePrefix {
        static func GetWSUrl(_ serviceType :WSRequestType) -> NSString {
        var serviceURl: NSString?
            
        switch serviceType {
        case .SocialRegister:
            serviceURl = "Registration/UserRegistration"
            break
        case .Login:
            serviceURl = "Registration/CheckLogin"
            break
        case .ForgotPassword:
            serviceURl = "Registration/ForgotPassword"
            break
        case .Logout:
            serviceURl = "Registration/Logout"
            break
        case .FoodPreferences :
            serviceURl = "Registration/FoodPreferences"
            break
        case .userslistnearby:
            serviceURl = "Users/userslistnearby"
            break
        case .SearchUsers:
            serviceURl = "Registration/SearchUsers"
            break
        case .UpdateProfile:
            serviceURl = "Registration/UpdateProfile"
            break
        case .chatlist:
            serviceURl = "users/chatlist"
            break
        case .sendMessage:
            serviceURl = "users/sendmessage"
            break
        case .ChangePassword:
            serviceURl = "Registration/ChangePassword"
            break
        case .DeleteUserImage:
            serviceURl = "Registration/DeleteUserImage"
            break
        case .CheckLoginwithphone:
            serviceURl = "Registration/CheckLoginwithphone"
            break
        case .Unmatch:
            serviceURl = "users/Unmatch"
            break
        case .PackageList:
            serviceURl = "Registration/PackageList"
            break
        case .FollowingList:
            serviceURl = "Follower/FollowingList"
            break
        case .MyFollowerFeed:
            serviceURl = "feed/MyFollowerFeed"
            break
        case .AddFeedView:
            serviceURl = "feed/AddFeedView"
            break
        case .DeleteFeedComment:
            serviceURl = "feed/DeleteFeedComment"
            break
        case .GetUserDetail:
            serviceURl = "Registration/GetUserDetail"
            break
        case .swipeuser:
            serviceURl = "users/swipeuser"
            break
        case .Chats:
            serviceURl = "users/chatdetail"
            break
        case .WelcomeScreen:
            serviceURl = "Registration/WelcomeScreen"
            break
        case .DeleteAccount:
            serviceURl = "Registration/DeleteUser"
            break
        case .BlockUser :
            serviceURl = "Registration/Blockuser"
            break
        case .UnblockUser :
            serviceURl = "Registration/Unblockuser"
            break
        case .ReportUser :
            serviceURl = "Registration/ReportUser"
            break
        case .BlockList :
            serviceURl = "Registration/Blockuserlists"
            break
        }
        return "\(SERVER_URL)\(serviceURl!)" as NSString
    }
}

