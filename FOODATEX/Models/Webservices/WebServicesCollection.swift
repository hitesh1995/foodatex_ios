//
//  WebServicesCollection.swift
//
//  Created by Devang Tandel  on 09/01/17.
//  Copyright © 2017. All rights reserved.
//

import Foundation
import Alamofire
import CoreLocation

class WebServicesCollection {
    
    static let sharedInstance = WebServicesCollection()
    
    // METHODS
    init() {}
    
    /*//MARK: - Get Countries
    func countries(_ responce:@escaping (_ data:Any?, _ error: NSError?, _ message: String?) -> Void){
        
        let URL =  (WebServicePrefix.GetWSUrl(WSRequestType.countries)) as String
        HttpRequestManager.sharedInstance.getRequest(URL, dataKey: "data", parameters: [String : Any]()) { (data, response,error, message, rstatus) in
            responce(data, error, message)
        }
    }*/

    
    //MARK: - REGISTER
    func SocialRegister(_ dict:[String : Any], response:@escaping (_ data:Any?, _ error:NSError?, _ message:String?) ->Void) {
        let URL = (WebServicePrefix.GetWSUrl(WSRequestType.SocialRegister)) as String
        HttpRequestManager.sharedInstance.postJSONRequest(URL, parameters: dict) { (data, respponse, error, message, rstatus)  in
             response(respponse, error, message)
        }
    }
    
    //MARK: - LOGIN WITH MOBILE
    func loginWithMobile(_ dict:[String : Any], response:@escaping (_ data:Any?, _ error:NSError?, _ message:String?) ->Void) {
        let URL = (WebServicePrefix.GetWSUrl(WSRequestType.CheckLoginwithphone)) as String
        HttpRequestManager.sharedInstance.postJSONRequest(URL, parameters: dict) { (data, respponse, error, message, rstatus)  in
            response(respponse, error, message)
        }
    }
    
    //MARK: - UpdateProfile
    func UpdateProfile(_ dict:[String : Any], response:@escaping (_ data:Any?, _ error:NSError?, _ message:String?, _ rstatus : Int) ->Void) {
        let URL = (WebServicePrefix.GetWSUrl(WSRequestType.UpdateProfile)) as String
        HttpRequestManager.sharedInstance.postJSONRequest(URL, parameters: dict) { (data, respponse, error, message, rstatus)  in
            response(respponse, error, message, rstatus)
        }
    }
    
    //MARK: - DeletUserImage
    func deleteUserImage(_ dict:[String : Any], response:@escaping (_ data:Any?, _ error:NSError?, _ message:String?, _ rstatus : Int) ->Void) {
        let URL = (WebServicePrefix.GetWSUrl(WSRequestType.DeleteUserImage)) as String
        HttpRequestManager.sharedInstance.postJSONRequest(URL, parameters: dict) { (data, respponse, error, message, rstatus)  in
            response(respponse, error, message, rstatus)
        }
    }
    
    //MARK: - FoodPreferences
    func FoodPreferences( response:@escaping (_ data:Any?, _ error:NSError?, _ message:String?) ->Void) {
        let URL = (WebServicePrefix.GetWSUrl(WSRequestType.FoodPreferences)) as String
        HttpRequestManager.sharedInstance.getRequestWithoutParams(URL) { (data, respponse, error, message)  in
            response(respponse, error, message)
        }
    }
    
    
    //MARK: - userslistnearby
    func userslistnearby(_ dict:[String : Any], response:@escaping (_ data:Any?, _ error:NSError?, _ message:String?) ->Void) {
        let URL = (WebServicePrefix.GetWSUrl(WSRequestType.userslistnearby)) as String
        HttpRequestManager.sharedInstance.postJSONRequest(URL, parameters: dict) { (data, respponse, error, message, rstatus)  in
            response(respponse, error, message)
        }
    }
    
    //MARK: - GET USER PROFILE DETAILS
    func GetUserDetails(_ dict:[String : Any], response:@escaping (_ data:Any?, _ error:NSError?, _ message:String?) ->Void) {
        let URL = (WebServicePrefix.GetWSUrl(WSRequestType.GetUserDetail)) as String
        HttpRequestManager.sharedInstance.postJSONRequest(URL, parameters: dict) { (data, respponse, error, message, rstatus)  in
            response(respponse, error, message)
        }
    }
    
    
    //MARK: - Search User
    func SearchUsers(_ dict:[String : Any], response:@escaping (_ data:Any?, _ error:NSError?, _ message:String?) ->Void) {
        let URL = (WebServicePrefix.GetWSUrl(WSRequestType.SearchUsers)) as String
        HttpRequestManager.sharedInstance.postJSONRequest(URL, parameters: dict) { (data, respponse, error, message, rstatus)  in
            response(respponse, error, message)
        }
    }
    
    //MARK: - Swipe User
    func swipeuser(_ dict:[String : Any], response:@escaping (_ data:Any?, _ error:NSError?, _ message:String?) ->Void) {
        let URL = (WebServicePrefix.GetWSUrl(WSRequestType.swipeuser)) as String
        HttpRequestManager.sharedInstance.postJSONRequest(URL, parameters: dict) { (data, respponse, error, message, rstatus)  in
            response(respponse, error, message)
        }
    }
    //MARK: - Unmatch User
    func deleteAccount(_ dict:[String : Any], response:@escaping (_ data:Any?, _ error:NSError?, _ message:String?) ->Void) {
        let URL = (WebServicePrefix.GetWSUrl(WSRequestType.DeleteAccount)) as String
        HttpRequestManager.sharedInstance.postJSONRequest(URL, parameters: dict) { (data, respponse, error, message, rstatus)  in
            response(respponse, error, message)
        }
    }
    //MARK: - Unmatch User
    func Unmatch(_ dict:[String : Any], response:@escaping (_ data:Any?, _ error:NSError?, _ message:String?) ->Void) {
        let URL = (WebServicePrefix.GetWSUrl(WSRequestType.Unmatch)) as String
        HttpRequestManager.sharedInstance.postJSONRequest(URL, parameters: dict) { (data, respponse, error, message, rstatus)  in
            response(respponse, error, message)
        }
    }
    
    //MARK: - Get ChatList
    func getChatList(_ dict:[String : Any], response:@escaping (_ data:Any?, _ error:NSError?, _ message:String?) ->Void) {
        let URL = (WebServicePrefix.GetWSUrl(WSRequestType.chatlist)) as String
        HttpRequestManager.sharedInstance.postJSONRequest(URL, parameters: dict) { (data, respponse, error, message, rstatus)  in
            response(respponse, error, message)
        }
    }
    
    //MARK: - Get Chats
    func getChat(_ dict:[String : Any], response:@escaping (_ data:Any?, _ error:NSError?, _ message:String?) ->Void) {
        let URL = (WebServicePrefix.GetWSUrl(WSRequestType.Chats)) as String
        HttpRequestManager.sharedInstance.postJSONRequest(URL, parameters: dict) { (data, respponse, error, message, rstatus)  in
            response(respponse, error, message)
        }
    }
    
    //MARK: - Send Message
    func sendMessage(_ dict:[String : Any], response:@escaping (_ data:Any?, _ error:NSError?, _ message:String?) ->Void) {
        let URL = (WebServicePrefix.GetWSUrl(WSRequestType.sendMessage)) as String
        HttpRequestManager.sharedInstance.postJSONRequest(URL, parameters: dict) { (data, respponse, error, message, rstatus)  in
            response(respponse, error, message)
        }
    }
    
    func uploadImage(_ dict:[String : Any], filePath : [URL], isImage : Bool ,response:@escaping (_ data : Any?, _ error:NSError?, _ message:String?) -> Void) {
        
        let url = "\(BASE_URL)upload/upload.php"
        HttpRequestManager.sharedInstance.uploadImages(endpointurl: url, parameters: dict, filePaths: filePath, isImage: isImage) { (error, message, rresponse, rstatus) in
            response(rresponse, error, message)
        }
    }
    
    
    //MARK: - PackageList
    ///Get Package List of Available in App Purchase
    func PackageList( response:@escaping (_ data:Any?, _ error:NSError?, _ message:String?) ->Void) {
        let URL = (WebServicePrefix.GetWSUrl(WSRequestType.PackageList)) as String
        HttpRequestManager.sharedInstance.getRequestWithoutParams(URL) { (data, respponse, error, message)  in
            response(respponse, error, message)
        }
    }
    
    //WelcomeScreen
    func WelcomeScreenList( response:@escaping (_ data:Any?, _ error:NSError?, _ message:String?) ->Void) {
        let URL = (WebServicePrefix.GetWSUrl(WSRequestType.WelcomeScreen)) as String
        HttpRequestManager.sharedInstance.getRequestWithoutParams(URL) { (data, respponse, error, message)  in
            response(respponse, error, message)
        }
    }
    
    //MARK:- Block User
    func blockUser(_ dict:[String : Any], response:@escaping (_ data:Any?, _ error:NSError?, _ message:String?) ->Void) {
        let URL = (WebServicePrefix.GetWSUrl(WSRequestType.BlockUser)) as String
        HttpRequestManager.sharedInstance.postJSONRequest(URL, parameters: dict) { (data, respponse, error, message, rstatus)  in
            response(respponse, error, message)
        }
    }
    
    //MARK:- Unblock User
    func unBlockUser(_ dict:[String : Any], response:@escaping (_ data:Any?, _ error:NSError?, _ message:String?) ->Void) {
        let URL = (WebServicePrefix.GetWSUrl(WSRequestType.UnblockUser)) as String
        HttpRequestManager.sharedInstance.postJSONRequest(URL, parameters: dict) { (data, respponse, error, message, rstatus)  in
            response(respponse, error, message)
        }
    }

    //MARK:- Report User
    func reportUser(_ dict:[String : Any], response:@escaping (_ data:Any?, _ error:NSError?, _ message:String?) ->Void) {
        let URL = (WebServicePrefix.GetWSUrl(WSRequestType.ReportUser)) as String
        HttpRequestManager.sharedInstance.postJSONRequest(URL, parameters: dict) { (data, respponse, error, message, rstatus)  in
            response(respponse, error, message)
        }
    }
    
    //MARK:- Block LIst
    func getBlockList(_ dict:[String : Any], response:@escaping (_ data:Any?, _ error:NSError?, _ message:String?) ->Void) {
        let URL = (WebServicePrefix.GetWSUrl(WSRequestType.BlockList)) as String
        HttpRequestManager.sharedInstance.postJSONRequest(URL, parameters: dict) { (data, respponse, error, message, rstatus)  in
            response(respponse, error, message)
        }
    }
}
