//
//  HttpRequestManager.swift
//
//  Created by Devang Tandel  on 09/01/17.
//  Copyright © 2017 Devang. All rights reserved.
//

import Foundation
import Alamofire
import SVProgressHUD

class HttpRequestManager {
    
    static let sharedInstance = HttpRequestManager()
    
    var responseObjectDic = Dictionary<String, Any>()
    var URLString : String!
    var Message : String!
    var resObjects:Any!
    
    // METHODS
    init() {}
    
    //MARK:- POST Request
    func postParameterRequest(_ endpointurl:String, parameters:NSDictionary,responseData:@escaping (_ data: Any?, _ error: NSError?, _ message: String?, _ status : Int) -> Void){
        
        ShowNetworkIndicator(true)
        Alamofire.request(endpointurl, method: .post , parameters: parameters as? [String: Any] , encoding: URLEncoding.default)
            .responseJSON { response in
                ShowNetworkIndicator(false)
                print(response)
                print(response.request ?? "")
                if let _ = response.result.error {
                    responseData(nil, response.result.error! as NSError, nil, 0)
                }else{
                    switch response.result{
                    case .success:
                        guard let responseJSON:NSDictionary = response.result.value as? NSDictionary else {
                            responseData(nil, nil,"Something went wrong, Please try again later!", 0)
                            return
                        }
                        self.Message = responseJSON["message"] as? String
                        switch (NSInteger(responseJSON["status"] as! String)!) {
                        case RESPONSE_STATUS.VALID.rawValue :
                            self.resObjects = (response.result.value as! [AnyHashable: Any])["data"]! as Any
                            break
                        case RESPONSE_STATUS.INVALID.rawValue :
                            self.resObjects = nil
                            break
                        default :
                            break
                        }
                        responseData(self.resObjects, nil, self.Message, NSInteger(responseJSON["status"] as! String)!)
                    case .failure(let error):
                        responseData(nil, error as NSError, nil, 0)
                    }
                }
        }
    }
    
    func postJSONRequest(_ endpointurl:String, parameters:[String : Any],responseData:@escaping (_ data: Any?, _ response:Any?,_ error: NSError?, _ message: String?, _ rstatus : Int) -> Void){
        
        ShowNetworkIndicator(true)
        Alamofire.request(endpointurl, method: .post , parameters: ["data" : parameters] , encoding: JSONEncoding.default)
            .responseJSON { response in
                ShowNetworkIndicator(false)
//                print(response.request ?? "")
//                 print(parameters)
//                print(response)
                if let _ = response.result.error {
                    responseData(nil, nil, response.result.error! as NSError, "Something went wrong",0)
                }else{
                    switch response.result {
                    case .success:
                        guard let responseJSON:NSDictionary = response.result.value as? NSDictionary else {
                            print("Invalid tag information received from the service")
                            responseData(nil, nil, nil, "Something went wrong", 0)
                            return
                        }
                        var respStatus = 0
                        if let rst : String = responseJSON["status"] as? String{
                            respStatus = Int(rst)!
                        }
                        if let  msgString : String = responseJSON["message"] as? String {
                            self.Message = msgString
                        }else{
                            self.Message = "Something went wrong"
                        }
                        responseData(response, response.data ,nil, self.Message, respStatus)
                    case .failure(let error):
                        print(error)
                        responseData(nil, nil , error as NSError, error.localizedDescription, 0)
                    }
                }
        }
    }
    

    func postJSONTokenRequest(_ endpointurl:String, parameters:NSDictionary,responseData:@escaping (_ data: Any?, _ error: NSError?, _ message: String?) -> Void){
        
        ShowNetworkIndicator(true)
        
        Alamofire.request(endpointurl, method: .post , parameters: parameters as? [String: Any] , encoding: URLEncoding.default)
            .responseJSON { response in
                
                ShowNetworkIndicator(false)
                
                if let _ = response.result.error {
                    responseData(nil, response.result.error! as NSError, "Something went wrong, Please try again")
                }else{
                    switch response.result {
                    case .success:
                        self.Message = ""
                        self.resObjects = (response.result.value as Any)
                        responseData(self.resObjects, nil, self.Message)
                    case .failure(let error):
                        responseData(nil, error as NSError, error.localizedDescription)
                    }
                }
        }
    }
    
    //MARK:- GET Request
    func getRequestWithoutParams(_ endpointurl:String,responseData:@escaping (_ data: Any?, _ response:Any?,_ error: NSError?, _ message: String?) -> Void){
        
        ShowNetworkIndicator(true)
        Alamofire.request(endpointurl , method:.get, encoding: JSONEncoding.default)
            .responseJSON { response in
                ShowNetworkIndicator(false)
                if let _ = response.result.error{
                    responseData(nil, nil, response.result.error! as NSError, "Something went wrong")
                }else{
                    switch response.result {
                    case .success:
                        guard let responseJSON:NSDictionary = response.result.value as? NSDictionary else {
                            print("Invalid tag information received from the service")
                            responseData(nil, nil, nil, "Something went wrong")
                            return
                        }
                        
                        if let  msgString : String = responseJSON["message"] as? String {
                            self.Message = msgString
                        }else{
                            self.Message = "Something went wrong"
                        }
                        guard let  dict = responseJSON["data"] else {
                            responseData(nil, nil, response.result.error! as NSError, self.Message)
                            return
                        }
                        self.resObjects = dict as Any
                        responseData(self.resObjects, response.data ,nil, self.Message)
                        print("packageListResponse==",response)
                    case .failure(let error):
                        print(error)
                        responseData(nil, nil , error as NSError, error.localizedDescription)
                    }
                }
        }
    }
    
    //MARK:- GET Request
    func getRequest(_ endpointurl:String,dataKey: String, parameters:NSDictionary,responseData:@escaping (_ data: Any?, _ response:Any?,_ error: NSError?, _ message: String?, _ rstatus: Bool) -> Void){
        
        ShowNetworkIndicator(true)
        Alamofire.request(endpointurl, method: .get , parameters: parameters as? [String: Any], encoding: URLEncoding.default)
            .responseJSON { response in
                print(response)
                ShowNetworkIndicator(false)
                if let _ = response.result.error {
                    responseData(nil, nil, response.result.error as NSError?, "Something went wrong, Please try again", false)
                }else{
                    switch response.result {
                    case .success:
                        guard let responseJSON:NSDictionary = response.result.value as? NSDictionary else {
                            responseData(nil, nil, nil,"Something went wrong, Please try again", false)
                            return
                        }
                        
                        if let  msgString : String = responseJSON["message"] as? String {
                            self.Message = msgString
                        }
                        else{ self.Message = "Something went wrong, Please try again" }
                        guard let  dict = responseJSON[dataKey] else {
                             responseData(nil, nil, response.result.error! as NSError, self.Message, false)
                            return
                        }
                            self.resObjects = dict as Any
                            responseData(self.resObjects, responseJSON ,nil, self.Message, true)
                        break
                    case .failure(let error):
                        print(error)
                        responseData(nil, nil , error as NSError, error.localizedDescription, false)
                        break
                    }
                }
        }
    }
    
    
    
    //MARK:- PUT Request
    func putRequest(_ endpointurl:String,parameters:NSDictionary,responseData:@escaping (_ data: Any?, _ error: NSError?, _ message: String?) -> Void){
        
        ShowNetworkIndicator(true)
        Alamofire.request(endpointurl, method: .put, parameters: parameters as? [String : Any])
            .responseJSON { response in
                ShowNetworkIndicator(false)
                if let _ = response.result.error{
                    responseData(nil, response.result.error! as NSError, "Something went wrong, Please try again")
                }else{
                    switch response.result {
                    case .success:
                        
                        guard let responseJSON:NSDictionary = response.result.value as? NSDictionary else {
                            print("Invalid tag information received from the service")
                            responseData(nil, nil,"Something went wrong, Please try again")
                            return
                        }
                        self.Message = responseJSON["message"] as? String
                        switch (responseJSON["status"] as! NSInteger) {
                        case RESPONSE_STATUS.VALID.rawValue :
                            self.resObjects = responseJSON["data"]! as Any
                            break
                        case RESPONSE_STATUS.INVALID.rawValue :
                            self.resObjects = responseJSON["data"]! as Any
                            break
                        default :
                            break
                        }
                        responseData(self.resObjects, nil, self.Message)
                    case .failure(let error):
                        print(error)
                        responseData(nil, error as NSError, error.localizedDescription)
                    }
                }
        }
    }
    
    //MARK: - MULTIPART REQUEST
    /*func uploadImages(endpointurl:String, parameters:NSDictionary, filePath:URL, fileName:String ,responseData:@escaping ( _ error: NSError?, _ message: String?, _ responseDict: Any?,_ rStaus:Bool) -> Void)  {
        
        let URL = try! URLRequest(url: endpointurl, method: .post, headers: nil)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value)in parameters{
                var valueStr:String = ""
                if let intVlaue:Int   = value as? Int{
                    valueStr = String(format:"%d",intVlaue)
                }else{
                    valueStr = value as! String
                }
                multipartFormData.append(valueStr.data(using: String.Encoding.utf8)!, withName: key as! String)
            }
            multipartFormData.append(filePath, withName: "image")
        }, with: URL, encodingCompletion: { (result) in
            
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON { response in
                    print(response)  // original URL request
                    if let JSON:NSDictionary = response.result.value as? NSDictionary {
                        print("JSON: \(JSON)")
                        self.Message = JSON["message"] as! String
                        switch (NSInteger(JSON["status"] as! String)!) {
                        case RESPONSE_STATUS.VALID.rawValue :
                            self.resObjects = (response.result.value as! [AnyHashable: Any])["data"]! as Any
                            break
                        case RESPONSE_STATUS.INVALID.rawValue :
                            self.resObjects = nil
                            break
                        default :
                            break
                        }
                        responseData(nil, "Sucess", JSON,true)
                    }else{
                        responseData(nil, "Something went wrong", nil, false)
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
                responseData(encodingError as NSError?, "Something went wrong", nil, false)
            }
        })
    }*/
    
    func uploadImages(endpointurl:String, parameters:[String : Any], filePaths:[URL], isImage : Bool ,responseData:@escaping ( _ error: NSError?, _ message: String?, _ responseDict: Any?,_ rStaus:Bool) -> Void)  {
        
        let URL = try! URLRequest(url: endpointurl, method: .post, headers: nil)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value)in parameters{
                var valueStr:String = ""
                if let intVlaue:Int   = value as? Int{
                    valueStr = String(format:"%d",intVlaue)
                }else{
                    valueStr = value as! String
                }
                multipartFormData.append(valueStr.data(using: String.Encoding.utf8)!, withName: key )
            }
            if filePaths.count == 1{
                 multipartFormData.append(filePaths[0], withName: "image")
            }else{
                for i in 0..<filePaths.count {
                    multipartFormData.append(filePaths[i], withName: "image\(i+1)")
                }
            }
        }, with: URL, encodingCompletion: { (result) in
            
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    SVProgressHUD.showProgress(Float(Progress.fractionCompleted), status: isImage ? "Uploading image.." : "Uploading video..")
                    //print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON { response in
                    print(response)  // original URL request
                    if let JSON:NSDictionary = response.result.value as? NSDictionary {
                        print("JSON: \(JSON)")
                        self.Message = JSON["message"] as? String
                        switch (NSInteger(JSON["status"] as! String)!) {
                        case RESPONSE_STATUS.VALID.rawValue :
                            self.resObjects = JSON["data"]! as Any
                            break
                        case RESPONSE_STATUS.INVALID.rawValue :
                            self.resObjects = nil
                            break
                        default :
                            break
                        }
                        responseData(nil, "Sucess", self.resObjects,true)
                    }else{
                        responseData(nil, "Something went wrong", nil, false)
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
                responseData(encodingError as NSError?, "Something went wrong", nil, false)
            }
        })
    }
}

