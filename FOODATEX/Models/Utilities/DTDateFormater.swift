//
//  DTDateFormater.swift
//  Ethnic Place
//
//  Created by Setblue on 13/06/17.
//  Copyright © 2017 V2ideas. All rights reserved.
//

import Foundation

public let GLOBAL_DATE_FORMAT: String = "yyyy-MM-dd HH:mm:ss"
public let ONLY_DATE_FORMAT: String = "yyyy-MM-dd"
public let ONLY_TIME_FORMAT: String = "HH:mm:00"
public let ONLY_DATE_TIME_FORMAT: String = "yyyy-MM-dd HH:mm:00"
public let BIRTH_DATE_FORMAT: String = "yyyy-MM-dd"
public let ONLY_DATE_MONTH_FORMAT: String = "dd/MM"
public let FULL_DATE_FORMAT: String = "dd MMM yyyy hh:mm a"
public let FB_BIRTH_DATE_FORMAT: String = "MM/dd/yyyy"
public let DIFF_DATE_FORMAT: String = "EEE d MMM yyyy"
public let ONLY_TIME_FORMAT_12HRS: String = "hh:mm a"
public let ONLY_DATEWALET_FORMAT: String = "dd/MM/yyyy"
public let  ONLY_DAY_FORMATE : String = "dd"
public let  ONLY_MONTHYEAR_FORMATE : String = "MMM yyyy"
public let FLIGHT_DATE_FORMAT: String = "dd/MM/yyyy"

class DTDateFormater {
    
    static let sharedInstance = DTDateFormater()
    
    private init() {
    
        sharedDateFormat = DateFormatter()
        sharedDateFormat!.timeZone = NSTimeZone.system
        let enUSPOSIXLocale: NSLocale = NSLocale(localeIdentifier: "en_US_POSIX")
        sharedDateFormat!.locale = enUSPOSIXLocale as Locale
        
    }
    
    var sharedDateFormat : DateFormatter? = nil
   
    //MARK: Convert To Local TimeZone
    class func convertDateToLocalTimeZone(givenDate: String) -> String {
        var final_date: String
        self.sharedInstance.sharedDateFormat!.dateFormat = GLOBAL_DATE_FORMAT
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        //SET TIME ZONE FORMAT OF SERVER HERE
        let ts_utc: NSDate = self.sharedInstance.sharedDateFormat!.date(from: givenDate)! as NSDate
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone.system
        self.sharedInstance.sharedDateFormat!.dateFormat = GLOBAL_DATE_FORMAT
        final_date = self.sharedInstance.sharedDateFormat!.string(from: ts_utc as Date)
        return final_date
    }
    
    class func convertDateToLocalTimeZoneForDate(givenDate: NSDate) -> NSDate {
        self.sharedInstance.sharedDateFormat!.dateFormat = GLOBAL_DATE_FORMAT
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        //SET TIME ZONE FORMAT OF SERVER HERE
        let strDate: String = self.sharedInstance.sharedDateFormat!.string(from: givenDate as Date)
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone.local
        self.sharedInstance.sharedDateFormat!.dateFormat = GLOBAL_DATE_FORMAT
        return self.sharedInstance.sharedDateFormat!.date(from: strDate)! as NSDate
    }
    
    class func convertDateToLocalTimeZoneForDateFromString(givenDate: String) -> NSDate {
        self.sharedInstance.sharedDateFormat!.dateFormat = GLOBAL_DATE_FORMAT
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        //SET TIME ZONE FORMAT OF SERVER HERE
        return self.sharedInstance.sharedDateFormat!.date(from: givenDate)! as NSDate
    }
    
    class func convertLocalDateTimeToServerTimeZone(givenDate: String) -> String {
        let final_date: String
        self.sharedInstance.sharedDateFormat!.dateFormat = GLOBAL_DATE_FORMAT
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone.system
        let ts_utc: NSDate = self.sharedInstance.sharedDateFormat!.date(from: givenDate)! as NSDate
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        self.sharedInstance.sharedDateFormat!.dateFormat = GLOBAL_DATE_FORMAT
        final_date = self.sharedInstance.sharedDateFormat!.string(from: ts_utc as Date)
        return final_date
    }
    
    class func generateDateForGivenDateToServerTimeZone(givenDate: NSDate) -> String {
        self.sharedInstance.sharedDateFormat!.dateFormat = ONLY_DATE_TIME_FORMAT
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        //SET TIME ZONE FORMAT OF SERVER HERE
        return self.sharedInstance.sharedDateFormat!.string(from: givenDate as Date)
    }
    
    class func getUTCDateFromUTCString(givenDate: String) -> NSDate {
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        self.sharedInstance.sharedDateFormat!.dateFormat = GLOBAL_DATE_FORMAT
        return self.sharedInstance.sharedDateFormat!.date(from: givenDate)! as NSDate
    }
    
    //MARK: Common Formats
    class func getStringFromDateString(givenDate: String) -> String {
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone.system
        self.sharedInstance.sharedDateFormat!.dateFormat = GLOBAL_DATE_FORMAT
        let date: NSDate = self.sharedInstance.sharedDateFormat!.date(from: givenDate)! as NSDate
        self.sharedInstance.sharedDateFormat!.dateFormat = GLOBAL_DATE_FORMAT
        return self.sharedInstance.sharedDateFormat!.string(from: date as Date)
    }
    
    class func getStringFromDate(givenDate: NSDate) -> String {
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone.system
        self.sharedInstance.sharedDateFormat!.dateFormat = GLOBAL_DATE_FORMAT
        return self.sharedInstance.sharedDateFormat!.string(from: givenDate as Date)
    }
    
    class func getFullDateStringFromDate(givenDate: NSDate) -> String {
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone.system
        self.sharedInstance.sharedDateFormat!.dateFormat = FULL_DATE_FORMAT
        return self.sharedInstance.sharedDateFormat!.string(from: givenDate as Date)
    }
    
    class func getFullDateStringFromString(givenDate: String) -> String {
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone.system
        self.sharedInstance.sharedDateFormat!.dateFormat = GLOBAL_DATE_FORMAT
        let date: NSDate = self.sharedInstance.sharedDateFormat!.date(from: givenDate)! as NSDate
        self.sharedInstance.sharedDateFormat!.dateFormat = FULL_DATE_FORMAT
        return self.sharedInstance.sharedDateFormat!.string(from: date as Date)
    }
    
    class func getDateFromString(givenDate: String) -> NSDate {
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone.system
        self.sharedInstance.sharedDateFormat!.dateFormat = GLOBAL_DATE_FORMAT
        return self.sharedInstance.sharedDateFormat!.date(from: givenDate)! as NSDate
    }
    
    class func getDateFromJustDateString(givenDate: String) -> NSDate {
        
        self.sharedInstance.sharedDateFormat!.dateFormat = ONLY_DATE_FORMAT
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone.system
        
        let newDate:NSDate =  self.sharedInstance.sharedDateFormat!.date(from: givenDate)! as NSDate
        
        self.sharedInstance.sharedDateFormat!.dateFormat = GLOBAL_DATE_FORMAT
        
        let newDateString:String = self.getStringFromDate(givenDate: newDate)
        
        return self.sharedInstance.sharedDateFormat!.date(from: newDateString)! as NSDate
    }
    
    class func getDateFromDate(givenDate: NSDate) -> NSDate {
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone.system
        self.sharedInstance.sharedDateFormat!.dateFormat = GLOBAL_DATE_FORMAT
        let strDate: String = self.sharedInstance.sharedDateFormat!.string(from: givenDate as Date)
        self.sharedInstance.sharedDateFormat!.dateFormat = GLOBAL_DATE_FORMAT
        return self.sharedInstance.sharedDateFormat!.date(from: strDate)! as NSDate
    }
    
    //MARK: Timestamp Formats
    class func getTimestampUTC() -> String {
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone(name:"UTC")! as TimeZone
        self.sharedInstance.sharedDateFormat!.dateFormat = GLOBAL_DATE_FORMAT
        return self.sharedInstance.sharedDateFormat!.string(from: NSDate() as Date)
    }
    
    class func getTimestampFromGivenDate(givenDate: String) -> NSDate {
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone(name:"UTC")! as TimeZone
        self.sharedInstance.sharedDateFormat!.dateFormat = GLOBAL_DATE_FORMAT
        return self.sharedInstance.sharedDateFormat!.date(from: givenDate)! as NSDate
    }
    
    //MARK: Generate Date
    class func generateDateForGivenDate(strDate: NSDate, andTime strTime: NSDate) -> String {
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone.system
        self.sharedInstance.sharedDateFormat!.dateFormat = ONLY_DATE_FORMAT
        let dt: String = self.sharedInstance.sharedDateFormat!.string(from: strDate as Date)
        self.sharedInstance.sharedDateFormat!.dateFormat = ONLY_TIME_FORMAT
        let tm: String = self.sharedInstance.sharedDateFormat!.string(from: strTime as Date)
        return "\(dt) \(tm)"
    }
    
    //MARK: Generate Date
    class func generateTimeForGivenDate(strDate: NSDate) -> String {
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone.system
        self.sharedInstance.sharedDateFormat!.dateFormat = ONLY_TIME_FORMAT_12HRS
        let tm: String = self.sharedInstance.sharedDateFormat!.string(from: strDate as Date)
        return "\(tm)"
    }
    
    
    
    class func generateDateForGivenDate(strDate: NSDate) -> String {
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone.system
        self.sharedInstance.sharedDateFormat!.dateFormat = ONLY_DATE_TIME_FORMAT
        return self.sharedInstance.sharedDateFormat!.string(from: strDate as Date)
    }
    
    class func generateOnlyDateForGivenDate(strDate: NSDate) -> String {
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone.system
        self.sharedInstance.sharedDateFormat!.dateFormat = ONLY_DATE_FORMAT
        return self.sharedInstance.sharedDateFormat!.string(from: strDate as Date)
    }
    
    //MARK: Birth Date
    class func getBirthDateFromString(givenDate: String) -> NSDate {
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone.system
        self.sharedInstance.sharedDateFormat!.dateFormat = ONLY_DATE_FORMAT
        return self.sharedInstance.sharedDateFormat!.date(from: givenDate)! as NSDate
    }
    
    class func getBirthDateStringFromDateString(givenDate: String) -> String {
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone.system
        self.sharedInstance.sharedDateFormat!.dateFormat = ONLY_DATE_FORMAT
        let date: NSDate = self.sharedInstance.sharedDateFormat!.date(from: givenDate)! as NSDate
        self.sharedInstance.sharedDateFormat!.dateFormat = DIFF_DATE_FORMAT
        return self.sharedInstance.sharedDateFormat!.string(from: date as Date)
    }
    
    
    class func generateBirthDateForGivenDate(strDate: NSDate) -> String {
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone.system
        self.sharedInstance.sharedDateFormat!.dateFormat = BIRTH_DATE_FORMAT
        return self.sharedInstance.sharedDateFormat!.string(from: strDate as Date)
    }
    
    class func generateBirthDateForGivenFacebookDate(strDate: String) -> NSDate {
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone.system
        self.sharedInstance.sharedDateFormat!.dateFormat = FB_BIRTH_DATE_FORMAT
        return self.sharedInstance.sharedDateFormat!.date(from: strDate)! as NSDate
    }
    
    
    class func getStringDateWithSuffix(givenDate: String) -> String {
        //Convert EEE dd MMM yyyy formate
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone.system
        self.sharedInstance.sharedDateFormat!.dateFormat = GLOBAL_DATE_FORMAT
        let date: NSDate = self.sharedInstance.sharedDateFormat!.date(from: givenDate)! as NSDate
        self.sharedInstance.sharedDateFormat!.dateFormat = DIFF_DATE_FORMAT
        var strDate: String = self.sharedInstance.sharedDateFormat!.string(from: date as Date)
        //To get only date from string
        let monthDayFormatter: DateFormatter = DateFormatter()
        monthDayFormatter.formatterBehavior = .behavior10_4
        
        monthDayFormatter.dateFormat = "d"
        let date_day: Int = Int(monthDayFormatter.string(from: date as Date))!
        monthDayFormatter.dateFormat = "yyyy"
        let date_year: Int = Int(monthDayFormatter.string(from: date as Date))!
        strDate = strDate.replacingOccurrences(of:"\(date_year)", with: "")
        
        //Add suffix
        var suffix: NSString?
        let ones: Int = date_day % 10
        let tens: Int = (date_day / 10) % 10
        if tens == 1 {
            suffix = "th"
        }
        else if ones == 1 {
            suffix = "st"
        }
        else if ones == 2 {
            suffix = "nd"
        }
        else if ones == 3 {
            suffix = "rd"
        }
        else {
            suffix = "th"
        }
        
        strDate = strDate.replacingOccurrences(of: "\(date_day)", with: "\(date_day)\(suffix!)")
        strDate = "\(strDate)\(date_year)"
        return strDate
        
    }
    
    class func convertFaceBookDateIntoString(givenDate:String) -> String{
        
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone.system
        self.sharedInstance.sharedDateFormat!.dateFormat = FB_BIRTH_DATE_FORMAT
        let date: NSDate = self.sharedInstance.sharedDateFormat!.date(from: givenDate)! as NSDate
        self.sharedInstance.sharedDateFormat!.dateFormat = ONLY_DATE_FORMAT
        return self.sharedInstance.sharedDateFormat!.string(from: date as Date)
        
    }
    
    
    class func convertWalletDateToString(givenDate: String) -> String {
        
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone.system
        self.sharedInstance.sharedDateFormat!.dateFormat = ONLY_DATEWALET_FORMAT
        let date: NSDate = self.sharedInstance.sharedDateFormat!.date(from: givenDate)! as NSDate
        self.sharedInstance.sharedDateFormat!.dateFormat = ONLY_DAY_FORMATE
        
        let myDate: String = self.sharedInstance.sharedDateFormat!.string(from: date as Date)
        
        self.sharedInstance.sharedDateFormat!.dateFormat = ONLY_MONTHYEAR_FORMATE
        
         let myMonth: String = self.sharedInstance.sharedDateFormat!.string(from: date as Date)
        
        return "\(myDate) \n \(myMonth)"
    }
    
    
    // TODO: Create function for Upcoming Date
    
    /* class func isMatchUpcomingDate(givenDate: String) -> Bool {
     self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone.systemTimeZone()
     self.sharedInstance.sharedDateFormat!.dateFormat = GLOBAL_DATE_FORMAT
     var date: NSDate = self.sharedInstance.sharedDateFormat!.dateFromString(givenDate)!
     var currentDate: NSDate = NSDate()
     var calendar: NSCalendar = NSCalendar.currentCalendar()
     
     let flags: NSCalendarUnit = .CalendarUnitYear | .CalendarUnitMonth | .CalendarUnitDay
     
     var comps: Int = (.Year, .Month, .Day)
     var date1Components: NSDateComponents = calendar.components(comps, fromDate: date)
     var date2Components: NSDateComponents = calendar.components(comps, fromDate: currentDate)
     date = calendar.dateFromComponents(date1Components)
     currentDate = calendar.dateFromComponents(date2Components)
     var result: NSComparisonResult = date.compare(currentDate)
     if result == NSOrderedAscending {
     return false
     else  if result == NSOrderedDescending {
     return true
     }
     else {
     return true
     }
     }*/
    
    class func getJustDateStringFromDate(givenDate: Date) -> String {
        self.sharedInstance.sharedDateFormat!.timeZone = NSTimeZone.system
        self.sharedInstance.sharedDateFormat!.dateFormat = FLIGHT_DATE_FORMAT
        return self.sharedInstance.sharedDateFormat!.string(from: givenDate)
    }
    
    class func getNotiDateFromString(givenDate: String) -> Date {
        
        self.sharedInstance.sharedDateFormat!.timeZone = TimeZone.current
        
        self.sharedInstance.sharedDateFormat!.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        if let myDate: Date = self.sharedInstance.sharedDateFormat!.date(from: givenDate){
            return myDate
        }else{
            
            self.sharedInstance.sharedDateFormat!.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
            return self.sharedInstance.sharedDateFormat!.date(from: givenDate)!
        }
        
    }
    
    class func getJustDateStringFromDateForNoti(givenDate: Date) -> String {
        self.sharedInstance.sharedDateFormat!.timeZone = TimeZone.current
        self.sharedInstance.sharedDateFormat!.dateFormat = FULL_DATE_FORMAT
        return self.sharedInstance.sharedDateFormat!.string(from: givenDate)
    }
    
}

extension Date {

    var timeAgoSinceNow: String {
        return getTimeAgoSinceNow()
    }

    private func getTimeAgoSinceNow() -> String {

        var interval = Calendar.current.dateComponents([.year], from: self, to: Date()).year!
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " Year Ago" : "\(interval)" + " Years Ago"
        }

        interval = Calendar.current.dateComponents([.month], from: self, to: Date()).month!
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " Month Ago" : "\(interval)" + " Months Ago"
        }

        interval = Calendar.current.dateComponents([.day], from: self, to: Date()).day!
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " Day Ago" : "\(interval)" + " Days Ago"
        }

        interval = Calendar.current.dateComponents([.hour], from: self, to: Date()).hour!
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " Hour Ago" : "\(interval)" + " Hours Ago"
        }

        interval = Calendar.current.dateComponents([.minute], from: self, to: Date()).minute!
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " Minute Ago" : "\(interval)" + " Minutes Ago"
        }

        return "Few Seconds Ago"
    }
}
