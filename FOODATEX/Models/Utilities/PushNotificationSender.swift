//
//  PushNotificationSender.swift
//  FOODATEX
//
//  Created by Hitesh Prajapati on 06/06/20.
//  Copyright © 2020 RudraApps. All rights reserved.
//

import UIKit

fileprivate let serverKey = "AAAAjebLgks:APA91bHT0xAZ5lraPk3FZw1qOTkxLe-el6il_npu30o4KbjvXoOm8Qn3xgPNvB1zcs08sXzNk-Qzd0O0jwAIcdSiFEjbIG2k7aPI9HfvU87hYet8620NIq3-B_NsLBgPnnwfb8A9ncsk"

enum pushClickAction : String{
    case match = "match"
    case chat = "chat"
    case null = ""
}

class PushNotificationSender {
    
    class func sendPushNotification(to token: String, title: String, body: String, data : NSDictionary) {
        let urlString = "https://fcm.googleapis.com/fcm/send"
        let url = NSURL(string: urlString)!
        let paramString: [String : Any] = ["to" : token,
                                           "notification" : ["title" : title, "body" : body],
                                           "data": data]
        
        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject:paramString, options: [.prettyPrinted])
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("key=\(serverKey)", forHTTPHeaderField: "Authorization")
        let task =  URLSession.shared.dataTask(with: request as URLRequest)  { (data, response, error) in
            do {
                if let jsonData = data {
                    if let jsonDataDict  = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: AnyObject] {
                        NSLog("Received data:\n\(jsonDataDict))")
                    }
                }
            } catch let err as NSError {
                print(err.debugDescription)
            }
        }
        task.resume()
    }
}
