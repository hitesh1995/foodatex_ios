//
//  Viewclass.swift
//
//  Created by Silver Shark on 22/03/17.
//  Copyright © 2017 Devang Tandel. All rights reserved.
//

import Foundation
import  UIKit

/// This extesion adds some useful functions to UIView
public extension UIView {

    func createCornerTriangle(isHide : Bool){
        
        let width = CGFloat( (SCREENWIDTH()/3) -  13)
        let sizeforTriangle : CGFloat = 40.0
        let path = CGMutablePath()
        path.move(to: CGPoint(x:width - sizeforTriangle,y:0))
        path.addLine(to: CGPoint(x:0,y:0))
        path.addLine(to: CGPoint(x:width,y:width))
        path.addLine(to: CGPoint(x:width,y:0))
        path.addLine(to: CGPoint(x:width,y:sizeforTriangle))
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.frame = self.bounds
        shapeLayer.path = path
        shapeLayer.fillColor = UIColor.black.cgColor
        shapeLayer.strokeColor = UIColor.clear.cgColor
        shapeLayer.bounds =  CGRect(x: 0, y: 0, width: sizeforTriangle, height: sizeforTriangle)
        shapeLayer.anchorPoint = CGPoint(x:0.0, y:0.0)
        shapeLayer.position = CGPoint(x:0.0,y:0.0)
        shapeLayer.zPosition = isHide ? -1 :0
        self.layer.addSublayer(shapeLayer)
    }

    func shakeView() {
        
        let shake: CAKeyframeAnimation = CAKeyframeAnimation(keyPath: "transform")
        shake.values = [NSValue(caTransform3D: CATransform3DMakeTranslation(-5.0, 0.0, 0.0)), NSValue(caTransform3D: CATransform3DMakeTranslation(5.0, 0.0, 0.0))]
        shake.autoreverses = true
        shake.repeatCount = 2.0
        shake.duration = 0.07
        
        self.layer.add(shake, forKey:"shake")
    }
    
    func addGradientBackground(firstColor: UIColor, secondColor: UIColor){
        
        self.layoutIfNeeded()
        clipsToBounds = true
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [firstColor.cgColor, secondColor.cgColor]
        gradientLayer.frame = self.bounds
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 0, y: 1)
        print(gradientLayer.frame)
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func createBordersWithColor(color: UIColor, radius: CGFloat, width: CGFloat) {
        
        self.layer.borderWidth = width
        self.layer.cornerRadius = radius
        self.layer.shouldRasterize = false
        self.layer.rasterizationScale = 2
        self.clipsToBounds = true
        self.layer.masksToBounds = true
        let cgColor: CGColor = color.cgColor
        self.layer.borderColor = cgColor
    }
    
    func removeBorders() {
        self.layer.borderWidth = 0
        self.layer.cornerRadius = 0
        self.layer.borderColor = nil
    }
    
    func removeShadow() {
        self.layer.shadowColor = UIColor.clear.cgColor
        self.layer.shadowOpacity = 0.0
        self.layer.shadowOffset = CGSize(width:0.0, height:0.0)
    }
    
    func setCornerRadius(radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
    func createRectShadowWithOffset(offset: CGSize, opacity: Float, radius: CGFloat) {
        self.layoutIfNeeded()
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = radius
        self.layer.masksToBounds = false
    }
    
    func createCornerRadiusWithShadow(cornerRadius: CGFloat, offset: CGSize, opacity: Float, radius: CGFloat) {
        let layer = self.layer
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = false
        layer.shadowOffset = offset
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
        let backgroundCGColor = self.backgroundColor?.cgColor
        self.backgroundColor = nil
        layer.backgroundColor =  backgroundCGColor
    }
    
    func createCornerRadiusWithShadowNew(cornerRadius: CGFloat, offset: CGSize, opacity: Float, radius: CGFloat) {
        self.layoutIfNeeded()
        self.layer.masksToBounds = false
        self.layer.cornerRadius = cornerRadius
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.layer.cornerRadius).cgPath
        self.layer.shadowOffset = offset
        self.layer.shadowOpacity = opacity
        self.layer.shadowRadius = radius
    }
    
    func createAppCornerRadiusWithShadow() {
        
        self.layoutIfNeeded()
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 8
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.layer.cornerRadius).cgPath
        self.layer.shadowOffset = .zero
        self.layer.shadowOpacity = 0.2
        self.layer.shadowRadius = 1
    }
    
    /**
     Make view Blurr
     */
    func makeBlurView(effectStyle: UIBlurEffectStyle) {
        
        let blurEffect = UIBlurEffect(style: effectStyle) //UIBlurEffectStyle.Light
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
        self.addSubview(blurEffectView)
    }
    
    func createAppBorder() {
        self.layer.borderWidth = 1;
        self.layer.cornerRadius = 8;
        self.layer.shouldRasterize = false
        self.layer.rasterizationScale = 2
        self.clipsToBounds = true
        self.layer.masksToBounds = true
        let cgColor: CGColor = UIColor.lightGray.cgColor
        self.layer.borderColor = cgColor
    }
    
    func dashedBorderLayerWithColor(_ color:CGColor){
        let border = CAShapeLayer()
        border.strokeColor = color
        border.fillColor = nil
        border.lineDashPattern = [4, 4]
        border.path = UIBezierPath(rect: self.bounds).cgPath
        border.frame = self.bounds
        self.layer.addSublayer(border)
    }
    
    func addBottomBorderWithColor(color: UIColor, height : CGFloat){
        let lblborder : UILabel = UILabel(frame: CGRect(x: 0, y: self.frame.height-height, width: self.frame.width, height: height))
        lblborder.backgroundColor = color
        lblborder.text = ""
        self.addSubview(lblborder)
    }
 }
