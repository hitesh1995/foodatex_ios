//
//  TVCProfileDetailImage.swift
//  FOODATEX
//
//  Created by Devang Tandel on 29/11/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit
protocol ProfileDetailImageDelegate {
     func customCollectionViewdidSelectAt(indexpath: IndexPath,strUrl:String?,passImage:UIImage?)
}

class TVCProfileDetailImage: UITableViewCell, UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
     var delegate : ProfileDetailImageDelegate!
    //MARK :- IBOUTLET AND PROPERTIES
    
    @IBOutlet weak var imgCollectView: UICollectionView!
    var arrImg : [UserProfileImage] = [UserProfileImage]()
//    var delegate : ProfileImageDelegate!
    
    //MARKS :- LIFECYCLE
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    
    //MARK :- LOAD COLLECTION VIEW METHOD
    
    func LoadCollectionView() {
        imgCollectView.delegate = self
        imgCollectView.dataSource = self
        imgCollectView.reloadData()
    }
    
    //MARK :- COLLECTION VIEW METHOD
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImg.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CVCProfileDetailImage", for: indexPath) as! CVCProfileDetailImage
        if let url : URL = URL(string: arrImg[indexPath.item].image ?? ""){
            cell.imgView.sd_setImage(with: url) { (image, error, cType, rurl) in
                if image == nil{
                    cell.imgView.image = Set_Local_Image("image_Placeholder")
                }
            }
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = imgCollectView.cellForItem(at: indexPath) as! CVCProfileDetailImage
        let url = arrImg[indexPath.item].image ?? ""
        if cell.imgView.image == nil{
            self.delegate.customCollectionViewdidSelectAt(indexpath: indexPath, strUrl: url, passImage: nil)
        }else{
            self.delegate.customCollectionViewdidSelectAt(indexpath: indexPath, strUrl: url, passImage: cell.imgView.image)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (SCREENWIDTH() - 50) / 4
        return CGSize(width: width, height: width)
    }
}
