//
//  ProfileImgGalleryCell.swift
//  FOODATEX
//
//  Created by Shailesh Panchal on 10/11/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit
import SDWebImage

protocol ProfileImageDelegate {
    func profileImageDeletedAtIndex (indexPath : IndexPath, imageId : String)
   
}

class ProfileImgGalleryCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    //MARK :- IBOUTLET AND PROPERTIES
    
    @IBOutlet weak var imgCollectView: UICollectionView!
    var arrImg : [UserProfileImage] = [UserProfileImage]()
    var delegate : ProfileImageDelegate!

    //MARKS :- LIFECYCLE
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    
    //MARK :- LOAD COLLECTION VIEW METHOD
    
    func LoadCollectionView() {
        imgCollectView.delegate = self
        imgCollectView.dataSource = self
        imgCollectView.reloadData()
    }
    
    //MARK :- COLLECTION VIEW METHOD
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImg.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : CVCProfileImageCell = imgCollectView.dequeueReusableCell(withReuseIdentifier: "CVCProfileImageCell", for: indexPath) as! CVCProfileImageCell
        if let url : URL = URL(string: arrImg[indexPath.item].image ?? ""){
            cell.imgView.sd_setImage(with: url) { (image, error, cType, rurl) in
                if image == nil{
                    cell.imgView.image = Set_Local_Image("image_Placeholder")
                }
            }
        }
        cell.btnDelete.tag = indexPath.item
        cell.btnDelete.addTarget(self, action: #selector(deleteTapped(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (SCREENWIDTH() - 50) / 4
        return CGSize(width: width, height: width)
    }
    
    
    @IBAction func deleteTapped( _ sender : UIButton) {
        
        let alert = UIAlertController(title: APP_NAME, message: "Are you sure you want to delete this image", preferredStyle: .alert)
        let delete = UIAlertAction(title: "Delete", style: .default) { (_) in
            self.delegate.profileImageDeletedAtIndex(indexPath: IndexPath(item: sender.tag, section: 0), imageId: self.arrImg[sender.tag].userImagesId ?? "")
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .default) { (_) in
        }
        alert.addAction(delete)
        alert.addAction(cancel)
        
        APP_DELEGATE.window?.rootViewController?.present(alert, animated: true, completion: nil)
    
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
     
       
    }
    
}
