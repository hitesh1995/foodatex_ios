//
//  TVCProfileCell.swift
//  FOODATEX
//
//  Created by Uffizio iMac on 24/01/19.
//  Copyright © 2019 RudraApps. All rights reserved.
//

import UIKit

class TVCProfileCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblValue: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
