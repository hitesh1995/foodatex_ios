//
//  TVCEditProfileCell.swift
//  FOODATEX
//
//  Created by Silver Shark on 13/11/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit

protocol UserInfoCellDelegate {
    func userInfoEditTapped(indexPath : IndexPath)
}

class TVCEditProfileCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    
    var myIndex : IndexPath!
    var delegate : UserInfoCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func btnEditTapped(_ sender: Any) {
        delegate.userInfoEditTapped(indexPath: myIndex)
    }
    
}
