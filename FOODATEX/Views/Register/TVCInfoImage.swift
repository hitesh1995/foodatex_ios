//
//  TVCInfoImage.swift
//  FOODATEX
//
//  Created by Silver Shark on 06/09/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit

protocol TVCInfoImageDelegate {
    func imageDeleted(_ index : Int)
}

class TVCInfoImage: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var btnImage: UIButton!
    @IBOutlet weak var colImage: UICollectionView!
    var arrImages : [String] = [String]()
    var delegate : TVCInfoImageDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadColView(){
        colImage.delegate = self
        colImage.dataSource =  self
        colImage.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "colcellImage", for: indexPath)
        let btnDelete = cell.viewWithTag(1002) as! UIButton
        let lblName = cell.viewWithTag(1001) as! UILabel
        lblName.text = arrImages[indexPath.item]
        btnDelete.addTarget(self, action: #selector(deleteImage(_:)), for: .touchUpInside)
        btnDelete.accessibilityIdentifier = "\(indexPath.item)"
        return cell
    }

    @objc func deleteImage( _ sender : UIButton){
        let Idf = sender.accessibilityIdentifier
        if let index: Int = Int(Idf!){
            arrImages.remove(at: index)
            colImage.reloadData()
            delegate.imageDeleted(index)
        }
    }
}
