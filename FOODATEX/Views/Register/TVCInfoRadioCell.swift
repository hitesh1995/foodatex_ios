//
//  TVCInfoRadioCell.swift
//  FOODATEX
//
//  Created by Silver Shark on 06/09/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit

class TVCInfoRadioCell: UITableViewCell {
    
    
    @IBOutlet weak var imgNo: UIImageView!
    @IBOutlet weak var imgYes: UIImageView!
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgOccasionaly: UIImageView!
    
    @IBOutlet weak var viewBG: UIView!
    var isTrue = "2"
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func btnYesTapped(_ sender: Any) {
        imgOccasionaly.image = UIImage(named: "icon_RadioUnchecked")
        imgNo.image = UIImage(named: "icon_RadioUnchecked")
        imgYes.image = UIImage(named: "icon_RadioChecked")
        isTrue = "2"
    }
    
    
    @IBAction func btnNoTapped(_ sender: Any) {
        imgOccasionaly.image = UIImage(named: "icon_RadioUnchecked")
        imgNo.image = UIImage(named: "icon_RadioChecked")
        imgYes.image = UIImage(named: "icon_RadioUnchecked")
        isTrue = "1"
    }
    @IBAction func btnOccasionallyTapped(_ sender: Any) {
        imgOccasionaly.image = UIImage(named: "icon_RadioChecked")
        imgNo.image = UIImage(named: "icon_RadioUnchecked")
        imgYes.image = UIImage(named: "icon_RadioUnchecked")
        isTrue = "3"
    }
    
}
