//
//  TVCInfoVideo.swift
//  FOODATEX
//
//  Created by Silver Shark on 06/09/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit

class TVCInfoVideo: UITableViewCell {

    @IBOutlet weak var btnVideo: UIButton!
    @IBOutlet weak var lblVideoName: UILabel!
    
    @IBOutlet weak var viewBG: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
