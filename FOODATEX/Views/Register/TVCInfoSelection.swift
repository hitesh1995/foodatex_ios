//
//  TVCInfoSelection.swift
//  FOODATEX
//
//  Created by Silver Shark on 02/09/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit

class TVCInfoSelection: UITableViewCell {
    
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblValue: UITextField!
    @IBOutlet weak var viewBG: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewBG.createAppCornerRadiusWithShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
