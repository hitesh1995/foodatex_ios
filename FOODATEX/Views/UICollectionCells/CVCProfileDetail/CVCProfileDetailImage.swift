//
//  CVCProfileDetailImage.swift
//  FOODATEX
//
//  Created by Devang Tandel on 29/11/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit

class CVCProfileDetailImage: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView!{
        didSet{
            imgView.contentMode = .scaleAspectFill
        }
    }
}
