//
//  CVCProfileImageCell.swift
//  FOODATEX
//
//  Created by Silver Shark on 23/11/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit

class CVCProfileImageCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnDelete: UIButton!
    
}
