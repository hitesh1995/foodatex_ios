//
//  TVCChatSenderCell.swift
//  FOODATEX
//
//  Created by Uffizio iMac on 05/11/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit

class TVCChatSenderCell: UITableViewCell {
    @IBOutlet var lblDateTime: UILabel!
    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var viewMessage: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
