//
//  TVChatHeaderCell.swift
//  FOODATEX
//
//  Created by Hitesh Prajapati on 20/08/19.
//  Copyright © 2019 RudraApps. All rights reserved.
//

import UIKit

class TVChatHeaderCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
