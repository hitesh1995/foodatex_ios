//
//  TVCChatReciverCell.swift
//  FOODATEX
//
//  Created by Uffizio iMac on 05/11/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit

class TVCChatReciverCell: UITableViewCell {

    @IBOutlet var lblDateTime: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var viewUserPic: UIView!
    @IBOutlet weak var viewMessage: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
