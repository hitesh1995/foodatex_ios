//
//  TVCChatList.swift
//  FOODATEX
//
//  Created by Silver Shark on 02/11/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit

class TVCChatList: UITableViewCell {

    @IBOutlet weak var viewImage: UIView!{
        didSet{
            viewImage.createCornerRadiusWithShadowNew(cornerRadius: viewImage.frame.width/2, offset: CGSize(width: 1, height: 1), opacity: 0.7, radius: 3)
        }
    }
    @IBOutlet weak var imgUser: UIImageView!{
        didSet{
            imgUser.setCornerRadius(radius: imgUser.frame.width/2)
        }
    }
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    
    @IBOutlet weak var lblDateTime : UILabel!
    @IBOutlet weak var lblUnreadCount : UILabel!{
        didSet{
            lblUnreadCount.setCornerRadius(radius: lblUnreadCount.frame.height/2)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
