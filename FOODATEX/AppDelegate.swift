//
//  AppDelegate.swift
//  FOODATEX
//
//  Created by Silver Shark on 03/08/18.
//  Copyright © 2018 RudraApps. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import UserNotifications
import SwiftyStoreKit
import StoreKit
import FacebookCore
import FacebookLogin
import FBSDKLoginKit
import Firebase
import FirebaseMessaging
import SVProgressHUD
import FirebaseAuth
import FirebaseDatabase
import GoogleMobileAds

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,MessagingDelegate {

    var window: UIWindow?
    var AppUser : UserData!
    var matchPopUp : MatchPopUpVC!
    var FIRDBReference: DatabaseReference!
    var swipeCount = 0
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
       
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        FIRDBReference = Database.database().reference()
        
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        
        IQKeyboardManager.shared.enable = true
        swiftyStoreKitReceiptMethod()
        swiftifyValidateReciept()// Checking receipt data for In app..
        if let appUser : UserData = getLoginDetails(){
            if getBooleanValueFromUserDefaults_ForKey(booleanKey: AppKeys.UD.kProfileUpdated){
                AppUser = appUser
                setHomeAsRoot()
            }
        }
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        
        SVProgressHUD.setDefaultStyle(.light)
        SVProgressHUD.setDefaultMaskType(.black)
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        UIApplication.shared.applicationIconBadgeNumber = 0
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        setOnlineStatus(status: "0")
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        setOnlineStatus(status: "1")
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        setOnlineStatus(status: "1")
        UIApplication.shared.applicationIconBadgeNumber = 0
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        if let token = fcmToken{
            print("Firebase registration token: \(token)")
            setStringValueToUserDefaults(strValue: token, ForKey: AppKeys.UD.kAPNSToken)
            let dataDict:[String: String] = ["token": token]
            NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        }
    }
    
    @available(iOS 9.0, *)
    private func application(application: UIApplication,openURL url: NSURL, options: [String: AnyObject]) -> Bool {
           return ApplicationDelegate.shared.application(application, open: url as URL, sourceApplication: UIApplicationOpenURLOptionsKey.sourceApplication.rawValue, annotation: UIApplicationOpenURLOptionsKey.annotation)
        
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print("Token : " + token)
        Auth.auth().setAPNSToken(deviceToken, type: .unknown)  //for phone authentication
       // setStringValueToUserDefaults(strValue: token, ForKey: AppKeys.UD.kAPNSToken)
    }
    
    func setHomeAsRoot(){
        let vcHome = loadVC(AppStoryboards.kSBMain, strVCId: AppNavigation.kNavHome)
        self.window?.rootViewController = vcHome
        window?.makeKeyAndVisible()
    }
    
    func setLOGINAsRoot(){
        UserDefaults.standard.removeObject(forKey: AppKeys.UD.kUser)
        self.window?.rootViewController = loadVC(AppStoryboards.kSBLogin, strVCId: AppNavigation.kNavLogin)
        window?.makeKeyAndVisible()
    }

    //MARK: - Swiftify Methods
    func swiftyStoreKitReceiptMethod(){
        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
            for purchase in purchases {
                switch purchase.transaction.transactionState {
                case .purchased, .restored:
                    if purchase.needsFinishTransaction {
                        // Deliver content from server, then:
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                        // self.swiftycheckValidation()
                    }
                // Unlock content
                case .failed, .purchasing, .deferred:
                    break // do nothing
                }
            }
        }
    }
    
    //MARK: - Swiftify Validate Receipt Method
    func swiftifyValidateReciept(){
        
//        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: INAppReceiptID)
        let appleValidator = AppleReceiptValidator(service: .sandbox, sharedSecret: INAppReceiptID)

        
        let productsId = [monthlySubscriptionId,sixMonthSubscriptionId,twelveMonthSubscriptionId]
        for i in productsId{
            let prod = i as String
            SwiftyStoreKit.verifyReceipt(using: appleValidator) { result in
                switch result {
                case .success(let receipt):
                    
                    let purchaseResult = SwiftyStoreKit.verifySubscription(
                        ofType: .autoRenewable,
                        productId: prod,
                        inReceipt: receipt)
                    
                    switch purchaseResult {
                    case .purchased(let expiryDate, _):
                        print("Product is valid until \(expiryDate)")
                        UserDefaults.standard.set(true, forKey: prod)
                        UserDefaults.standard.synchronize()
                    case .expired(let expiryDate, _):
                        print("Product is expired since \(expiryDate)")
                        UserDefaults.standard.set(false, forKey: prod)
                        UserDefaults.standard.synchronize()
                    case .notPurchased:
                        print("This product has never been purchased")
                        UserDefaults.standard.set(false, forKey: prod)
                        UserDefaults.standard.synchronize()
                    }
                case .error(let error):
                    print("Receipt verification failed: \(error)")
                    UserDefaults.standard.set(false, forKey: prod)
                    UserDefaults.standard.synchronize()
                }
            }
            
        }
        
    }
    
    
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "FOODATEX")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let userInfo = notification.request.content.userInfo
         print(userInfo)
        completionHandler([.alert, .badge, .sound])
    }
    
    // MARK :- FB Method
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Swift.Void) {
        let userInfo = response.notification.request.content.userInfo
        print(userInfo)
        handleNotification(userInfo: userInfo, isNotificationTap: true)
        completionHandler()
    }
}


extension AppDelegate{
    
    func handleNotification(userInfo: [AnyHashable: Any], isNotificationTap: Bool) {
        
        if isNotificationTap {
            
            
            guard let notificationData : [String : Any] = userInfo["aps"] as? [String : Any] else {
                return
            }
            print(notificationData["data"] as Any)
            guard let notificationContent : [String : Any] = userInfo as? [String : Any] else {
                return
            }
            
            print(notificationContent)
            if let target : String = userInfo["click_action"] as? String, target.lowercased() == "match" {
                //Match Notification
                if matchPopUp != nil{
                    matchPopUp.hideCustomPicker()
                    matchPopUp = nil
                }
                matchPopUp = MatchPopUpVC(nibName: "MatchPopUpVC", bundle : nil)
                matchPopUp.matchDict = notificationContent
                matchPopUp.delegate = self
                matchPopUp.show()
            }else if let target : String = userInfo["click_action"] as? String, target.lowercased() == "chat" {
                // Chat Notification
                if let chat : NewChatVC = UIApplication.shared.topMostViewController() as? NewChatVC{
                    if let senderID = getLoginDetails()?.userId{
                        chat.senderID = senderID
                        chat.receiverID = userInfo["sender_id"] as? String ?? "0"
                        chat.userName = userInfo["sender_name"] as? String ?? "User"
                        chat.receiverImage = userInfo["sender_image"] as? String ?? ""
                        if let token = userInfo["deviceToken"] as? String, token != ""{
                            chat.receiverToken = token
                        }
                        chat.reloadChatWithNewUser()
                    }
                }else if let topViewCntroller : UIViewController = UIApplication.shared.topMostViewController(){
                    if let senderID = getLoginDetails()?.userId{
                        let chat = loadVC(AppStoryboards.kSBChat, strVCId: AppViewControllers.Chat.kVCChat) as! NewChatVC
                        chat.senderID = senderID
                        chat.receiverID = userInfo["sender_id"] as? String ?? "0"
                        chat.userName = userInfo["sender_name"] as? String ?? "User"
                        chat.receiverImage = userInfo["sender_image"] as? String ?? ""
                        if let token = userInfo["deviceToken"] as? String, token != ""{
                            chat.receiverToken = token
                        }
                        topViewCntroller.navigationController?.pushViewController(chat, animated: true)
                    }
                }
            }else if let target : String = userInfo["click_action"] as? String, target.lowercased() == "supermatch" {
                
                if let topViewCntroller : UIViewController = UIApplication.shared.topMostViewController(){
                    
                    let profileVC = loadVC(AppStoryboards.kSBMain, strVCId:AppViewControllers.kVCProfileDetail) as! ProfileDetailsVC
                    profileVC.userID = userInfo["sender_id"] as? String ?? "0"
                    profileVC.userName = userInfo["sender_name"] as? String ?? ""
                    
                    topViewCntroller.present(UINavigationController(rootViewController: profileVC), animated: true, completion: nil)
                    
                }
            }
        }
    }
}

extension AppDelegate : MatchPopupDelegate {
    
    func sendMessageToUserId(otherUserId: String, userName: String, timeAgo: String, image : String, token : String) {
        
        if matchPopUp != nil{
            matchPopUp.hideCustomPicker()
            matchPopUp = nil
        }
        
        if let topViewCntroller : UIViewController = UIApplication.shared.topMostViewController() {
            if let senderID = getLoginDetails()?.userId{
                let chat = loadVC(AppStoryboards.kSBChat, strVCId: AppViewControllers.Chat.kVCChat) as! NewChatVC
                chat.senderID = senderID
                chat.receiverID = otherUserId
                chat.receiverImage = image
                chat.userName = userName
                chat.receiverToken = token
                topViewCntroller.navigationController?.pushViewController(chat, animated: true)
            }
        }
        
    }
}

extension UIViewController {
    func topMostViewController() -> UIViewController {
        
        if let presented = self.presentedViewController {
            return presented.topMostViewController()
        }
        
        if let navigation = self as? UINavigationController {
            return navigation.visibleViewController?.topMostViewController() ?? navigation
        }
        
        if let tab = self as? UITabBarController {
            return tab.selectedViewController?.topMostViewController() ?? tab
        }
        
        return self
    }
}

extension UIApplication {
    func topMostViewController() -> UIViewController? {
        return self.keyWindow?.rootViewController?.topMostViewController()
    }
}
